## TV Utils

### Example shelf IDs with different end_date

* QwAYkYmx8nxw: end_date 2020
* 3Z3pMVvnpywD: end_date 2021-01
* M7yRgdrbe2rX: end_date 2023

end_date: null

* nwV6dKJxK4qw
* NZY2b452LWp9
* Q82MvjdVDgDZ

### Shelf Query

```json
{
  "query": {
      "bool": {
          "minimum_should_match": 1,
          "should": [
              {
                  "bool": {
                      "filter": [
                          {"term": {"_id": shelf_id}}, 
                          {"term": {"_index": "cms-shelf-progressive"}},
                          {
                              "nested": {
                                  "path": "shelf_list",
                                  "query": {
                                      "bool": {
                                          "filter": [
                                              {"term" : {"shelf_list.ative": true}},
                                              {"range": {"shelf_list.start_date": {"lte": "now"}}}
                                          ],
                                          "should": [
                                              {"range": {"shelf_list.end_date": {"gte": "now"}}},
                                              {"bool" : {"must_not": [{"exists": {"field": "shelf_list.end_date"}}]}}
                                          ],
                                          "minimum_should_match": 1,
                                      }
                                  },
                                  "inner_hits": {
                                      "_source": [
                                          "shelf_list.active",
                                          "shelf_list.shelf_items",
                                          "shelf_list.start_date",
                                          "shelf_list.end_date"
                                      ]
                                  }
                              }
                          }
                      ]
                  }
              }, 
              {
                  "bool": {
                      "filter": [
                          {"term": {"_id": shelf_id}},
                          {"term": {"_index": "content-shelf-master"}}
                      ]
                  }
              }
          ]
      }
    }
}
```

### Content Query

```json
// After
GET /preprod-*/_search
{
    "query": {
        "bool": {
            "filter": {
                "bool": {
                    "must": [
                        {"range": {"publish_date": {"lte": "now/h"}}},
                        {"match": {"status": "publish"}},
                        {"match": {"searchable": "Y"}},
                        {"term" : {"lang": "th"}},
                        {"terms": {
                                "id.keyword": [
                                    "1RjbVpxZloKR"
                                ]
                            }
                        }
                    ],
                    "must_not": [
                        {"match_phrase": {"title": "empty title"}},
                        {"match": {"is_trailer": "yes"}},
                        {"match": {"is_vod_layer": "Y"}},
                        {"match": {"is_promo": "yes"}}
                    ]
                }
            },
            "minimum_should_match": 1, 
            "should": [
              {"range": {"expire_date": {"gt": "now/h"}}},
              {"bool" : {"must_not": [{"exists": {"field": "expire_date"}}]}}
            ]
        }
    }
}
```