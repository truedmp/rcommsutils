from setuptools import setup, find_packages

REQUIRED_PACKAGES = [
    'cassandra-driver==3.24.0', 'elasticsearch==7.10.0',
    'elasticsearch-dsl==7.3.0', 'redis==3.5.3', 'redis-py-cluster==2.1.0',
    'requests>=2.21.0', 'orjson>=3.6.1'
]

setup(name='rcommsutils',
      version='4.7.15',
      packages=find_packages(exclude=('tests',)),
      url='https://bitbucket.org/truedmp/rcommsutils',
      include_package_data=True,
      description='Rcom common model server utils',
      author='Patcharin Cheng',
      author_email='patcharin.che@truedigital.com',
      install_requires=REQUIRED_PACKAGES,
      zip_safe=False)
