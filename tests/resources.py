# Movies
movies = {
    "TEST_MOVIE_1": {
        "lang":
            "en",
        "id":
            "TEST_MOVIE_1",
        "original_id":
            "TEST_MOVIE_1",
        "content_type":
            "movie",
        "title_en":
            "Pokemon Season 19 : XYZ (Catch-up)",
        "title_th":
            "โปเกมอน ซีซั่น 19 : XYZ (Catch-up)",
        "thumb":
            "http://obmsbke.truevisions.tv/ImgLib/PACKAGE/57a378a2ac55efd0aea643c86fcb7688/60c2cf3b8e1095d254488d6a960bc6b3.jpg",
        "thumb_list": {
            "ott_portrait":
                "",
            "trueid_landscape":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "square_image":
                "",
            "landscape_image":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "portrait_image":
                "",
            "ott_landscape":
                "",
            "poster":
                "https://cms.dmpcdn.com/movie/2018/08/14/ff164494-122c-4692-a0d2-00f396f10190.jpg"
        },
        "detail_en":
            "Follow Satoshi, Pikachu, and their friends as they explore the deepest mysteries of the Kalos region! Team Flare has plans for the Legendary Pok?mon Zygarde and the secret it holds. Alan’s ongoing search for the source of Mega Evolution intersects with our heroes’ adventures. And true to Gym Leader Gojika’s prediction, Ash and his Gekkogashira will work together to reach surprising new heights! Journey into uncharted territory with this exciting season of Pok?mon animation!",
        "detail_th":
            "ดีเทล ภาษาไทยนะ",
        "tvod_flag":
            "N",
        "tvod_flag_th":
            "N",
        "tvod_flag_en":
            "N",
        "subscription_tiers": ["basic"],
        "subscription_tiers_th": ["basic"],
        "subscription_tiers_en": ["basic"],
        "count_views":
            8629,
        "count_likes":
            1,
        "count_likes_th":
            1,
        "count_likes_en":
            1,
        "ep_items":
            None,
        "ep_items_th":
            None,
        "ep_items_en":
            None,
        "movie_type":
            "movie",
        "movie_type_th":
            "movie",
        "movie_type_en":
            "movie",
        "article_category_en": ["Animation"],
        "article_category_th": ["Animation"],
        "genres_en": ["Animation"],
        "genres_th": ["Animation"],
        "tags":
            None,
        "status":
            "publish",
        "publish_date":
            "2017-11-08T18:31:05.000Z",
        "expire_date":
            "2020-12-31T16:59:59.000Z",
        "create_date":
            "2017-11-05T00:00:00.000Z",
        "update_date":
            "2017-11-21T21:00:00.000Z",
        "create_by":
            None,
        "update_by":
            None,
        "access":
            None,
        "searchable":
            "Y",
        "synopsis_short":
            None,
        "studio":
            None,
        "release_year":
            None,
        "actor_en":
            None,
        "actor_th":
            None,
        "director_en":
            None,
        "director_th":
            None,
        "writer":
            None,
        "country":
            None,
        "audio": ["TH", "EN"],
        "rate":
            "PG-13",
        "synopsis_en":
            "This is english synopsis",
        "synopsis_th":
            "เรื่องย่อภาษาไทย",
        "subtitle": ["TH"],
        "duration":
            "132",
        "movie_type":
            "TV_PROGRAM",
        "business_models":
            "svod",
        "tv_show_code":
            "AUTUMN",
        "package_code":
            "TEST_MOVIE_1",
        "HIT_COUNT_MONTH":
            95,
        "HIT_COUNT_WEEK":
            60,
        "HIT_COUNT_YEAR":
            32079,
    },
    'TEST_MOVIE_2': {
        "lang":
            "en",
        "id":
            "TEST_MOVIE_2",
        "original_id":
            "TEST_MOVIE_2",
        "content_type":
            "movie",
        "title_en":
            "Pokemon Season 19 : XYZ (Catch-up)",
        "title_th":
            "โปเกมอน ซีซั่น 19 : XYZ (Catch-up)",
        "thumb":
            "http://obmsbke.truevisions.tv/ImgLib/PACKAGE/57a378a2ac55efd0aea643c86fcb7688/60c2cf3b8e1095d254488d6a960bc6b3.jpg",
        "thumb_list": {
            "ott_portrait":
                "",
            "trueid_landscape":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "square_image":
                "",
            "landscape_image":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "portrait_image":
                "",
            "ott_landscape":
                "",
            "poster":
                "https://cms.dmpcdn.com/movie/2018/08/14/ff164494-122c-4692-a0d2-00f396f10190.jpg"
        },
        "detail_en":
            "Follow Satoshi, Pikachu, and their friends as they explore the deepest mysteries of the Kalos region! Team Flare has plans for the Legendary Pok?mon Zygarde and the secret it holds. Alan’s ongoing search for the source of Mega Evolution intersects with our heroes’ adventures. And true to Gym Leader Gojika’s prediction, Ash and his Gekkogashira will work together to reach surprising new heights! Journey into uncharted territory with this exciting season of Pok?mon animation!",
        "detail_th":
            "ดีเทล ภาษาไทยนะ",
        "tvod_flag":
            "N",
        "tvod_flag_th":
            "N",
        "tvod_flag_en":
            "N",
        "subscription_tiers": ["basic"],
        "subscription_tiers_th": ["basic"],
        "subscription_tiers_en": ["basic"],
        "count_views":
            8629,
        "count_likes":
            1,
        "count_likes_th":
            1,
        "count_likes_en":
            1,
        "ep_items":
            None,
        "ep_items_th":
            None,
        "ep_items_en":
            None,
        "movie_type":
            "movie",
        "movie_type_th":
            "movie",
        "movie_type_en":
            "movie",
        "article_category_en": ["Animation"],
        "article_category_th": ["Animation"],
        "genres_en": ["Animation"],
        "genres_th": ["Animation"],
        "tags":
            None,
        "status":
            "publish",
        "publish_date":
            "2017-11-08T18:31:10.000Z",
        "expire_date":
            "2020-12-31T16:59:59.000Z",
        "create_date":
            "2017-11-05T00:00:00.000Z",
        "update_date":
            "2017-11-21T21:00:00.000Z",
        "create_by":
            None,
        "update_by":
            None,
        "access":
            None,
        "searchable":
            "Y",
        "synopsis_short":
            None,
        "studio":
            None,
        "release_year":
            None,
        "actor_en":
            None,
        "actor_th":
            None,
        "director_en":
            None,
        "director_th":
            None,
        "writer":
            None,
        "country":
            None,
        "rate":
            "PG-13",
        "audio": ["TH"],
        "synopsis_en":
            "This is english synopsis",
        "synopsis_th":
            "เรื่องย่อภาษาไทย",
        "subtitle": ["TH"],
        "duration":
            "132",
        "movie_type":
            "TV_PROGRAM",
        "business_models":
            "svod",
        "tv_show_code":
            "AUTUMN",
        "package_code":
            "TEST_MOVIE_2",
        "HIT_COUNT_MONTH":
            45,
        "HIT_COUNT_WEEK":
            40,
        "HIT_COUNT_YEAR":
            12345
    },
    'TEST_MOVIE_3': {
        "lang":
            "th",
        "id":
            "TEST_MOVIE_3",
        "original_id":
            "TEST_MOVIE_3",
        "content_type":
            "movie",
        "title_en":
            "Best of times",
        "title_th":
            "ความจำสั้น แต่รักฉันยาว",
        "thumb":
            "http://obmsbke.truevisions.tv/ImgLib/PACKAGE/57a378a2ac55efd0aea643c86fcb7688/60c2cf3b8e1095d254488d6a960bc6b3.jpg",
        "thumb_list": {
            "ott_portrait":
                "",
            "trueid_landscape":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "square_image":
                "",
            "landscape_image":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "portrait_image":
                "",
            "ott_landscape":
                "",
            "poster":
                "https://cms.dmpcdn.com/movie/2018/08/14/ff164494-122c-4692-a0d2-00f396f10190.jpg"
        },
        'detail_th':
            'คลาวด์ แอตลาส หยุดโลกข้ามเวลา (อังกฤษ: Cloud Atlas) เป็นภาพยนตร์แนววิทยาศาสตร์ดราม่าสัญชาติเยอรมัน เขียนบทและกำกับโดย Lana และ Andy Wachowski และ Tom Tykwer ดัดแปลงจากหนังสือนิยาย เมฆาสัญจร (Cloud Atlas) ของเดวิด มิตเชลล์ ทุนสร้าง 102 ล้านดอลลาร์สหรัฐ เป็นภาพยนตร์อิสระที่ใช้ทุนสร้างสูงสุดเรื่องหนึ่ง',
        'detail_en':
            'This is in English',
        "tvod_flag":
            "N",
        "tvod_flag_th":
            "N",
        "tvod_flag_en":
            "N",
        "subscription_tiers": ["basic"],
        "subscription_tiers_th": ["basic"],
        "subscription_tiers_en": ["basic"],
        "count_views":
            8629,
        "count_likes":
            1,
        "count_likes_th":
            1,
        "count_likes_en":
            1,
        "ep_items":
            None,
        "ep_items_th":
            None,
        "ep_items_en":
            None,
        "movie_type":
            "movie",
        "movie_type_th":
            "movie",
        "movie_type_en":
            "movie",
        "article_category_th": ["หนังไทย"],
        "article_category_en": ["Thai Movie"],
        "genres_th": ["หนังไทย"],
        "genres_en": ["Thai Movie"],
        "tags":
            None,
        "status":
            "publish",
        "publish_date":
            "2017-12-08T18:31:05.000Z",
        "expire_date":
            "2020-12-31T16:59:59.000Z",
        "create_date":
            "2017-12-08T18:31:00.000Z",
        "update_date":
            "2017-12-08T18:31:00.000Z",
        "create_by":
            None,
        "update_by":
            None,
        "access":
            None,
        "searchable":
            "Y",
        "synopsis_short":
            None,
        "studio":
            None,
        "release_year":
            None,
        "actor_en":
            None,
        "actor_th":
            None,
        "director_en":
            None,
        "director_th":
            None,
        "writer":
            None,
        "country":
            None,
        "movie_type":
            "movie",
        "business_models":
            "svod",
        "tv_show_code":
            None,
        "package_code":
            "TEST_MOVIE_3",
        "HIT_COUNT_MONTH":
            8888,
        "HIT_COUNT_WEEK":
            5555,
        "HIT_COUNT_YEAR":
            9999,
        "rate":
            "PG-13",
        "audio": ["TH"],
        "synopsis_en":
            "This is english synopsis",
        "synopsis_th":
            "เรื่องย่อภาษาไทย",
        "subtitle": ["TH"],
        "duration":
            "132",
    },
    "TEST_MOVIE_4": {
        "lang":
            "en",
        "id":
            "TEST_MOVIE_4",
        "original_id":
            "TEST_MOVIE_4",
        "content_type":
            "movie",
        "title_en":
            "Wonder Woman",
        "title_th":
            "วันเดอร์ วูแมน",
        "thumb":
            "http://obmsbke.truevisions.tv/ImgLib/TV_PROGRAM/59a6dc9af7c8515eb8993a32a2f43098/a2d5ca72a2d2e30c1188d87827081494.jpg",
        "thumb_list": {
            "ott_portrait":
                "",
            "trueid_landscape":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "square_image":
                "",
            "landscape_image":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "portrait_image":
                "",
            "ott_landscape":
                "",
            "poster":
                "https://cms.dmpcdn.com/movie/2018/08/14/ff164494-122c-4692-a0d2-00f396f10190.jpg"
        },
        "detail_en":
            "Wonder Woman Diana, princess of the Amazons, trained to be an unconquerable warrior. Raised on a sheltered island paradise, when a pilot crashes on their shores and tells of a massive conflict raging in the outside world, Diana leaves her home, convinced she can stop the threat. Fighting alongside man in a war to end all wars, Diana will discover her full powers and her true destiny.",
        "detail_th":
            "ไทยน้าา",
        "tvod_flag":
            "N",
        "tvod_flag_th":
            "N",
        "tvod_flag_en":
            "N",
        "subscription_tiers": ["basic"],
        "subscription_tiers_th": ["basic"],
        "subscription_tiers_en": ["basic"],
        "count_views":
            8629,
        "count_likes":
            1,
        "count_likes_th":
            1,
        "count_likes_en":
            1,
        "ep_items":
            None,
        "ep_items_th":
            None,
        "ep_items_en":
            None,
        "movie_type":
            "movie",
        "movie_type_th":
            "movie",
        "movie_type_en":
            "movie",
        "article_category_en": ["action & adventure", "fantasy"],
        "article_category_th": ["action & adventure", "fantasy"],
        "genres_en": ["action & adventure", "fantasy"],
        "genres_th": ["action & adventure", "fantasy"],
        "tags":
            None,
        "status":
            "publish",
        "publish_date":
            "2017-12-07T01:46:00.000Z",
        "expire_date":
            "2020-12-31T16:59:59.000Z",
        "create_date":
            "2017-12-07T04:57:00.000Z",
        "update_date":
            "2017-12-21T21:49:00.000Z",
        "create_by":
            None,
        "update_by":
            None,
        "access":
            None,
        "searchable":
            "Y",
        "synopsis_short":
            None,
        "studio":
            None,
        "release_year":
            "2017",
        "actor_en": ["Chris Pine", "Gal Gadot", "Robin Wright"],
        "actor_th": ["Chris Pine", "Gal Gadot", "Robin Wright"],
        "director_en": ["Patty Jenkins"],
        "director_th": ["Patty Jenkins"],
        "writer":
            None,
        "country":
            None,
        "movie_type":
            "movie",
        "business_models":
            "tvod",
        "tv_show_code":
            None,
        "package_code":
            "TEST_MOVIE_4",
        "HIT_COUNT_MONTH":
            1200,
        "HIT_COUNT_WEEK":
            888,
        "HIT_COUNT_YEAR":
            2134,
        "rate":
            "PG-13",
        "audio": ["TH"],
        "synopsis_en":
            "Diana, princess of the Amazons, trained to be an unconquerable warrior. Raised on a sheltered island paradise, when a pilot crashes on their shores and tells of a massive conflict raging in the outside world, Diana leaves her home, convinced she can stop the threat. Fighting alongside man in a war to end all wars, Diana will discover her full powers and her true destiny.",
        "synopsis_th":
            "ภาษาไทยน้าา",
        "subtitle": ["TH"],
        "duration":
            "132",
    },
    "expired_TEST_MOVIE": {
        "lang":
            "en",
        "id":
            "expired_TEST_MOVIE",
        "original_id":
            "expired_TEST_MOVIE",
        "content_type":
            "movie",
        "title_en":
            "expired Wonder Woman",
        "title_th":
            "expired วันเดอร์ วูแมน",
        "thumb":
            "http://obmsbke.truevisions.tv/ImgLib/TV_PROGRAM/59a6dc9af7c8515eb8993a32a2f43098/a2d5ca72a2d2e30c1188d87827081494.jpg",
        "thumb_list": {
            "ott_portrait":
                "",
            "trueid_landscape":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "square_image":
                "",
            "landscape_image":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "portrait_image":
                "",
            "ott_landscape":
                "",
            "poster":
                "https://cms.dmpcdn.com/movie/2018/08/14/ff164494-122c-4692-a0d2-00f396f10190.jpg"
        },
        "detail_en":
            "Wonder Woman Diana, princess of the Amazons, trained to be an unconquerable warrior. Raised on a sheltered island paradise, when a pilot crashes on their shores and tells of a massive conflict raging in the outside world, Diana leaves her home, convinced she can stop the threat. Fighting alongside man in a war to end all wars, Diana will discover her full powers and her true destiny.",
        "detail_th":
            "ไทยน้าา",
        "tvod_flag":
            "N",
        "tvod_flag_th":
            "N",
        "tvod_flag_en":
            "N",
        "subscription_tiers": ["basic"],
        "subscription_tiers_th": ["basic"],
        "subscription_tiers_en": ["basic"],
        "count_views":
            8629,
        "count_likes":
            1,
        "count_likes_th":
            1,
        "count_likes_en":
            1,
        "ep_items":
            None,
        "ep_items_th":
            None,
        "ep_items_en":
            None,
        "movie_type":
            "movie",
        "movie_type_th":
            "movie",
        "movie_type_en":
            "movie",
        "article_category_en": ["xxx", "yyy"],
        "article_category_th": ["xxx", "yyy"],
        "genres_en": ["xxx", "yyy"],
        "genres_th": ["xxx", "yyy"],
        "tags":
            None,
        "status":
            "publish",
        "publish_date":
            "2017-12-07T01:46:00.000Z",
        "expire_date":
            "2018-01-01T16:59:59.000Z",
        "create_date":
            "2017-12-07T04:57:00.000Z",
        "update_date":
            "2017-12-21T21:49:00.000Z",
        "create_by":
            None,
        "update_by":
            None,
        "access":
            None,
        "searchable":
            "Y",
        "synopsis_short":
            None,
        "studio":
            None,
        "release_year":
            "2017",
        "actor_en": ["Chris Pine", "Gal Gadot", "Robin Wright"],
        "actor_th": ["Chris Pine", "Gal Gadot", "Robin Wright"],
        "director_en": ["Patty Jenkins"],
        "director_th": ["Patty Jenkins"],
        "writer":
            None,
        "country":
            None,
        "movie_type":
            "movie",
        "business_models":
            "tvod",
        "tv_show_code":
            None,
        "package_code":
            "expired_TEST_MOVIE",
        "HIT_COUNT_MONTH":
            12,
        "HIT_COUNT_WEEK":
            1234,
        "HIT_COUNT_YEAR":
            1234,
        "rate":
            "PG-13",
        "audio": ["TH"],
        "synopsis_en":
            "Diana, princess of the Amazons, trained to be an unconquerable warrior. Raised on a sheltered island paradise, when a pilot crashes on their shores and tells of a massive conflict raging in the outside world, Diana leaves her home, convinced she can stop the threat. Fighting alongside man in a war to end all wars, Diana will discover her full powers and her true destiny.",
        "synopsis_th":
            "ภาษาไทยน้าา",
        "subtitle": ["TH"],
        "duration":
            "132",
    },
    "not_searchable_TEST_MOVIE": {
        "lang":
            "en",
        "id":
            "not_searchable_TEST_MOVIE",
        "original_id":
            "not_searchable_TEST_MOVIE",
        "content_type":
            "movie",
        "title_en":
            "not_searchable Wonder Woman",
        "title_th":
            "not_searchable วันเดอร์ วูแมน",
        "thumb":
            "http://obmsbke.truevisions.tv/ImgLib/TV_PROGRAM/59a6dc9af7c8515eb8993a32a2f43098/a2d5ca72a2d2e30c1188d87827081494.jpg",
        "thumb_list": {
            "ott_portrait":
                "",
            "trueid_landscape":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "square_image":
                "",
            "landscape_image":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "portrait_image":
                "",
            "ott_landscape":
                "",
            "poster":
                "https://cms.dmpcdn.com/movie/2018/08/14/ff164494-122c-4692-a0d2-00f396f10190.jpg"
        },
        "detail_en":
            "Wonder Woman Diana, princess of the Amazons, trained to be an unconquerable warrior. Raised on a sheltered island paradise, when a pilot crashes on their shores and tells of a massive conflict raging in the outside world, Diana leaves her home, convinced she can stop the threat. Fighting alongside man in a war to end all wars, Diana will discover her full powers and her true destiny.",
        "detail_th":
            "ไทยน้าา",
        "tvod_flag":
            "N",
        "tvod_flag_th":
            "N",
        "tvod_flag_en":
            "N",
        "subscription_tiers": ["basic"],
        "subscription_tiers_th": ["basic"],
        "subscription_tiers_en": ["basic"],
        "count_views":
            8629,
        "count_likes":
            1,
        "count_likes_th":
            1,
        "count_likes_en":
            1,
        "ep_items":
            None,
        "ep_items_th":
            None,
        "ep_items_en":
            None,
        "movie_type":
            "movie",
        "movie_type_th":
            "movie",
        "movie_type_en":
            "movie",
        "article_category_en": ["xxx", "yyy"],
        "article_category_th": ["xxx", "yyy"],
        "genres_en": ["xxx", "yyy"],
        "genres_th": ["xxx", "yyy"],
        "tags":
            None,
        "status":
            "publish",
        "publish_date":
            "2017-12-07T01:46:00.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "create_date":
            "2017-12-07T04:57:00.000Z",
        "update_date":
            "2017-12-21T21:49:00.000Z",
        "create_by":
            None,
        "update_by":
            None,
        "access":
            None,
        "searchable":
            "N",
        "synopsis_short":
            None,
        "studio":
            None,
        "release_year":
            "2017",
        "actor_en": ["Chris Pine", "Gal Gadot", "Robin Wright"],
        "actor_th": ["Chris Pine", "Gal Gadot", "Robin Wright"],
        "director_en": ["Patty Jenkins"],
        "director_th": ["Patty Jenkins"],
        "writer":
            None,
        "country":
            None,
        "movie_type":
            "movie",
        "business_models":
            "tvod",
        "tv_show_code":
            None,
        "package_code":
            "not_searchable_TEST_MOVIE",
        "HIT_COUNT_MONTH":
            12,
        "HIT_COUNT_WEEK":
            1234,
        "HIT_COUNT_YEAR":
            1234,
        "rate":
            "PG-13",
        "audio": ["TH"],
        "synopsis_en":
            "Diana, princess of the Amazons, trained to be an unconquerable warrior. Raised on a sheltered island paradise, when a pilot crashes on their shores and tells of a massive conflict raging in the outside world, Diana leaves her home, convinced she can stop the threat. Fighting alongside man in a war to end all wars, Diana will discover her full powers and her true destiny.",
        "synopsis_th":
            "ภาษาไทยน้าา",
        "subtitle": ["TH"],
        "duration":
            "132",
    },
    "draft_TEST_MOVIE": {
        "lang":
            "en",
        "id":
            "draft_TEST_MOVIE",
        "original_id":
            "draft_TEST_MOVIE",
        "content_type":
            "movie",
        "title_en":
            "draft Wonder Woman",
        "title_th":
            "draft วันเดอร์ วูแมน",
        "thumb":
            "http://obmsbke.truevisions.tv/ImgLib/TV_PROGRAM/59a6dc9af7c8515eb8993a32a2f43098/a2d5ca72a2d2e30c1188d87827081494.jpg",
        "thumb_list": {
            "ott_portrait":
                "",
            "trueid_landscape":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "square_image":
                "",
            "landscape_image":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "portrait_image":
                "",
            "ott_landscape":
                "",
            "poster":
                "https://cms.dmpcdn.com/movie/2018/08/14/ff164494-122c-4692-a0d2-00f396f10190.jpg"
        },
        "detail_en":
            "Wonder Woman Diana, princess of the Amazons, trained to be an unconquerable warrior. Raised on a sheltered island paradise, when a pilot crashes on their shores and tells of a massive conflict raging in the outside world, Diana leaves her home, convinced she can stop the threat. Fighting alongside man in a war to end all wars, Diana will discover her full powers and her true destiny.",
        "detail_th":
            "ไทยน้าา",
        "tvod_flag":
            "N",
        "tvod_flag_th":
            "N",
        "tvod_flag_en":
            "N",
        "subscription_tiers": ["basic"],
        "subscription_tiers_th": ["basic"],
        "subscription_tiers_en": ["basic"],
        "count_views":
            8629,
        "count_likes":
            1,
        "count_likes_th":
            1,
        "count_likes_en":
            1,
        "ep_items":
            None,
        "ep_items_th":
            None,
        "ep_items_en":
            None,
        "movie_type":
            "movie",
        "movie_type_th":
            "movie",
        "movie_type_en":
            "movie",
        "article_category_en": ["xxx", "yyy"],
        "article_category_th": ["xxx", "yyy"],
        "genres_en": ["xxx", "yyy"],
        "genres_th": ["xxx", "yyy"],
        "tags":
            None,
        "status":
            "draft",
        "publish_date":
            "2017-12-07T01:46:00.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "create_date":
            "2017-12-07T04:57:00.000Z",
        "update_date":
            "2017-12-21T21:49:00.000Z",
        "create_by":
            None,
        "update_by":
            None,
        "access":
            None,
        "searchable":
            "Y",
        "synopsis_short":
            None,
        "studio":
            None,
        "release_year":
            "2017",
        "actor_en": ["Chris Pine", "Gal Gadot", "Robin Wright"],
        "actor_th": ["Chris Pine", "Gal Gadot", "Robin Wright"],
        "director_en": ["Patty Jenkins"],
        "director_th": ["Patty Jenkins"],
        "writer":
            None,
        "country":
            None,
        "movie_type":
            "movie",
        "business_models":
            "tvod",
        "tv_show_code":
            None,
        "package_code":
            "draft_TEST_MOVIE",
        "HIT_COUNT_MONTH":
            12,
        "HIT_COUNT_WEEK":
            1234,
        "HIT_COUNT_YEAR":
            1234,
        "rate":
            "PG-13",
        "audio": ["TH"],
        "synopsis_en":
            "Diana, princess of the Amazons, trained to be an unconquerable warrior. Raised on a sheltered island paradise, when a pilot crashes on their shores and tells of a massive conflict raging in the outside world, Diana leaves her home, convinced she can stop the threat. Fighting alongside man in a war to end all wars, Diana will discover her full powers and her true destiny.",
        "synopsis_th":
            "ภาษาไทยน้าา",
        "subtitle": ["TH"],
        "duration":
            "132",
    },
    "not_published_TEST_MOVIE": {
        "lang":
            "en",
        "id":
            "not_published_TEST_MOVIE",
        "original_id":
            "not_published_TEST_MOVIE",
        "content_type":
            "movie",
        "title_en":
            "not published Wonder Woman",
        "title_th":
            "not published วันเดอร์ วูแมน",
        "thumb":
            "http://obmsbke.truevisions.tv/ImgLib/TV_PROGRAM/59a6dc9af7c8515eb8993a32a2f43098/a2d5ca72a2d2e30c1188d87827081494.jpg",
        "thumb_list": {
            "ott_portrait":
                "",
            "trueid_landscape":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "square_image":
                "",
            "landscape_image":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "portrait_image":
                "",
            "ott_landscape":
                "",
            "poster":
                "https://cms.dmpcdn.com/movie/2018/08/14/ff164494-122c-4692-a0d2-00f396f10190.jpg"
        },
        "detail_en":
            "Wonder Woman Diana, princess of the Amazons, trained to be an unconquerable warrior. Raised on a sheltered island paradise, when a pilot crashes on their shores and tells of a massive conflict raging in the outside world, Diana leaves her home, convinced she can stop the threat. Fighting alongside man in a war to end all wars, Diana will discover her full powers and her true destiny.",
        "detail_th":
            "ไทยน้าา",
        "tvod_flag":
            "N",
        "tvod_flag_th":
            "N",
        "tvod_flag_en":
            "N",
        "subscription_tiers": ["basic"],
        "subscription_tiers_th": ["basic"],
        "subscription_tiers_en": ["basic"],
        "count_views":
            8629,
        "count_likes":
            1,
        "count_likes_th":
            1,
        "count_likes_en":
            1,
        "ep_items":
            None,
        "ep_items_th":
            None,
        "ep_items_en":
            None,
        "movie_type":
            "movie",
        "movie_type_th":
            "movie",
        "movie_type_en":
            "movie",
        "article_category_en": ["xxx", "yyy"],
        "article_category_th": ["xxx", "yyy"],
        "genres_en": ["xxx", "yyy"],
        "genres_th": ["xxx", "yyy"],
        "tags":
            None,
        "status":
            "publish",    # status is published, but publish_date is not arrived yet
        "publish_date":
            "2025-12-07T01:46:00.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "create_date":
            "2017-12-07T04:57:00.000Z",
        "update_date":
            "2017-12-21T21:49:00.000Z",
        "create_by":
            None,
        "update_by":
            None,
        "access":
            None,
        "searchable":
            "Y",
        "synopsis_short":
            None,
        "studio":
            None,
        "release_year":
            "2017",
        "actor_en": ["Chris Pine", "Gal Gadot", "Robin Wright"],
        "actor_th": ["Chris Pine", "Gal Gadot", "Robin Wright"],
        "director_en": ["Patty Jenkins"],
        "director_th": ["Patty Jenkins"],
        "writer":
            None,
        "country":
            None,
        "movie_type":
            "movie",
        "business_models":
            "tvod",
        "tv_show_code":
            None,
        "package_code":
            "not_published_TEST_MOVIE",
        "HIT_COUNT_MONTH":
            12,
        "HIT_COUNT_WEEK":
            1234,
        "HIT_COUNT_YEAR":
            1234,
        "rate":
            "PG-13",
        "audio":
            None,
        "synopsis_en":
            "Diana, princess of the Amazons, trained to be an unconquerable warrior. Raised on a sheltered island paradise, when a pilot crashes on their shores and tells of a massive conflict raging in the outside world, Diana leaves her home, convinced she can stop the threat. Fighting alongside man in a war to end all wars, Diana will discover her full powers and her true destiny.",
        "synopsis_th":
            "ภาษาไทยน้าา",
        "subtitle":
            None,
        "duration":
            "132",
    }
}
#############################################

# Series
series = {
    "series_1": {
        "lang":
            "en",
        "id":
            "series_1",
        "original_id":
            "series_1",
        "content_type":
            "movie",
        "title_en":
            "series_1 title",
        "title_th":
            "series_1 title",
        "thumb":
            "http://obmsbke.truevisions.tv/ImgLib/TV_PROGRAM/59a6dc9af7c8515eb8993a32a2f43098/a2d5ca72a2d2e30c1188d87827081494.jpg",
        "thumb_list": {
            "ott_portrait":
                "",
            "trueid_landscape":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "square_image":
                "",
            "landscape_image":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "portrait_image":
                "",
            "ott_landscape":
                "",
            "poster":
                "https://cms.dmpcdn.com/movie/2018/08/14/ff164494-122c-4692-a0d2-00f396f10190.jpg"
        },
        "detail_en":
            "series 1 detail",
        "detail_th":
            "ไทยน้าา",
        "tvod_flag":
            "N",
        "tvod_flag_th":
            "N",
        "tvod_flag_en":
            "N",
        "subscription_tiers": ["basic"],
        "subscription_tiers_th": ["basic"],
        "subscription_tiers_en": ["basic"],
        "count_views":
            8629,
        "count_likes":
            1,
        "count_likes_th":
            1,
        "count_likes_en":
            1,
        "ep_items":
            None,
        "ep_items_th":
            None,
        "ep_items_en":
            None,
        "movie_type":
            "series",
        "movie_type_th":
            "series",
        "movie_type_en":
            "series",
        "article_category_en": ["xxx", "yyy"],
        "article_category_th": ["xxx", "yyy"],
        "genres_en": ["xxx", "yyy"],
        "genres_th": ["xxx", "yyy"],
        "tags":
            None,
        "status":
            "publish",
        "publish_date":
            "2017-12-07T01:46:00.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "create_date":
            "2017-12-07T04:57:00.000Z",
        "update_date":
            "2017-12-21T21:49:00.000Z",
        "create_by":
            None,
        "update_by":
            None,
        "access":
            None,
        "searchable":
            "Y",
        "synopsis_short":
            None,
        "studio":
            None,
        "release_year":
            "2017",
        "actor_en": ["Chris Pine", "Gal Gadot", "Robin Wright"],
        "actor_th": ["Chris Pine", "Gal Gadot", "Robin Wright"],
        "director_en": ["Patty Jenkins"],
        "director_th": ["Patty Jenkins"],
        "writer":
            None,
        "country":
            None,
        "movie_type":
            "series",
        "business_models":
            "tvod",
        "tv_show_code":
            None,
        "package_code":
            "series_1",
        "HIT_COUNT_MONTH":
            1234,
        "HIT_COUNT_WEEK":
            12,
        "HIT_COUNT_YEAR":
            5555,
        "rate":
            "PG-13",
        "audio": ["TH"],
        "synopsis_en":
            "xxx",
        "synopsis_th":
            "ภาษาไทยน้าา",
        "subtitle": ["TH"],
        "duration":
            "132",
    },
    "series_2": {
        "lang":
            "en",
        "id":
            "series_2",
        "original_id":
            "series_2",
        "content_type":
            "movie",
        "title_en":
            "series_2 title",
        "title_th":
            "series_2 title",
        "thumb":
            "http://obmsbke.truevisions.tv/ImgLib/TV_PROGRAM/59a6dc9af7c8515eb8993a32a2f43098/a2d5ca72a2d2e30c1188d87827081494.jpg",
        "thumb_list": {
            "ott_portrait":
                "",
            "trueid_landscape":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "square_image":
                "",
            "landscape_image":
                "https://cms.dmpcdn.com/movie/2018/05/08/a9f24f61-e328-4b02-bad3-7ad306b4ba99.jpg",
            "portrait_image":
                "",
            "ott_landscape":
                "",
            "poster":
                "https://cms.dmpcdn.com/movie/2018/08/14/ff164494-122c-4692-a0d2-00f396f10190.jpg"
        },
        "detail_en":
            "series_2 detail",
        "detail_th":
            "ไทยน้าา",
        "tvod_flag":
            "N",
        "tvod_flag_th":
            "N",
        "tvod_flag_en":
            "N",
        "subscription_tiers": ["basic"],
        "subscription_tiers_th": ["basic"],
        "subscription_tiers_en": ["basic"],
        "count_views":
            8629,
        "count_likes":
            1,
        "count_likes_th":
            1,
        "count_likes_en":
            1,
        "ep_items":
            None,
        "ep_items_th":
            None,
        "ep_items_en":
            None,
        "movie_type":
            "series",
        "movie_type_th":
            "series",
        "movie_type_en":
            "series",
        "article_category_en": ["abc", "abc"],
        "article_category_th": ["xxx", "yyy"],
        "genres_en": ["abc", "abc"],
        "genres_th": ["xxx", "yyy"],
        "tags":
            None,
        "status":
            "publish",
        "publish_date":
            "2016-12-07T01:46:00.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "create_date":
            "2017-12-07T04:57:00.000Z",
        "update_date":
            "2017-12-21T21:49:00.000Z",
        "create_by":
            None,
        "update_by":
            None,
        "access":
            None,
        "searchable":
            "Y",
        "synopsis_short":
            None,
        "studio":
            None,
        "release_year":
            "2017",
        "actor_en": ["Chris Pine", "Gal Gadot", "Robin Wright"],
        "actor_th": ["Chris Pine", "Gal Gadot", "Robin Wright"],
        "director_en": ["Patty Jenkins"],
        "director_th": ["Patty Jenkins"],
        "writer":
            None,
        "country":
            None,
        "movie_type":
            "series",
        "business_models":
            "tvod",
        "tv_show_code":
            None,
        "package_code":
            "series_2",
        "HIT_COUNT_MONTH":
            1234,
        "HIT_COUNT_WEEK":
            500,
        "HIT_COUNT_YEAR":
            5678,
        "rate":
            "PG-13",
        "audio": ["TH"],
        "synopsis_en":
            "xxx",
        "synopsis_th":
            "ภาษาไทยน้าา",
        "subtitle": ["TH"],
        "duration":
            "132",
    }
}

# Music

music = {
    'TEST_MUSIC_1': {
        "LANGUAGE": "TH",
        "ALBUM_NAME": "Oxyluciferous",
        "ENABLE_TIME": "2013-10-24T17:00:00.000Z",
        "HIT_COUNT_MONTH": 400,
        "GENRE": "Dance",
        "UPDATE_TIME": "2017-04-02T10:12:20.000Z",
        "DESC": "Test description",
        "EXPIRY_DATE": "2099-12-30T17:00:00.000Z",
        "NAME": "Burn It Down (Tom Swoon Remix)",
        "HIT_COUNT_WEEK": 100,
        "CODE": "0000000000000001",
        "PUBLISH_DATE": "2013-10-24T17:00:00.000Z",
        "CREATE_TIME": "2015-11-06T03:02:42.000Z",
        "HIT_COUNT_YEAR": 32079,
        "DURATION": 1000,
        "ARTIST_NAME": "Linkin Park"
    },
    'TEST_MUSIC_2': {
        "LANGUAGE": "TH",
        "ALBUM_NAME": "Oxyluciferous",
        "ENABLE_TIME": "2013-10-24T17:00:00.000Z",
        "HIT_COUNT_MONTH": 900,
        "GENRE": "Dance",
        "UPDATE_TIME": "2017-04-02T10:12:20.000Z",
        "DESC": "Test description",
        "EXPIRY_DATE": "2099-12-30T17:00:00.000Z",
        "NAME": "Burn It Down (Tom Swoon Remix)",
        "HIT_COUNT_WEEK": 500,
        "CODE": "0000000000000002",
        "PUBLISH_DATE": "2013-10-24T17:00:00.000Z",
        "CREATE_TIME": "2015-11-06T03:02:42.000Z",
        "HIT_COUNT_YEAR": 32079,
        "DURATION": 100,
        "ARTIST_NAME": ["Linkin Park"]
    },
    'TEST_MUSIC_3': {
        "LANGUAGE": "TH",
        "ALBUM_NAME": "Oxyluciferous",
        "ENABLE_TIME": "2013-10-24T17:00:00.000Z",
        "HIT_COUNT_MONTH": 1500,
        "GENRE": "Dance",
        "UPDATE_TIME": "2017-04-02T10:12:20.000Z",
        "DESC": "Test description",
        "EXPIRY_DATE": "2099-12-30T17:00:00.000Z",
        "NAME": "Burn It Down (Tom Swoon Remix)",
        "HIT_COUNT_WEEK": 1000,
        "CODE": "0000000000000003",
        "PUBLISH_DATE": "2013-10-24T17:00:00.000Z",
        "CREATE_TIME": "2015-11-06T03:02:42.000Z",
        "DURATION": 10,
        "ARTIST_NAME": ["Linkin Park"]
    },
    'TEST_MUSIC_4': {
        "LANGUAGE": "TH",
        "ALBUM_NAME": "Oxyluciferous",
        "ENABLE_TIME": "2050-10-24T17:00:00.000Z",
        "HIT_COUNT_MONTH": 95,
        "GENRE": "Dance",
        "UPDATE_TIME": "2017-04-02T10:12:20.000Z",
        "DESC": "Test description",
        "EXPIRY_DATE": "2099-12-30T17:00:00.000Z",
        "NAME": "Burn It Down (Tom Swoon Remix)",
        "HIT_COUNT_WEEK": 70,
        "CODE": "0000000000000004",
        "PUBLISH_DATE": "2013-10-24T17:00:00.000Z",
        "CREATE_TIME": "2015-11-06T03:02:42.000Z",
        "HIT_COUNT_YEAR": 32079,
        "DURATION": 1,
        "ARTIST_NAME": ["Linkin Park"]
    },
    'TEST_MUSIC_5': {
        "LANGUAGE": "TH",
        "ALBUM_NAME": "Oxyluciferous",
        "ENABLE_TIME": "2013-10-24T17:00:00.000Z",
        "HIT_COUNT_MONTH": 95,
        "GENRE": "Dance",
        "UPDATE_TIME": "2017-04-02T10:12:20.000Z",
        "DESC": "Test description",
        "EXPIRY_DATE": "2013-12-30T17:00:00.000Z",
        "NAME": "Burn It Down (Tom Swoon Remix)",
        "HIT_COUNT_WEEK": 60,
        "CODE": "0000000000000005",
        "PUBLISH_DATE": "2013-10-24T17:00:00.000Z",
        "CREATE_TIME": "2015-11-06T03:02:42.000Z",
        "HIT_COUNT_YEAR": 32079,
        "DURATION": 0,
        "ARTIST_NAME": ["Linkin Park"]
    }
}

#############################################

# these fields are added to simplify the test only. They do not exist in real CMS indices.
TEST_MERCHANT_ID = "merchant_id"
TEST_MASTER_ID = "master_id"
TEST_BRANCH_ID = "branch_id"
TEST_MERCHANT_TITLE = "merchant_name"

privileges = {
    'TEST_PRIV_1': {
        "ussd":
            "*878*38428#",
        "id":
            "n0K499qzV2wN",
        "original_id":
            "5050836",
        "content_type":
            "privilege",
        "title":
            "ลูกค้าทรู ฟรี ซุปกระดูกหมู เมื่อเช็คอิน",
        "title_th":
            "ลูกค้าทรู ฟรี ซุปกระดูกหมู เมื่อเช็คอิน",
        "title_en":
            "ลูกค้าทรู ฟรี ซุปกระดูกหมู เมื่อเช็คอิน",
        "thumb":
            None,
        "detail":
            "<p><span style=\"text-decoration: underline;\"><strong>Conditions</strong></span><br />- Show approval code displayed on phone to get your offer.<br />- This privilege can be used at a certain promotion period only.<br />- This special offer cannot be used with other promotions and cannot be exchanged for cash. <br />- Reserves the rights to make changes to the Terms && Conditions without prior notice.</p><p><span style=\"text-decoration: underline;\"><strong>How to get a redemption code?</strong></span><br />- TrueMove H customers dial <span style=\"font-weight: bold; color: red;\">*878*38428#</span> then press call (no charge)<br />- TrueVisions TrueOnline and VPCT (different mobile network) customers, get privilege via Website here or TrueYou Application (<a style=\"text-decoration: underline; font-weight: bold; color: blue;\" href=\"http://is.gd/trueyouapp\" target=\"_blank\">Download from Play Store or App Store</a>)</p>",
        "detail_th":
            "<p style=\"width: 632.1875px; \"><span style=\"text - decoration: underline; \"><strong>เงื่อนไข</strong></span><br />- ลูกค้านำรหัสที่ได้รับไปจากการกดรับสิทธิ์ ไปแสดงต่อเจ้าหน้าที่ร้านค้าเพื่อรับสิทธิ์<br />- สิทธิพิเศษนี้สามารถใช้ได้ในช่วงวันที่กำหนดเท่านั้น<br />- สงวนสิทธิ์ในการเปลี่ยนแปลงเงื่อนไข โดยไม่ต้องแจ้งให้ทราบล่วงหน้าและไม่สามารถใช้ร่วมกับรายการส่งเสริมการขาย หรือส่วนลดอื่นๆ ได้<br />- สิทธิพิเศษนี้ไม่สามารถเปลี่ยนแปลงเป็นเงินสด</p><p style=\"width: 632.1875px; \"><span style=\"font - weight: bold; text - decoration: underline; \">วิธีการขอรหัสสิทธิประโยชน์</span><br />- ลูกค้าเครือข่ายทรูมูฟ เอช กด&nbsp;<span style=\"font-weight:bold; color:red;\">*878*38428#</span></strong>&nbsp;โทรออก (ไม่เสียค่าบริการ)<br />- ลูกค้าทรูวิชั่นส์ ทรูออนไลน์ วีพีซีที (กรณีใช้มือถือต่างเครือข่าย) รับสิทธิ์ผ่านทาง Website ที่นี่ หรือรับสิทธิ์ผ่าน TrueYou Application (<a style=\"text - decoration: underline; font - weight: bold; color: blue; \" href=\"http://is.gd/trueyouapp\" target=\"_blank\">เพียงดาวน์โหลด ผ่าน Play Store หรือ App Store&&nbsp;</a>)</p>",
        "detail_en":
            "<p><span style=\"text-decoration: underline;\"><strong>Conditions</strong></span><br />- Show approval code displayed on phone to get your offer.<br />- This privilege can be used at a certain promotion period only.<br />- This special offer cannot be used with other promotions and cannot be exchanged for cash. <br />- Reserves the rights to make changes to the Terms && Conditions without prior notice.</p><p><span style=\"text-decoration: underline;\"><strong>How to get a redemption code?</strong></span><br />- TrueMove H customers dial <span style=\"font-weight: bold; color: red;\">*878*38428#</span> then press call (no charge)<br />- TrueVisions TrueOnline and VPCT (different mobile network) customers, get privilege via Website here or TrueYou Application (<a style=\"text-decoration: underline; font-weight: bold; color: blue;\" href=\"http://is.gd/trueyouapp\" target=\"_blank\">Download from Play Store or App Store</a>)</p>",
        "article_category":
            None,
        "article_category_th":
            None,
        "article_category_en":
            None,
        "tags":
            None,
        "tags_th":
            None,
        "tags_en":
            None,
        "status":
            "publish",
        "publish_date":
            "2018-06-01T17:00:00.000Z",
        "expire_date":
            "2019-06-30T16:59:59.000Z",
        "searchable":
            "Y",
        "campaign_code":
            "PRIV-38428",
        "term_and_condition":
            "<p>- The privilege cannot use as co-promotion with other campaigns, and cannot change to cash or any value<br />- The privilege condition can change without advance notification</p>",
        "term_and_condition_th":
            "<p>- สิทธิพิเศษนี้ไม่สามารถใช้ร่วมกับโปรโมชั่นอื่นๆ ได้ และไม่สามารถแลกเป็นเงินสด หรือเปลี่ยนแปลงมูลค่าได้<br />- ขอสงวนสิทธิ์ในการเปลี่ยนแปลงเงื่อนไข โดยมิต้องแจ้งให้ทราบล่วงหน้า</p>",
        "term_and_condition_en":
            "<p>- The privilege cannot use as co-promotion with other campaigns, and cannot change to cash or any value<br />- The privilege condition can change without advance notification</p>",
        "HIT_COUNT_YEAR":
            32051,
        "HIT_COUNT_MONTH":
            32051,
        "HIT_COUNT_WEEK":
            2123,
        "HIT_COUNT_DAY_1":
            2,
        "HIT_COUNT_DAY_7":
            12,
        "HIT_COUNT_DAY_14":
            22,
        "HIT_COUNT_DAY_30":
            32,
        "HIT_COUNT_DAY_365":
            42,
        TEST_MERCHANT_ID:
            "y4YQgglRpkB4",
        TEST_MASTER_ID:
            "3111989",
        TEST_BRANCH_ID:
            "All branches",
        TEST_MERCHANT_TITLE:
            'Dusit Hotels &amp; Resort'
    },
    'TEST_PRIV_2': {
        "ussd":
            "*878*38765#",
        "campaign_code":
            "PRIV-38765",
        "term_and_condition":
            "<p>- The privilege cannot use as co-promotion with other campaigns, and cannot change to cash or any value<br />- The privilege condition can change without advance notification</p>",
        "term_and_condition_th":
            "<p>- สิทธิพิเศษนี้ไม่สามารถใช้ร่วมกับโปรโมชั่นอื่นๆ ได้ และไม่สามารถแลกเป็นเงินสด หรือเปลี่ยนแปลงมูลค่าได้<br />- ขอสงวนสิทธิ์ในการเปลี่ยนแปลงเงื่อนไข โดยมิต้องแจ้งให้ทรา",
        "term_and_condition_en":
            "<p>- The privilege cannot use as co-promotion with other campaigns, and cannot change to cash or any value<br />- The privilege condition can change without advance notification</p>",
        "id":
            "v78wWGjB5b06",
        "original_id":
            "5050904",
        "content_type":
            "privilege",
        "title":
            "ลูกค้าทรู ซื้อ 1 ฟรี 1 เมื่อสั่งทำกระเป๋าใบใหญ่แถมพวงกุญแจ",
        "title_th":
            "ลูกค้าทรู ซื้อ 1 ฟรี 1 เมื่อสั่งทำกระเป๋าใบใหญ่แถมพวงกุญแจ",
        "title_en":
            "ลูกค้าทรู ซื้อ 1 ฟรี 1 เมื่อสั่งทำกระเป๋าใบใหญ่แถมพวงกุญแจ",
        "thumb":
            None,
        "detail":
            "<p><span style=\"text-decoration: underline;\"><strong>Conditions</strong></span><br />- Show approval code displayed on phone to get your offer.<br />- This privilege can be used at a certain promotion period only.<br />- This special offer cannot be used with other promotions and cannot be exchanged for cash. <br />- Reserves the rights to make changes to the Terms && Conditions without prior notice.</p><p><span style=\"text-decoration: underline;\"><strong>How to get a redemption code?</strong></span><br />- TrueMove H customers dial <span style=\"font-weight: bold; color: red;\">*878*38765#</span> then press call (no charge)<br />- TrueVisions TrueOnline and VPCT (different mobile network) customers, get privilege via Website here or TrueYou Application (<a style=\"text-decoration: underline; font-weight: bold; color: blue;\" href=\"http://is.gd/trueyouapp\" target=\"_blank\">Download from Play Store or App Store</a>)</p>",
        "detail_th":
            "<p style=\"width: 632.1875px; \"><span style=\"text - decoration: underline; \"><strong>เงื่อนไข</strong></span><br />- ลูกค้านำรหัสที่ได้รับไปจากการกดรับสิทธิ์ ไปแสดงต่อเจ้าหน้าที่ร้านค้าเพื่อรับสิทธิ์<br />- สิทธิพิเศษนี้สามารถใช้ได้ในช่วงวันที่กำหนดเท่านั้น<br />- สงวนสิทธิ์ในการเปลี่ยนแปลงเงื่อนไข โดยไม่ต้องแจ้งให้ทราบล่วงหน้าและไม่สามารถใช้ร่วมกับรายการส่งเสริมการขาย หรือส่วนลดอื่นๆ ได้<br />- สิทธิพิเศษนี้ไม่สามารถเปลี่ยนแปลงเป็นเงินสด</p><p style=\"width: 632.1875px; \"><span style=\"font - weight: bold; text - decoration: underline; \">วิธีการขอรหัสสิทธิประโยชน์</span><br />- ลูกค้าเครือข่ายทรูมูฟ เอช กด&nbsp;<span style=\"font-weight:bold; color:red;\">*878*38765#</span></strong>&nbsp;โทรออก (ไม่เสียค่าบริการ)<br />- ลูกค้าทรูวิชั่นส์ ทรูออนไลน์ วีพีซีที (กรณีใช้มือถือต่างเครือข่าย) รับสิทธิ์ผ่านทาง Website ที่นี่ หรือรับสิทธิ์ผ่าน TrueYou Application (<a style=\"text - decoration: underline; font - weight: bold; color: blue; \" href=\"http://is.gd/trueyouapp\" target=\"_blank\">เพียงดาวน์โหลด ผ่าน Play Store หรือ App Store&&nbsp;</a>)</p>",
        "detail_en":
            "<p><span style=\"text-decoration: underline;\"><strong>Conditions</strong></span><br />- Show approval code displayed on phone to get your offer.<br />- This privilege can be used at a certain promotion period only.<br />- This special offer cannot be used with other promotions and cannot be exchanged for cash. <br />- Reserves the rights to make changes to the Terms && Conditions without prior notice.</p><p><span style=\"text-decoration: underline;\"><strong>How to get a redemption code?</strong></span><br />- TrueMove H customers dial <span style=\"font-weight: bold; color: red;\">*878*38765#</span> then press call (no charge)<br />- TrueVisions TrueOnline and VPCT (different mobile network) customers, get privilege via Website here or TrueYou Application (<a style=\"text-decoration: underline; font-weight: bold; color: blue;\" href=\"http://is.gd/trueyouapp\" target=\"_blank\">Download from Play Store or App Store</a>)</p>",
        "article_category":
            None,
        "article_category_th":
            None,
        "article_category_en":
            None,
        "tags": ["กระเป๋า"],
        "tags_th": ["กระเป๋า"],
        "tags_en": ["handbags"],
        "status":
            "publish",
        "searchable":
            "Y",
        "publish_date":
            "2018-06-02T17:00:00.000Z",
        "expire_date":
            "2019-06-30T16:59:59.000Z",
        "HIT_COUNT_YEAR":
            33010,
        "HIT_COUNT_MONTH":
            10267,
        "HIT_COUNT_WEEK":
            2016,
        "HIT_COUNT_DAY_1":
            1,
        "HIT_COUNT_DAY_7":
            11,
        "HIT_COUNT_DAY_14":
            21,
        "HIT_COUNT_DAY_30":
            31,
        "HIT_COUNT_DAY_365":
            41,
        TEST_MERCHANT_ID:
            "y4YQgglRpkB4",
        TEST_MASTER_ID:
            "3111989",
        TEST_BRANCH_ID:
            "All branches",
        TEST_MERCHANT_TITLE:
            'Dusit Hotels &amp; Resort'
    },
    'TEST_PRIV_3': {
        "id":
            "Yzdy3MrVjomQ",
        "original_id":
            "5050889",
        "content_type":
            "privilege",
        "title":
            "ลูกค้าทรู ลด 10% เมื่อซื้อเสื้อผ้า ลิปสติก เซตเครื่องสำอางแต่งหน้า",
        "title_th":
            "ลูกค้าทรู ลด 10% เมื่อซื้อเสื้อผ้า ลิปสติก เซตเครื่องสำอางแต่งหน้า",
        "title_en":
            "ลูกค้าทรู ลด 10% เมื่อซื้อเสื้อผ้า ลิปสติก เซตเครื่องสำอางแต่งหน้า",
        "thumb":
            None,
        "detail":
            "<p><span style=\"text-decoration: underline;\"><strong>Conditions</strong></span><br />- Show approval code displayed on phone to get your offer.<br />- This privilege can be used at a certain promotion period only.<br />- This special offer cannot be used with other promotions and cannot be exchanged for cash. <br />- Reserves the rights to make changes to the Terms && Conditions without prior notice.</p><p><span style=\"text-decoration: underline;\"><strong>How to get a redemption code?</strong></span><br />- TrueMove H customers dial <span style=\"font-weight: bold; color: red;\">*878*38771#</span> then press call (no charge)<br />- TrueVisions TrueOnline and VPCT (different mobile network) customers, get privilege via Website here or TrueYou Application (<a style=\"text-decoration: underline; font-weight: bold; color: blue;\" href=\"http://is.gd/trueyouapp\" target=\"_blank\">Download from Play Store or App Store</a>)</p>",
        "detail_th":
            "<p style=\"width: 632.1875px; \"><span style=\"text - decoration: underline; \"><strong>เงื่อนไข</strong></span><br />- ลูกค้านำรหัสที่ได้รับไปจากการกดรับสิทธิ์ ไปแสดงต่อเจ้าหน้าที่ร้านค้าเพื่อรับสิทธิ์<br />- สิทธิพิเศษนี้สามารถใช้ได้ในช่วงวันที่กำหนดเท่านั้น<br />- สงวนสิทธิ์ในการเปลี่ยนแปลงเงื่อนไข โดยไม่ต้องแจ้งให้ทราบล่วงหน้าและไม่สามารถใช้ร่วมกับรายการส่งเสริมการขาย หรือส่วนลดอื่นๆ ได้<br />- สิทธิพิเศษนี้ไม่สามารถเปลี่ยนแปลงเป็นเงินสด</p><p style=\"width: 632.1875px; \"><span style=\"font - weight: bold; text - decoration: underline; \">วิธีการขอรหัสสิทธิประโยชน์</span><br />- ลูกค้าเครือข่ายทรูมูฟ เอช กด&nbsp;<span style=\"font-weight:bold; color:red;\">*878*38771#</span></strong>&nbsp;โทรออก (ไม่เสียค่าบริการ)<br />- ลูกค้าทรูวิชั่นส์ ทรูออนไลน์ วีพีซีที (กรณีใช้มือถือต่างเครือข่าย) รับสิทธิ์ผ่านทาง Website ที่นี่ หรือรับสิทธิ์ผ่าน TrueYou Application (<a style=\"text - decoration: underline; font - weight: bold; color: blue; \" href=\"http://is.gd/trueyouapp\" target=\"_blank\">เพียงดาวน์โหลด ผ่าน Play Store หรือ App Store&&nbsp;</a>)</p>",
        "detail_en":
            "<p><span style=\"text-decoration: underline;\"><strong>Conditions</strong></span><br />- Show approval code displayed on phone to get your offer.<br />- This privilege can be used at a certain promotion period only.<br />- This special offer cannot be used with other promotions and cannot be exchanged for cash. <br />- Reserves the rights to make changes to the Terms && Conditions without prior notice.</p><p><span style=\"text-decoration: underline;\"><strong>How to get a redemption code?</strong></span><br />- TrueMove H customers dial <span style=\"font-weight: bold; color: red;\">*878*38771#</span> then press call (no charge)<br />- TrueVisions TrueOnline and VPCT (different mobile network) customers, get privilege via Website here or TrueYou Application (<a style=\"text-decoration: underline; font-weight: bold; color: blue;\" href=\"http://is.gd/trueyouapp\" target=\"_blank\">Download from Play Store or App Store</a>)</p>",
        "article_category":
            None,
        "article_category_th":
            None,
        "article_category_en":
            None,
        "tags": ["clothes"],
        "tags_th": ["เสื้อผ้า"],
        "tags_en": ["clothes"],
        "status":
            "publish",
        "publish_date":
            "2018-06-02T17:00:00.000Z",
        "searchable":
            "Y",
        "ussd":
            "*878*38771#",
        "expire_date":
            "2019-06-30T16:59:59.000Z",
        "campaign_code":
            "PRIV-38771",
        "term_and_condition":
            "<p>- The privilege cannot use as co-promotion with other campaigns, and cannot change to cash or any value<br />- The privilege condition can change without advance notification</p>",
        "term_and_condition_th":
            "<p>- สิทธิพิเศษนี้ไม่สามารถใช้ร่วมกับโปรโมชั่นอื่นๆ ได้ และไม่สามารถแลกเป็นเงินสด หรือเปลี่ยนแปลงมูลค่าได้<br />- ขอสงวนสิทธิ์ในการเปลี่ยนแปลงเงื่อนไข โดยมิต้องแจ้งให้ทราบล่วงหน้า</p>",
        "term_and_condition_en":
            "<p>- The privilege cannot use as co-promotion with other campaigns, and cannot change to cash or any value<br />- The privilege condition can change without advance notification</p>",
        "HIT_COUNT_YEAR":
            9082,
        "HIT_COUNT_MONTH":
            9082,
        "HIT_COUNT_WEEK":
            90,
        "HIT_COUNT_DAY_1":
            3,
        "HIT_COUNT_DAY_7":
            13,
        "HIT_COUNT_DAY_14":
            23,
        "HIT_COUNT_DAY_30":
            33,
        "HIT_COUNT_DAY_365":
            43,
        TEST_MERCHANT_ID:
            "jAjOdd9Y9lvJ",
        TEST_MASTER_ID:
            "5065707",
        TEST_BRANCH_ID:
            "All branches",
        TEST_MERCHANT_TITLE:
            'Foodland'
    },
    'TEST_PRIV_4': {
        "id":
            "ZkpXzzQL38lk",
        "original_id":
            "5050842",
        "content_type":
            "privilege",
        "title":
            "ลูกค้าทรู ลด 20% ในคอร์สเรียนที่ 2 เมื่อสมัครคอร์สเรียน 5,000 บาท",
        "title_th":
            "ลูกค้าทรู ลด 20% ในคอร์สเรียนที่ 2 เมื่อสมัครคอร์สเรียน 5,000 บาท",
        "title_en":
            "ลูกค้าทรู ลด 20% ในคอร์สเรียนที่ 2 เมื่อสมัครคอร์สเรียน 5,000 บาท",
        "thumb":
            None,
        "detail":
            "<p><span style=\"text-decoration: underline;\"><strong>Conditions</strong></span><br />- Show approval code displayed on phone to get your offer.<br />- This privilege can be used at a certain promotion period only.<br />- This special offer cannot be used with other promotions and cannot be exchanged for cash. <br />- Reserves the rights to make changes to the Terms && Conditions without prior notice.</p><p><span style=\"text-decoration: underline;\"><strong>How to get a redemption code?</strong></span><br />- TrueMove H customers dial <span style=\"font-weight: bold; color: red;\">*878*38425#</span> then press call (no charge)<br />- TrueVisions TrueOnline and VPCT (different mobile network) customers, get privilege via Website here or TrueYou Application (<a style=\"text-decoration: underline; font-weight: bold; color: blue;\" href=\"http://is.gd/trueyouapp\" target=\"_blank\">Download from Play Store or App Store</a>)</p>",
        "detail_th":
            "<p style=\"width: 632.1875px; \"><span style=\"text - decoration: underline; \"><strong>เงื่อนไข</strong></span><br />- ลูกค้านำรหัสที่ได้รับไปจากการกดรับสิทธิ์ ไปแสดงต่อเจ้าหน้าที่ร้านค้าเพื่อรับสิทธิ์<br />- สิทธิพิเศษนี้สามารถใช้ได้ในช่วงวันที่กำหนดเท่านั้น<br />- สงวนสิทธิ์ในการเปลี่ยนแปลงเงื่อนไข โดยไม่ต้องแจ้งให้ทราบล่วงหน้าและไม่สามารถใช้ร่วมกับรายการส่งเสริมการขาย หรือส่วนลดอื่นๆ ได้<br />- สิทธิพิเศษนี้ไม่สามารถเปลี่ยนแปลงเป็นเงินสด</p><p style=\"width: 632.1875px; \"><span style=\"font - weight: bold; text - decoration: underline; \">วิธีการขอรหัสสิทธิประโยชน์</span><br />- ลูกค้าเครือข่ายทรูมูฟ เอช กด&nbsp;<span style=\"font-weight:bold; color:red;\">*878*38425#</span></strong>&nbsp;โทรออก (ไม่เสียค่าบริการ)<br />- ลูกค้าทรูวิชั่นส์ ทรูออนไลน์ วีพีซีที (กรณีใช้มือถือต่างเครือข่าย) รับสิทธิ์ผ่านทาง Website ที่นี่ หรือรับสิทธิ์ผ่าน TrueYou Application (<a style=\"text - decoration: underline; font - weight: bold; color: blue; \" href=\"http://is.gd/trueyouapp\" target=\"_blank\">เพียงดาวน์โหลด ผ่าน Play Store หรือ App Store&&nbsp;</a>)</p>",
        "detail_en":
            "<p><span style=\"text-decoration: underline;\"><strong>Conditions</strong></span><br />- Show approval code displayed on phone to get your offer.<br />- This privilege can be used at a certain promotion period only.<br />- This special offer cannot be used with other promotions and cannot be exchanged for cash. <br />- Reserves the rights to make changes to the Terms && Conditions without prior notice.</p><p><span style=\"text-decoration: underline;\"><strong>How to get a redemption code?</strong></span><br />- TrueMove H customers dial <span style=\"font-weight: bold; color: red;\">*878*38425#</span> then press call (no charge)<br />- TrueVisions TrueOnline and VPCT (different mobile network) customers, get privilege via Website here or TrueYou Application (<a style=\"text-decoration: underline; font-weight: bold; color: blue;\" href=\"http://is.gd/trueyouapp\" target=\"_blank\">Download from Play Store or App Store</a>)</p>",
        "article_category":
            None,
        "article_category_th":
            None,
        "article_category_en":
            None,
        "tags":
            None,
        "tags_th":
            None,
        "tags_en":
            None,
        "status":
            "publish",
        "publish_date":
            "2018-06-01T17:00:00.000Z",
        "searchable":
            "Y",
        "ussd":
            "*878*38425#",
        "expire_date":
            "2019-06-30T16:59:59.000Z",
        "campaign_code":
            "PRIV-38425",
        "term_and_condition":
            "<p>- The privilege cannot use as co-promotion with other campaigns, and cannot change to cash or any value<br />- The privilege condition can change without advance notification</p>",
        "term_and_condition_th":
            "<p>- สิทธิพิเศษนี้ไม่สามารถใช้ร่วมกับโปรโมชั่นอื่นๆ ได้ และไม่สามารถแลกเป็นเงินสด หรือเปลี่ยนแปลงมูลค่าได้<br />- ขอสงวนสิทธิ์ในการเปลี่ยนแปลงเงื่อนไข โดยมิต้องแจ้งให้ทราบล่วงหน้า</p>",
        "term_and_condition_en":
            "<p>- The privilege cannot use as co-promotion with other campaigns, and cannot change to cash or any value<br />- The privilege condition can change without advance notification</p>",
        "HIT_COUNT_YEAR":
            14335,
        "HIT_COUNT_MONTH":
            7415,
        "HIT_COUNT_WEEK":
            10000,
        "HIT_COUNT_DAY_1":
            4,
        "HIT_COUNT_DAY_7":
            14,
        "HIT_COUNT_DAY_14":
            24,
        "HIT_COUNT_DAY_30":
            34,
        "HIT_COUNT_DAY_365":
            44,
        TEST_MERCHANT_ID:
            "NmB0BBQRle1o",
        TEST_MASTER_ID:
            "5056550",
        TEST_BRANCH_ID:
            "All branches",
        TEST_MERCHANT_TITLE:
            'Skirtlandshop (Nonthaburi)'
    },
    'TEST_PRIV_5': {
        "id":
            "85yb7xOamvV1",
        "original_id":
            "5050937",
        "content_type":
            "privilege",
        "title":
            "ลูกค้าทรู ลด 10% เมื่อซื้อสินคัาครบ 500 บาท",
        "title_th":
            "ลูกค้าทรู ลด 10% เมื่อซื้อสินคัาครบ 500 บาท",
        "title_en":
            "ลูกค้าทรู ลด 10% เมื่อซื้อสินคัาครบ 500 บาท",
        "thumb":
            None,
        "detail":
            "<p><span style=\"text-decoration: underline;\"><strong>Conditions</strong></span><br />- Show approval code displayed on phone to get your offer.<br />- This privilege can be used at a certain promotion period only.<br />- This special offer cannot be used with other promotions and cannot be exchanged for cash. <br />- Reserves the rights to make changes to the Terms && Conditions without prior notice.</p><p><span style=\"text-decoration: underline;\"><strong>How to get a redemption code?</strong></span><br />- TrueMove H customers dial <span style=\"font-weight: bold; color: red;\">*878*38750#</span> then press call (no charge)<br />- TrueVisions TrueOnline and VPCT (different mobile network) customers, get privilege via Website here or TrueYou Application (<a style=\"text-decoration: underline; font-weight: bold; color: blue;\" href=\"http://is.gd/trueyouapp\" target=\"_blank\">Download from Play Store or App Store</a>)</p>",
        "detail_th":
            "<p style=\"width: 632.1875px; \"><span style=\"text - decoration: underline; \"><strong>เงื่อนไข</strong></span><br />- ลูกค้านำรหัสที่ได้รับไปจากการกดรับสิทธิ์ ไปแสดงต่อเจ้าหน้าที่ร้านค้าเพื่อรับสิทธิ์<br />- สิทธิพิเศษนี้สามารถใช้ได้ในช่วงวันที่กำหนดเท่านั้น<br />- สงวนสิทธิ์ในการเปลี่ยนแปลงเงื่อนไข โดยไม่ต้องแจ้งให้ทราบล่วงหน้าและไม่สามารถใช้ร่วมกับรายการส่งเสริมการขาย หรือส่วนลดอื่นๆ ได้<br />- สิทธิพิเศษนี้ไม่สามารถเปลี่ยนแปลงเป็นเงินสด</p><p style=\"width: 632.1875px; \"><span style=\"font - weight: bold; text - decoration: underline; \">วิธีการขอรหัสสิทธิประโยชน์</span><br />- ลูกค้าเครือข่ายทรูมูฟ เอช กด&nbsp;<span style=\"font-weight:bold; color:red;\">*878*38750#</span></strong>&nbsp;โทรออก (ไม่เสียค่าบริการ)<br />- ลูกค้าทรูวิชั่นส์ ทรูออนไลน์ วีพีซีที (กรณีใช้มือถือต่างเครือข่าย) รับสิทธิ์ผ่านทาง Website ที่นี่ หรือรับสิทธิ์ผ่าน TrueYou Application (<a style=\"text - decoration: underline; font - weight: bold; color: blue; \" href=\"http://is.gd/trueyouapp\" target=\"_blank\">เพียงดาวน์โหลด ผ่าน Play Store หรือ App Store&&nbsp;</a>)</p>",
        "detail_en":
            "<p><span style=\"text-decoration: underline;\"><strong>Conditions</strong></span><br />- Show approval code displayed on phone to get your offer.<br />- This privilege can be used at a certain promotion period only.<br />- This special offer cannot be used with other promotions and cannot be exchanged for cash. <br />- Reserves the rights to make changes to the Terms && Conditions without prior notice.</p><p><span style=\"text-decoration: underline;\"><strong>How to get a redemption code?</strong></span><br />- TrueMove H customers dial <span style=\"font-weight: bold; color: red;\">*878*38750#</span> then press call (no charge)<br />- TrueVisions TrueOnline and VPCT (different mobile network) customers, get privilege via Website here or TrueYou Application (<a style=\"text-decoration: underline; font-weight: bold; color: blue;\" href=\"http://is.gd/trueyouapp\" target=\"_blank\">Download from Play Store or App Store</a>)</p>",
        "article_category":
            None,
        "article_category_th":
            None,
        "article_category_en":
            None,
        "tags":
            None,
        "tags_th":
            None,
        "tags_en":
            None,
        "status":
            "publish",
        "publish_date":
            "2018-06-02T17:00:00.000Z",
        "searchable":
            "Y",
        "ussd":
            "*878*38750#",
        "expire_date":
            "2019-06-30T16:59:59.000Z",
        "campaign_code":
            "PRIV-38750",
        "term_and_condition":
            "<p>- The privilege cannot use as co-promotion with other campaigns, and cannot change to cash or any value<br />- The privilege condition can change without advance notification</p>",
        "term_and_condition_th":
            "<p>- สิทธิพิเศษนี้ไม่สามารถใช้ร่วมกับโปรโมชั่นอื่นๆ ได้ และไม่สามารถแลกเป็นเงินสด หรือเปลี่ยนแปลงมูลค่าได้<br />- ขอสงวนสิทธิ์ในการเปลี่ยนแปลงเงื่อนไข โดยมิต้องแจ้งให้ทราบล่วงหน้า</p>",
        "term_and_condition_en":
            "<p>- The privilege cannot use as co-promotion with other campaigns, and cannot change to cash or any value<br />- The privilege condition can change without advance notification</p>",
        "HIT_COUNT_YEAR":
            64030,
        "HIT_COUNT_MONTH":
            6403,
        "HIT_COUNT_WEEK":
            0,
        "HIT_COUNT_DAY_1":
            5,
        "HIT_COUNT_DAY_7":
            15,
        "HIT_COUNT_DAY_14":
            25,
        "HIT_COUNT_DAY_30":
            35,
        "HIT_COUNT_DAY_365":
            45,
        TEST_MERCHANT_ID:
            "y4YQgglRpkB4",
        TEST_MASTER_ID:
            "3111989",
        TEST_BRANCH_ID:
            "Y1KjqqVD8eMd",
        TEST_MERCHANT_TITLE:
            'Dusit Hotels &amp; Resort'
    },
    'not_searchable_TEST_PRIVILEGE': {
        "id":
            "not_searchable_TEST_PRIVILEGE",
        "original_id":
            "not_searchable_TEST_PRIVILEGE",
        "content_type":
            "privilege",
        "title":
            "not_searchable ลูกค้าทรู ลด 10% เมื่อซื้อสินคัาครบ 500 บาท",
        "title_th":
            "not_searchable ลูกค้าทรู ลด 10% เมื่อซื้อสินคัาครบ 500 บาท",
        "title_en":
            "not_searchable ลูกค้าทรู ลด 10% เมื่อซื้อสินคัาครบ 500 บาท",
        "thumb":
            None,
        "searchable":
            "N",
        "detail":
            "<p><span style=\"text-decoration: underline;\"><strong>Conditions</strong></span><br />- Show approval code displayed on phone to get your offer.<br />- This privilege can be used at a certain promotion period only.<br />- This special offer cannot be used with other promotions and cannot be exchanged for cash. <br />- Reserves the rights to make changes to the Terms && Conditions without prior notice.</p><p><span style=\"text-decoration: underline;\"><strong>How to get a redemption code?</strong></span><br />- TrueMove H customers dial <span style=\"font-weight: bold; color: red;\">*878*38750#</span> then press call (no charge)<br />- TrueVisions TrueOnline and VPCT (different mobile network) customers, get privilege via Website here or TrueYou Application (<a style=\"text-decoration: underline; font-weight: bold; color: blue;\" href=\"http://is.gd/trueyouapp\" target=\"_blank\">Download from Play Store or App Store</a>)</p>",
        "detail_th":
            "<p style=\"width: 632.1875px; \"><span style=\"text - decoration: underline; \"><strong>เงื่อนไข</strong></span><br />- ลูกค้านำรหัสที่ได้รับไปจากการกดรับสิทธิ์ ไปแสดงต่อเจ้าหน้าที่ร้านค้าเพื่อรับสิทธิ์<br />- สิทธิพิเศษนี้สามารถใช้ได้ในช่วงวันที่กำหนดเท่านั้น<br />- สงวนสิทธิ์ในการเปลี่ยนแปลงเงื่อนไข โดยไม่ต้องแจ้งให้ทราบล่วงหน้าและไม่สามารถใช้ร่วมกับรายการส่งเสริมการขาย หรือส่วนลดอื่นๆ ได้<br />- สิทธิพิเศษนี้ไม่สามารถเปลี่ยนแปลงเป็นเงินสด</p><p style=\"width: 632.1875px; \"><span style=\"font - weight: bold; text - decoration: underline; \">วิธีการขอรหัสสิทธิประโยชน์</span><br />- ลูกค้าเครือข่ายทรูมูฟ เอช กด&nbsp;<span style=\"font-weight:bold; color:red;\">*878*38750#</span></strong>&nbsp;โทรออก (ไม่เสียค่าบริการ)<br />- ลูกค้าทรูวิชั่นส์ ทรูออนไลน์ วีพีซีที (กรณีใช้มือถือต่างเครือข่าย) รับสิทธิ์ผ่านทาง Website ที่นี่ หรือรับสิทธิ์ผ่าน TrueYou Application (<a style=\"text - decoration: underline; font - weight: bold; color: blue; \" href=\"http://is.gd/trueyouapp\" target=\"_blank\">เพียงดาวน์โหลด ผ่าน Play Store หรือ App Store&&nbsp;</a>)</p>",
        "detail_en":
            "<p><span style=\"text-decoration: underline;\"><strong>Conditions</strong></span><br />- Show approval code displayed on phone to get your offer.<br />- This privilege can be used at a certain promotion period only.<br />- This special offer cannot be used with other promotions and cannot be exchanged for cash. <br />- Reserves the rights to make changes to the Terms && Conditions without prior notice.</p><p><span style=\"text-decoration: underline;\"><strong>How to get a redemption code?</strong></span><br />- TrueMove H customers dial <span style=\"font-weight: bold; color: red;\">*878*38750#</span> then press call (no charge)<br />- TrueVisions TrueOnline and VPCT (different mobile network) customers, get privilege via Website here or TrueYou Application (<a style=\"text-decoration: underline; font-weight: bold; color: blue;\" href=\"http://is.gd/trueyouapp\" target=\"_blank\">Download from Play Store or App Store</a>)</p>",
        "article_category":
            None,
        "article_category_th":
            None,
        "article_category_en":
            None,
        "tags":
            None,
        "tags_th":
            None,
        "tags_en":
            None,
        "status":
            "publish",
        "publish_date":
            "2018-06-02T17:00:00.000Z",
        "ussd":
            "*878*38750#",
        "expire_date":
            "2019-06-30T16:59:59.000Z",
        "campaign_code":
            "PRIV-38750",
        "term_and_condition":
            "<p>- The privilege cannot use as co-promotion with other campaigns, and cannot change to cash or any value<br />- The privilege condition can change without advance notification</p>",
        "term_and_condition_th":
            "<p>- สิทธิพิเศษนี้ไม่สามารถใช้ร่วมกับโปรโมชั่นอื่นๆ ได้ และไม่สามารถแลกเป็นเงินสด หรือเปลี่ยนแปลงมูลค่าได้<br />- ขอสงวนสิทธิ์ในการเปลี่ยนแปลงเงื่อนไข โดยมิต้องแจ้งให้ทราบล่วงหน้า</p>",
        "term_and_condition_en":
            "<p>- The privilege cannot use as co-promotion with other campaigns, and cannot change to cash or any value<br />- The privilege condition can change without advance notification</p>",
        "HIT_COUNT_YEAR":
            64025,
        "HIT_COUNT_MONTH":
            6403,
        "HIT_COUNT_WEEK":
            0,
        "HIT_COUNT_DAY_1":
            6,
        "HIT_COUNT_DAY_7":
            16,
        "HIT_COUNT_DAY_14":
            26,
        "HIT_COUNT_DAY_30":
            36,
        "HIT_COUNT_DAY_365":
            46,
        TEST_MERCHANT_ID:
            "Not found",
        TEST_MASTER_ID:
            "Not found",
        TEST_BRANCH_ID:
            "Not found",
        TEST_MERCHANT_TITLE:
            "Not found"
    }
}

shops = {
    'TEST_SHOP_1': {
        "id":
            "y4YQgglRpkB4",
        "original_id":
            "3111989",
        "searchable":
            "Y",
        "detail":
            "&lt;p&gt;&lt;span style=&quot;text-decoration: underline;&quot;&gt;&lt;strong&gt;Dusit International&lt;/strong&gt;&lt;/span&gt; has been in the hotel &amp;amp; service industry for over 60 year established by Thanpuying Chanat Piyaoui, Honourary President. Princess hotel, the group’s first hotel, was opened in Bangkok on Charoenkrung road in 1949. As one of the most glamous hotel, inspired by Thai traditions and cultures, of its kind in those days, people still remember it. Excellent service to give the most satisfaction to guests, with warm welcome as per Thai customs. Dusit now delivers experience that enlivens the individual spirit no matter the journey.&lt;/p&gt;\n&lt;p&gt;More information please visit &lt;a href=&quot;http://www.dusit.com&quot; target=&quot;_blank&quot; rel=&quot;noopener&quot;&gt;http://www.dusit.com&lt;/a&gt;&lt;/p&gt;",
        "detail_th":
            "&lt;p&gt;&lt;span style=&quot;text-decoration: underline;&quot;&gt;&lt;strong&gt;ดุสิต อินเตอร์เนชั่นแนล&lt;/strong&gt;&lt;/span&gt; มีประสบการณ์ด้านธุรกิจ โรงแรมและงานบริการมานานกว่า 60 ปี ก่อตั้งขึ้นเมื่อ พ.ศ. 2492 โดย ท่านผู้หญิงชนัตถ์ ปิยะอุย ประธานกิตติมศักดิ์ ซึ่งเป็นผู้ริเริ่มตั้งโรงแรมแห่งแรกในชื่อ โรงแรมปริ๊นเซสบนถนนเจริญกรุง ของกรุงเทพฯ ด้วย ภาพลักษณ์อันโดดเด่น และเป็นเอกลักษณ์ ของดุสิต ทำให้ผู้คนต่างจดจำได้ว่าเป็นโรงแรมสุดหรู ที่สร้างขึ้นภายใต้แรงบันดาลใจจากประเพณีและวัฒนธรรมไทย อันวิจิตรงดงามและทรงคุณค่า อีกทั้งสร้างสรรค์งานบริการที่ดี ที่พร้อมมอบความพึงพอใจให้ผู้มาเยือน ด้วยการต้อนรับอย่างอบอุ่นแบบไทย ซึ่งเป็นลักษณะเฉพาะ ของเครือดุสิต อันเป็นการส่งผ่านประสบการณ์ ไม่ว่าจุดประสงค์ในการมาเยือนจะเป็นอะไร ก็จะสามารถรู้สึกได้ว่าการได้สัมผัสกับการบริการในแบบฉบับของดุสิตนั้น เปรียบเสมือนการเติมพลังให้กับชีวิต&lt;/p&gt;\n&lt;p&gt;ข้อมูลเพิ่มเติม &lt;a href=&quot;http://www.dusit.com&quot; target=&quot;_blank&quot; rel=&quot;noopener&quot;&gt;http://www.dusit.com&lt;/a&gt;&lt;/p&gt;",
        "detail_en":
            "&lt;p&gt;&lt;span style=&quot;text-decoration: underline;&quot;&gt;&lt;strong&gt;Dusit International&lt;/strong&gt;&lt;/span&gt; has been in the hotel &amp;amp; service industry for over 60 year established by Thanpuying Chanat Piyaoui, Honourary President. Princess hotel, the group’s first hotel, was opened in Bangkok on Charoenkrung road in 1949. As one of the most glamous hotel, inspired by Thai traditions and cultures, of its kind in those days, people still remember it. Excellent service to give the most satisfaction to guests, with warm welcome as per Thai customs. Dusit now delivers experience that enlivens the individual spirit no matter the journey.&lt;/p&gt;\n&lt;p&gt;More information please visit &lt;a href=&quot;http://www.dusit.com&quot; target=&quot;_blank&quot; rel=&quot;noopener&quot;&gt;http://www.dusit.com&lt;/a&gt;&lt;/p&gt;",
        "status":
            "publish",
        "thumb_list": {
            "logo_s":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/8063112/web-80x60.jpg",
            "highlight":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/2756382/web-795x258-trueid.jpg"
        },
        "title":
            "Dusit Hotels &amp; Resort",
        "title_th":
            "ดุสิตโฮเท็ล แอนด์ รีสอร์ท",
        "title_en":
            "Dusit Hotels &amp; Resort",
        "content_type":
            "trueyoumerchant",
        "article_category":
            None,
        "article_category_th":
            None,
        "article_category_en":
            None,
        "address":
            None,
        "address_th":
            None,
        "address_en":
            None,
        "location":
            None,
        "publish_date":
            "2014-05-07T09:29:05.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "tags": [
            "Dusit", "accommodation", "dusit", "hotels", "resort", "resorts",
            "rooms", "tour package", "travel", "ดุสิต", "ที่พัก", "ท่องเที่ยว",
            "รีสอร์ท", "ห้องพัก", "เดินทาง", "โรงแรม"
        ],
        "tags_th": [
            "Dusit", "accommodation", "dusit", "hotels", "resort", "resorts",
            "rooms", "tour package", "travel", "ดุสิต", "ที่พัก", "ท่องเที่ยว",
            "รีสอร์ท", "ห้องพัก", "เดินทาง", "โรงแรม"
        ],
        "tags_en": [
            "Dusit", "accommodation", "dusit", "hotels", "resort", "resorts",
            "rooms", "tour package", "travel", "ดุสิต", "ที่พัก", "ท่องเที่ยว",
            "รีสอร์ท", "ห้องพัก", "เดินทาง", "โรงแรม"
        ],
        "HIT_COUNT_YEAR":
            100,
        "HIT_COUNT_MONTH":
            50,
        "HIT_COUNT_WEEK":
            90,
        "privilege_list": ["n0K499qzV2wN", "v78wWGjB5b06"],
        "discover":
            "y",
        "merchant_type":
            None,
        "denormalize_privilege": [{
            "campaign_type": "point",
            "quota_over_existed": 0,
        }],
    },
    'TEST_SHOP_2': {
        "id":
            "jAjOdd9Y9lvJ",
        "original_id":
            "5065707",
        "detail":
            "&lt;p&gt;ฟู้ดแลนด์เป็นซุปเปอร์มาร์เก็ตแห่งแรกที่มีร้านอาหารและซุปเปอร์มาร์เก็ตที่เปิดบริการตลอด 24 ชั่วโมง ที่สร้างมาตรฐานใหม่ที่สูงขึ้นสำหรับ ผลิตภัณฑ์อาหารสดในประเทศไทย ชื่อเสียงของฟู้ดแลนด์ซุปเปอร์มาร์เก็ตเป็นที่รู้จักยาวนานในด้านคุณภาพอาหารสดที่ดี ผลิตภัณฑ์ที่มีให้เลือก อย่างหลากหลาย ความสดของส่วนประกอบที่นำมาใช้ทำอาหารและการคงไว้ซึ่งการบริการที่ดี เรามุ่งมั่นที่จะดำเนินตามวิสัยทัศน์ในการเป็น ซุปเปอร์มาร์เก็ตที่ให้บริการสินค้าอาหารสดที่มีคุณภาพให้แก่ลูกค้าด้วยราคามาตรฐานยุติธรรม เรามุ่งมั่นให้การบริการอย่างดีที่สุด และมีมาตรฐานเป็นเลิศ คัดสรรเฉพาะสินค้าอุปโภคบริโภคคุณภาพจากทั้งในและต่างประเทศ&lt;/p&gt;",
        "detail_th":
            "&lt;p&gt;ฟู้ดแลนด์เป็นซุปเปอร์มาร์เก็ตแห่งแรกที่มีร้านอาหารและซุปเปอร์มาร์เก็ตที่เปิดบริการตลอด 24 ชั่วโมง ที่สร้างมาตรฐานใหม่ที่สูงขึ้นสำหรับ ผลิตภัณฑ์อาหารสดในประเทศไทย ชื่อเสียงของฟู้ดแลนด์ซุปเปอร์มาร์เก็ตเป็นที่รู้จักยาวนานในด้านคุณภาพอาหารสดที่ดี ผลิตภัณฑ์ที่มีให้เลือก อย่างหลากหลาย ความสดของส่วนประกอบที่นำมาใช้ทำอาหารและการคงไว้ซึ่งการบริการที่ดี เรามุ่งมั่นที่จะดำเนินตามวิสัยทัศน์ในการเป็น ซุปเปอร์มาร์เก็ตที่ให้บริการสินค้าอาหารสดที่มีคุณภาพให้แก่ลูกค้าด้วยราคามาตรฐานยุติธรรม เรามุ่งมั่นให้การบริการอย่างดีที่สุด และมีมาตรฐานเป็นเลิศ คัดสรรเฉพาะสินค้าอุปโภคบริโภคคุณภาพจากทั้งในและต่างประเทศ&lt;/p&gt;",
        "detail_en":
            "&lt;p&gt;ฟู้ดแลนด์เป็นซุปเปอร์มาร์เก็ตแห่งแรกที่มีร้านอาหารและซุปเปอร์มาร์เก็ตที่เปิดบริการตลอด 24 ชั่วโมง ที่สร้างมาตรฐานใหม่ที่สูงขึ้นสำหรับ ผลิตภัณฑ์อาหารสดในประเทศไทย ชื่อเสียงของฟู้ดแลนด์ซุปเปอร์มาร์เก็ตเป็นที่รู้จักยาวนานในด้านคุณภาพอาหารสดที่ดี ผลิตภัณฑ์ที่มีให้เลือก อย่างหลากหลาย ความสดของส่วนประกอบที่นำมาใช้ทำอาหารและการคงไว้ซึ่งการบริการที่ดี เรามุ่งมั่นที่จะดำเนินตามวิสัยทัศน์ในการเป็น ซุปเปอร์มาร์เก็ตที่ให้บริการสินค้าอาหารสดที่มีคุณภาพให้แก่ลูกค้าด้วยราคามาตรฐานยุติธรรม เรามุ่งมั่นให้การบริการอย่างดีที่สุด และมีมาตรฐานเป็นเลิศ คัดสรรเฉพาะสินค้าอุปโภคบริโภคคุณภาพจากทั้งในและต่างประเทศ&lt;/p&gt;",
        "publish_date":
            "2018-06-21T07:21:43.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "status":
            "publish",
        "thumb_list": {
            "logo_s":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/8063112/web-80x60.jpg",
            "highlight":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/2756382/web-795x258-trueid.jpg"
        },
        "article_category":
            None,
        "article_category_th":
            None,
        "article_category_en":
            None,
        "content_type":
            "trueyoumerchant",
        "address":
            None,
        "address_th":
            None,
        "address_en":
            None,
        "location":
            None,
        "searchable":
            "Y",
        "title":
            "Foodland",
        "title_th":
            "ฟู้ดแลนด์",
        "title_en":
            "Foodland",
        "tags": [
            "buying",
            "food",
            "sausage",
            "shopping",
            "shops",
            "stores",
            "supermarkets",
        ],
        "tags_th": [
            "ช้อปปิ้ง", "ซื้อของ", "ซูเปอร์มาร์เก็ต", "ร้านค้า", "อาหาร",
            "อาหารสด", "ไส้กรอก"
        ],
        "tags_en": [
            "buying",
            "food",
            "sausage",
            "shopping",
            "shops",
            "stores",
            "supermarkets",
        ],
        "HIT_COUNT_YEAR":
            10,
        "HIT_COUNT_MONTH":
            90,
        "HIT_COUNT_WEEK":
            80,
        "privilege_list": ["Yzdy3MrVjomQ"],
        "discover":
            "y",
        "merchant_type":
            None,
        "denormalize_privilege": [{
            "campaign_type": "privilege",
            "quota_over_existed": 0,
        }],
    },
    'TEST_SHOP_3': {
        "id":
            "NmB0BBQRle1o",
        "original_id":
            "5056550",
        "searchable":
            "Y",
        "title":
            "Skirtlandshop (Nonthaburi)",
        "title_th":
            "สเกิร์ตแลนด์ช็อป (จ.นนทบุรี)",
        "title_en":
            "Skirtlandshop (Nonthaburi)",
        "detail":
            None,
        "detail_th":
            "&lt;p&gt;ร้านค้าออนไลน์จำหน่ายกระโปรงแฟชั่น กระโปรงยีนส์ สั่งตัวเดียวก็ได้หรือหลายตัวก็ได้ราคาส่ง&lt;/p&gt;",
        "detail_en":
            None,
        "publish_date":
            "2018-06-01T04:32:49.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "status":
            "publish",
        "thumb_list": {
            "logo_s":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/8063112/web-80x60.jpg",
            "highlight":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/2756382/web-795x258-trueid.jpg"
        },
        "article_category":
            None,
        "article_category_en":
            None,
        "article_category_th":
            None,
        "content_type":
            "trueyoumerchant",
        "address":
            None,
        "address_th":
            None,
        "address_en":
            None,
        "location":
            None,
        "tags": [
            "ช้อปปิ้ง", "ซื้อของ", "นนทบุรี", "ร้านค้า", "ร้านค้าออนไลน์",
            "เสื้อผ้า", "แต่งกาย", "แต่งตัว", "แฟชั่น"
        ],
        "tags_th": [
            "ช้อปปิ้ง", "ซื้อของ", "นนทบุรี", "ร้านค้า", "ร้านค้าออนไลน์",
            "เสื้อผ้า", "แต่งกาย", "แต่งตัว", "แฟชั่น"
        ],
        "tags_en": [
            "ช้อปปิ้ง", "ซื้อของ", "นนทบุรี", "ร้านค้า", "ร้านค้าออนไลน์",
            "เสื้อผ้า", "แต่งกาย", "แต่งตัว", "แฟชั่น"
        ],
        "HIT_COUNT_YEAR":
            30,
        "HIT_COUNT_MONTH":
            1,
        "HIT_COUNT_WEEK":
            660,
        "privilege_list": ["ZkpXzzQL38lk"],
        "discover":
            "y",
        "merchant_type":
            None,
        "denormalize_privilege": [{
            "campaign_type": "privilege",
            "quota_over_existed": None,
        }],
    },
    'TEST_SHOP_4': {
        "id":
            "eZMjEE9yvMp6",
        "original_id":
            "5060950",
        "searchable":
            "Y",
        "title":
            "A AND B Coffee Milk (Chon Buri)",
        "title_th":
            "เอ แอนด์ บี คอฟฟี่ มิลล์ (จ.ชลบุรี)",
        "title_en":
            "A AND B Coffee Milk (Chon Buri)",
        "detail":
            None,
        "detail_th":
            None,
        "detail_en":
            None,
        "publish_date":
            "2018-06-12T07:18:23.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "status":
            "publish",
        "thumb_list": {
            "logo_s":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/8063112/web-80x60.jpg",
            "highlight":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/2756382/web-795x258-trueid.jpg"
        },
        "article_category":
            None,
        "article_category_en":
            None,
        "article_category_th":
            None,
        "content_type":
            "trueyoumerchant",
        "address":
            None,
        "address_th":
            None,
        "address_en":
            None,
        "location":
            None,
        "tags": [
            "กินข้าว",
            "ชลบุรี",
        ],
        "tags_en": [
            "กินข้าว", "ชลบุรี", "น้ำผลไม้", "ร้านอาหาร", "อาหารนานาชาติ",
            "เครื่องดื่ม"
        ],
        "tags_th": [
            "กินข้าว",
            "ชลบุรี",
        ],
        "HIT_COUNT_YEAR":
            40,
        "HIT_COUNT_MONTH":
            2,
        "HIT_COUNT_WEEK":
            670,
        "privilege_list": ["ZkpXzzQL38lk"],
        "discover":
            "y",
        "merchant_type":
            None,
        "denormalize_privilege": [{
            "campaign_type": "privilege",
            "quota_over_existed": None,
        }],
    },
    'TEST_SHOP_5': {
        "id":
            "JV8B11QRQnLA",
        "original_id":
            "5065276",
        "searchable":
            "Y",
        "title":
            "Bomb dining (Udomsuk)",
        "title_th":
            "บอม อิ่มอร่อย (สาขา 1 อุดมสุข)",
        "title_en":
            "Bomb dining (Udomsuk)",
        "address":
            None,
        "address_en":
            None,
        "address_th":
            None,
        "detail":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "detail_th":
            "&lt;p&gt;บรรยากาศร้านอบอุ่น อาหารเลิศรส กับ ทอดมันปลากราย จากลุ่มแม่น้ำโขง&lt;/p&gt;",
        "detail_en":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "publish_date":
            "2018-06-20T06:23:03.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "status":
            "publish",
        "thumb_list": {
            "logo_s":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/8063112/web-80x60.jpg",
            "highlight":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/2756382/web-795x258-trueid.jpg"
        },
        "content_type":
            "trueyoumerchant",
        "article_category":
            None,
        "article_category_en":
            None,
        "article_category_th":
            None,
        "location":
            None,
        "tags": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_en": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_th": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "HIT_COUNT_YEAR":
            60,
        "HIT_COUNT_MONTH":
            100,
        "HIT_COUNT_WEEK":
            20,
        "privilege_list": ["ZkpXzzQL38lk"],
        "discover":
            "y",
        "merchant_type":
            None,
        "denormalize_privilege": [{
            "campaign_type": "privilege",
            "quota_over_existed": None,
        }],
    },
    'TEST_SHOP_6': {
        "id":
            "JV8B11QRQnLA",
        "original_id":
            "5065276",
        "searchable":
            "Y",
        "title":
            "Bomb dining (Udomsuk)",
        "title_th":
            "บอม อิ่มอร่อย (สาขา 1 อุดมสุข)",
        "title_en":
            "Bomb dining (Udomsuk)",
        "address":
            None,
        "address_en":
            None,
        "address_th":
            None,
        "detail":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "detail_th":
            "&lt;p&gt;บรรยากาศร้านอบอุ่น อาหารเลิศรส กับ ทอดมันปลากราย จากลุ่มแม่น้ำโขง&lt;/p&gt;",
        "detail_en":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "publish_date":
            "2018-06-20T06:23:03.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "status":
            "publish",
        "thumb_list": {
            "logo_s":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/8063112/web-80x60.jpg",
            "highlight":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/2756382/web-795x258-trueid.jpg"
        },
        "content_type":
            "trueyoumerchant",
        "article_category":
            None,
        "article_category_en":
            None,
        "article_category_th":
            None,
        "location":
            None,
        "tags": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_en": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_th": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "HIT_COUNT_YEAR":
            60,
        "HIT_COUNT_MONTH":
            100,
        "HIT_COUNT_WEEK":
            20,
        "privilege_list": ["ZkpXzzQL38lk"],
        "discover":
            "y",
        "merchant_type":
            None,
        "denormalize_privilege": [{
            "campaign_type": "privilege",
            "quota_over_existed": None,
        }],
    },
    'TEST_SHOP_7': {
        "id":
            "JV8B11QRQnLA",
        "original_id":
            "5065276",
        "searchable":
            "Y",
        "title":
            "Bomb dining (Udomsuk)",
        "title_th":
            "บอม อิ่มอร่อย (สาขา 1 อุดมสุข)",
        "title_en":
            "Bomb dining (Udomsuk)",
        "address":
            None,
        "address_en":
            None,
        "address_th":
            None,
        "detail":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "detail_th":
            "&lt;p&gt;บรรยากาศร้านอบอุ่น อาหารเลิศรส กับ ทอดมันปลากราย จากลุ่มแม่น้ำโขง&lt;/p&gt;",
        "detail_en":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "publish_date":
            "2018-06-20T06:23:03.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "status":
            "publish",
        "thumb_list": {
            "logo_s":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/8063112/web-80x60.jpg",
            "highlight":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/2756382/web-795x258-trueid.jpg"
        },
        "content_type":
            "trueyoumerchant",
        "article_category":
            None,
        "article_category_en":
            None,
        "article_category_th":
            None,
        "location":
            None,
        "tags": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_en": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_th": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "HIT_COUNT_YEAR":
            60,
        "HIT_COUNT_MONTH":
            100,
        "HIT_COUNT_WEEK":
            20,
        "privilege_list": ["ZkpXzzQL38lk"],
        "discover":
            "y",
        "merchant_type":
            None,
        "denormalize_privilege": [{
            "campaign_type": "privilege",
            "quota_over_existed": None,
        }],
    },
    'TEST_SHOP_8': {
        "id":
            "JV8B11QRQnLA",
        "original_id":
            "5065276",
        "searchable":
            "Y",
        "title":
            "Bomb dining (Udomsuk)",
        "title_th":
            "บอม อิ่มอร่อย (สาขา 1 อุดมสุข)",
        "title_en":
            "Bomb dining (Udomsuk)",
        "address":
            None,
        "address_en":
            None,
        "address_th":
            None,
        "detail":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "detail_th":
            "&lt;p&gt;บรรยากาศร้านอบอุ่น อาหารเลิศรส กับ ทอดมันปลากราย จากลุ่มแม่น้ำโขง&lt;/p&gt;",
        "detail_en":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "publish_date":
            "2018-06-20T06:23:03.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "status":
            "publish",
        "thumb_list": {
            "logo_s":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/8063112/web-80x60.jpg",
            "highlight":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/2756382/web-795x258-trueid.jpg"
        },
        "content_type":
            "trueyoumerchant",
        "article_category":
            None,
        "article_category_en":
            None,
        "article_category_th":
            None,
        "location":
            None,
        "tags": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_en": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_th": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "HIT_COUNT_YEAR":
            60,
        "HIT_COUNT_MONTH":
            100,
        "HIT_COUNT_WEEK":
            20,
        "privilege_list": ["ZkpXzzQL38lk"],
        "discover":
            "y",
        "merchant_type":
            None,
        "denormalize_privilege": [{
            "campaign_type": "privilege",
            "quota_over_existed": None,
        }],
    },
    'TEST_SHOP_9': {
        "id":
            "JV8B11QRQnLA",
        "original_id":
            "5065276",
        "searchable":
            "Y",
        "title":
            "Bomb dining (Udomsuk)",
        "title_th":
            "บอม อิ่มอร่อย (สาขา 1 อุดมสุข)",
        "title_en":
            "Bomb dining (Udomsuk)",
        "address":
            None,
        "address_en":
            None,
        "address_th":
            None,
        "detail":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "detail_th":
            "&lt;p&gt;บรรยากาศร้านอบอุ่น อาหารเลิศรส กับ ทอดมันปลากราย จากลุ่มแม่น้ำโขง&lt;/p&gt;",
        "detail_en":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "publish_date":
            "2018-06-20T06:23:03.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "status":
            "publish",
        "thumb_list": {
            "logo_s":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/8063112/web-80x60.jpg",
            "highlight":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/2756382/web-795x258-trueid.jpg"
        },
        "content_type":
            "trueyoumerchant",
        "article_category":
            None,
        "article_category_en":
            None,
        "article_category_th":
            None,
        "location":
            None,
        "tags": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_en": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_th": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "HIT_COUNT_YEAR":
            60,
        "HIT_COUNT_MONTH":
            100,
        "HIT_COUNT_WEEK":
            20,
        "privilege_list": ["ZkpXzzQL38lk"],
        "discover":
            "y",
        "merchant_type":
            None,
        "denormalize_privilege": [{
            "campaign_type": "privilege",
            "quota_over_existed": None,
        }],
    },
    'TEST_SHOP_10': {
        "id":
            "JV8B11QRQnLA",
        "original_id":
            "5065276",
        "searchable":
            "Y",
        "title":
            "Bomb dining (Udomsuk)",
        "title_th":
            "บอม อิ่มอร่อย (สาขา 1 อุดมสุข)",
        "title_en":
            "Bomb dining (Udomsuk)",
        "address":
            None,
        "address_en":
            None,
        "address_th":
            None,
        "detail":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "detail_th":
            "&lt;p&gt;บรรยากาศร้านอบอุ่น อาหารเลิศรส กับ ทอดมันปลากราย จากลุ่มแม่น้ำโขง&lt;/p&gt;",
        "detail_en":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "publish_date":
            "2018-06-20T06:23:03.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "status":
            "publish",
        "thumb_list": {
            "logo_s":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/8063112/web-80x60.jpg",
            "highlight":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/2756382/web-795x258-trueid.jpg"
        },
        "content_type":
            "trueyoumerchant",
        "article_category":
            None,
        "article_category_en":
            None,
        "article_category_th":
            None,
        "location":
            None,
        "tags": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_en": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_th": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "HIT_COUNT_YEAR":
            60,
        "HIT_COUNT_MONTH":
            100,
        "HIT_COUNT_WEEK":
            20,
        "privilege_list": ["ZkpXzzQL38lk"],
        "discover":
            "y",
        "merchant_type":
            None,
        "denormalize_privilege": [{
            "campaign_type": "privilege",
            "quota_over_existed": None,
        }],
    },
    'TEST_SHOP_11': {
        "id":
            "JV8B11QRQnLA",
        "original_id":
            "5065276",
        "searchable":
            "Y",
        "title":
            "Bomb dining (Udomsuk)",
        "title_th":
            "บอม อิ่มอร่อย (สาขา 1 อุดมสุข)",
        "title_en":
            "Bomb dining (Udomsuk)",
        "address":
            None,
        "address_en":
            None,
        "address_th":
            None,
        "detail":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "detail_th":
            "&lt;p&gt;บรรยากาศร้านอบอุ่น อาหารเลิศรส กับ ทอดมันปลากราย จากลุ่มแม่น้ำโขง&lt;/p&gt;",
        "detail_en":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "publish_date":
            "2018-06-20T06:23:03.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "status":
            "publish",
        "thumb_list": {
            "logo_s":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/8063112/web-80x60.jpg",
            "highlight":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/2756382/web-795x258-trueid.jpg"
        },
        "content_type":
            "trueyoumerchant",
        "article_category":
            None,
        "article_category_en":
            None,
        "article_category_th":
            None,
        "location":
            None,
        "tags": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_en": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_th": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "HIT_COUNT_YEAR":
            60,
        "HIT_COUNT_MONTH":
            100,
        "HIT_COUNT_WEEK":
            20,
        "privilege_list": ["ZkpXzzQL38lk"],
        "discover":
            "y",
        "merchant_type":
            None,
        "denormalize_privilege": [{
            "campaign_type": "privilege",
            "quota_over_existed": None,
        }],
    },
    'not_searchable_TEST_SHOP': {
        "id":
            "not_searchable_TEST_SHOP",
        "original_id":
            "not_searchable_TEST_SHOP",
        "searchable":
            "N",
        "title":
            "not_searchable Bomb dining (Udomsuk)",
        "title_th":
            "not_searchable บอม อิ่มอร่อย (สาขา 1 อุดมสุข)",
        "title_en":
            "not_searchable Bomb dining (Udomsuk)",
        "address":
            None,
        "address_en":
            None,
        "address_th":
            None,
        "detail":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "detail_th":
            "&lt;p&gt;บรรยากาศร้านอบอุ่น อาหารเลิศรส กับ ทอดมันปลากราย จากลุ่มแม่น้ำโขง&lt;/p&gt;",
        "detail_en":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "publish_date":
            "2018-06-20T06:23:03.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "status":
            "publish",
        "thumb_list": {
            "logo_s":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/8063112/web-80x60.jpg",
            "highlight":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/2756382/web-795x258-trueid.jpg"
        },
        "content_type":
            "trueyoumerchant",
        "article_category":
            None,
        "article_category_en":
            None,
        "article_category_th":
            None,
        "location":
            None,
        "tags": ["xซื้อของ"],
        "tags_en": ["xซื้อของ"],
        "tags_th": ["xซื้อของ"],
        "HIT_COUNT_YEAR":
            59,
        "HIT_COUNT_MONTH":
            99,
        "HIT_COUNT_WEEK":
            19,
        "privilege_list": ["ZkpXzzQL38lk"],
        "discover":
            "y",
        "merchant_type":
            None,
        "denormalize_privilege": [{
            "campaign_type": "privilege",
            "quota_over_existed": None,
        }],
    },
    'not_published_TEST_SHOP': {
        "id":
            "not_published_TEST_SHOP",
        "original_id":
            "not_published_TEST_SHOP",
        "searchable":
            "Y",
        "title":
            "not_published Bomb dining (Udomsuk)",
        "title_th":
            "not_published บอม อิ่มอร่อย (สาขา 1 อุดมสุข)",
        "title_en":
            "not_published Bomb dining (Udomsuk)",
        "address":
            None,
        "address_en":
            None,
        "address_th":
            None,
        "detail":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "detail_th":
            "&lt;p&gt;บรรยากาศร้านอบอุ่น อาหารเลิศรส กับ ทอดมันปลากราย จากลุ่มแม่น้ำโขง&lt;/p&gt;",
        "detail_en":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "publish_date":
            "2025-12-07T01:46:00.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "status":
            "publish",    # status is published, but publish_date is not arrived yet
        "thumb_list": {
            "logo_s":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/8063112/web-80x60.jpg",
            "highlight":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/2756382/web-795x258-trueid.jpg"
        },
        "content_type":
            "trueyoumerchant",
        "article_category":
            None,
        "article_category_en":
            None,
        "article_category_th":
            None,
        "location":
            None,
        "tags": ["xซื้อของ"],
        "tags_en": ["xซื้อของ"],
        "tags_th": ["xซื้อของ"],
        "HIT_COUNT_YEAR":
            60,
        "HIT_COUNT_MONTH":
            100,
        "HIT_COUNT_WEEK":
            20,
        "privilege_list": ["ZkpXzzQL38lk"],
        "discover":
            "y",
        "merchant_type":
            None,
        "denormalize_privilege": [{
            "campaign_type": "privilege",
            "quota_over_existed": None,
        }],
    },
    'draft_TEST_SHOP': {
        "id":
            "draft_TEST_SHOP",
        "original_id":
            "draft_TEST_SHOP",
        "searchable":
            "Y",
        "title":
            "Bomb dining (Udomsuk)",
        "title_th":
            "บอม อิ่มอร่อย (สาขา 1 อุดมสุข)",
        "title_en":
            "Bomb dining (Udomsuk)",
        "address":
            None,
        "address_en":
            None,
        "address_th":
            None,
        "detail":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "detail_th":
            "&lt;p&gt;บรรยากาศร้านอบอุ่น อาหารเลิศรส กับ ทอดมันปลากราย จากลุ่มแม่น้ำโขง&lt;/p&gt;",
        "detail_en":
            "&lt;p&gt;The atmosphere is warm and delicious with fish cakes. The Mekong River&lt;/p&gt;",
        "publish_date":
            "2018-06-20T06:23:03.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "status":
            "draft",
        "thumb_list": {
            "logo_s":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/8063112/web-80x60.jpg",
            "highlight":
                "http://chrm-backoffice.trueyou.co.th/tmp-upload/2018/10/16/10/34/25/2756382/web-795x258-trueid.jpg"
        },
        "content_type":
            "trueyoumerchant",
        "article_category":
            None,
        "article_category_en":
            None,
        "article_category_th":
            None,
        "location":
            None,
        "tags": ["xซื้อของ"],
        "tags_en": ["xซื้อของ"],
        "tags_th": ["xซื้อของ"],
        "HIT_COUNT_YEAR":
            60,
        "HIT_COUNT_MONTH":
            100,
        "HIT_COUNT_WEEK":
            20,
        "privilege_list": ["ZkpXzzQL38lk"],
        "discover":
            "y",
        "merchant_type":
            None,
        "denormalize_privilege": [{
            "campaign_type": "privilege",
            "quota_over_existed": None,
        }],
    },
    'NO_HIGHLIGHT_TEST_SHOP': {
        "id":
            "NO_HIGHLIGHT_TEST_SHOP",
        "original_id":
            "NO_HIGHLIGHT_TEST_SHOP",
        "searchable":
            "Y",
        "title":
            "NO_HIGHLIGHT_TEST_SHOP",
        "title_th":
            "NO_HIGHLIGHT_TEST_SHOP",
        "title_en":
            "NO_HIGHLIGHT_TEST_SHOP",
        "address":
            None,
        "address_en":
            None,
        "address_th":
            None,
        "detail":
            "NO_HIGHLIGHT_TEST_SHOP",
        "detail_th":
            "NO_HIGHLIGHT_TEST_SHOP",
        "detail_en":
            "NO_HIGHLIGHT_TEST_SHOP",
        "publish_date":
            "2018-06-20T06:23:03.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "status":
            "publish",
        "content_type":
            "trueyoumerchant",
        "article_category":
            None,
        "article_category_en":
            None,
        "article_category_th":
            None,
        "location":
            None,
        "thumb_list": {},
        "tags": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_en": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_th": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "HIT_COUNT_YEAR":
            1,
        "HIT_COUNT_MONTH":
            20,
        "HIT_COUNT_WEEK":
            3,
        "privilege_list": ["ZkpXzzQL38lk"],
        "discover":
            "y",
        "merchant_type":
            None,
        "denormalize_privilege": [{
            "campaign_type": "privilege",
            "quota_over_existed": None,
        }],
    },
    'NO_THUMBLIST_TEST_SHOP': {
        "id":
            "NO_THUMBLIST_TEST_SHOP",
        "original_id":
            "NO_THUMBLIST_TEST_SHOP",
        "searchable":
            "Y",
        "title":
            "NO_THUMBLIST_TEST_SHOP",
        "title_th":
            "NO_THUMBLIST_TEST_SHOP",
        "title_en":
            "NO_THUMBLIST_TEST_SHOP",
        "address":
            None,
        "address_en":
            None,
        "address_th":
            None,
        "detail":
            "NO_THUMBLIST_TEST_SHOP",
        "detail_th":
            "NO_THUMBLIST_TEST_SHOP",
        "detail_en":
            "NO_THUMBLIST_TEST_SHOP",
        "publish_date":
            "2018-06-20T06:23:03.000Z",
        "expire_date":
            "2030-01-01T16:59:59.000Z",
        "status":
            "publish",
        "content_type":
            "trueyoumerchant",
        "article_category":
            None,
        "article_category_en":
            None,
        "article_category_th":
            None,
        "location":
            None,
        "tags": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_en": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_th": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "HIT_COUNT_YEAR":
            6,
        "HIT_COUNT_MONTH":
            10,
        "HIT_COUNT_WEEK":
            2,
        "privilege_list": ["ZkpXzzQL38lk"],
        "discover":
            "y",
        "merchant_type":
            None,
        "denormalize_privilege": [{
            "campaign_type": "privilege",
            "quota_over_existed": 0,
        }],
    },
    'EXPIRED_TEST_SHOP': {
        "id":
            "EXPIRED_TEST_SHOP",
        "original_id":
            "EXPIRED_TEST_SHOP",
        "searchable":
            "Y",
        "title":
            "EXPIRED_TEST_SHOP",
        "title_th":
            "EXPIRED_TEST_SHOP",
        "title_en":
            "EXPIRED_TEST_SHOP",
        "address":
            None,
        "address_en":
            None,
        "address_th":
            None,
        "detail":
            "EXPIRED_TEST_SHOP",
        "detail_th":
            "EXPIRED_TEST_SHOP",
        "detail_en":
            "EXPIRED_TEST_SHOP",
        "publish_date":
            "2018-06-20T06:23:03.000Z",
        "expire_date":
            "2000-01-01T16:59:59.000Z",
        "status":
            "publish",
        "content_type":
            "trueyoumerchant",
        "article_category":
            None,
        "article_category_en":
            None,
        "article_category_th":
            None,
        "location":
            None,
        "tags": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_en": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "tags_th": [
            "xซื้อของ", "xตลาดนัด", "xร้านค้า", "xสินค้าไอที", "xสุโขทัย",
            "xแก็ดเจ็ต", "ช้อปปิ้ง"
        ],
        "HIT_COUNT_YEAR":
            6,
        "HIT_COUNT_MONTH":
            10,
        "HIT_COUNT_WEEK":
            2,
        "privilege_list": ["ZkpXzzQL38lk"],
        "discover":
            "y",
        "merchant_type":
            None,
        "denormalize_privilege": [{
            "campaign_type": "privilege",
            "quota_over_existed": 1,
        }],
    },
}

branches = {
    "TEST_BRANCH_1_1": {
        "lang": "th",
        "lang_en": "en",
        "id": "Y1KjqqVD8eMd",
        "id_en": "Y1KjqqVD8eMd",
        "original_id": "3111989",
        "original_id_en": "3111989",
        "content_type": "trueyoubranch",
        "content_type_en": "trueyoubranch",
        "title": "Dusit Hotels &amp; Resort",
        "title_en": "Dusit Hotels &amp; Resort",
        "thumb": None,
        "thumb_en": None,
        "detail": None,
        "detail_en": None,
        "article_category": None,
        "article_category_en": None,
        "tags": None,
        "tags_en": None,
        "status": "publish",
        "status_en": "publish",
        "publish_date": "2013-10-03T07:29:17.000Z",
        "publish_date_en": "2013-10-03T07:29:17.000Z",
        "create_date": "2013-10-03T07:29:17.000Z",
        "create_date_en": "2013-10-03T07:29:17.000Z",
        "update_date": "2018-06-15T09:55:45.000Z",
        "update_date_en": "2018-06-15T09:55:45.000Z",
        "create_by": None,
        "create_by_en": None,
        "create_by_ssoid": None,
        "create_by_ssoid_en": None,
        "update_by": None,
        "update_by_en": None,
        "update_by_ssoid": None,
        "update_by_ssoid_en": None,
        "searchable": "Y",
        "searchable_en": "Y",
        "master_id": "y4YQgglRpkB4",
        "master_id_en": "y4YQgglRpkB4",
        "location": None,
        "location_en": None,
        "zoom_level": None,
        "zoom_level_en": None,
        "privilege_list": ["85yb7xOamvV1"],
        "privilege_list_en": ["85yb7xOamvV1"],
        "@timestamp": "2018-06-21T16:13:26.749+07:00",
        "lang_th": "th",
        "master_id_th": "y4YQgglRpkB4",
        "content_type_th": "trueyoubranch",
        "zoom_level_th": None,
        "detail_th": None,
        "article_category_th": None,
        "searchable_th": "Y",
        "update_by_ssoid_th": None,
        "publish_date_th": "2013-10-03T07:29:17.000Z",
        "create_by_th": None,
        "update_by_th": None,
        "location_th": None,
        "thumb_th": None,
        "original_id_th": "3006806",
        "create_date_th": "2013-10-03T07:29:17.000Z",
        "tags_th": None,
        "id_th": "Y1KjqqVD8eMd",
        "title_th": "Dusit Hotels &amp; Resort",
        "update_date_th": "2018-06-15T09:55:45.000Z",
        "create_by_ssoid_th": None,
        "status_th": "publish",
        "privilege_list_th": ["85yb7xOamvV1"],
    }
}

branches_geo_location = {
    'hits': {
        'hits': [{
            '_source': {
                'id':
                    'NMXpO1JNZ3JM',
                'master_id':
                    'y4YQgglRpkB4',
                'location':
                    '13.6925779,100.750777',
                'privilege_list': [
                    'DAKPYYJYwQ3O',
                    'VZeqLLDkwrVZ',
                    'GZoVaaADQ8W9',
                ]
            }
        }, {
            '_source': {
                'id':
                    'nNe3DWpDl8lQ',
                'master_id':
                    '7wzZ0e0KjQ0',
                'location':
                    '13.6925779,100.750777',
                'privilege_list': [
                    'Gv5ozzxW18wO', '8Dzg44KGN5jO', 'VZeqLLDkwrVZ',
                    'GZoVaaADQ8W9', 'aYOg11qPnl2Y', 'DAKPYYJYwQ3O'
                ]
            }
        }, {
            '_source': {
                'id':
                    'nNe3DWpDl2T',
                'master_id':
                    '7wzZ0e0KjQ0',
                'location':
                    '13.6925779,100.750777',
                'privilege_list': [
                    'Gv5ozzxW18wO', '8Dzg44KGN5jO', 'DAKPYYJYwQ3O'
                ]
            }
        }]
    }
}

es_merchant = {
    'hits': {
        'hits': [{
            '_source': {
                'id':
                    'y4YQgglRpkB4',
                'privilege_list': [
                    'DAKPYYJYwQ3O',
                    'VZeqLLDkwrVZ',
                    'GZoVaaADQ8W9',
                ]
            }
        }, {
            '_source': {
                'id':
                    '7wzZ0e0KjQ0',
                'privilege_list': [
                    'Gv5ozzxW18wO', '8Dzg44KGN5jO', 'VZeqLLDkwrVZ',
                    'GZoVaaADQ8W9', 'aYOg11qPnl2Y', 'DAKPYYJYwQ3O'
                ]
            }
        }]
    }
}
###################################
