#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 16 22:38:05 2017

@author: Champ
"""
import unittest as ut
from unittest.mock import call, patch, MagicMock
import rcommsutils.cassandra.cassandra_utils as csh
import rcommsutils.cassandra.settings as stc


class CassandraUtilsTest(ut.TestCase):

    TEST_MODEL_NAME = "test_recommender"
    TEST_TABLE = "test"    # without the "_x" suffix where x is the running number
    TEST_DEPLOYED_VERSION = 4

    def test_get_host_details_env_vars(self):
        hosts, port, user, password = csh._get_host_details(None)
        self.assertEqual(hosts, stc.RCOM_CASSANDRA_IP)
        self.assertEqual(port, stc.RCOM_CASSANDRA_PORT)
        self.assertEqual(user, stc.RCOM_CASSANDRA_USER)
        self.assertEqual(password, stc.RCOM_CASSANDRA_PASSWORD)

    def test_get_host_details(self):
        host_details = csh.HostDetails("127.0.0.1,127.0.0.2", 123, "user",
                                       "password")
        hosts, port, user, password = csh._get_host_details(host_details)
        self.assertEqual(hosts, ["127.0.0.1", "127.0.0.2"])
        self.assertEqual(port, 123)
        self.assertEqual(user, "user")
        self.assertEqual(password, "password")

    @patch("rcommsutils.cassandra.cassandra_utils.SimpleStatement")
    @patch("rcommsutils.cassandra.cassandra_utils.get_table_update_info")
    def test_Statement(self, mock_get_table, mock_stmt):
        session = MagicMock()
        mock_get_table.return_value.deployed_version = self.TEST_DEPLOYED_VERSION
        mock_get_table.return_value.keyspace_name = self.TEST_MODEL_NAME
        stmt = csh.Statement(self.TEST_MODEL_NAME,
                             "SELECT * FROM " + csh.TABLE_PLACEHOLDER,
                             self.TEST_TABLE, session)
        stmt.execute()
        mock_get_table.assert_called_once_with(self.TEST_MODEL_NAME)
        mock_stmt.assert_called_once_with(
            "SELECT * FROM test_recommender.test_4")
        session.execute.assert_called_once_with(mock_stmt(), ())

    @patch("rcommsutils.cassandra.cassandra_utils.SimpleStatement")
    @patch("rcommsutils.cassandra.cassandra_utils.get_table_update_info")
    def test_Statement_with_params(self, mock_get_table, mock_stmt):
        session = MagicMock()
        cql = "SELECT user, role, age FROM " + csh.TABLE_PLACEHOLDER + " WHERE user = %s"
        mock_get_table.return_value.deployed_version = self.TEST_DEPLOYED_VERSION
        mock_get_table.return_value.keyspace_name = self.TEST_MODEL_NAME
        stmt = csh.Statement(self.TEST_MODEL_NAME, cql, self.TEST_TABLE,
                             session)
        stmt.execute(params=('Murph',))
        mock_get_table.assert_called_once_with(self.TEST_MODEL_NAME)
        mock_stmt.assert_called_once_with(
            "SELECT user, role, age FROM test_recommender.test_4 WHERE user = %s"
        )
        session.execute.assert_called_once_with(mock_stmt(), ('Murph',))

    @patch("rcommsutils.cassandra.cassandra_utils.SimpleStatement")
    @patch("rcommsutils.cassandra.cassandra_utils.get_table_update_info")
    def test_PreparedStatement(self, mock_get_table, mock_stmt):
        session = MagicMock()
        mock_get_table.return_value.deployed_version = self.TEST_DEPLOYED_VERSION
        mock_get_table.return_value.keyspace_name = self.TEST_MODEL_NAME

        pstmt = csh.PreparedStatement(self.TEST_MODEL_NAME,
                                      "SELECT * FROM " + csh.TABLE_PLACEHOLDER,
                                      self.TEST_TABLE, session)
        pstmt.execute()
        exp_query = "SELECT * FROM test_recommender.test_4"
        mock_stmt.assert_called_once_with(exp_query)
        session.prepare.assert_called_once_with(exp_query)
        session.execute.assert_called_once_with(session.prepare(), ())

    @patch("rcommsutils.cassandra.cassandra_utils.SimpleStatement")
    @patch("rcommsutils.cassandra.cassandra_utils.get_table_update_info")
    def test_PreparedStatement_with_params(self, mock_get_table, mock_stmt):
        session = MagicMock()
        cql = "SELECT user, role, age FROM " + csh.TABLE_PLACEHOLDER + " WHERE user = ?"
        mock_get_table.return_value.deployed_version = self.TEST_DEPLOYED_VERSION
        mock_get_table.return_value.keyspace_name = self.TEST_MODEL_NAME
        pstmt = csh.PreparedStatement(self.TEST_MODEL_NAME, cql,
                                      self.TEST_TABLE, session)
        pstmt.execute(params=('Murph',))
        exp_query = "SELECT user, role, age FROM test_recommender.test_4 WHERE user = ?"
        mock_stmt.assert_called_once_with(exp_query)
        session.prepare.assert_called_once_with(exp_query)
        session.execute.assert_called_once_with(session.prepare(), ('Murph',))

    @patch("rcommsutils.cassandra.cassandra_utils._get_helper_session")
    def test_get_table_update_info(self, mock_helper):
        host_details = MagicMock()
        csh.get_table_update_info(self.TEST_MODEL_NAME, host_details)
        exp_query = "SELECT keyspace_name, deployed_version, latest_version \
             FROM %s.%s WHERE model_code_name = '%s'" \
             % (stc.RCOM_CASSANDRA_COMMON_KEYSPACE, stc.RCOM_CASSANDRA_MODEL_UPDATE_TABLE, self.TEST_MODEL_NAME)
        mock_helper.assert_called_once_with(host_details)
        mock_helper.return_value.execute.assert_called_once_with(exp_query)

    @patch("rcommsutils.cassandra.cassandra_utils._execute_concurrent")
    def test_execute_concurrent(self, mock_con):
        session = MagicMock()
        pstmt = session.prepare("SELECT * FROM %s.%s" %
                                (stc.RCOM_CASSANDRA_TEST_KEYSPACE,
                                 stc.RCOM_CASSANDRA_TEST_MODEL_UPDATE_TABLE))
        statements_and_params = []
        statements_and_params.append((pstmt, []))
        statements_and_params.append((pstmt, []))
        statements_and_params.append((pstmt, []))
        csh.execute_concurrent(statements_and_params, session=session)
        mock_con.assert_called_once_with(session,
                                         statements_and_params,
                                         concurrency=3,
                                         raise_on_first_error=False,
                                         results_generator=False)

    @patch("rcommsutils.cassandra.cassandra_utils._execute_concurrent")
    def test_execute_concurrent_with_params(self, mock_con):
        session = MagicMock()
        pstmt = session.prepare(
            "SELECT * FROM %s.%s WHERE model_code_name = ?" %
            (stc.RCOM_CASSANDRA_TEST_KEYSPACE,
             stc.RCOM_CASSANDRA_TEST_MODEL_UPDATE_TABLE))
        statements_and_params = []
        statements_and_params.append((pstmt, (self.TEST_MODEL_NAME,)))
        statements_and_params.append((pstmt, (self.TEST_MODEL_NAME,)))
        csh.execute_concurrent(statements_and_params, session=session)
        mock_con.assert_called_once_with(session,
                                         statements_and_params,
                                         concurrency=2,
                                         raise_on_first_error=False,
                                         results_generator=False)

    @patch("rcommsutils.cassandra.cassandra_utils._get_helper_session")
    def test_get_connection_status_when_connected(self, mock_helper):
        host_details = MagicMock()
        csh.get_connection_status(host_details)
        mock_helper.assert_called_once_with(host_details)
        mock_helper.return_value.get_pool_state.assert_called_once_with()


if __name__ == '__main__':
    ut.main()
