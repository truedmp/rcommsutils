#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import datetime
import orjson as json
import unittest as ut
from unittest.mock import call, patch, MagicMock

import rcommsutils.cms.settings as ste
from rcommsutils import tv_utils
from rcommsutils.cms.tv_utils import Fields, PopularityPeriod, _get_return_fields, _API_COMMON_FIELDS, \
    _build_result_set, _to_timestamp, _get_subscription_tiers_condition, \
    _get_milliseconds_from_now, _build_fields_result


class MockResponse:

    def __init__(self, data, exception=None):
        self.data = data
        self.exception = exception

    def raise_for_status(self):
        if self.exception:
            raise self.exception

    def json(self):
        return json.loads(self.data)


class TVUtilsTest(ut.TestCase):

    maxDiff = None

    def test_get_return_fields(self):
        expected = _API_COMMON_FIELDS
        actual = _get_return_fields(fields=None)
        self.assertEqual(expected, actual)

    def test_build_result_set(self):
        stub_item_source = {
            Fields.ID: "item_id",
            Fields.RELEASE_YEAR: "item_release_year",
            Fields.THUMBLIST: "item_thumblist",
            Fields.THUMBNAIL: "item_thumbnail",
            Fields.TVOD_FLAG: "item_tvod_flag",
            Fields.SUBSCRIPTION_TIERS: "item_subscription_year",
            Fields.EP_ITEMS: "item_ep_items",
            Fields.MOVIE_TYPE: "item_movie_type",
            Fields.OTHER_TYPE: "item_other_type",
            Fields.CONTENT_RIGHTS: "item_content_rights",
            Fields.RATE: "item_rate",
            Fields.AUDIO: "item_audio",
            Fields.SUBTITLE: "item_subtitle",
            Fields.DURATION: "item_duration",
            Fields.DRM: "item_drm",
            Fields.DISPLAY_QUALITIES: "item_display_qualities",
            Fields.HIT_COUNT_WEEK: "item_hit_count_week",
            Fields.HIT_COUNT_MONTH: "item_hit_count_month",
            Fields.HIT_COUNT_YEAR: "item_hit_count_year",
            Fields.COUNT_LIKES: "item_count_likes",
            Fields.COUNT_VIEWS: "item_count_views",
            Fields.TITLE: "item_title",
            Fields.ACTORS: "item_actors",
            Fields.DETAIL: "item_detail",
            Fields.CATEGORY: "item_category",
            Fields.GENRES: "item_genres",
            Fields.SYNOPSIS: "item_synopsis",
            Fields.CONTENT_TYPE: "content_type",
            Fields.PARTNER_RELATED: "partner_related",
            Fields.EXCLUSIVE_BADGE_TYPE: "exclusive_badge_type",
            Fields.EXCLUSIVE_BADGE_START: "exclusive_badge_start",
            Fields.EXCLUSIVE_BADGE_END: "exclusive_badge_end",
            Fields.EXPIRY_DATE: "expire_date",
        }
        items = [{"_source": stub_item_source}]
        expected = [{
            ste.RCOM_API_GLOBAL_ITEM_ID: "item_id",
            ste.RCOM_API_RELEASE_YEAR: "item_release_year",
            ste.RCOM_API_TTV_THUMBNAIL: "item_thumbnail",
            ste.RCOM_API_TTV_THUMBLIST: "item_thumblist",
            ste.RCOM_API_TTV_TVOD_FLAG: "item_tvod_flag",
            ste.RCOM_API_TTV_SUBSCRIPTION_TIERS: "item_subscription_year",
            ste.RCOM_API_TTV_EP_ITEMS: "item_ep_items",
            ste.RCOM_API_TTV_MOVIE_TYPE: "item_movie_type",
            ste.RCOM_API_TTV_RATE: "item_rate",
            ste.RCOM_API_TTV_AUDIO: "item_audio",
            ste.RCOM_API_TTV_SUBTITLE: "item_subtitle",
            ste.RCOM_API_TTV_DURATION: "item_duration",
            ste.RCOM_API_TTV_DRM: "item_drm",
            ste.RCOM_API_TTV_DISPLAY_QUALITIES: "item_display_qualities",
            ste.RCOM_API_TTV_TRAILER: None,
            ste.RCOM_API_TTV_CONTENT_RIGHTS: 'item_content_rights',
            ste.RCOM_API_TTV_OTHER_TYPE: 'item_other_type',
            ste.RCOM_API_NEWEPI_BADGE_START: None,
            ste.RCOM_API_NEWEPI_BADGE_END: None,
            ste.RCOM_API_NEWEPI_BADGE_TYPE: None,
            ste.RCOM_API_HIT_COUNT_WEEK: "item_hit_count_week",
            ste.RCOM_API_HIT_COUNT_MONTH: "item_hit_count_month",
            ste.RCOM_API_HIT_COUNT_YEAR: "item_hit_count_year",
            ste.RCOM_API_TTV_COUNT_LIKES: "item_count_likes",
            ste.RCOM_API_TTV_COUNT_VIEWS: "item_count_views",
            ste.RCOM_API_NAME: "item_title",
            ste.RCOM_API_ACTORS: "item_actors",
            ste.RCOM_API_DESC: "item_detail",
            ste.RCOM_API_ARTICLE_CATEGORY: "item_category",
            ste.RCOM_API_GENRES: "item_genres",
            ste.RCOM_API_SUB_GENRES: None,
            ste.RCOM_API_TTV_SYNOPSIS: "item_synopsis",
            ste.RCOM_API_TTV_PACKAGE_ALACARTE: None,
            ste.RCOM_API_TTV_CONTENT_TYPE: "content_type",
            ste.RCOM_API_TTV_PARTNER_RELATED: "partner_related",
            ste.RCOM_API_TTV_EXCLUSIVE_BADGE_TYPE: "exclusive_badge_type",
            ste.RCOM_API_TTV_EXCLUSIVE_BADGE_START: "exclusive_badge_start",
            ste.RCOM_API_TTV_EXCLUSIVE_BADGE_END: "exclusive_badge_end",
            ste.RCOM_API_EXPIRE_DATE: "expire_date",
        }]
        actual = _build_result_set(items)
        self.assertListEqual(expected, actual)

    def test_build_fields_result_return_every_field_in_source(self):
        mock_data = [{'_source': {'id': 'A'}},
                     {'_source': {'id': 'B'}}]
        expect = [{'id': 'A'},
                  {'id': 'B'}]
        actual = _build_fields_result(mock_data)
        self.assertEqual(expect, actual)

    def test_to_timestamp(self):
        expected = datetime.datetime(2019, 7, 1, 00, 00, 00, 000)
        expected = int(expected.timestamp())
        expected = str(expected)

        actual = _to_timestamp("2019-07-01T00:00:00.000Z")
        self.assertEqual(expected, actual)

    def test_get_items_from_elasticsearch_dsl_with_must_not_conditions(self):
        es_session = MagicMock()
        ids = ["sample_id"]
        expected_must_not_conditions = [
            {'match_phrase': {'title': 'empty title'}},
            {'match': {'is_trailer': 'yes'}},
            {'match': {'is_promo': 'yes'}},
            {'terms': {'content_provider': ['true_vision']}}
        ]

        tv_utils._get_items_from_elasticsearch(es_session, ids=ids)
        _, kwargs = es_session.search.call_args
        actual = kwargs['body']['query']['bool']['filter']['bool']['must_not']
        self.assertEqual(actual, expected_must_not_conditions)

    def test_build_result_set_empty_input(self):
        expected = []
        movies = []
        actual = tv_utils._build_result_set(movies)
        self.assertEqual(expected, actual)

    def test_get_items_from_elasticsearch_movies_no_criteria_specified_expect_exception_thrown(
            self):
        session = MagicMock()
        with self.assertRaisesRegex(ValueError, "'ids' must be specified"):
            tv_utils._get_items_from_elasticsearch(session)

    @patch("rcommsutils.cms.tv_utils._get_return_fields")
    def test_get_random_items_from_elasticsearch_with_custom_filter(
            self, mock_return_fields):
        session = MagicMock()
        seed = 322421
        limit = 110
        expected_dsl = {
            'query': {
                'bool': {
                    "must": {
                        "function_score": {
                            "functions": [{"random_score": {"seed": seed, "field": "_seq_no"}}]
                        }
                    },
                    'filter': {
                        'bool': {
                            'must': [
                                {'range': {'publish_date': {'lte': 'now/h'}}},
                                {'match': {'status': 'publish'}},
                                {'match': {'searchable': 'Y'}},
                                {"term" : {'lang': 'en'}},
                                {'match': {'tvod_flag': "Y"}},
                                {'terms': {'content_rights.keyword': ['svod', 'tvod']}},
                                {'terms': {'rate.keyword': ["G", "PG-13"]}},
                                {'terms': {'subscription_tiers.keyword': ["premium", "basic"]}},
                                {'terms': {'genres.keyword': ['genres']}},
                                {'terms': {'article_category.keyword': ["article_categories"]}},
                                {'terms': {'package_alacarte_list.keyword': ["MOVIETVOD149_BBB_OC_30D_ALA",
                                                                             "MOVIETVOD119_BBB_OC_30D_ALA"]}}
                            ],
                            'must_not': [
                                {'match_phrase': {'title': 'empty title'}},
                                {'match': {'is_trailer': 'yes'}}, {'match': {'is_promo': 'yes'}},
                                {'terms': {'content_provider': ['true_vision']}}
                            ],
                            "should": [
                                {"range": {"expire_date": {"gt": "now/h"}}},
                                {"bool": {"must_not": [{"exists": {"field": "expire_date"}}]}},
                                {"bool": {"must": {"term": {"movie_type": "movie"}}}},
                                {"bool": {
                                    "must": [
                                        {"term": {"movie_type": "series"}},
                                        {"term": {"is_vod_layer": "Y"}}
                                    ]
                                }
                            }],
                            "minimum_should_match": 2
                        }
                    }
                }
            }
        }
        tv_utils._get_random_items_from_elasticsearch(session,
                                                      seed=seed,
                                                      size=limit,
                                                      type=None,
                                                      genres=['genres'],
                                                      article_categories=['article_categories'],
                                                      language="EN",
                                                      tvod_flag='Y',
                                                      content_rights=['svod', 'tvod'],
                                                      subscription_tiers=["premium", "basic"],
                                                      rates=["G", "PG-13"],
                                                      package_alacarte_list=["MOVIETVOD149_BBB_OC_30D_ALA",
                                                                             "MOVIETVOD119_BBB_OC_30D_ALA"],
                                                      timeout="3s",
                                                      fields=None,
                                                      is_vod_layer='Y')

        session.search.assert_called_once_with(
            _source=mock_return_fields.return_value,
            body=expected_dsl,
            index=f'{ste.RCOM_ES_TRUETV_INDEX}',
            size=limit,
            timeout="3s")

    def test_get_items_from_elasticsearch_movies(self):
        session = MagicMock()
        expected_dsl = {
            'query': {
                'bool': {
                    'filter': {
                        'bool': {
                            'must': [
                                {'range': {'publish_date': {'lte': 'now/h'}}},
                                {'match': {'status': 'publish'}},
                                {'match': {'searchable': 'Y'}},
                                {"term" : {'lang': 'en'}},
                                {'terms': {'_id': ["en-id_1", "en-id_2"]}},
                                {'match': {'tvod_flag': "Y"}},
                                {'terms': {'content_rights.keyword': ['svod', 'tvod']}},
                                {'terms': {'rate.keyword': ["G", "PG-13"]}},
                                {'terms': {'subscription_tiers.keyword': ["premium", "basic"]}},
                                {'terms': {'genres.keyword': ['action', 'fantasy']}},
                                {'terms': {'article_category.keyword': ["action", "family"]}},
                                {'terms': {'package_alacarte_list.keyword': ["MOVIETVOD149_BBB_OC_30D_ALA",
                                                                             "MOVIETVOD119_BBB_OC_30D_ALA"]}}                            ],
                            'must_not': [
                                {'match_phrase': {'title': 'empty title'}},
                                {'match': {'is_trailer': 'yes'}},
                                {'match': {'is_promo': 'yes'}},
                                {'terms': {'content_provider': ['true_vision']}}
                            ],
                            "should": [
                                {"range": {"expire_date": {"gt": "now/h"}}},
                                {"bool": {"must_not": [{"exists": {"field": "expire_date"}}]}},
                                {"bool": {"must": {"term": {"movie_type": "movie"}}}},
                                {"bool": {
                                    "must": {"term": {"movie_type": "series"}},
                                    "must_not": {"term": {"is_vod_layer": "Y"}}
                                }
                            }],
                            "minimum_should_match": 2
                        }
                    }
                }
            }
        }

        tv_utils._get_items_from_elasticsearch(session,
                                               ids=["id_1", "id_2"],
                                               genres=["action", "fantasy"],
                                               tvod_flag="Y",
                                               content_rights=["svod", "tvod"],
                                               subscription_tiers=["premium", "basic"],
                                               article_categories=["action", "family"],
                                               rates=["G", "PG-13"],
                                               package_alacarte_list=[
                                                   "MOVIETVOD149_BBB_OC_30D_ALA",
                                                   "MOVIETVOD119_BBB_OC_30D_ALA"],
                                               fields=["id", "title", "genres"])
        session.search.assert_called_once_with(
            _source=["id", "title", "genres"],
            body=expected_dsl,
            index=f'{ste.RCOM_ES_TRUETV_INDEX}',
            size=2,
            timeout="10s")

    @patch("rcommsutils.cms.tv_utils._get_return_fields")
    def test_get_items_from_elasticsearch_movies_with_is_vod_layer(
            self, mock_return_fields):
        session = MagicMock()
        expected_dsl = {
            'query': {
                'bool': {
                    'filter': {
                        'bool': {
                            'must': [
                                {'range': {'publish_date': {'lte': 'now/h'}}},
                                {'match': {'status': 'publish'}},
                                {'match': {'searchable': 'Y'}},
                                {"term" : {'lang': 'en'}},
                                {"terms": {'_id': ['en-id_1']}}
                            ],
                            "must_not": [
                                {'match_phrase': {'title': 'empty title'}},
                                {'match': {'is_trailer': 'yes'}},
                                {'match': {'is_promo': 'yes'}},
                                {'terms': {'content_provider': ['true_vision']}}
                            ],
                            "should": [
                                {"range": {"expire_date": {"gt": "now/h"}}},
                                {"bool": {"must_not": [{"exists": {"field": "expire_date"}}]}},
                                {"bool": {"must": {"term": {"movie_type": "movie"}}}},
                                {"bool": {
                                    "must": [
                                        {"term": {"movie_type": "series"}},
                                        {"term": {"is_vod_layer": "Y"}}
                                    ]
                                }
                            }],
                            "minimum_should_match": 2
                        }
                    }
                }
            }
        }
        ids = ["id_1"]
        tv_utils._get_items_from_elasticsearch(session,
                                               ids=ids,
                                               is_vod_layer='Y')
        session.search.assert_called_once_with(
            _source=mock_return_fields.return_value,
            body=expected_dsl,
            index=f'{ste.RCOM_ES_TRUETV_INDEX}',
            size=1,
            timeout="10s")
        
    @patch("rcommsutils.cms.tv_utils._get_return_fields")
    def test_get_items_from_elasticsearch_movies_with_must_nots(
            self, mock_return_fields):
        session = MagicMock()
        expected_dsl = {
            'query': {
                'bool': {
                    'filter': {
                        'bool': {
                            'must': [
                                {'range': {'publish_date': {'lte': 'now/h'}}},
                                {'match': {'status': 'publish'}},
                                {'match': {'searchable': 'Y'}},
                                {"term" : {'lang': 'en'}},
                                {"terms": {'_id': ['en-id_1']}}
                            ],
                            "must_not": [
                                {'match_phrase': {'title': 'empty title'}},
                                {'match': {'is_trailer': 'yes'}},
                                {'match': {'is_promo': 'yes'}},
                                {'terms': {'content_provider': ['true_vision']}},
                                {'match': {'studio': 'YG Plus'}}
                            ],
                            "should": [
                                {"range": {"expire_date": {"gt": "now/h"}}},
                                {"bool": {"must_not": [{"exists": {"field": "expire_date"}}]}},
                                {"bool": {"must": {"term": {"movie_type": "movie"}}}},
                                {"bool": {
                                    "must": [
                                        {"term": {"movie_type": "series"}},
                                        {"term": {"is_vod_layer": "Y"}}
                                    ]
                                }
                            }],
                            "minimum_should_match": 2
                        }
                    }
                }
            }
        }
        ids = ["id_1"]
        tv_utils._get_items_from_elasticsearch(session,
                                               ids=ids,
                                               is_vod_layer='Y',
                                               must_nots=[{'match': {'studio': 'YG Plus'}}])
        session.search.assert_called_once_with(
            _source=mock_return_fields.return_value,
            body=expected_dsl,
            index=f'{ste.RCOM_ES_TRUETV_INDEX}',
            size=1,
            timeout="10s")

    @patch("rcommsutils.cms.tv_utils._sort_es_doc")
    def test_get_latest_items(self, mock_sort_es_doc):
        session = MagicMock()
        tv_utils.get_latest_items(session,
                                  type='series',
                                  size=2,
                                  language="TH",
                                  tvod_flag="N",
                                  rates=["PG-13"],
                                  subscription_tiers=["premium", "basic"],
                                  package_alacarte_list=["MOVIETVOD119_BBB_OC_30D_ALA", "MOVIETVOD149_BBB_OC_30D_ALA"],
                                  filter_ids=["15qaWX93Qrd5", "Z6exd4rmnV8y"],
                                  genres=["animation", "fantasy"],
                                  fields=['abc'],
                                  content_rights=["right"],
                                  is_vod_layer="Y",
                                  other_types='movie')
        mock_sort_es_doc.assert_called_once_with(session=session,
                                                 type="series",
                                                 article_categories=[],
                                                 size=2,
                                                 language="TH",
                                                 genres=["animation", "fantasy"],
                                                 sort_field=Fields.PUBLISH_DATE,
                                                 sort_by="desc",
                                                 tvod_flag="N",
                                                 subscription_tiers=["premium", "basic"],
                                                 rates=["PG-13"],
                                                 package_alacarte_list=["MOVIETVOD119_BBB_OC_30D_ALA", "MOVIETVOD149_BBB_OC_30D_ALA"],
                                                 filter_ids=["15qaWX93Qrd5", "Z6exd4rmnV8y"],
                                                 fields=['abc'],
                                                 is_vod_layer="Y",
                                                 content_rights=["right"],
                                                 other_types='movie',
                                                 must_nots=[])

    def test_get_latest_items_invalid_type(self):
        session = MagicMock()
        with self.assertRaisesRegex(
                ValueError, "Valid types are 'movie', 'series', or 'None'."):
            tv_utils.get_latest_items(session, type='xxx')

    @patch("rcommsutils.cms.tv_utils._sort_es_doc")
    def test_get_most_popular_items(self, mock_sort_es_doc):
        session = MagicMock()
        tv_utils.get_most_popular_items(session,
                                        type='series',
                                        size=2,
                                        language="TH",
                                        tvod_flag="N",
                                        rates=["PG-13"],
                                        subscription_tiers=["premium", "basic"],
                                        package_alacarte_list=["MOVIETVOD119_BBB_OC_30D_ALA", "MOVIETVOD149_BBB_OC_30D_ALA"],
                                        filter_ids=["15qaWX93Qrd5", "Z6exd4rmnV8y"],
                                        genres=["animation", "fantasy"],
                                        fields=['abc'],
                                        content_rights=["right"],
                                        is_vod_layer="Y",
                                        popularity_sort=PopularityPeriod.PAST_YEAR,
                                        other_types='movie')
        mock_sort_es_doc.assert_called_once_with(session=session,
                                                 type="series",
                                                 size=2,
                                                 language="TH",
                                                 genres=["animation", "fantasy"],
                                                 article_categories=[],
                                                 sort_field=PopularityPeriod.PAST_YEAR,
                                                 sort_by="desc",
                                                 tvod_flag="N",
                                                 subscription_tiers=["premium", "basic"],
                                                 rates=["PG-13"],
                                                 package_alacarte_list=["MOVIETVOD119_BBB_OC_30D_ALA", "MOVIETVOD149_BBB_OC_30D_ALA"],
                                                 filter_ids=["15qaWX93Qrd5", "Z6exd4rmnV8y"],
                                                 fields=['abc'],
                                                 is_vod_layer="Y",
                                                 content_rights=["right"],
                                                 other_types='movie',
                                                 must_nots=[])

    def test_get_most_popular_invalid_type(self):
        session = MagicMock()
        with self.assertRaisesRegex(
                ValueError, "Valid types are 'movie', 'series', or 'None'."):
            tv_utils.get_most_popular_items(session, type='xxx')

    def test_validate_type_non_happy_path(self):
        with self.assertRaisesRegex(
                ValueError, "Valid types are 'movie', 'series', or 'None'."):
            tv_utils._validate_type("xxx")

    def test_validate_type_movie_happy_path(self):
        self.assertIsNone(tv_utils._validate_type("movie"))

    def test_validate_type_series_happy_path(self):
        self.assertIsNone(tv_utils._validate_type("series"))

    def test_get_subscription_tiers_condition(self):
        subscription_tiers = ["premium", "basic"]
        expected = {"terms": {"subscription_tiers.keyword": ["premium", "basic"]}}
        actual = _get_subscription_tiers_condition(subscription_tiers)
        self.assertEqual(expected, actual)

    def test_sort_es_doc_dsl_with_must_not_conditions(self):
        es_session = MagicMock()
        expected_must_not_conditions = [
            {'match_phrase': {'title': 'empty title'}},
            {'match': {'is_trailer': 'yes'}},
            {'match': {'is_promo': 'yes'}},
            {'terms': {'content_provider': ['true_vision']}},
            {'terms': {'_id': ['en-15qaWX93Qrd5', 'en-Z6exd4rmnV8y']}}
        ]

        tv_utils._sort_es_doc(es_session, type="movie", filter_ids=["15qaWX93Qrd5", "Z6exd4rmnV8y"])
        _, kwargs = es_session.search.call_args
        actual = kwargs['body']['query']['constant_score']['filter']['bool']['must_not']
        self.assertEqual(actual, expected_must_not_conditions)

    def test_sort_es_doc_dsl(self):
        es_session = MagicMock()
        expected_conditions = {
            "must": [
                {'range': {'publish_date': {'lte': 'now/h'}}},
                {'match': {'status': 'publish'}},
                {'match': {'searchable': 'Y'}},
                {"term" : {'lang': 'en'}},
                {'match': {'tvod_flag': 'Y'}},
                {'terms': {'rate.keyword': ['G', 'PG-13']}},
                {'terms': {'subscription_tiers.keyword': ['premium', 'basic']}},
                {'terms': {'package_alacarte_list.keyword': ['MOVIETVOD149_BBB_OC_30D_ALA']}},
                {'terms': {'other_type.keyword': ['documentary']}},
                {'match': {'movie_type': 'series'}},
                {'match': {'ep_master': 'Y'}},            
            ],
            "must_not": [
                {'match_phrase': {'title': 'empty title'}},
                {'match': {'is_trailer': 'yes'}},
                {'match': {'is_promo': 'yes'}},
                {'terms': {'content_provider': ['true_vision']}},
                {'terms': {'_id': ['en-15qaWX93Qrd5', 'en-Z6exd4rmnV8y']}},
                {'term' : {'is_vod_layer': 'Y'}}
            ],
            "should": [
                {"range": {"expire_date": {"gt": "now/h"}}},
                {"bool": {"must_not": [{"exists": {"field": "expire_date"}}]}}
            ],
            "minimum_should_match": 1
        }

        tv_utils._sort_es_doc(es_session,
                              type="series",
                              tvod_flag="Y",
                              rates=["G", "PG-13"],
                              subscription_tiers=["premium", "basic"],
                              package_alacarte_list=['MOVIETVOD149_BBB_OC_30D_ALA'],
                              filter_ids=["15qaWX93Qrd5", "Z6exd4rmnV8y"],
                              other_types=['documentary'])
        
        _, kwargs = es_session.search.call_args
        actual = kwargs['body']['query']['constant_score']['filter']['bool']
        self.assertEqual(actual, expected_conditions)


    @patch("rcommsutils.cms.tv_utils._build_fields_result")
    def test_sort_es_doc_dsl_with_fields_call_build_fields_result(self, mock_fields):
        es_session = MagicMock()
        expect = {'test': 'test'}
        es_session.search.return_value = {'hits': {'hits': expect}}
        tv_utils._sort_es_doc(es_session,
                              type="movie",
                              fields=["id", "di"])
        mock_fields.assert_called_once_with(expect)

    def test_get_items_from_redis_should_call_redis_mget_with_prefix(self):
        redis_session = MagicMock()
        ids = ["id_1", "id_2"]
        prefix = "my_prefix:"
        expected = ["my_prefix:th-id_1", "my_prefix:th-id_2"]
        tv_utils._get_items_from_redis(redis_session, ids=ids, prefix=prefix)
        redis_session.mget.assert_called_once_with(expected)

    def test_get_items_from_redis_should_return_none_when_no_redis_session(
            self):
        redis_session = None
        ids = ["id_1", "id_2"]
        prefix = "my_prefix:"
        expected = [None, None]
        actual = tv_utils._get_items_from_redis(redis_session,
                                                ids=ids,
                                                prefix=prefix)
        self.assertEqual(expected, actual)

    def test_get_items_from_redis_should_return_none_when_redis_raise_error(
            self):
        redis_session = MagicMock()
        redis_session.mget.side_effect = ConnectionError()
        ids = ["id_1", "id_2"]
        prefix = "my_prefix:"
        expected = [None, None]
        actual = tv_utils._get_items_from_redis(redis_session,
                                                ids=ids,
                                                prefix=prefix)
        self.assertEqual(expected, actual)

    def test_get_items_from_redis_should_return_dict_items(self):
        redis_session = MagicMock()
        ids = ["id_1", "id_2"]
        prefix = "my_prefix:"
        expected = [{"id": "id_1"}, {"id": "id_2"}]
        redis_session.mget.return_value = [
            json.dumps({"id": "id_1"}),
            json.dumps({"id": "id_2"})
        ]
        actual = tv_utils._get_items_from_redis(redis_session,
                                                ids=ids,
                                                prefix=prefix)
        self.assertEqual(expected, actual)

    def test_get_items_from_redis_should_return_None_if_item_not_found(self):
        redis_session = MagicMock()
        ids = ["id_1", "id_2"]
        prefix = "my_prefix:"
        expected = [{"id": "id_1"}, None]
        redis_session.mget.return_value = [json.dumps({"id": "id_1"}), None]
        actual = tv_utils._get_items_from_redis(redis_session,
                                                ids=ids,
                                                prefix=prefix)
        self.assertEqual(expected, actual)

    def test_get_milliseconds_from_now_should_return_correct_milliseconds(self):
        mock_now_string = "2020-03-11T00:00:00.000Z"
        input_datetime = "2020-03-12T00:00:00.000Z"
        now = datetime.datetime.strptime(mock_now_string,
                                         "%Y-%m-%dT%H:%M:%S.%fZ")

        expected = 86400000
        actual = _get_milliseconds_from_now(input_datetime, now=now)
        self.assertEqual(expected, actual)

    def test_filter_items_from_conditions_with_is_vod_layer(self):
        items = [
            {"_source": {"id": "flag_a", "movie_type": "movie", "is_vod_layer": "Y"}},
            {"_source": {"id": "flag_b", "movie_type": "movie", "is_vod_layer": None}},
            {"_source": {"id": "flag_c", "movie_type": "series", "is_vod_layer": "Y"}},
            {"_source": {"id": "flag_d", "movie_type": "series", "is_vod_layer": None}}
        ]
        expected = [
            {"_source": {"id": "flag_a", "movie_type": "movie", "is_vod_layer": "Y"}},
            {"_source": {"id": "flag_b", "movie_type": "movie", "is_vod_layer": None}},
            {"_source": {"id": "flag_c", "movie_type": "series", "is_vod_layer": "Y"}}
        ]
        actual = tv_utils._filter_items_from_conditions(items, is_vod_layer="Y")
        self.assertEqual(expected, actual)

    def test_filter_items_from_conditions_without_is_vod_layer(self):
        items = [
            {"_source": {"id": "flag_a", "movie_type": "movie", "is_vod_layer": "Y"}},
            {"_source": {"id": "flag_b", "movie_type": "movie", "is_vod_layer": None}},
            {"_source": {"id": "flag_c", "movie_type": "series", "is_vod_layer": "Y"}},
            {"_source": {"id": "flag_d", "movie_type": "series", "is_vod_layer": None}}
        ]
        expected = [
            {"_source": {"id": "flag_a", "movie_type": "movie", "is_vod_layer": "Y"}},
            {"_source": {"id": "flag_b", "movie_type": "movie", "is_vod_layer": None}},
            {"_source": {"id": "flag_d", "movie_type": "series", "is_vod_layer": None}}
        ]
        actual = tv_utils._filter_items_from_conditions(items, is_vod_layer=None)
        self.assertEqual(expected, actual)

    def test_filter_items_from_conditions_with_type_series(self):
        items = [
            {"_source": {"id": "id_1","movie_type": "series","ep_master": "Y"}},
            {"_source": {"id": "id_2","movie_type": "series","ep_master": "N"}},
            {"_source": {"id": "id_3","movie_type": "movie",}}
        ]
        expected = [
            {"_source": {"id": "id_1","movie_type": "series","ep_master": "Y"}}
        ]
        actual = tv_utils._filter_items_from_conditions(items, type="series")
        self.assertEqual(expected, actual)
        
    def test_filter_items_from_conditions_with_is_in_list(self):
        items = [
            {"_source": {"rate": "G",  "content_rights": "svod", "other_type": "movie"}},
            {"_source": {"rate": "G",  "content_rights": "tvod", "other_type": "documentary"}},
            {"_source": {"rate": "PG", "content_rights": "avod", "other_type": "animation"}},
            {"_source": {"rate": "PG", "content_rights": "svod", "other_type": "series"}},
            {"_source": {"rate": " ",  "content_rights": "tvod", "other_type": "knowledge"}},
            {"_source": {"rate": " ",  "content_rights": "fvod", "other_type": "vr"}},
        ]
        expected = [
            {"_source": {"rate": "G",  "content_rights": "svod", "other_type": "movie"}},
            {"_source": {"rate": "PG", "content_rights": "svod", "other_type": "series"}}
        ]
        actual = tv_utils._filter_items_from_conditions(items, 
                                                        rates=["G","PG"],
                                                        content_rights=["svod"],
                                                        other_types=[],
                                                        )
        self.assertEqual(expected, actual)
        
    def test_filter_items_from_conditions_with_is_list_equal(self):
        items = [
            {"_source": {"package_alacarte_list": [],     "genres": ["action"],  "subscription_tiers": ["premium"]}},
            {"_source": {"package_alacarte_list": [],     "genres": ["drama"],   "subscription_tiers": ["premium"]}},
            {"_source": {"package_alacarte_list": None,   "genres": ["comedy"],  "subscription_tiers": ["trueid_plus"]}},
            {"_source": {"package_alacarte_list": None,   "genres": ["horor"],   "subscription_tiers": ["non_login"]}},
            {"_source": {"package_alacarte_list": [None], "genres": [],          "subscription_tiers": ["basic"]}},
            {"_source": {"package_alacarte_list": [None], "genres": ["cartoon"], "subscription_tiers": ["basic"]}},
        ]
        expected = [
            {"_source": {"package_alacarte_list": [],     "genres": ["action"],  "subscription_tiers": ["premium"]}},
            {"_source": {"package_alacarte_list": None,   "genres": ["comedy"],  "subscription_tiers": ["trueid_plus"]}}
        ]
        actual = tv_utils._filter_items_from_conditions(items,
                                                        package_alacarte_list=[],
                                                        genres=["action","comedy","cartoon"],
                                                        subscription_tiers=["premium","trueid_plus"])
        self.assertEqual(expected, actual)

    def test_get_package_alacarte_list_condition(self):
        package_alacarte_list = ["MOVIETVOD119_BBB_OC_30D_ALA", "MOVIETVOD149_BBB_OC_30D_ALA"]
        actual = tv_utils._get_package_alacarte_list_condition(package_alacarte_list)
        expect = {
            "terms": {
                Fields.PACKAGE_ALACARTE_LIST + '.keyword':
                    ["MOVIETVOD119_BBB_OC_30D_ALA", "MOVIETVOD149_BBB_OC_30D_ALA"]
            }
        }
        self.assertEquals(expect, actual)

    @patch("rcommsutils.cms.tv_utils._build_result_set")
    @patch("rcommsutils.cms.tv_utils._filter_items_from_conditions")
    @patch("rcommsutils.cms.tv_utils._set_cache_items")
    @patch("rcommsutils.cms.tv_utils._get_items_from_elasticsearch")
    @patch("rcommsutils.cms.tv_utils._get_items_from_redis")
    def test_get_items_should_call_elastic_if_no_cached(
            self, mock_get_items_from_redis, mock_get_items_from_elasticsearch,
            mock_set_cache_items, mock_filter_items_from_conditions,
            mock_build_result_set):
        es_session = MagicMock()
        redis_session = MagicMock()
        mock_get_items_from_redis.return_value = [
            {"id": "id_1"}, None, 
            {"id": "id_3"}
        ]
        ids = ["id_1", "id_2", "id_3"]
        expected_ids = ["id_2"]
        tv_utils.get_items(
            es_session,
            redis_session,
            ids=ids,
            article_categories=["family"],
            tvod_flag="Y",
            subscription_tiers=["non_login"],
            rates=["PG-13"],
            package_alacarte_list=["MOVIETVOD149_BBB_OC_30D_ALA"])
        mock_get_items_from_elasticsearch.assert_called_once_with(
            es_session=es_session,
            ids=expected_ids,
            type=None,
            genres=[],
            article_categories=["family"],
            language="EN",
            tvod_flag="Y",
            content_rights=[],
            subscription_tiers=["non_login"],
            rates=["PG-13"],
            package_alacarte_list=["MOVIETVOD149_BBB_OC_30D_ALA"],
            timeout="10s",
            is_vod_layer=None,
            other_types=[],
            must_nots=[])
        
    @patch("rcommsutils.cms.tv_utils._build_result_set")
    @patch("rcommsutils.cms.tv_utils._filter_items_from_conditions")
    @patch("rcommsutils.cms.tv_utils._set_cache_items")
    @patch("rcommsutils.cms.tv_utils._get_items_from_elasticsearch")
    @patch("rcommsutils.cms.tv_utils._get_items_from_redis")
    def test_get_items_call_elastic_with_must_nots_condition(
            self, mock_get_items_from_redis, mock_get_items_from_elasticsearch,
            mock_set_cache_items, mock_filter_items_from_conditions,
            mock_build_result_set):
        es_session = MagicMock()
        redis_session = MagicMock()
        mock_get_items_from_redis.return_value = [
            {"id": "id_1"}, None, 
            {"id": "id_3"}
        ]
        ids = ["id_1", "id_2", "id_3"]
        expected_ids = ["id_2"]
        tv_utils.get_items(
            es_session,
            redis_session,
            ids=ids,
            article_categories=["family"],
            tvod_flag="Y",
            subscription_tiers=["non_login"],
            rates=["PG-13"],
            package_alacarte_list=["MOVIETVOD149_BBB_OC_30D_ALA"],
            must_nots=[{"terms": {"studio.keyword": ["Golden A", "YG"]}}, {"match":{"actor.keyword":"Lisa"}}])
        mock_get_items_from_elasticsearch.assert_called_once_with(
            es_session=es_session,
            ids=expected_ids,
            type=None,
            genres=[],
            article_categories=["family"],
            language="EN",
            tvod_flag="Y",
            content_rights=[],
            subscription_tiers=["non_login"],
            rates=["PG-13"],
            package_alacarte_list=["MOVIETVOD149_BBB_OC_30D_ALA"],
            timeout="10s",
            is_vod_layer=None,
            other_types=[],
            must_nots=[{"terms": {"studio.keyword": ["Golden A", "YG"]}}, {"match":{"actor.keyword":"Lisa"}}])

    @patch("rcommsutils.cms.tv_utils._build_result_set")
    @patch("rcommsutils.cms.tv_utils._filter_items_from_conditions")
    @patch("rcommsutils.cms.tv_utils._set_cache_items")
    @patch("rcommsutils.cms.tv_utils._get_items_from_elasticsearch")
    @patch("rcommsutils.cms.tv_utils._get_items_from_redis")
    def test_get_items_should_call_set_cache_items(
            self, mock_get_items_from_redis, mock_get_items_from_elasticsearch,
            mock_set_cache_items, mock_filter_items_from_conditions,
            mock_build_result_set):
        es_session = MagicMock()
        redis_session = MagicMock()
        expected_non_cached_items = [{"_source": {"id": "id_2"}}]
        mock_get_items_from_redis.return_value = [{
            "id": "id_1"}, None, {
            "id": "id_3"
        }]
        ids = ["id_1", "id_2", "id_3"]
        mock_get_items_from_elasticsearch.return_value = expected_non_cached_items
        tv_utils.get_items(es_session, redis_session, ids=ids)
        mock_set_cache_items.assert_called_once_with(
            redis_session,
            items=expected_non_cached_items,
            prefix=ste.REDIS_MOVIE_PREFIX,
            language='EN')

    @patch("rcommsutils.cms.tv_utils._build_result_set")
    @patch("rcommsutils.cms.tv_utils._filter_items_from_conditions")
    @patch("rcommsutils.cms.tv_utils._set_cache_items")
    @patch("rcommsutils.cms.tv_utils._get_items_from_elasticsearch")
    @patch("rcommsutils.cms.tv_utils._get_items_from_redis")
    def test_get_items_should_call_filtered_items_from_condition(
            self, mock_get_items_from_redis, mock_get_items_from_elasticsearch,
            mock_set_cache_items, mock_filter_items_from_conditions,
            mock_build_result_set):
        es_session = MagicMock()
        redis_session = MagicMock()
        mock_get_items_from_redis.return_value = [
            {"_source": {"id": "id_1"}}, None, 
            {"_source": {"id": "id_3"}}
        ]
        ids = ["id_1", "id_2", "id_3"]
        expected_items = [
            {"_source": {"id": "id_1"}},
            {"_source": {"id": "id_3"}}
        ]
        tv_utils.get_items(
            es_session,
            redis_session,
            ids=ids,
            genres=["action"],
            article_categories=["action"],
            tvod_flag="Y",
            subscription_tiers=["non_login"],
            rates=["PG-13"],
            package_alacarte_list=["MOVIETVOD149_BBB_OC_30D_ALA"])

        mock_filter_items_from_conditions.assert_called_once_with(
            items=expected_items,
            genres=["action"],
            type=None,
            article_categories=["action"],
            tvod_flag="Y",
            content_rights=[],
            subscription_tiers=["non_login"],
            rates=["PG-13"],
            package_alacarte_list=["MOVIETVOD149_BBB_OC_30D_ALA"],
            is_vod_layer=None,
            other_types=[])

    @patch("rcommsutils.cms.tv_utils._build_result_set")
    @patch("rcommsutils.cms.tv_utils._filter_items_from_conditions")
    @patch("rcommsutils.cms.tv_utils._set_cache_items")
    @patch("rcommsutils.cms.tv_utils._get_items_from_elasticsearch")
    @patch("rcommsutils.cms.tv_utils._get_items_from_redis")
    def test_get_items_should_call_build_result_set(
            self, mock_get_items_from_redis, mock_get_items_from_elasticsearch,
            mock_set_cache_items, mock_filter_items_from_conditions,
            mock_build_result_set):
        es_session = MagicMock()
        redis_session = MagicMock()
        ids = ["id_1", "id_2", "id_3"]
        cached_items = [
            {"_source": {"id": "id_1"}},
            {"_source": {"id": "id_3"}}
        ]
        non_cached_items = [{"_source": {"id": "id_2"}}]
        expected_items = [
            {"_source": {"id": "id_1"}},
            {"_source": {"id": "id_3"}},
            {"_source": {"id": "id_2"}}
        ]
        mock_get_items_from_redis.return_value = [
            expected_items[0], None, expected_items[2]
        ]
        mock_get_items_from_elasticsearch.return_value = non_cached_items
        mock_filter_items_from_conditions.return_value = cached_items
        tv_utils.get_items(es_session,
                           redis_session,
                           ids=ids,
                           genres=["action"],
                           tvod_flag="Y",
                           subscription_tiers=["non_login"],
                           language="TH")
        mock_build_result_set.assert_called_once_with(
            [*cached_items, *non_cached_items])

    @patch("rcommsutils.cms.tv_utils._build_fields_result")
    @patch("rcommsutils.cms.tv_utils._get_items_from_elasticsearch")
    def test_get_items_define_fields_should_call_get_items_from_elasticsearch(
            self, mock_get_items_from_elasticsearch, mock_build_fields_result):
        es_session = MagicMock()
        ids = ["id_1", "id_2", "id_3"]

        tv_utils.get_items_define_fields(es_session,
                                         ids=ids,
                                         genres=["action"],
                                         tvod_flag="Y",
                                         content_rights=["svod"],
                                         subscription_tiers=["non_login"],
                                         language="TH")
        mock_get_items_from_elasticsearch.assert_called_once_with(
            es_session=es_session,
            ids=ids,
            fields=['id'],
            genres=['action'],
            language='TH',
            article_categories=[],
            package_alacarte_list=[],
            rates=[],
            content_rights=["svod"],
            subscription_tiers=['non_login'],
            timeout='10s',
            tvod_flag='Y',
            type=None,
            is_vod_layer=None,
            other_types=[],
            must_nots=[])

    @patch("rcommsutils.cms.tv_utils._build_fields_result")
    @patch("rcommsutils.cms.tv_utils._get_items_from_elasticsearch")
    def test_get_items_define_fields_should_call_build_fields_result(
            self, mock_get_items_from_elasticsearch, _build_fields_result):
        es_session = MagicMock()
        ids = ["id_1", "id_2", "id_3"]
        mock_get_items_from_elasticsearch.return_value = ['data']

        tv_utils.get_items_define_fields(es_session, ids=ids)
        _build_fields_result.assert_called_once_with(['data'])

    @patch("rcommsutils.cms.tv_utils.get_items")
    @patch("rcommsutils.cms.tv_utils.get_items_define_fields")
    def test_get_items_with_filter_should_call_get_items_define_fields(
            self, mock_get_items_define_fields, mock_get_items):
        es_session = MagicMock()
        ids = ["id_1", "id_2", "id_3"]

        tv_utils.get_items_with_filter(es_session,
                                       ids=ids,
                                       genres=["action"],
                                       tvod_flag="Y",
                                       content_rights=["svod"],
                                       subscription_tiers=["non_login"],
                                       language="TH",
                                       max_items=2)
        mock_get_items_define_fields.assert_called_once_with(
            es_session,
            ids=ids,
            fields=['id'],
            genres=['action'],
            language='TH',
            article_categories=[],
            package_alacarte_list=[],
            rates=[],
            content_rights=["svod"],
            subscription_tiers=['non_login'],
            timeout='10s',
            tvod_flag='Y',
            type=None,
            is_vod_layer=None,
            other_types=[],
            must_nots=[])

    @patch("rcommsutils.cms.tv_utils.get_items")
    @patch("rcommsutils.cms.tv_utils.get_items_define_fields")
    def test_get_items_with_filter_should_call_get_items_order_ids(
            self, mock_get_items_define_fields, mock_get_items):
        es_session = MagicMock()
        ids = ["id_1", "id_2", "id_3"]
        _return_ids = [
            {'id': 'id_1'},
            {'id': 'id_3'},
            {'id': 'id_2'}
        ]
        mock_get_items_define_fields.return_value = _return_ids

        tv_utils.get_items_with_filter(es_session,
                                       ids=ids,
                                       genres=["action"],
                                       tvod_flag="Y",
                                       content_rights=["svod"],
                                       subscription_tiers=["non_login"],
                                       language="TH",
                                       is_sort_item=True)
        mock_get_items.assert_called_once_with(es_session,
                                               ids=['id_1', 'id_2', 'id_3'],
                                               redis_session=None,
                                               genres=['action'],
                                               language='TH',
                                               article_categories=[],
                                               package_alacarte_list=[],
                                               rates=[],
                                               content_rights=["svod"],
                                               subscription_tiers=['non_login'],
                                               timeout='10s',
                                               tvod_flag='Y',
                                               type=None,
                                               is_vod_layer=None,
                                               other_types=[],
                                               must_nots=[])

    @patch("rcommsutils.cms.tv_utils._build_result_set")
    @patch("rcommsutils.cms.tv_utils._filter_items_from_conditions")
    @patch("rcommsutils.cms.tv_utils._set_cache_items")
    @patch("rcommsutils.cms.tv_utils._get_items_from_elasticsearch")
    @patch("rcommsutils.cms.tv_utils._get_items_from_redis")
    def test_get_items_return_only_from_redis_when_elasticsearch_raise_error(
            self, mock_get_items_from_redis, mock_get_items_from_elasticsearch,
            mock_set_cache_items, mock_filter_items_from_conditions,
            mock_build_result_set):
        es_session = MagicMock()
        redis_session = MagicMock()
        ids = ["id_1", "id_2", "id_3"]
        cached_items = [
            {"_source": {"id": "id_1"}},
            {"_source": {"id": "id_3"}}
        ]
        mock_get_items_from_elasticsearch.side_effect = Exception()
        mock_filter_items_from_conditions.return_value = cached_items
        tv_utils.get_items(es_session,
                           redis_session,
                           ids=ids,
                           genres=["action"],
                           tvod_flag="Y",
                           subscription_tiers=["non_login"],
                           language="TH")
        mock_build_result_set.assert_called_once_with([*cached_items])

    @patch("rcommsutils.cms.tv_utils._get_return_fields")
    def test_get_shelf_items_from_elasticsearch_with_shelf_id(
            self, mock_return_fields):
        session = MagicMock()
        id = "id__1"
        expected_dsl = {
            "query": {
                "bool": {
                    "minimum_should_match": 1,
                    "should": [
                        {
                            "bool": {
                                "filter": [
                                    {"term": {"id.keyword": f"{id}"}},
                                    {"term": {"_index": ste.RCOM_ES_PROGRESSIVE_INDEX}},
                                    {
                                        "nested": {
                                            "path": Fields.SHELF_LIST,
                                            "query": {
                                                "bool": {
                                                    "filter": [
                                                        {"term": {f"{Fields.SHELF_LIST}.{Fields.SHELF_ACTIVE}": True}},
                                                        {"range": {f"{Fields.SHELF_LIST}.{Fields.SHELF_START_DATE}": {"lte": ""}}}
                                                    ],
                                                "should": [
                                                    {"range": {f"{Fields.SHELF_LIST}.{Fields.END_DATE}": {"gte": ""}}},
                                                    {"bool" : {"must_not": [{"exists": {"field": f"{Fields.SHELF_LIST}.{Fields.END_DATE}"}}]}}
                                                ],
                                                "minimum_should_match": 1
                                                }
                                            },
                                            "inner_hits": {
                                                "_source": [
                                                    f"{Fields.SHELF_LIST}.{Fields.SHELF_ACTIVE}",
                                                    f"{Fields.SHELF_LIST}.{Fields.SHELF_ITEMS}",
                                                    f"{Fields.SHELF_LIST}.{Fields.SHELF_START_DATE}",
                                                    f"{Fields.SHELF_LIST}.{Fields.SHELF_END_DATE}"
                                                ]
                                            }
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            "bool": {
                                "filter": [
                                    {"term": {"id": id}},
                                    {"term": {"_index": ste.RCOM_ES_CONTENTS_SHELF_INDEX}}
                                ]
                            }
                        }
                    ]
                }
            }
        }

        tv_utils.get_shelf_items_from_elasticsearch(session, shelf_id=id)
        session.search.assert_called_once_with(
            _source=[Fields.SHELF_ITEMS],
            body=expected_dsl,
            index=[
                ste.RCOM_ES_CONTENTS_SHELF_INDEX, ste.RCOM_ES_PROGRESSIVE_INDEX
            ],
            size=1,
            timeout="10s")

    @patch("rcommsutils.cms.tv_utils._get_return_fields")
    def test_get_shelf_items_from_elasticsearch_with_wrong_shelf_id(
            self, mock_return_fields):
        session = MagicMock()
        id = "id__1"
        session.search.return_value = {"hits": {"hits": []}}
        with self.assertRaises(Exception) as err:
            tv_utils.get_shelf_items_from_elasticsearch(session, shelf_id=id)

        self.assertTrue('No shelf_id : id__1' in str(err.exception))

    @patch("rcommsutils.cms.tv_utils._get_return_fields")
    def test_get_shelf_items_from_elasticsearch_with_empty_shelf_items(
            self, mock_return_fields):
        session = MagicMock()
        id = "id__1"
        session.search.return_value = {
            "hits": {
                "hits": [{
                    "_index": ste.RCOM_ES_CONTENTS_SHELF_INDEX,
                    "_source": {
                        "shelf_items": []
                    }
                }]
            }
        }

        actual = tv_utils.get_shelf_items_from_elasticsearch(session,
                                                             shelf_id=id)
        self.assertEquals(([], None), actual)

    @patch("rcommsutils.cms.tv_utils._get_return_fields")
    def test_get_shelf_items_from_elasticsearch_with_empty_inner_hits_shelf_items(
            self, mock_return_fields):
        session = MagicMock()
        id = "id__1"
        session.search.return_value = {
            "hits": {
                "hits": [{
                    "_index": ste.RCOM_ES_PROGRESSIVE_INDEX,
                    "_source": {},
                    "inner_hits": {
                        "shelf_list": {
                            "hits": {
                                "hits": [{
                                    "_source": {
                                        "shelf_items": [],
                                        "end_date": "2021-11-13T07:41:38.071Z"
                                    }
                                }]
                            }
                        }
                    }
                }]
            }
        }

        actual = tv_utils.get_shelf_items_from_elasticsearch(
            session,
            shelf_id=id,
            active=True,
            timestamp="2019-11-13T07:41:38.071Z")
        self.assertEqual(([], "2021-11-13T07:41:38.071Z"), actual)

    @patch("rcommsutils.cms.tv_utils.datetime")
    @patch("rcommsutils.cms.tv_utils._set_cache_items")
    @patch("rcommsutils.cms.tv_utils.get_shelf_items_from_elasticsearch")
    @patch("rcommsutils.cms.tv_utils._get_items_from_redis")
    def test_get_shelf_items_with_empty_timestamp_should_be_call_time(
            self, mock_get_items_from_redis,
            mock_get_shelf_items_from_elasticsearch,
            mock_set_cache_items, mock_datetime):
        es_session = MagicMock()
        redis_session = MagicMock()
        current_time = "2019-11-13T07:41:38.071Z"

        mock_get_items_from_redis.return_value = [None]
        mock_datetime.datetime.fromtimestamp().strftime.return_value = current_time
        shelf_id = "id__1"
        active = True
        timestamp = ''
        tv_utils.get_shelf_items(es_session,
                                 redis_session,
                                 shelf_id=shelf_id,
                                 active=active,
                                 timestamp=timestamp)
        mock_get_shelf_items_from_elasticsearch.assert_called_once_with(
            es_session, shelf_id, current_time, active, "10s", ())

    @patch("rcommsutils.cms.tv_utils._set_cache_items")
    @patch("rcommsutils.cms.tv_utils.get_shelf_items_from_elasticsearch")
    @patch("rcommsutils.cms.tv_utils._get_items_from_redis")
    def test_get_shelf_items_should_call_get_items_from_redis(
            self, mock_get_items_from_redis,
            mock_get_shelf_items_from_elasticsearch, mock_set_cache_items):
        es_session = MagicMock()
        redis_session = MagicMock()
        shelf_id = "id__1"
        tv_utils.get_shelf_items(es_session, redis_session, shelf_id=shelf_id)
        mock_get_items_from_redis.assert_called_once_with(
            redis_session, ids=[shelf_id],
            prefix=ste.REDIS_SHELF_PREFIX,
            language='TH')

    @patch("rcommsutils.cms.tv_utils._set_cache_items")
    @patch("rcommsutils.cms.tv_utils.get_shelf_items_from_elasticsearch")
    @patch("rcommsutils.cms.tv_utils._get_items_from_redis")
    def test_get_shelf_items_should_call_elastic_if_no_cached(
            self, mock_get_items_from_redis,
            mock_get_shelf_items_from_elasticsearch, mock_set_cache_items):
        es_session = MagicMock()
        redis_session = MagicMock()
        mock_get_items_from_redis.return_value = [None]
        shelf_id = "id__1"
        active = True
        timestamp = '2021-03-19T03:30:00.000Z'
        tv_utils.get_shelf_items(es_session,
                                 redis_session,
                                 shelf_id=shelf_id,
                                 active=active,
                                 timestamp=timestamp)
        mock_get_shelf_items_from_elasticsearch.assert_called_once_with(
            es_session, shelf_id, timestamp, active, "10s", ())

    @patch("rcommsutils.cms.tv_utils._set_cache_items")
    @patch("rcommsutils.cms.tv_utils.get_shelf_items_from_elasticsearch")
    @patch("rcommsutils.cms.tv_utils._get_items_from_redis")
    def test_get_shelf_items_should_call_set_cache_items(
            self, mock_get_items_from_redis,
            mock_get_shelf_items_from_elasticsearch, mock_set_cache_items):
        es_session = MagicMock()
        redis_session = MagicMock()
        shelf_id = "id__1"
        active = True
        timestamp = '2021-03-19T03:30:00.000Z'
        mock_get_items_from_redis.return_value = [None]
        mock_get_shelf_items_from_elasticsearch.return_value = (['a',
                                                                 'b'], None)
        tv_utils.get_shelf_items(es_session,
                                 redis_session,
                                 shelf_id=shelf_id,
                                 active=active,
                                 timestamp=timestamp)
        mock_set_cache_items.assert_called_once_with(
            redis_session,
            items=[{
                'id': shelf_id,
                'shelf_items': ['a', 'b']
            }],
            prefix=ste.REDIS_SHELF_PREFIX,
            language='TH',
            default_expire_ms=ste.REDIS_DEFAULT_EXPIRE_MS)

    @patch("rcommsutils.cms.tv_utils._get_return_fields")
    def test_get_items_from_elasticsearch_dsl_with_type_series(
            self, mock_return_fields):
        session = MagicMock()
        expected_dsl = {
            'query': {
                'bool': {
                    'filter': {
                        'bool': {
                            'must': [
                                {'range': {'publish_date': {'lte': 'now/h'}}},
                                {'match': {'status': 'publish'}},
                                {'match': {'searchable': 'Y'}},
                                {"term" : {'lang': 'en'}},
                                {'terms': {'_id': ["en-id_1", "en-id_2"]}},
                                {'match': {'movie_type': 'series'}},
                                {'match': {'ep_master': 'Y'}},
                            ],
                            'must_not': [
                                {'match_phrase': {'title': 'empty title'}},
                                {'match': {'is_trailer': 'yes'}},
                                {'match': {'is_promo': 'yes'}},
                                {'terms': {'content_provider': ['true_vision']}},
                                {'term' : {'is_vod_layer': 'Y'}}
                            ],
                            'should': [
                                {"range": {"expire_date": {"gt": "now/h"}}},
                                {"bool": {"must_not": [{"exists": {"field": "expire_date"}}]}}
                            ],
                            "minimum_should_match": 1
                        }
                    }
                }
            }
        }
        ids = ["id_1", "id_2"]
        tv_utils._get_items_from_elasticsearch(session, ids=ids, type="series")
        session.search.assert_called_once_with(
            _source=mock_return_fields.return_value,
            body=expected_dsl,
            index=f'{ste.RCOM_ES_TRUETV_INDEX}',
            size=2,
            timeout="10s")

    def test_add_lang_prefix_for_ids_list_en(self):
        ids = ['id_1', 'id_2', 'id_3']
        language = 'EN'
        expect = ['en-id_1', 'en-id_2', 'en-id_3']
        actual = tv_utils._add_lang_prefix_for_ids(ids, language)
        self.assertListEqual(expect, actual)

    def test_add_lang_prefix_for_ids_list_th(self):
        ids = ['id_1', 'id_2', 'id_3']
        language = 'th'
        expect = ['th-id_1', 'th-id_2', 'th-id_3']
        actual = tv_utils._add_lang_prefix_for_ids(ids, language)
        self.assertListEqual(expect, actual)

    def test_add_lang_prefix_for_ids_str_en(self):
        id = 'id_1'
        language = 'en'
        expect = 'en-id_1'
        actual = tv_utils._add_lang_prefix_for_ids([id], language)[0]
        self.assertEquals(expect, actual)

    def test_add_lang_prefix_for_ids_str_th(self):
        id = 'id_1'
        language = 'TH'
        expect = 'th-id_1'
        actual = tv_utils._add_lang_prefix_for_ids([id], language)[0]
        self.assertEquals(expect, actual)

    def test_get_item_titles(self):
        session = MagicMock()
        session.search.return_value = {
            'hits': {
                'hits': [{
                    "_id": "en-a",
                    '_source': {
                        "title": "xxx",
                        "id": "a"
                    }
                }]
            }
        }

        dsl = {
            "query": {
                "constant_score": {
                    "filter": {
                        "bool": {
                            "must": [{
                                "terms": {
                                    "_id": ['en-a']
                                }
                            }]
                        }
                    }
                }
            }
        }
        language = "EN"
        ids = ['a']
        actual = tv_utils.get_item_titles(session, ids, language)
        exp = {'a': 'xxx'}
        self.assertDictEqual(exp, actual)
        session.search.assert_called_once_with(index=ste.RCOM_ES_TRUETV_INDEX,
                                               body=dsl,
                                               size=len(ids),
                                               _source=["id", "title"])

    def test_set_cache_items_should_set_items_with_correct_name(self):
        redis_session = MagicMock()
        items = [{"_id": "id_1"}, {"_id": "id_2"}]
        tv_utils._set_cache_items(redis_session,
                                  items=items,
                                  prefix=ste.REDIS_MOVIE_PREFIX,
                                  default_expire_ms=20)
        actual_calls = redis_session.pipeline.return_value.set.call_args_list
        expected_calls = [
            call(name=f"{ste.REDIS_MOVIE_PREFIX}id_1",
                 value=json.dumps(items[0]),
                 px=20),
            call(name=f"{ste.REDIS_MOVIE_PREFIX}id_2",
                 value=json.dumps(items[1]),
                 px=20)
        ]
        self.assertEqual(expected_calls, actual_calls)

    @patch("rcommsutils.cms.tv_utils._get_milliseconds_from_now")
    def test_set_cache_items_with_item_expire_date_less_than_dafault_default_expire_ms(
            self, mock_get_milliseconds_from_now):
        redis_session = MagicMock()
        items = [{
            "_id": "id_1",
            "_source": {
                "expire_date": "2020-03-12T09:34:00.000Z"
            }
        }]
        expire_date_milliseconds = 10
        mock_get_milliseconds_from_now.return_value = expire_date_milliseconds
        tv_utils._set_cache_items(redis_session,
                                  items=items,
                                  prefix=ste.REDIS_MOVIE_PREFIX,
                                  default_expire_ms=20)
        redis_session.pipeline.return_value.set.assert_called_once_with(
            name=f"{ste.REDIS_MOVIE_PREFIX}id_1",
            value=json.dumps(items[0]),
            px=expire_date_milliseconds)

    @patch("rcommsutils.cms.tv_utils._get_milliseconds_from_now")
    def test_set_cache_items_with_item_expire_date_None_should_call_set_cache(
            self, mock_get_milliseconds_from_now):
        redis_session = MagicMock()
        items = [{"_id": "id_1", "_source": {"expire_date": "0"}}]
        expire_date_milliseconds = None
        mock_get_milliseconds_from_now.return_value = expire_date_milliseconds
        tv_utils._set_cache_items(redis_session,
                                  items=items,
                                  prefix=ste.REDIS_MOVIE_PREFIX,
                                  default_expire_ms=20)
        redis_session.pipeline.return_value.set.assert_called_once()

    def test_set_cache_items_with_redis_session_is_None(self):
        redis_session = None
        items = [{"_id": "id_1"}]
        try:
            tv_utils._set_cache_items(redis_session,
                                      items=items,
                                      prefix=ste.REDIS_MOVIE_PREFIX,
                                      default_expire_ms=20)
        except Exception:
            self.fail("Failed when calling _set_cache_items")

    def test_set_cache_items_when_redis_session_raises(self):
        redis_session = MagicMock()
        redis_session.pipeline.return_value.set.side_effect = Exception()
        items = [{"_id": "id_1"}]
        try:
            tv_utils._set_cache_items(redis_session,
                                      items=items,
                                      prefix=ste.REDIS_MOVIE_PREFIX,
                                      default_expire_ms=20)
        except Exception:
            self.fail("Failed when calling _set_cache_items")

    def test_set_cache_items_with_id(self):
        redis_session = MagicMock()
        items = [{"id": "id_1"}]
        tv_utils._set_cache_items(redis_session,
                                  items=items,
                                  prefix=ste.REDIS_MOVIE_PREFIX,
                                  default_expire_ms=20)
        redis_session.pipeline.return_value.set.assert_called_once_with(
            name=f"{ste.REDIS_MOVIE_PREFIX}th-id_1",
            value=json.dumps(items[0]),
            px=20)

    def test_set_cache_items_with_type_string(self):
        redis_session = MagicMock()
        items = ["id_1"]
        tv_utils._set_cache_items(redis_session,
                                  items=items,
                                  prefix=ste.REDIS_MOVIE_PREFIX,
                                  default_expire_ms=20)
        redis_session.pipeline.return_value.set.assert_not_called()

    def test_set_cache_items_with_incorrect_id(self):
        redis_session = MagicMock()
        items = [{"incorrect_id": "id_1"}, {"id": "id_2"}]
        tv_utils._set_cache_items(redis_session,
                                  items=items,
                                  prefix=ste.REDIS_MOVIE_PREFIX,
                                  default_expire_ms=20)
        redis_session.pipeline.return_value.set.assert_called_once_with(
            name=f"{ste.REDIS_MOVIE_PREFIX}th-id_2",
            value=json.dumps(items[1]),
            px=20)


if __name__ == '__main__':
    ut.main()
