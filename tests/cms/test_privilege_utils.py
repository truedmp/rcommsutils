#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 19 12:15:37 2017

@author: root
"""
import unittest as ut
from unittest.mock import MagicMock, call, patch

import rcommsutils.cms.settings as ste
import tests.resources as rsc
from rcommsutils import privilege_utils
from rcommsutils.cms.privilege_utils import Fields, PopularityPeriod, _build_result_set, _API_SHOP_RETURN_FIELDS


class PrivilegeUtilsTest(ut.TestCase):
    # This map corresponses to the test data in test.resources

    EXPECTED_PRIV_MERCHANT_MAP = {
        '85yb7xOamvV1': {
            'branch_id': 'Y1KjqqVD8eMd',
            'master_id': '3111989',
            'merchant_id': 'y4YQgglRpkB4',
            'merchant_name': rsc.shops["TEST_SHOP_1"]["title_en"]
        },
        'Yzdy3MrVjomQ': {
            'branch_id': 'All branches',
            'master_id': '5065707',
            'merchant_id': 'jAjOdd9Y9lvJ',
            'merchant_name': rsc.shops["TEST_SHOP_2"]["title_en"]
        },
        'ZkpXzzQL38lk': {
            'branch_id': 'All branches',
            'master_id': '5056550',
            'merchant_id': 'NmB0BBQRle1o',
            'merchant_name': rsc.shops["TEST_SHOP_3"]["title_en"]
        },
        'n0K499qzV2wN': {
            'branch_id': 'All branches',
            'master_id': '3111989',
            'merchant_id': 'y4YQgglRpkB4',
            'merchant_name': rsc.shops["TEST_SHOP_1"]["title_en"]
        },
        'not_searchable_TEST_PRIVILEGE': {
            'branch_id': privilege_utils._MERCHANT_MAP_MERCHANT_NOT_FOUND,
            'master_id': privilege_utils._MERCHANT_MAP_MERCHANT_NOT_FOUND,
            'merchant_id': privilege_utils._MERCHANT_MAP_MERCHANT_NOT_FOUND,
            'merchant_name': privilege_utils._MERCHANT_MAP_MERCHANT_NOT_FOUND
        },
        'v78wWGjB5b06': {
            'branch_id': 'All branches',
            'master_id': '3111989',
            'merchant_id': 'y4YQgglRpkB4',
            'merchant_name': rsc.shops["TEST_SHOP_1"]["title_en"]
        }
    }

    EXPECTED_PRIV_MERCHANT_MAP_FIELD_NONE = {
        '85yb7xOamvV1': {
            'branch_id': 'Y1KjqqVD8eMd',
            'master_id': '3111989',
            'merchant_id': 'y4YQgglRpkB4',
            'merchant_name': None
        },
        'Yzdy3MrVjomQ': {
            'branch_id': 'All branches',
            'master_id': '5065707',
            'merchant_id': 'jAjOdd9Y9lvJ',
            'merchant_name': None
        },
        'ZkpXzzQL38lk': {
            'branch_id': 'All branches',
            'master_id': '5056550',
            'merchant_id': 'NmB0BBQRle1o',
            'merchant_name': None
        },
        'n0K499qzV2wN': {
            'branch_id': 'All branches',
            'master_id': '3111989',
            'merchant_id': 'y4YQgglRpkB4',
            'merchant_name': None
        },
        'not_searchable_TEST_PRIVILEGE': {
            'branch_id': privilege_utils._MERCHANT_MAP_MERCHANT_NOT_FOUND,
            'master_id': privilege_utils._MERCHANT_MAP_MERCHANT_NOT_FOUND,
            'merchant_id': privilege_utils._MERCHANT_MAP_MERCHANT_NOT_FOUND,
            'merchant_name': None
        },
        'v78wWGjB5b06': {
            'branch_id': 'All branches',
            'master_id': '3111989',
            'merchant_id': 'y4YQgglRpkB4',
            'merchant_name': None
        }
    }

    def json_ordered(self, obj):
        if isinstance(obj, dict):
            return sorted((k, self.json_ordered(v)) for k, v in obj.items())
        if isinstance(obj, list):
            return sorted(self.json_ordered(x) for x in obj)
        else:
            return obj

    def assert_prvbasic_equal(self, actuals, expected, type):
        self.assertEqual(len(actuals), len(expected))
        for i in range(len(actuals)):
            actual = actuals[i]
            exp = expected[i]
            if type == 'privilege':
                self.assertEqual(actual[ste.RCOM_API_PRIV_ID], exp[Fields.ID])
                self.assertEqual(actual[ste.RCOM_API_OLD_PRIV_ID],
                                 exp[Fields.ORIGINAL_ID])
                self.assertEqual(actual[ste.RCOM_API_MERCHANT_ID],
                                 exp[rsc.TEST_MERCHANT_ID])
                self.assertEqual(actual[ste.RCOM_API_OLD_MERCHANT_ID],
                                 exp[rsc.TEST_MASTER_ID])
                self.assertEqual(actual[ste.RCOM_API_BRANCH_ID],
                                 exp[rsc.TEST_BRANCH_ID])
                self.assertEqual(actual[ste.RCOM_API_USSD], exp[Fields.USSD])
                self.assertEqual(actual[ste.RCOM_API_CAMPAIGN_CODE],
                                 exp[Fields.CAMPAIGN_CODE])
                self.assertEqual(actual[ste.RCOM_API_TERM_CONDITION],
                                 exp[Fields.TERM_CONDITION])
                self.assertEqual(actual[ste.RCOM_API_TITLE_PRIVILEGE],
                                 exp[Fields.TITLE])
                self.assertEqual(actual[ste.RCOM_API_TITLE],
                                 exp[rsc.TEST_MERCHANT_TITLE])
                self.assertEqual(actual[ste.RCOM_API_THUMBNAIL],
                                 exp[Fields.THUMB])
            else:
                self.assertEqual(actual[ste.RCOM_API_MERCHANT_ID],
                                 exp[Fields.ID])
                self.assertEqual(actual[ste.RCOM_API_OLD_MERCHANT_ID],
                                 exp[Fields.ORIGINAL_ID])
                self.assertEqual(actual[ste.RCOM_API_LOCATION],
                                 exp[Fields.LOCATION])
                self.assertEqual(actual[ste.RCOM_API_ADDRESS],
                                 exp[Fields.ADDRESS])
                self.assertEqual(actual[ste.RCOM_API_TITLE], exp[Fields.TITLE])
                self.assertEqual(actual[ste.RCOM_API_PRIVILEGE_LIST],
                                 exp[Fields.PRIVILEGE_LIST])
                self.assertEqual(actual[ste.RCOM_API_DISCOVER],
                                 exp[Fields.DISCOVER])
                self.assertEqual(actual[ste.RCOM_API_MERCHANT_TYPE],
                                 exp[Fields.MERCHANT_TYPE])
                if exp.get(Fields.THUMB_LIST) != None:
                    self.assertEqual(actual[ste.RCOM_API_THUMBLIST],
                                     exp[Fields.THUMB_LIST])
                    self.assertEqual(
                        actual[ste.RCOM_API_THUMBNAIL],
                        exp[Fields.THUMB_LIST].get(Fields.HIGHLIGHT))
                else:
                    self.assertEqual(actual[ste.RCOM_API_THUMBNAIL], None)

            self.assertEqual(actual[ste.RCOM_API_EXPIRE_DATE],
                             exp[Fields.EXPIRE_DATE])
            self.assertEqual(actual[ste.RCOM_API_PUBLISH_DATE],
                             exp[Fields.PUBLISH_DATE])
            self.assertEqual(actual[ste.RCOM_API_CONTENT_TYPE],
                             exp[Fields.CONTENT_TYPE])
            self.assertEqual(actual[ste.RCOM_API_PRV_HIT_COUNT_WEEK],
                             exp[Fields.HIT_COUNT_WEEK])
            self.assertEqual(actual[ste.RCOM_API_PRV_HIT_COUNT_MONTH],
                             exp[Fields.HIT_COUNT_MONTH])
            self.assertEqual(actual[ste.RCOM_API_PRV_HIT_COUNT_YEAR],
                             exp[Fields.HIT_COUNT_YEAR])
            self.assertEqual(actual[ste.RCOM_API_TAGS], exp[Fields.TAGS])
            self.assertEqual(actual[ste.RCOM_API_DETAIL], exp[Fields.DETAIL])
            self.assertEqual(actual[ste.RCOM_API_ARTICLE_CATEGORY],
                             exp[Fields.ARTICLE_CATEGORY])

    def test_build_test_sort_pop_shop_yearlymsearch_query_body(self):
        index = "test_index"
        priv_ids = ["1", "2", "3"]
        language = "th"
        expected = '{"index": "test_index"}\n{"query": {"bool": {"filter": {"bool": {"must": [{"match": {"privilege_list": "1"}}, {"match": {"status": "publish"}}]}}}}}\n{"index": "test_index"}\n{"query": {"bool": {"filter": {"bool": {"must": [{"match": {"privilege_list": "2"}}, {"match": {"status": "publish"}}]}}}}}\n{"index": "test_index"}\n{"query": {"bool": {"filter": {"bool": {"must": [{"match": {"privilege_list": "3"}}, {"match": {"status": "publish"}}]}}}}}\n'
        actual = privilege_utils._build_msearch_query_body(
            index, priv_ids, language)
        self.assertEqual(expected, actual)

    def test_get_merchant_id_for_privileges_from_merchant(self):
        session = MagicMock()
        merchant_responses = {
            "responses": [{
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['n0K499qzV2wN']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['n0K499qzV2wN']
                                ['master_id'],
                            "title":
                                self.EXPECTED_PRIV_MERCHANT_MAP['n0K499qzV2wN']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['v78wWGjB5b06']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['v78wWGjB5b06']
                                ['master_id'],
                            "title":
                                self.EXPECTED_PRIV_MERCHANT_MAP['v78wWGjB5b06']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['Yzdy3MrVjomQ']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['Yzdy3MrVjomQ']
                                ['master_id'],
                            "title":
                                self.EXPECTED_PRIV_MERCHANT_MAP['Yzdy3MrVjomQ']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['ZkpXzzQL38lk']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['ZkpXzzQL38lk']
                                ['master_id'],
                            "title":
                                self.EXPECTED_PRIV_MERCHANT_MAP['ZkpXzzQL38lk']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['85yb7xOamvV1']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['85yb7xOamvV1']
                                ['master_id'],
                            "title":
                                self.EXPECTED_PRIV_MERCHANT_MAP['85yb7xOamvV1']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP[
                                    'not_searchable_TEST_PRIVILEGE']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP[
                                    'not_searchable_TEST_PRIVILEGE']
                                ['master_id'],
                            "title":
                                self.EXPECTED_PRIV_MERCHANT_MAP[
                                    'not_searchable_TEST_PRIVILEGE']
                                ['merchant_name']
                        }
                    }]
                }
            }]
        }
        exp = {
            'n0K499qzV2wN': {
                'merchant_id': 'y4YQgglRpkB4',
                'master_id': '3111989',
                'branch_id': 'All branches',
                'merchant_name': rsc.shops["TEST_SHOP_1"]["title"]
            },
            'v78wWGjB5b06': {
                'merchant_id': 'y4YQgglRpkB4',
                'master_id': '3111989',
                'branch_id': 'All branches',
                'merchant_name': rsc.shops["TEST_SHOP_1"]["title"]
            },
            'Yzdy3MrVjomQ': {
                'merchant_id': 'jAjOdd9Y9lvJ',
                'master_id': '5065707',
                'branch_id': 'All branches',
                'merchant_name': rsc.shops["TEST_SHOP_2"]["title"]
            },
            'ZkpXzzQL38lk': {
                'merchant_id': 'NmB0BBQRle1o',
                'master_id': '5056550',
                'branch_id': 'All branches',
                'merchant_name': rsc.shops["TEST_SHOP_3"]["title"]
            },
            '85yb7xOamvV1': {
                'merchant_id': 'y4YQgglRpkB4',
                'master_id': '3111989',
                'branch_id': 'All branches',
                'merchant_name': rsc.shops["TEST_SHOP_1"]["title"]
            },
            'not_searchable_TEST_PRIVILEGE': {
                'merchant_id':
                    privilege_utils._MERCHANT_MAP_MERCHANT_NOT_FOUND,
                'master_id':
                    privilege_utils._MERCHANT_MAP_MERCHANT_NOT_FOUND,
                'branch_id':
                    'All branches',
                'merchant_name':
                    privilege_utils._MERCHANT_MAP_MERCHANT_NOT_FOUND
            },
        }
        session.msearch.side_effect = [merchant_responses]
        prv = list(map(lambda x: dict(_source=x), rsc.privileges.values()))
        actual = privilege_utils._get_merchant_id_for_privileges(
            session, prv, "EN")
        self.assertEqual(dict(sorted(exp.items())),
                         dict(sorted(actual.items())))

    def test_get_merchant_id_for_privileges_from_merchant_with_missing_field(
            self):
        session = MagicMock()
        merchant_responses = {
            "responses": [{
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['n0K499qzV2wN']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['n0K499qzV2wN']
                                ['master_id'],
                            "title_en":
                                self.EXPECTED_PRIV_MERCHANT_MAP['n0K499qzV2wN']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['v78wWGjB5b06']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['v78wWGjB5b06']
                                ['master_id'],
                            "title_en":
                                self.EXPECTED_PRIV_MERCHANT_MAP['v78wWGjB5b06']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['Yzdy3MrVjomQ']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['Yzdy3MrVjomQ']
                                ['master_id'],
                            "title_en":
                                self.EXPECTED_PRIV_MERCHANT_MAP['Yzdy3MrVjomQ']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['ZkpXzzQL38lk']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['ZkpXzzQL38lk']
                                ['master_id'],
                            "title_en":
                                self.EXPECTED_PRIV_MERCHANT_MAP['ZkpXzzQL38lk']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['85yb7xOamvV1']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['85yb7xOamvV1']
                                ['master_id'],
                            "title_en":
                                self.EXPECTED_PRIV_MERCHANT_MAP['85yb7xOamvV1']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP[
                                    'not_searchable_TEST_PRIVILEGE']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP[
                                    'not_searchable_TEST_PRIVILEGE']
                                ['master_id'],
                            "title_en":
                                self.EXPECTED_PRIV_MERCHANT_MAP[
                                    'not_searchable_TEST_PRIVILEGE']
                                ['merchant_name']
                        }
                    }]
                }
            }]
        }
        exp = {
            '85yb7xOamvV1': {
                'merchant_id': 'y4YQgglRpkB4',
                'master_id': '3111989',
                'branch_id': 'All branches',
                'merchant_name': None
            },
            'Yzdy3MrVjomQ': {
                'merchant_id': 'jAjOdd9Y9lvJ',
                'master_id': '5065707',
                'branch_id': 'All branches',
                'merchant_name': None
            },
            'ZkpXzzQL38lk': {
                'merchant_id': 'NmB0BBQRle1o',
                'master_id': '5056550',
                'branch_id': 'All branches',
                'merchant_name': None
            },
            'n0K499qzV2wN': {
                'merchant_id': 'y4YQgglRpkB4',
                'master_id': '3111989',
                'branch_id': 'All branches',
                'merchant_name': None
            },
            'not_searchable_TEST_PRIVILEGE': {
                'merchant_id': privilege_utils._MERCHANT_MAP_MERCHANT_NOT_FOUND,
                'master_id': privilege_utils._MERCHANT_MAP_MERCHANT_NOT_FOUND,
                'branch_id': 'All branches',
                'merchant_name': None
            },
            'v78wWGjB5b06': {
                'merchant_id': 'y4YQgglRpkB4',
                'master_id': '3111989',
                'branch_id': 'All branches',
                'merchant_name': None
            }
        }
        session.msearch.side_effect = [merchant_responses]
        prv = list(map(lambda x: dict(_source=x), rsc.privileges.values()))
        actual = privilege_utils._get_merchant_id_for_privileges(
            session, prv, "TH")
        self.assertEqual(exp, actual)

    def test_get_merchant_id_for_privileges_from_branch(self):
        session = MagicMock()
        merchant_responses = {
            "responses": [{
                "hits": {
                    "hits": []
                }
            }, {
                "hits": {
                    "hits": []
                }
            }, {
                "hits": {
                    "hits": []
                }
            }, {
                "hits": {
                    "hits": []
                }
            }, {
                "hits": {
                    "hits": []
                }
            }, {
                "hits": {
                    "hits": []
                }
            }]
        }

        branch_response = {
            "responses": [{
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['n0K499qzV2wN']
                                ['branch_id'],
                            "master_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['n0K499qzV2wN']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['n0K499qzV2wN']
                                ['master_id'],
                            "title":
                                self.EXPECTED_PRIV_MERCHANT_MAP['n0K499qzV2wN']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['v78wWGjB5b06']
                                ['branch_id'],
                            "master_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['v78wWGjB5b06']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['v78wWGjB5b06']
                                ['master_id'],
                            "title":
                                self.EXPECTED_PRIV_MERCHANT_MAP['v78wWGjB5b06']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['Yzdy3MrVjomQ']
                                ['branch_id'],
                            "master_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['Yzdy3MrVjomQ']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['Yzdy3MrVjomQ']
                                ['master_id'],
                            "title":
                                self.EXPECTED_PRIV_MERCHANT_MAP['Yzdy3MrVjomQ']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['ZkpXzzQL38lk']
                                ['branch_id'],
                            "master_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['ZkpXzzQL38lk']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['ZkpXzzQL38lk']
                                ['master_id'],
                            "title":
                                self.EXPECTED_PRIV_MERCHANT_MAP['ZkpXzzQL38lk']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['85yb7xOamvV1']
                                ['branch_id'],
                            "master_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['85yb7xOamvV1']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['85yb7xOamvV1']
                                ['master_id'],
                            "title":
                                self.EXPECTED_PRIV_MERCHANT_MAP['85yb7xOamvV1']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP[
                                    'not_searchable_TEST_PRIVILEGE']
                                ['branch_id'],
                            "master_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP[
                                    'not_searchable_TEST_PRIVILEGE']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP[
                                    'not_searchable_TEST_PRIVILEGE']
                                ['master_id'],
                            "title":
                                self.EXPECTED_PRIV_MERCHANT_MAP[
                                    'not_searchable_TEST_PRIVILEGE']
                                ['merchant_name']
                        }
                    }]
                }
            }]
        }
        session.msearch.side_effect = [merchant_responses, branch_response]
        prv = list(map(lambda x: dict(_source=x), rsc.privileges.values()))
        actual = privilege_utils._get_merchant_id_for_privileges(
            session, prv, "EN")
        self.assertEqual(self.EXPECTED_PRIV_MERCHANT_MAP, actual)

    def test_get_merchant_id_for_privileges_from_branch_with_missing_field(
            self):
        session = MagicMock()
        merchant_responses = {
            "responses": [
                {
                    "hits": {
                        "hits": []
                    }
                },
                {
                    "hits": {
                        "hits": []
                    }
                },
                {
                    "hits": {
                        "hits": []
                    }
                },
                {
                    "hits": {
                        "hits": []
                    }
                },
                {
                    "hits": {
                        "hits": []
                    }
                },
                {
                    "hits": {
                        "hits": []
                    }
                },
            ]
        }

        branch_response = {
            "responses": [{
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['n0K499qzV2wN']
                                ['branch_id'],
                            "master_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['n0K499qzV2wN']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['n0K499qzV2wN']
                                ['master_id'],
                            "title_en":
                                self.EXPECTED_PRIV_MERCHANT_MAP['n0K499qzV2wN']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['v78wWGjB5b06']
                                ['branch_id'],
                            "master_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['v78wWGjB5b06']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['v78wWGjB5b06']
                                ['master_id'],
                            "title_en":
                                self.EXPECTED_PRIV_MERCHANT_MAP['v78wWGjB5b06']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['Yzdy3MrVjomQ']
                                ['branch_id'],
                            "master_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['Yzdy3MrVjomQ']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['Yzdy3MrVjomQ']
                                ['master_id'],
                            "title_en":
                                self.EXPECTED_PRIV_MERCHANT_MAP['Yzdy3MrVjomQ']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['ZkpXzzQL38lk']
                                ['branch_id'],
                            "master_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['ZkpXzzQL38lk']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['ZkpXzzQL38lk']
                                ['master_id'],
                            "title_en":
                                self.EXPECTED_PRIV_MERCHANT_MAP['ZkpXzzQL38lk']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['85yb7xOamvV1']
                                ['branch_id'],
                            "master_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['85yb7xOamvV1']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP['85yb7xOamvV1']
                                ['master_id'],
                            "title_en":
                                self.EXPECTED_PRIV_MERCHANT_MAP['85yb7xOamvV1']
                                ['merchant_name']
                        }
                    }]
                }
            }, {
                "hits": {
                    "hits": [{
                        "_source": {
                            "id":
                                self.EXPECTED_PRIV_MERCHANT_MAP[
                                    'not_searchable_TEST_PRIVILEGE']
                                ['branch_id'],
                            "master_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP[
                                    'not_searchable_TEST_PRIVILEGE']
                                ['merchant_id'],
                            "original_id":
                                self.EXPECTED_PRIV_MERCHANT_MAP[
                                    'not_searchable_TEST_PRIVILEGE']
                                ['master_id'],
                            "title_en":
                                self.EXPECTED_PRIV_MERCHANT_MAP[
                                    'not_searchable_TEST_PRIVILEGE']
                                ['merchant_name']
                        }
                    }]
                }
            }]
        }
        session.msearch.side_effect = [merchant_responses, branch_response]
        prv = list(map(lambda x: dict(_source=x), rsc.privileges.values()))
        actual = privilege_utils._get_merchant_id_for_privileges(
            session, prv, "TH")
        self.assertEqual(self.EXPECTED_PRIV_MERCHANT_MAP_FIELD_NONE, actual)

    def test_build_result_set_prv(self):
        expected_result = rsc.privileges.copy()
        expected_result.pop("not_searchable_TEST_PRIVILEGE")
        expected = sorted(list(expected_result.values()), key=lambda x: x['id'])
        prv = list(map(lambda x: dict(_source=x), rsc.privileges.values()))
        actual = sorted(_build_result_set(prv, ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
                                          self.EXPECTED_PRIV_MERCHANT_MAP),
                        key=lambda x: x[ste.RCOM_API_PRIV_ID])
        self.assert_prvbasic_equal(actual, expected,
                                   ste.RCOM_ES_PRIVILEGE_PRIVILEGE)

    def test_build_result_set_shop(self):
        expected = sorted(list(rsc.shops.values()), key=lambda x: x['id'])
        shop = list(map(lambda x: dict(_source=x), rsc.shops.values()))
        actual = sorted(_build_result_set(shop, ste.RCOM_ES_PRIVILEGE_SHOP),
                        key=lambda x: x[ste.RCOM_API_MERCHANT_ID])
        self.assert_prvbasic_equal(actual, expected, ste.RCOM_ES_PRIVILEGE_SHOP)

    def test_build_result_set_empty_input_prv(self):
        expected = []
        prv = []
        actual = _build_result_set(prv, ste.RCOM_ES_PRIVILEGE_PRIVILEGE)
        self.assertEqual(expected, actual)

    def test_build_result_set_empty_input_shop(self):
        expected = []
        shop = []
        actual = _build_result_set(shop, ste.RCOM_ES_PRIVILEGE_SHOP)
        self.assertEqual(expected, actual)

    def test_get_campaign_code_blacklist(self):
        privilege_utils._blacklist = None
        self.assertEqual(privilege_utils._get_campaign_code_blacklist(None), [])
        privilege_utils._blacklist = None
        self.assertEqual(privilege_utils._get_campaign_code_blacklist("111"),
                         ["111"])
        privilege_utils._blacklist = None
        self.assertEqual(
            privilege_utils._get_campaign_code_blacklist("111,222"),
            ["111", "222"])
        privilege_utils._blacklist = None

    @patch("rcommsutils.privilege_utils._sort_es_doc")
    def test_sort_in_get_pop_prv_weekly(self, mock_sort):
        session = MagicMock()
        privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_WEEK,
            size=2,
            type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
            language="EN")

        mock_sort.assert_called_once_with(session,
                                          type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
                                          size=2,
                                          tags=[],
                                          campaign_type=None,
                                          card_type=None,
                                          article_category=None,
                                          allow_app=None,
                                          sort_field=PopularityPeriod.PAST_WEEK,
                                          sort_by="desc",
                                          language="EN",
                                          search_after=[], 
                                          exclude_ids=[],
                                          merchant_type=[])

    @patch("rcommsutils.privilege_utils._sort_es_doc")
    def test_sort_in_get_pop_prv_monthly(self, mock_sort):
        session = MagicMock()
        privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_MONTH,
            size=2,
            type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
            language="EN")

        mock_sort.assert_called_once_with(
            session,
            type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
            size=2,
            tags=[],
            campaign_type=None,
            card_type=None,
            article_category=None,
            allow_app=None,
            sort_field=PopularityPeriod.PAST_MONTH,
            sort_by="desc",
            language="EN",
            search_after=[], 
            exclude_ids=[],
            merchant_type=[])

    @patch("rcommsutils.privilege_utils._sort_es_doc")
    def test_sort_in_get_pop_prv_yearly(self, mock_sort):
        session = MagicMock()
        privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_YEAR,
            size=3,
            type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
            language="EN")

        mock_sort.assert_called_once_with(session,
                                          type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
                                          size=3,
                                          tags=[],
                                          campaign_type=None,
                                          card_type=None,
                                          article_category=None,
                                          allow_app=None,
                                          sort_field=PopularityPeriod.PAST_YEAR,
                                          sort_by="desc",
                                          language="EN",
                                          search_after=[], 
                                          exclude_ids=[],
                                          merchant_type=[])

    @patch("rcommsutils.privilege_utils._sort_es_doc")
    def test_sort_in_get_pop_prv_with_tags(self, mock_sort):
        session = MagicMock()
        privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_WEEK,
            size=2,
            tags=["clothes"],
            type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
            language="EN")

        mock_sort.assert_called_once_with(session,
                                          type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
                                          size=2,
                                          tags=["clothes"],
                                          campaign_type=None,
                                          card_type=None,
                                          article_category=None,
                                          allow_app=None,
                                          sort_field=PopularityPeriod.PAST_WEEK,
                                          sort_by="desc",
                                          language="EN",
                                          search_after=[], 
                                          exclude_ids=[],
                                          merchant_type=[])

    @patch("rcommsutils.privilege_utils._sort_es_doc")
    def test_sort_in_get_pop_prv_with_card_type(self, mock_sort):
        session = MagicMock()
        privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_WEEK,
            size=2,
            card_type=["black"],
            type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
            language="EN")

        mock_sort.assert_called_once_with(session,
                                          type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
                                          size=2,
                                          tags=[],
                                          campaign_type=None,
                                          card_type=["black"],
                                          article_category=None,
                                          allow_app=None,
                                          sort_field=PopularityPeriod.PAST_WEEK,
                                          sort_by="desc",
                                          language="EN",
                                          search_after=[], 
                                          exclude_ids=[],
                                          merchant_type=[])

    @patch("rcommsutils.privilege_utils._sort_es_doc")
    def test_sort_in_get_pop_prv_with_article_category(self, mock_sort):
        session = MagicMock()
        privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_WEEK,
            size=2,
            article_category=["cate"],
            type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
            language="EN")

        mock_sort.assert_called_once_with(session,
                                          type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
                                          size=2,
                                          tags=[],
                                          campaign_type=None,
                                          card_type=None,
                                          article_category=["cate"],
                                          allow_app=None,
                                          sort_field=PopularityPeriod.PAST_WEEK,
                                          sort_by="desc",
                                          language="EN",
                                          search_after=[], 
                                          exclude_ids=[],
                                          merchant_type = [])
    
    @patch("rcommsutils.privilege_utils._sort_es_doc")
    def test_sort_in_get_pop_prv_with_cursor(self, mock_sort):
        session = MagicMock()
        privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_WEEK,
            size=2,
            article_category=["cate"],
            type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
            language="EN",
            search_after=["abc",123])

        mock_sort.assert_called_once_with(session,
                                          type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
                                          size=2,
                                          tags=[],
                                          campaign_type=None,
                                          card_type=None,
                                          article_category=["cate"],
                                          allow_app=None,
                                          sort_field=PopularityPeriod.PAST_WEEK,
                                          sort_by="desc",
                                          language="EN",
                                          search_after=["abc",123], 
                                          exclude_ids=[],
                                          merchant_type=[])

    @patch("rcommsutils.privilege_utils._sort_es_doc")
    def test_sort_in_get_pop_prv_with_exclude_ids(self, mock_sort):
        session = MagicMock()
        privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_WEEK,
            size=2,
            article_category=["cate"],
            type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
            language="EN",
            search_after=["abc",123],
            exclude_ids=["a","b","c"])

        mock_sort.assert_called_once_with(session,
                                          type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
                                          size=2,
                                          tags=[],
                                          campaign_type=None,
                                          card_type=None,
                                          article_category=["cate"],
                                          allow_app=None,
                                          sort_field=PopularityPeriod.PAST_WEEK,
                                          sort_by="desc",
                                          language="EN",
                                          search_after=["abc",123], 
                                          exclude_ids=["a","b","c"],
                                          merchant_type=[])

    @patch("rcommsutils.privilege_utils._sort_es_doc")
    def test_sort_in_get_pop_prv_with_allow_app(self, mock_sort):
        session = MagicMock()
        privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_WEEK,
            size=2,
            allow_app=["trueidweb"],
            type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
            language="EN")

        mock_sort.assert_called_once_with(session,
                                          type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
                                          size=2,
                                          tags=[],
                                          campaign_type=None,
                                          card_type=None,
                                          article_category=None,
                                          allow_app=["trueidweb"],
                                          sort_field=PopularityPeriod.PAST_WEEK,
                                          sort_by="desc",
                                          language="EN",
                                          search_after=[], 
                                          exclude_ids=[],
                                          merchant_type=[])

    @patch("rcommsutils.privilege_utils._get_merchant_id_for_privileges")
    def test_get_pop_prv_weekly(self, mock_get_merchant):
        session = MagicMock()
        dsl = {
            'sort': {
                'HIT_COUNT_WEEK': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "en"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'exists': {
                                    'field': 'article_category'
                                }
                            }, {
                                "match": {
                                    "searchable": "Y"
                                }
                            }],
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }, {
                                'range': {
                                    'quota_over_existed': {'gt': 0}
                                }
                            }]
                        }
                    }
                }
            }
        }

        search_result = {
            "hits": {
                "hits": [{
                    "_source": rsc.privileges['TEST_PRIV_4']
                }, {
                    "_source": rsc.privileges['TEST_PRIV_2']
                }]
            }
        }

        expected = [
            rsc.privileges['TEST_PRIV_4'], rsc.privileges['TEST_PRIV_2']
        ]
        session.search.return_value = search_result
        mock_get_merchant.return_value = {
            rsc.privileges['TEST_PRIV_4']['id']:
                self.EXPECTED_PRIV_MERCHANT_MAP[
                    rsc.privileges['TEST_PRIV_4']['id']],
            rsc.privileges['TEST_PRIV_2']['id']:
                self.EXPECTED_PRIV_MERCHANT_MAP[
                    rsc.privileges['TEST_PRIV_2']['id']],
        }

        actual = privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_WEEK,
            size=2,
            type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
            language="EN")

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)
        mock_get_merchant.assert_called_once_with(session,
                                                  search_result["hits"]["hits"],
                                                  "EN")

        self.assert_prvbasic_equal(actual, expected,
                                   ste.RCOM_ES_PRIVILEGE_PRIVILEGE)

    @patch("rcommsutils.privilege_utils._get_merchant_id_for_privileges")
    def test_get_pop_prv_monthly(self, mock_get_merchant):
        session = MagicMock()
        dsl = {
            'sort': {
                'HIT_COUNT_MONTH': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "en"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'exists': {
                                    'field': 'article_category'
                                }
                            }, {
                                "match": {
                                    "searchable": "Y"
                                }
                            }],
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }, {
                                'range': {
                                    'quota_over_existed': {'gt': 0}
                                }
                            }]
                        },
                    }
                }
            }
        }

        search_result = {
            "hits": {
                "hits": [{
                    "_source": rsc.privileges['TEST_PRIV_1']
                }, {
                    "_source": rsc.privileges['TEST_PRIV_2']
                }]
            }
        }

        expected = [
            rsc.privileges['TEST_PRIV_1'], rsc.privileges['TEST_PRIV_2']
        ]
        session.search.return_value = search_result
        mock_get_merchant.return_value = {
            rsc.privileges['TEST_PRIV_1']['id']:
                self.EXPECTED_PRIV_MERCHANT_MAP[
                    rsc.privileges['TEST_PRIV_1']['id']],
            rsc.privileges['TEST_PRIV_2']['id']:
                self.EXPECTED_PRIV_MERCHANT_MAP[
                    rsc.privileges['TEST_PRIV_2']['id']],
        }

        actual = privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_MONTH,
            size=2,
            type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
            language="EN")

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

        mock_get_merchant.assert_called_once_with(session,
                                                  search_result["hits"]["hits"],
                                                  "EN")

        self.assert_prvbasic_equal(actual, expected,
                                   ste.RCOM_ES_PRIVILEGE_PRIVILEGE)

    @patch("rcommsutils.privilege_utils._get_merchant_id_for_privileges")
    def test_get_pop_prv_yearly(self, mock_get_merchant):
        session = MagicMock()
        dsl = {
            'sort': {
                'HIT_COUNT_YEAR': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "en"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'exists': {
                                    'field': 'article_category'
                                }
                            }, {
                                "match": {
                                    "searchable": "Y"
                                }
                            }],
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }, {
                                'range': {
                                    'quota_over_existed': {'gt': 0}
                                }
                            }]
                        }
                    }
                }
            }
        }

        search_result = {
            "hits": {
                "hits": [{
                    "_source": rsc.privileges['TEST_PRIV_5']
                }, {
                    "_source": rsc.privileges['TEST_PRIV_2']
                }]
            }
        }

        expected = [
            rsc.privileges['TEST_PRIV_5'], rsc.privileges['TEST_PRIV_2']
        ]
        session.search.return_value = search_result
        mock_get_merchant.return_value = {
            rsc.privileges['TEST_PRIV_5']['id']:
                self.EXPECTED_PRIV_MERCHANT_MAP[
                    rsc.privileges['TEST_PRIV_5']['id']],
            rsc.privileges['TEST_PRIV_2']['id']:
                self.EXPECTED_PRIV_MERCHANT_MAP[
                    rsc.privileges['TEST_PRIV_2']['id']],
        }

        actual = privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_YEAR,
            size=3,
            type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
            language="EN")

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

        mock_get_merchant.assert_called_once_with(session,
                                                  search_result["hits"]["hits"],
                                                  "EN")

        self.assert_prvbasic_equal(actual, expected,
                                   ste.RCOM_ES_PRIVILEGE_PRIVILEGE)

    @patch("rcommsutils.privilege_utils._get_merchant_id_for_privileges")
    def test_get_pop_prv_with_blacklist(self, mock_get_merchant):
        session = MagicMock()
        dsl = {
            'sort': {
                'HIT_COUNT_WEEK': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "en"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'exists': {
                                    'field': 'article_category'
                                }
                            }, {
                                "match": {
                                    "searchable": "Y"
                                }
                            }],
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }, {
                                'terms': {
                                    'campaign_code.keyword': [
                                        'PRIV-38425', 'PRIV-38765'
                                    ]
                                }
                            }, {
                                "range": {
                                    "quota_over_existed": {
                                        "gt": 0
                                    }
                                }
                            }]
                        }
                    }
                }
            }
        }

        search_result = {
            "hits": {
                "hits": [{
                    "_source": rsc.privileges['TEST_PRIV_3']
                }, {
                    "_source": rsc.privileges['TEST_PRIV_1']
                }]
            }
        }

        privilege_utils._blacklist = ["PRIV-38425", "PRIV-38765"]
        expected = [
            rsc.privileges['TEST_PRIV_3'], rsc.privileges['TEST_PRIV_1']
        ]
        session.search.return_value = search_result
        mock_get_merchant.return_value = {
            rsc.privileges['TEST_PRIV_3']['id']:
                self.EXPECTED_PRIV_MERCHANT_MAP[
                    rsc.privileges['TEST_PRIV_3']['id']],
            rsc.privileges['TEST_PRIV_1']['id']:
                self.EXPECTED_PRIV_MERCHANT_MAP[
                    rsc.privileges['TEST_PRIV_1']['id']],
        }

        actual = privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_WEEK,
            size=2,
            type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
            language="EN")

        privilege_utils._blacklist = None

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

        mock_get_merchant.assert_called_once_with(session,
                                                  search_result["hits"]["hits"],
                                                  "EN")

        self.assert_prvbasic_equal(actual, expected,
                                   ste.RCOM_ES_PRIVILEGE_PRIVILEGE)

    @patch("rcommsutils.privilege_utils._get_merchant_id_for_privileges")
    def test_get_pop_prv_with_tags(self, mock_get_merchant):
        session = MagicMock()
        dsl = {
            'sort': {
                'HIT_COUNT_WEEK': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }, {
                                "range": {
                                    "quota_over_existed": {
                                        "gt": 0
                                    }
                                }
                            }],
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "en"
                                }
                            }, {
                                'terms': {
                                    'tags': ['clothes']
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'exists': {
                                    'field': 'article_category'
                                }
                            }, {
                                "match": {
                                    "searchable": "Y"
                                }
                            }]
                        }
                    }
                }
            }
        }

        search_result = {
            "hits": {
                "hits": [{
                    "_source": rsc.privileges['TEST_PRIV_3']
                }]
            }
        }

        expected = [rsc.privileges['TEST_PRIV_3']]
        session.search.return_value = search_result
        mock_get_merchant.return_value = {
            rsc.privileges['TEST_PRIV_3']['id']:
                self.EXPECTED_PRIV_MERCHANT_MAP[
                    rsc.privileges['TEST_PRIV_3']['id']]
        }

        actual = privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_WEEK,
            size=2,
            tags=["clothes"],
            type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
            language="EN")

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

        mock_get_merchant.assert_called_once_with(session,
                                                  search_result["hits"]["hits"],
                                                  "EN")

        self.assert_prvbasic_equal(actual, expected,
                                   ste.RCOM_ES_PRIVILEGE_PRIVILEGE)

    @patch("rcommsutils.privilege_utils._get_merchant_id_for_privileges")
    def test_get_pop_prv_with_card_type(self, mock_get_merchant):
        session = MagicMock()
        dsl = {
            'sort': {
                'HIT_COUNT_WEEK': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }, {
                                "range": {
                                    "quota_over_existed": {
                                        "gt": 0
                                    }
                                }
                            }],
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "en"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'exists': {
                                    'field': 'article_category'
                                }
                            }, {
                                "match": {
                                    "searchable": "Y"
                                }
                            }, {
                                "terms": {
                                    "card_type.keyword": ["red"]
                                }
                            }]
                        }
                    }
                }
            }
        }

        search_result = {
            "hits": {
                "hits": [{
                    "_source": rsc.privileges['TEST_PRIV_3']
                }]
            }
        }

        expected = [rsc.privileges['TEST_PRIV_3']]
        session.search.return_value = search_result
        mock_get_merchant.return_value = {
            rsc.privileges['TEST_PRIV_3']['id']:
                self.EXPECTED_PRIV_MERCHANT_MAP[
                    rsc.privileges['TEST_PRIV_3']['id']]
        }

        actual = privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_WEEK,
            size=2,
            card_type=['red'],
            type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
            language="EN")

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

        mock_get_merchant.assert_called_once_with(session,
                                                  search_result["hits"]["hits"],
                                                  "EN")

        self.assert_prvbasic_equal(actual, expected,
                                   ste.RCOM_ES_PRIVILEGE_PRIVILEGE)

    @patch("rcommsutils.privilege_utils._get_merchant_id_for_privileges")
    def test_get_pop_prv_with_article_category(self, mock_get_merchant):
        session = MagicMock()
        dsl = {
            'sort': {
                'HIT_COUNT_WEEK': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }, {
                                "range": {
                                    "quota_over_existed": {
                                        "gt": 0
                                    }
                                }
                            }],
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "en"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'exists': {
                                    'field': 'article_category'
                                }
                            }, {
                                "match": {
                                    "searchable": "Y"
                                }
                            }, {
                                "terms": {
                                    "article_category": ["cate1", "cate2"]
                                }
                            }]
                        }
                    }
                }
            }
        }

        search_result = {
            "hits": {
                "hits": [{
                    "_source": rsc.privileges['TEST_PRIV_3']
                }]
            }
        }

        expected = [rsc.privileges['TEST_PRIV_3']]
        session.search.return_value = search_result
        mock_get_merchant.return_value = {
            rsc.privileges['TEST_PRIV_3']['id']:
                self.EXPECTED_PRIV_MERCHANT_MAP[
                    rsc.privileges['TEST_PRIV_3']['id']]
        }

        actual = privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_WEEK,
            size=2,
            article_category=["cate1", "cate2"],
            type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
            language="EN")

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

        mock_get_merchant.assert_called_once_with(session,
                                                  search_result["hits"]["hits"],
                                                  "EN")

        self.assert_prvbasic_equal(actual, expected,
                                   ste.RCOM_ES_PRIVILEGE_PRIVILEGE)

    @patch("rcommsutils.privilege_utils._get_merchant_id_for_privileges")
    def test_get_pop_prv_with_allow_app(self, mock_get_merchant):
        session = MagicMock()
        dsl = {
            'sort': {
                'HIT_COUNT_WEEK': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }, {
                                "range": {
                                    "quota_over_existed": {
                                        "gt": 0
                                    }
                                }
                            }],
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "en"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'exists': {
                                    'field': 'article_category'
                                }
                            }, {
                                "match": {
                                    "searchable": "Y"
                                }
                            }, {
                                "terms": {
                                    "allow_app.keyword": ["trueyouweb"]
                                }
                            }]
                        }
                    }
                }
            }
        }

        search_result = {
            "hits": {
                "hits": [{
                    "_source": rsc.privileges['TEST_PRIV_3']
                }]
            }
        }

        expected = [rsc.privileges['TEST_PRIV_3']]
        session.search.return_value = search_result
        mock_get_merchant.return_value = {
            rsc.privileges['TEST_PRIV_3']['id']:
                self.EXPECTED_PRIV_MERCHANT_MAP[
                    rsc.privileges['TEST_PRIV_3']['id']]
        }

        actual = privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_WEEK,
            size=2,
            allow_app=["trueyouweb"],
            type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
            language="EN")

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

        mock_get_merchant.assert_called_once_with(session,
                                                  search_result["hits"]["hits"],
                                                  "EN")

        self.assert_prvbasic_equal(actual, expected,
                                   ste.RCOM_ES_PRIVILEGE_PRIVILEGE)

    @patch("rcommsutils.privilege_utils._sort_es_doc")
    def test_sort_in_get_pop_shop_weekly(self, mock_sort):
        session = MagicMock()
        privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_WEEK,
            size=2,
            type=ste.RCOM_ES_PRIVILEGE_SHOP,
            language="EN")

        mock_sort.assert_called_once_with(session,
                                          type=ste.RCOM_ES_PRIVILEGE_SHOP,
                                          size=2,
                                          tags=[],
                                          campaign_type=None,
                                          card_type=None,
                                          article_category=None,
                                          allow_app=None,
                                          sort_field=PopularityPeriod.PAST_WEEK,
                                          sort_by="desc",
                                          language="EN",
                                          search_after=[],
                                          exclude_ids=[],
                                          merchant_type=[])

    @patch("rcommsutils.privilege_utils._sort_es_doc")
    def test_sort_in_get_pop_shop_monthly(self, mock_sort):
        session = MagicMock()
        privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_MONTH,
            size=2,
            type=ste.RCOM_ES_PRIVILEGE_SHOP,
            language="EN")

        mock_sort.assert_called_once_with(
            session,
            type=ste.RCOM_ES_PRIVILEGE_SHOP,
            size=2,
            tags=[],
            campaign_type=None,
            card_type=None,
            article_category=None,
            allow_app=None,
            sort_field=PopularityPeriod.PAST_MONTH,
            sort_by="desc",
            language="EN",
            search_after=[],
            exclude_ids=[],
            merchant_type=[])

    @patch("rcommsutils.privilege_utils._sort_es_doc")
    def test_sort_in_get_pop_shop_yearly(self, mock_sort):
        session = MagicMock()
        privilege_utils.get_most_popular_items(
            session=session,
            popularity_sort=PopularityPeriod.PAST_YEAR,
            size=3,
            type=ste.RCOM_ES_PRIVILEGE_SHOP,
            language="EN")

        mock_sort.assert_called_once_with(session,
                                          type=ste.RCOM_ES_PRIVILEGE_SHOP,
                                          size=3,
                                          tags=[],
                                          campaign_type=None,
                                          card_type=None,
                                          article_category=None,
                                          allow_app=None,
                                          sort_field=PopularityPeriod.PAST_YEAR,
                                          sort_by="desc",
                                          language="EN",
                                          search_after=[],
                                          exclude_ids=[],
                                          merchant_type=[])
    def test_sort_pop_shop_weekly(self):
        session = MagicMock()
        dsl = {
            'sort': {
                'HIT_COUNT_WEEK': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'must_not': [
                                {
                                    'match': {
                                        'allow_recommend': 'false'
                                    }
                                },
                            ],
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                "term": {
                                    "searchable": "Y"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            },
                                {
                                    'nested': {
                                        'path': 'denormalize_privilege',
                                        'query': {
                                            'bool': {
                                                'must_not': [{
                                                    'term': {
                                                        'denormalize_privilege.quota_over_existed.keyword':
                                                            1
                                                    }
                                                }]
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    }
                }
            }
        }

        search_result = {
            "hits": {
                "hits": [{
                    "_source": rsc.shops['TEST_SHOP_4']
                }, {
                    "_source": rsc.shops['TEST_SHOP_3']
                }]
            }
        }

        expected = [rsc.shops['TEST_SHOP_4'], rsc.shops['TEST_SHOP_3']]
        session.search.return_value = search_result

        actual = privilege_utils._sort_es_doc(
            session=session,
            sort_field=PopularityPeriod.PAST_WEEK,
            size=2,
            type=ste.RCOM_ES_PRIVILEGE_SHOP)

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertListEqual(expected_dsl, actual_dsl)

        self.assert_prvbasic_equal(actual, expected, ste.RCOM_ES_PRIVILEGE_SHOP)

    def test_sort_pop_shop_monthly(self):
        session = MagicMock()
        dsl = {
            'sort': {
                'HIT_COUNT_MONTH': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                "term": {
                                    "searchable": "Y"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must_not': [{
                                                'term': {
                                                    'denormalize_privilege.quota_over_existed.keyword':
                                                        1
                                                }
                                            }]
                                        }
                                    }
                                }
                            }],
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }]
                        }
                    }
                }
            }
        }

        search_result = {
            "hits": {
                "hits": [{
                    "_source": rsc.shops['TEST_SHOP_5']
                }, {
                    "_source": rsc.shops['not_searchable_TEST_SHOP']
                }]
            }
        }

        expected = [
            rsc.shops['TEST_SHOP_5'], rsc.shops['not_searchable_TEST_SHOP']
        ]
        session.search.return_value = search_result

        actual = privilege_utils._sort_es_doc(
            session=session,
            sort_field=PopularityPeriod.PAST_MONTH,
            size=2,
            type=ste.RCOM_ES_PRIVILEGE_SHOP)

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

        self.assert_prvbasic_equal(actual, expected, ste.RCOM_ES_PRIVILEGE_SHOP)

    def test_sort_pop_shop_yearly(self):
        session = MagicMock()
        dsl = {
            'sort': {
                'HIT_COUNT_YEAR': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'must': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must': [],
                                            'must_not': [{
                                                'term': {
                                                    'denormalize_privilege.quota_over_existed.keyword':
                                                        1
                                                }
                                            }]
                                        }
                                    }
                                }
                            }],
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }]
                        }
                    }
                }
            }
        }

        search_result = {
            "hits": {
                "hits": [{
                    "_source": rsc.shops['TEST_SHOP_1']
                }, {
                    "_source": rsc.shops['TEST_SHOP_5']
                }]
            }
        }

        expected = [rsc.shops['TEST_SHOP_1'], rsc.shops['TEST_SHOP_5']]
        session.search.return_value = search_result

        actual = privilege_utils._sort_es_doc(
            session=session,
            sort_field=PopularityPeriod.PAST_YEAR,
            size=2,
            type=ste.RCOM_ES_PRIVILEGE_SHOP)

        session.search._sort_es_doc(index=ste.RCOM_ES_SHOP_INDEX,
                                    body=dsl,
                                    size=2,
                                    _source=_API_SHOP_RETURN_FIELDS)

        self.assert_prvbasic_equal(actual, expected, ste.RCOM_ES_PRIVILEGE_SHOP)

    def test_sort_with_pop_shop_expired_date(self):
        session = MagicMock()
        dsl = {
            'sort': {
                'HIT_COUNT_YEAR': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                "term": {
                                    "searchable": "Y"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must_not': [{
                                                'term': {
                                                    'denormalize_privilege.quota_over_existed.keyword':
                                                        1
                                                }
                                            }]
                                        }
                                    }
                                }
                            }],
                            "must_not": [
                                {
                                    "match": {
                                        "allow_recommend": "false"
                                    }
                                }
                            ],
                        }
                    }
                }
            }
        }

        search_result = {
            "hits": {
                "hits": [{
                    "_source": rsc.shops['TEST_SHOP_1']
                }, {
                    "_source": rsc.shops['TEST_SHOP_5']
                }, {
                    "_source": rsc.shops['not_searchable_TEST_SHOP']
                }, {
                    "_source": rsc.shops['TEST_SHOP_4']
                }, {
                    "_source": rsc.shops['TEST_SHOP_3']
                }, {
                    "_source": rsc.shops['TEST_SHOP_2']
                }, {
                    "_source": rsc.shops['NO_THUMBLIST_TEST_SHOP']
                }, {
                    "_source": rsc.shops['NO_HIGHLIGHT_TEST_SHOP']
                }]
            }
        }

        expected = [
            rsc.shops['TEST_SHOP_1'], rsc.shops['TEST_SHOP_5'],
            rsc.shops['not_searchable_TEST_SHOP'], rsc.shops['TEST_SHOP_4'],
            rsc.shops['TEST_SHOP_3'], rsc.shops['TEST_SHOP_2'],
            rsc.shops['NO_THUMBLIST_TEST_SHOP'],
            rsc.shops['NO_HIGHLIGHT_TEST_SHOP']
        ]
        session.search.return_value = search_result

        actual = privilege_utils._sort_es_doc(
            session=session,
            sort_field=PopularityPeriod.PAST_YEAR,
            size=10,
            type=ste.RCOM_ES_PRIVILEGE_SHOP)

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

        self.assert_prvbasic_equal(actual, expected, ste.RCOM_ES_PRIVILEGE_SHOP)

    def test_validate_type_non_happy_path(self):
        with self.assertRaisesRegex(
                ValueError,
                "Valid types are 'privilege', 'branch' and 'shop'."):
            privilege_utils._validate_type("xxx")

    def test_validate_type_privilege_happy_path(self):
        self.assertIsNone(privilege_utils._validate_type("privilege"))

    def test_validate_type_shop_happy_path(self):
        self.assertIsNone(privilege_utils._validate_type("shop"))

    def test_validate_type_branch_happy_path(self):
        self.assertIsNone(privilege_utils._validate_type("branch"))

    @patch("rcommsutils.privilege_utils._sort_es_doc")
    def test_get_items_privilege(self, mock_sort):
        session = MagicMock()
        ids = ['not_searchable_TEST_PRIVILEGE']
        privilege_utils.get_items(session, type='privilege', ids=ids)
        mock_sort.assert_called_once_with(session,
                                          type=ste.RCOM_ES_PRIVILEGE_PRIVILEGE,
                                          size=1,
                                          ids=['not_searchable_TEST_PRIVILEGE'],
                                          tags=[],
                                          campaign_type=None,
                                          card_type=None,
                                          article_category=None,
                                          allow_app=None,
                                          language="TH",
                                          filter_blacklisted_privileges=True)

    @patch("rcommsutils.privilege_utils._sort_es_doc")
    def test_get_items_shop(self, mock_sort):
        session = MagicMock()
        ids = ['not_published_TEST_SHOP']
        privilege_utils.get_items(session, type='shop', ids=ids)

        mock_sort.assert_called_once_with(session,
                                          type=ste.RCOM_ES_PRIVILEGE_SHOP,
                                          size=1,
                                          ids=['not_published_TEST_SHOP'],
                                          tags=[],
                                          campaign_type=None,
                                          card_type=None,
                                          article_category=None,
                                          allow_app=None,
                                          language="TH",
                                          filter_blacklisted_privileges=True)

    @patch("rcommsutils.privilege_utils._sort_es_doc")
    def test_get_items_shop_with_is_trueyou_eat(self, mock_sort):
        session = MagicMock()
        ids = ['not_published_TEST_SHOP']
        privilege_utils.get_items(session,
                                  type='shop',
                                  ids=ids)

        mock_sort.assert_called_once_with(session,
                                          type=ste.RCOM_ES_PRIVILEGE_SHOP,
                                          size=1,
                                          ids=['not_published_TEST_SHOP'],
                                          tags=[],
                                          campaign_type=None,
                                          card_type=None,
                                          article_category=None,
                                          allow_app=None,
                                          language="TH",
                                          filter_blacklisted_privileges=True)

    @patch("rcommsutils.privilege_utils._sort_es_doc")
    def test_get_items_shop_with_campaign_type_privilege(self, mock_sort):
        session = MagicMock()
        ids = ['test_ids']
        privilege_utils.get_items(session,
                                  type=ste.RCOM_ES_PRIVILEGE_SHOP,
                                  ids=ids,
                                  campaign_type="privilege",
                                  language="TH")

        mock_sort.assert_called_once_with(session,
                                          type=ste.RCOM_ES_PRIVILEGE_SHOP,
                                          size=1,
                                          ids=['test_ids'],
                                          tags=[],
                                          campaign_type="privilege",
                                          card_type=None,
                                          article_category=None,
                                          allow_app=None,
                                          language="TH",
                                          filter_blacklisted_privileges=True)

    @patch("rcommsutils.privilege_utils._sort_es_doc")
    def test_get_items_shop_with_campaign_type_point(self, mock_sort):
        session = MagicMock()
        ids = ['test_ids']
        privilege_utils.get_items(session,
                                  type=ste.RCOM_ES_PRIVILEGE_SHOP,
                                  ids=ids,
                                  campaign_type="point",
                                  language="TH")

        mock_sort.assert_called_once_with(session,
                                          type=ste.RCOM_ES_PRIVILEGE_SHOP,
                                          size=1,
                                          ids=['test_ids'],
                                          tags=[],
                                          campaign_type="point",
                                          card_type=None,
                                          article_category=None,
                                          allow_app=None,
                                          language="TH",
                                          filter_blacklisted_privileges=True)

    @patch("rcommsutils.privilege_utils._get_merchant_id_for_privileges")
    def test_get_items_privilege_not_searchable_should_return_nothing(
            self, mock_get_merchant):
        session = MagicMock()
        ids = ['not_searchable_TEST_PRIVILEGE']
        dsl = {
            'sort': {
                'HIT_COUNT_WEEK': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'must': [{
                                'terms': {
                                    '_id': ['th-not_searchable_TEST_PRIVILEGE']
                                }
                            }],
                            "must_not": [{
                                "match": {
                                    "allow_recommend": "false"
                                }
                            }, {
                                "range": {
                                    "quota_over_existed": {
                                        "gt": 0
                                    }
                                }
                            }],
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                "match": {
                                    "searchable": "Y"
                                }
                            }, {
                                "exists": {
                                    "field": "article_category"
                                }
                            }]
                        }
                    }
                }
            }
        }

        search_result = {"hits": {"hits": []}}

        session.search.return_value = search_result

        actuals = privilege_utils.get_items(session, type='privilege', ids=ids)
        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)
        mock_get_merchant.assert_called_once_with(session,
                                                  search_result["hits"]["hits"],
                                                  "TH")

        self.assertListEqual(actuals, [])

    def test_get_items_shop_not_published_should_return_nothing(self):
        session = MagicMock()
        ids = ['not_published_TEST_SHOP']
        dsl = {
            'sort': {
                'HIT_COUNT_WEEK': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'must': [{
                                'terms': {
                                    '_id': ['th-not_published_TEST_SHOP']
                                }
                            }],
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }],
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                "term": {
                                    "searchable": "Y"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must_not': [{
                                                'term': {
                                                    'denormalize_privilege.quota_over_existed.keyword':
                                                        1
                                                }
                                            }]
                                        }
                                    }
                                }
                            }]
                        }
                    }
                }
            }
        }

        search_result = {"hits": {"hits": []}}

        session.search.return_value = search_result

        actuals = privilege_utils.get_items(session, type='shop', ids=ids)
        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

        self.assertListEqual(actuals, [])

    def test_get_items_shop_draft_should_return_nothing(self):
        session = MagicMock()
        ids = ['draft_TEST_SHOP']
        dsl = {
            'sort': {
                'HIT_COUNT_WEEK': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'must': [{
                                'terms': {
                                    '_id': ['th-draft_TEST_SHOP']
                                }
                            }],
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }],
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                "term": {
                                    "searchable": "Y"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must_not': [{
                                                'term': {
                                                    'denormalize_privilege.quota_over_existed.keyword':
                                                        1
                                                }
                                            }]
                                        }
                                    }
                                }
                            }]
                        }
                    }
                }
            }
        }

        search_result = {"hits": {"hits": []}}

        session.search.return_value = search_result

        actuals = privilege_utils.get_items(session, type='shop', ids=ids)

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

        self.assertListEqual(actuals, [])

    @patch("rcommsutils.privilege_utils._get_merchant_id_for_privileges")
    def test_get_items_privilege_in_blacklist(self, mock_get_merchant):
        session = MagicMock()
        ids = [rsc.privileges['TEST_PRIV_1']["id"]]
        dsl = {
            'sort': {
                'HIT_COUNT_WEEK': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'must': [{
                                'terms': {
                                    '_id': ['th-n0K499qzV2wN']
                                }
                            }],
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }, {
                                "range": {
                                    "quota_over_existed": {
                                        "gt": 0
                                    }
                                }
                            }, {
                                'terms': {
                                    'campaign_code.keyword': [
                                        rsc.privileges['TEST_PRIV_1']
                                        ["campaign_code"]
                                    ]
                                }
                            }],
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                "match": {
                                    "searchable": "Y"
                                }
                            }, {
                                "exists": {
                                    "field": "article_category"
                                }
                            }]
                        }
                    }
                }
            }
        }

        search_result = {"hits": {"hits": []}}

        session.search.return_value = search_result
        privilege_utils._blacklist = [
            rsc.privileges['TEST_PRIV_1']["campaign_code"]
        ]

        actuals = privilege_utils.get_items(session, type='privilege', ids=ids)
        privilege_utils._blacklist = None
        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)
        mock_get_merchant.assert_called_once_with(session,
                                                  search_result["hits"]["hits"],
                                                  "TH")

        self.assertListEqual(actuals, [])

    def test_get_geo_location_filtered_items_branch(self):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        expected = ["NMXpO1JNZ3JM", "nNe3DWpDl8lQ", "nNe3DWpDl2T"]
        actual = privilege_utils.get_geo_location_filtered_items(
            session,
            type="branch",
            lat=13.7565591,
            lon=100.7966439,
            radius=9,
            language='TH')

        session_search_expected = [
            call(_source=['id', 'original_id', 'privilege_list', 'master_id'],
                 body={
                     'sort': [
                         "_score", {
                             "_geo_distance": {
                                 "location.location": {
                                     "lat": 13.7565591,
                                     "lon": 100.7966439
                                 },
                                 "order": "asc",
                                 "unit": "km",
                                 "mode": "min",
                                 "distance_type": "arc"
                             }
                         }
                     ],
                     'query': {
                         'function_score': {
                             'query': {
                                 'bool': {
                                     'must_not': [{
                                         'match': {
                                             'allow_recommend': 'false'
                                         }
                                     }],
                                     'filter': [{
                                         'range': {
                                             'publish_date': {
                                                 'lte': 'now/h'
                                             }
                                         }
                                     }, {
                                         'range': {
                                             'expire_date': {
                                                 'gt': 'now/h'
                                             }
                                         }
                                     }, {
                                         'match': {
                                             'status': 'publish'
                                         }
                                     }, {
                                         "term": {
                                             "lang": "th"
                                         }
                                     }, {
                                         'exists': {
                                             'field': 'privilege_list'
                                         }
                                     }]
                                 }
                             },
                             "functions": [{
                                 "filter": {
                                     "geo_distance": {
                                         "distance": "9km",
                                         "location.location": {
                                             "lat": 13.7565591,
                                             "lon": 100.7966439
                                         }
                                     }
                                 },
                                 "weight": 50
                             }],
                             "score_mode": "sum",
                             "boost_mode": "sum"
                         }
                     }
                 },
                 index=f'{ste.RCOM_ES_BRANCH_INDEX}',
                 size=10)
        ]

        session_search_actual = session.search.call_args_list

        self.assertCountEqual(actual, expected)
        self.assertListEqual(actual, expected)

        self.assertCountEqual(session_search_actual, session_search_expected)
        self.assertEqual(session_search_actual, session_search_expected)

    def test_get_geo_location_filtered_items_branch_change_size(self):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        privilege_utils.get_geo_location_filtered_items(session,
                                                        type="branch",
                                                        lat=13.7565591,
                                                        lon=100.7966439,
                                                        radius=9,
                                                        size=9)

        session_search_expected = [
            call(_source=['id', 'original_id', 'privilege_list', 'master_id'],
                 body={
                     'sort': [
                         "_score", {
                             "_geo_distance": {
                                 "location.location": {
                                     "lat": 13.7565591,
                                     "lon": 100.7966439
                                 },
                                 "order": "asc",
                                 "unit": "km",
                                 "mode": "min",
                                 "distance_type": "arc"
                             }
                         }
                     ],
                     'query': {
                         'function_score': {
                             'query': {
                                 'bool': {
                                     'must_not': [{
                                         'match': {
                                             'allow_recommend': 'false'
                                         }
                                     }],
                                     'filter': [{
                                         'range': {
                                             'publish_date': {
                                                 'lte': 'now/h'
                                             }
                                         }
                                     }, {
                                         'range': {
                                             'expire_date': {
                                                 'gt': 'now/h'
                                             }
                                         }
                                     }, {
                                         'match': {
                                             'status': 'publish'
                                         }
                                     }, {
                                         "term": {
                                             "lang": "th"
                                         }
                                     }, {
                                         'exists': {
                                             'field': 'privilege_list'
                                         }
                                     }]
                                 }
                             },
                             "functions": [{
                                 "filter": {
                                     "geo_distance": {
                                         "distance": "9km",
                                         "location.location": {
                                             "lat": 13.7565591,
                                             "lon": 100.7966439
                                         }
                                     }
                                 },
                                 "weight": 50
                             }],
                             "score_mode": "sum",
                             "boost_mode": "sum"
                         }
                     }
                 },
                 index=f'{ste.RCOM_ES_BRANCH_INDEX}',
                 size=9)
        ]

        session_search_actual = session.search.call_args_list

        self.assertCountEqual(session_search_actual, session_search_expected)
        self.assertEqual(session_search_actual, session_search_expected)

    def test_get_geo_location_filtered_items_branch_change_lat(self):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        privilege_utils.get_geo_location_filtered_items(session,
                                                        type="branch",
                                                        lat=13.77777,
                                                        lon=100.7966439,
                                                        radius=9,
                                                        size=9)

        session_search_expected = [
            call(_source=['id', 'original_id', 'privilege_list', 'master_id'],
                 body={
                     'sort': [
                         "_score", {
                             "_geo_distance": {
                                 "location.location": {
                                     "lat": 13.77777,
                                     "lon": 100.7966439
                                 },
                                 "order": "asc",
                                 "unit": "km",
                                 "mode": "min",
                                 "distance_type": "arc"
                             }
                         }
                     ],
                     'query': {
                         'function_score': {
                             'query': {
                                 'bool': {
                                     'must_not': [{
                                         'match': {
                                             'allow_recommend': 'false'
                                         }
                                     }],
                                     'filter': [{
                                         'range': {
                                             'publish_date': {
                                                 'lte': 'now/h'
                                             }
                                         }
                                     }, {
                                         'range': {
                                             'expire_date': {
                                                 'gt': 'now/h'
                                             }
                                         }
                                     }, {
                                         'match': {
                                             'status': 'publish'
                                         }
                                     }, {
                                         "term": {
                                             "lang": "th"
                                         }
                                     }, {
                                         'exists': {
                                             'field': 'privilege_list'
                                         }
                                     }]
                                 }
                             },
                             "functions": [{
                                 "filter": {
                                     "geo_distance": {
                                         "distance": "9km",
                                         "location.location": {
                                             "lat": 13.77777,
                                             "lon": 100.7966439
                                         }
                                     }
                                 },
                                 "weight": 50
                             }],
                             "score_mode": "sum",
                             "boost_mode": "sum"
                         }
                     }
                 },
                 index=f'{ste.RCOM_ES_BRANCH_INDEX}',
                 size=9)
        ]
        session_search_actual = session.search.call_args_list

        self.assertCountEqual(session_search_actual, session_search_expected)
        self.assertEqual(session_search_actual, session_search_expected)

    def test_get_geo_location_filtered_items_branch_change_lon(self):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        privilege_utils.get_geo_location_filtered_items(session,
                                                        type="branch",
                                                        lat=13.7565591,
                                                        lon=100.77777,
                                                        radius=9,
                                                        size=9)

        session_search_expected = [
            call(_source=['id', 'original_id', 'privilege_list', 'master_id'],
                 body={
                     'sort': [
                         "_score", {
                             "_geo_distance": {
                                 "location.location": {
                                     "lat": 13.7565591,
                                     "lon": 100.77777
                                 },
                                 "order": "asc",
                                 "unit": "km",
                                 "mode": "min",
                                 "distance_type": "arc"
                             }
                         }
                     ],
                     'query': {
                         'function_score': {
                             'query': {
                                 'bool': {
                                     'must_not': [{
                                         'match': {
                                             'allow_recommend': 'false'
                                         }
                                     }],
                                     'filter': [{
                                         'range': {
                                             'publish_date': {
                                                 'lte': 'now/h'
                                             }
                                         }
                                     }, {
                                         'range': {
                                             'expire_date': {
                                                 'gt': 'now/h'
                                             }
                                         }
                                     }, {
                                         'match': {
                                             'status': 'publish'
                                         }
                                     }, {
                                         "term": {
                                             "lang": "th"
                                         }
                                     }, {
                                         'exists': {
                                             'field': 'privilege_list'
                                         }
                                     }]
                                 }
                             },
                             "functions": [{
                                 "filter": {
                                     "geo_distance": {
                                         "distance": "9km",
                                         "location.location": {
                                             "lat": 13.7565591,
                                             "lon": 100.77777
                                         }
                                     }
                                 },
                                 "weight": 50
                             }],
                             "score_mode": "sum",
                             "boost_mode": "sum"
                         }
                     }
                 },
                 index=f'{ste.RCOM_ES_BRANCH_INDEX}',
                 size=9)
        ]
        session_search_actual = session.search.call_args_list

        self.assertCountEqual(session_search_actual, session_search_expected)
        self.assertEqual(session_search_actual, session_search_expected)

    def test_get_geo_location_filtered_items_branch_change_radius(self):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        privilege_utils.get_geo_location_filtered_items(session,
                                                        type="branch",
                                                        lat=13.7565591,
                                                        lon=100.7966439,
                                                        radius=50,
                                                        size=9)

        session_search_expected = [
            call(_source=['id', 'original_id', 'privilege_list', 'master_id'],
                 body={
                     'sort': [
                         "_score", {
                             "_geo_distance": {
                                 "location.location": {
                                     "lat": 13.7565591,
                                     "lon": 100.7966439
                                 },
                                 "order": "asc",
                                 "unit": "km",
                                 "mode": "min",
                                 "distance_type": "arc"
                             }
                         }
                     ],
                     'query': {
                         'function_score': {
                             'query': {
                                 'bool': {
                                     'must_not': [{
                                         'match': {
                                             'allow_recommend': 'false'
                                         }
                                     }],
                                     'filter': [{
                                         'range': {
                                             'publish_date': {
                                                 'lte': 'now/h'
                                             }
                                         }
                                     }, {
                                         'range': {
                                             'expire_date': {
                                                 'gt': 'now/h'
                                             }
                                         }
                                     }, {
                                         'match': {
                                             'status': 'publish'
                                         }
                                     }, {
                                         "term": {
                                             "lang": "th"
                                         }
                                     }, {
                                         'exists': {
                                             'field': 'privilege_list'
                                         }
                                     }]
                                 }
                             },
                             "functions": [{
                                 "filter": {
                                     "geo_distance": {
                                         "distance": "50km",
                                         "location.location": {
                                             "lat": 13.7565591,
                                             "lon": 100.7966439
                                         }
                                     }
                                 },
                                 "weight": 50
                             }],
                             "score_mode": "sum",
                             "boost_mode": "sum"
                         }
                     }
                 },
                 index=f'{ste.RCOM_ES_BRANCH_INDEX}',
                 size=9)
        ]
        session_search_actual = session.search.call_args_list

        self.assertCountEqual(session_search_actual, session_search_expected)
        self.assertEqual(session_search_actual, session_search_expected)

    def test_get_geo_location_filtered_items_branch_set_geo_weight(self):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        privilege_utils.get_geo_location_filtered_items(session,
                                                        type="branch",
                                                        lat=13.7565591,
                                                        lon=100.7966439,
                                                        radius=50,
                                                        size=9,
                                                        geo_weight=100)

        session_search_expected = [
            call(_source=['id', 'original_id', 'privilege_list', 'master_id'],
                 body={
                     'sort': [
                         "_score", {
                             "_geo_distance": {
                                 "location.location": {
                                     "lat": 13.7565591,
                                     "lon": 100.7966439
                                 },
                                 "order": "asc",
                                 "unit": "km",
                                 "mode": "min",
                                 "distance_type": "arc"
                             }
                         }
                     ],
                     'query': {
                         'function_score': {
                             'query': {
                                 'bool': {
                                     'must_not': [{
                                         'match': {
                                             'allow_recommend': 'false'
                                         }
                                     }],
                                     'filter': [{
                                         'range': {
                                             'publish_date': {
                                                 'lte': 'now/h'
                                             }
                                         }
                                     }, {
                                         'range': {
                                             'expire_date': {
                                                 'gt': 'now/h'
                                             }
                                         }
                                     }, {
                                         'match': {
                                             'status': 'publish'
                                         }
                                     }, {
                                         "term": {
                                             "lang": "th"
                                         }
                                     }, {
                                         'exists': {
                                             'field': 'privilege_list'
                                         }
                                     }]
                                 }
                             },
                             "functions": [{
                                 "filter": {
                                     "geo_distance": {
                                         "distance": "50km",
                                         "location.location": {
                                             "lat": 13.7565591,
                                             "lon": 100.7966439
                                         }
                                     }
                                 },
                                 "weight": 100
                             }],
                             "score_mode": "sum",
                             "boost_mode": "sum"
                         }
                     }
                 },
                 index=f'{ste.RCOM_ES_BRANCH_INDEX}',
                 size=9)
        ]
        session_search_actual = session.search.call_args_list

        self.assertCountEqual(session_search_actual, session_search_expected)
        self.assertEqual(session_search_actual, session_search_expected)

    def test_get_geo_location_filtered_items_branch_set_score_mode(self):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        privilege_utils.get_geo_location_filtered_items(session,
                                                        type="branch",
                                                        lat=13.7565591,
                                                        lon=100.7966439,
                                                        radius=50,
                                                        size=9,
                                                        score_mode="avg")

        session_search_expected = [
            call(_source=['id', 'original_id', 'privilege_list', 'master_id'],
                 body={
                     'sort': [
                         "_score", {
                             "_geo_distance": {
                                 "location.location": {
                                     "lat": 13.7565591,
                                     "lon": 100.7966439
                                 },
                                 "order": "asc",
                                 "unit": "km",
                                 "mode": "min",
                                 "distance_type": "arc"
                             }
                         }
                     ],
                     'query': {
                         'function_score': {
                             'query': {
                                 'bool': {
                                     'must_not': [{
                                         'match': {
                                             'allow_recommend': 'false'
                                         }
                                     }],
                                     'filter': [{
                                         'range': {
                                             'publish_date': {
                                                 'lte': 'now/h'
                                             }
                                         }
                                     }, {
                                         'range': {
                                             'expire_date': {
                                                 'gt': 'now/h'
                                             }
                                         }
                                     }, {
                                         'match': {
                                             'status': 'publish'
                                         }
                                     }, {
                                         "term": {
                                             "lang": "th"
                                         }
                                     }, {
                                         'exists': {
                                             'field': 'privilege_list'
                                         }
                                     }]
                                 }
                             },
                             "functions": [{
                                 "filter": {
                                     "geo_distance": {
                                         "distance": "50km",
                                         "location.location": {
                                             "lat": 13.7565591,
                                             "lon": 100.7966439
                                         }
                                     }
                                 },
                                 "weight": 50
                             }],
                             "score_mode": "avg",
                             "boost_mode": "sum"
                         }
                     }
                 },
                 index=f'{ste.RCOM_ES_BRANCH_INDEX}',
                 size=9)
        ]
        session_search_actual = session.search.call_args_list

        self.assertCountEqual(session_search_actual, session_search_expected)
        self.assertEqual(session_search_actual, session_search_expected)

    def test_get_geo_location_filtered_items_branch_set_boost_mode(self):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        privilege_utils.get_geo_location_filtered_items(session,
                                                        type="branch",
                                                        lat=13.7565591,
                                                        lon=100.7966439,
                                                        radius=50,
                                                        size=9,
                                                        boost_mode="avg")

        session_search_expected = [
            call(_source=['id', 'original_id', 'privilege_list', 'master_id'],
                 body={
                     'sort': [
                         "_score", {
                             "_geo_distance": {
                                 "location.location": {
                                     "lat": 13.7565591,
                                     "lon": 100.7966439
                                 },
                                 "order": "asc",
                                 "unit": "km",
                                 "mode": "min",
                                 "distance_type": "arc"
                             }
                         }
                     ],
                     'query': {
                         'function_score': {
                             'query': {
                                 'bool': {
                                     'must_not': [{
                                         'match': {
                                             'allow_recommend': 'false'
                                         }
                                     }],
                                     'filter': [{
                                         'range': {
                                             'publish_date': {
                                                 'lte': 'now/h'
                                             }
                                         }
                                     }, {
                                         'range': {
                                             'expire_date': {
                                                 'gt': 'now/h'
                                             }
                                         }
                                     }, {
                                         'match': {
                                             'status': 'publish'
                                         }
                                     }, {
                                         "term": {
                                             "lang": "th"
                                         }
                                     }, {
                                         'exists': {
                                             'field': 'privilege_list'
                                         }
                                     }]
                                 }
                             },
                             "functions": [{
                                 "filter": {
                                     "geo_distance": {
                                         "distance": "50km",
                                         "location.location": {
                                             "lat": 13.7565591,
                                             "lon": 100.7966439
                                         }
                                     }
                                 },
                                 "weight": 50
                             }],
                             "score_mode": "sum",
                             "boost_mode": "avg"
                         }
                     }
                 },
                 index=f'{ste.RCOM_ES_BRANCH_INDEX}',
                 size=9)
        ]
        session_search_actual = session.search.call_args_list

        self.assertCountEqual(session_search_actual, session_search_expected)
        self.assertEqual(session_search_actual, session_search_expected)

    def test_get_geo_location_filtered_shop(self):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        expected = ["7wzZ0e0KjQ0", "y4YQgglRpkB4"]
        actual = privilege_utils.get_geo_location_filtered_items(
            session, type='shop', lat=13.7565591, lon=100.7966439, radius=9)

        session_search_expected = [
            call(_source=['id', 'original_id', 'privilege_list', 'master_id'],
                 body={
                     'sort': [
                         "_score", {
                             "_geo_distance": {
                                 "location.location": {
                                     "lat": 13.7565591,
                                     "lon": 100.7966439
                                 },
                                 "order": "asc",
                                 "unit": "km",
                                 "mode": "min",
                                 "distance_type": "arc"
                             }
                         }
                     ],
                     'query': {
                         'function_score': {
                             'query': {
                                 'bool': {
                                     'must_not': [{
                                         'match': {
                                             'allow_recommend': 'false'
                                         }
                                     }],
                                     'filter': [{
                                         'range': {
                                             'publish_date': {
                                                 'lte': 'now/h'
                                             }
                                         }
                                     }, {
                                         'range': {
                                             'expire_date': {
                                                 'gt': 'now/h'
                                             }
                                         }
                                     }, {
                                         'match': {
                                             'status': 'publish'
                                         }
                                     }, {
                                         "term": {
                                             "lang": "th"
                                         }
                                     }, {
                                         'exists': {
                                             'field': 'privilege_list'
                                         }
                                     }, {
                                         'exists': {
                                             'field': 'master_id'
                                         }
                                     }]
                                 }
                             },
                             "functions": [{
                                 "filter": {
                                     "geo_distance": {
                                         "distance": "9km",
                                         "location.location": {
                                             "lat": 13.7565591,
                                             "lon": 100.7966439
                                         }
                                     }
                                 },
                                 "weight": 50
                             }],
                             "score_mode": "sum",
                             "boost_mode": "sum"
                         }
                     }
                 },
                 index=f'{ste.RCOM_ES_BRANCH_INDEX}',
                 size=10)
        ]

        session_search_actual = session.search.call_args_list

        self.assertCountEqual(actual, expected)
        self.assertListEqual(sorted(actual), sorted(expected))

        self.assertCountEqual(session_search_actual, session_search_expected)
        self.assertEqual(session_search_actual, session_search_expected)

    def test_get_geo_location_filtered_shop_set_geo_weight(self):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        expected = ["7wzZ0e0KjQ0", "y4YQgglRpkB4"]
        actual = privilege_utils.get_geo_location_filtered_items(
            session,
            type='shop',
            lat=13.7565591,
            lon=100.7966439,
            radius=9,
            geo_weight=60)

        session_search_expected = [
            call(_source=['id', 'original_id', 'privilege_list', 'master_id'],
                 body={
                     'sort': [
                         "_score", {
                             "_geo_distance": {
                                 "location.location": {
                                     "lat": 13.7565591,
                                     "lon": 100.7966439
                                 },
                                 "order": "asc",
                                 "unit": "km",
                                 "mode": "min",
                                 "distance_type": "arc"
                             }
                         }
                     ],
                     'query': {
                         'function_score': {
                             'query': {
                                 'bool': {
                                     'must_not': [{
                                         'match': {
                                             'allow_recommend': 'false'
                                         }
                                     }],
                                     'filter': [{
                                         'range': {
                                             'publish_date': {
                                                 'lte': 'now/h'
                                             }
                                         }
                                     }, {
                                         'range': {
                                             'expire_date': {
                                                 'gt': 'now/h'
                                             }
                                         }
                                     }, {
                                         'match': {
                                             'status': 'publish'
                                         }
                                     }, {
                                         "term": {
                                             "lang": "th"
                                         }
                                     }, {
                                         'exists': {
                                             'field': 'privilege_list'
                                         }
                                     }, {
                                         'exists': {
                                             'field': 'master_id'
                                         }
                                     }]
                                 }
                             },
                             "functions": [{
                                 "filter": {
                                     "geo_distance": {
                                         "distance": "9km",
                                         "location.location": {
                                             "lat": 13.7565591,
                                             "lon": 100.7966439
                                         }
                                     }
                                 },
                                 "weight": 60
                             }],
                             "score_mode": "sum",
                             "boost_mode": "sum"
                         }
                     }
                 },
                 index=f'{ste.RCOM_ES_BRANCH_INDEX}',
                 size=10)
        ]

        session_search_actual = session.search.call_args_list

        self.assertCountEqual(actual, expected)
        self.assertListEqual(sorted(actual), sorted(expected))

        self.assertCountEqual(session_search_actual, session_search_expected)
        self.assertEqual(session_search_actual, session_search_expected)

    def test_get_geo_location_filtered_shop_set_score_mode(self):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        expected = ["7wzZ0e0KjQ0", "y4YQgglRpkB4"]
        actual = privilege_utils.get_geo_location_filtered_items(
            session,
            type='shop',
            lat=13.7565591,
            lon=100.7966439,
            radius=9,
            score_mode="avg")

        session_search_expected = [
            call(_source=['id', 'original_id', 'privilege_list', 'master_id'],
                 body={
                     'sort': [
                         "_score", {
                             "_geo_distance": {
                                 "location.location": {
                                     "lat": 13.7565591,
                                     "lon": 100.7966439
                                 },
                                 "order": "asc",
                                 "unit": "km",
                                 "mode": "min",
                                 "distance_type": "arc"
                             }
                         }
                     ],
                     'query': {
                         'function_score': {
                             'query': {
                                 'bool': {
                                     'must_not': [{
                                         'match': {
                                             'allow_recommend': 'false'
                                         }
                                     }],
                                     'filter': [{
                                         'range': {
                                             'publish_date': {
                                                 'lte': 'now/h'
                                             }
                                         }
                                     }, {
                                         'range': {
                                             'expire_date': {
                                                 'gt': 'now/h'
                                             }
                                         }
                                     }, {
                                         'match': {
                                             'status': 'publish'
                                         }
                                     }, {
                                         "term": {
                                             "lang": "th"
                                         }
                                     }, {
                                         'exists': {
                                             'field': 'privilege_list'
                                         }
                                     }, {
                                         'exists': {
                                             'field': 'master_id'
                                         }
                                     }]
                                 }
                             },
                             "functions": [{
                                 "filter": {
                                     "geo_distance": {
                                         "distance": "9km",
                                         "location.location": {
                                             "lat": 13.7565591,
                                             "lon": 100.7966439
                                         }
                                     }
                                 },
                                 "weight": 50
                             }],
                             "score_mode": "avg",
                             "boost_mode": "sum"
                         }
                     }
                 },
                 index=f'{ste.RCOM_ES_BRANCH_INDEX}',
                 size=10)
        ]

        session_search_actual = session.search.call_args_list

        self.assertCountEqual(actual, expected)
        self.assertListEqual(sorted(actual), sorted(expected))

        self.assertCountEqual(session_search_actual, session_search_expected)
        self.assertEqual(session_search_actual, session_search_expected)

    def test_get_geo_location_filtered_shop_set_boost_mode(self):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        expected = ["7wzZ0e0KjQ0", "y4YQgglRpkB4"]
        actual = privilege_utils.get_geo_location_filtered_items(
            session,
            type='shop',
            lat=13.7565591,
            lon=100.7966439,
            radius=9,
            boost_mode="avg")

        session_search_expected = [
            call(_source=['id', 'original_id', 'privilege_list', 'master_id'],
                 body={
                     'sort': [
                         "_score", {
                             "_geo_distance": {
                                 "location.location": {
                                     "lat": 13.7565591,
                                     "lon": 100.7966439
                                 },
                                 "order": "asc",
                                 "unit": "km",
                                 "mode": "min",
                                 "distance_type": "arc"
                             }
                         }
                     ],
                     'query': {
                         'function_score': {
                             'query': {
                                 'bool': {
                                     'must_not': [{
                                         'match': {
                                             'allow_recommend': 'false'
                                         }
                                     }],
                                     'filter': [{
                                         'range': {
                                             'publish_date': {
                                                 'lte': 'now/h'
                                             }
                                         }
                                     }, {
                                         'range': {
                                             'expire_date': {
                                                 'gt': 'now/h'
                                             }
                                         }
                                     }, {
                                         'match': {
                                             'status': 'publish'
                                         }
                                     }, {
                                         "term": {
                                             "lang": "th"
                                         }
                                     }, {
                                         'exists': {
                                             'field': 'privilege_list'
                                         }
                                     }, {
                                         'exists': {
                                             'field': 'master_id'
                                         }
                                     }]
                                 }
                             },
                             "functions": [{
                                 "filter": {
                                     "geo_distance": {
                                         "distance": "9km",
                                         "location.location": {
                                             "lat": 13.7565591,
                                             "lon": 100.7966439
                                         }
                                     }
                                 },
                                 "weight": 50
                             }],
                             "score_mode": "sum",
                             "boost_mode": "avg"
                         }
                     }
                 },
                 index=f'{ste.RCOM_ES_BRANCH_INDEX}',
                 size=10)
        ]

        session_search_actual = session.search.call_args_list

        self.assertCountEqual(actual, expected)
        self.assertListEqual(sorted(actual), sorted(expected))

        self.assertCountEqual(session_search_actual, session_search_expected)
        self.assertEqual(session_search_actual, session_search_expected)

    def test_get_geo_location_filtered_shop_randomized(self):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        expected = ["y4YQgglRpkB4", "7wzZ0e0KjQ0"]
        actual = privilege_utils.get_geo_location_filtered_items(
            session,
            type='shop',
            lat=13.7565591,
            lon=100.7966439,
            radius=9,
            randomized=True,
            seed=10000)

        session_search_expected = [
            call(_source=['id', 'original_id', 'privilege_list', 'master_id'],
                 body={
                     'sort': [
                         "_score", {
                             "_geo_distance": {
                                 "location.location": {
                                     "lat": 13.7565591,
                                     "lon": 100.7966439
                                 },
                                 "order": "asc",
                                 "unit": "km",
                                 "mode": "min",
                                 "distance_type": "arc"
                             }
                         }
                     ],
                     'query': {
                         'function_score': {
                             'query': {
                                 'bool': {
                                     'must_not': [{
                                         'match': {
                                             'allow_recommend': 'false'
                                         }
                                     }],
                                     'filter': [{
                                         'range': {
                                             'publish_date': {
                                                 'lte': 'now/h'
                                             }
                                         }
                                     }, {
                                         'range': {
                                             'expire_date': {
                                                 'gt': 'now/h'
                                             }
                                         }
                                     }, {
                                         'match': {
                                             'status': 'publish'
                                         }
                                     }, {
                                         "term": {
                                             "lang": "th"
                                         }
                                     }, {
                                         'exists': {
                                             'field': 'privilege_list'
                                         }
                                     }, {
                                         'exists': {
                                             'field': 'master_id'
                                         }
                                     }]
                                 }
                             },
                             "functions": [{
                                 "filter": {
                                     "geo_distance": {
                                         "distance": "9km",
                                         "location.location": {
                                             "lat": 13.7565591,
                                             "lon": 100.7966439
                                         }
                                     }
                                 },
                                 "weight": 50,
                                 "random_score": {
                                     "seed": "10000"
                                 }
                             }],
                             "score_mode": "sum",
                             "boost_mode": "sum"
                         }
                     }
                 },
                 index=f'{ste.RCOM_ES_BRANCH_INDEX}',
                 size=10)
        ]

        session_search_actual = session.search.call_args_list

        self.assertListEqual(sorted(actual), sorted(expected))
        self.assertCountEqual(session_search_actual, session_search_expected)
        self.assertEqual(session_search_actual, session_search_expected)

    def test_get_geo_location_filtered_privilege(self):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        expected = [
            'DAKPYYJYwQ3O', '8Dzg44KGN5jO', 'GZoVaaADQ8W9', 'Gv5ozzxW18wO',
            'aYOg11qPnl2Y', 'VZeqLLDkwrVZ'
        ]
        actual = privilege_utils.get_geo_location_filtered_items(
            session,
            type='privilege',
            lat=13.7565591,
            lon=100.7966439,
            radius=9)

        self.assertCountEqual(actual, expected)

    def test_get_geo_location_filtered_privilege_randomized(self):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        expected = [
            'DAKPYYJYwQ3O', '8Dzg44KGN5jO', 'GZoVaaADQ8W9', 'Gv5ozzxW18wO',
            'aYOg11qPnl2Y', 'VZeqLLDkwrVZ'
        ]
        actual1 = privilege_utils.get_geo_location_filtered_items(
            session,
            type='privilege',
            lat=13.7565591,
            lon=100.7966439,
            radius=9,
            randomized=True)
        actual2 = privilege_utils.get_geo_location_filtered_items(
            session,
            type='privilege',
            lat=13.7565591,
            lon=100.7966439,
            radius=9,
            randomized=True)

        self.assertFalse(actual1 == actual2)
        self.assertEqual(set(expected), set(actual1))
        self.assertEqual(set(expected), set(actual2))

    def test_get_branches_randomized(self):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        privilege_utils.get_geo_location_filtered_items(session,
                                                        type='branch',
                                                        randomized=True,
                                                        seed=10000)
        dsl = {
            'sort': ['_score'],
            'query': {
                'function_score': {
                    'query': {
                        'bool': {
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }],
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                'exists': {
                                    'field': 'privilege_list'
                                }
                            }]
                        }
                    },
                    'random_score': {
                        'seed': '10000'
                    }
                }
            }
        }

        session_search_actual = session.search.call_args_list

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

    def test_get_shop_randomized(self):
        session = MagicMock()
        session.search.return_value = rsc.es_merchant

        privilege_utils.get_geo_location_filtered_items(session,
                                                        type='shop',
                                                        randomized=True,
                                                        seed=10000)
        dsl = {
            'query': {
                'function_score': {
                    'query': {
                        'bool': {
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                'exists': {
                                    'field': 'privilege_list'
                                }
                            }],
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }]
                        }
                    },
                    'random_score': {
                        'seed': '10000'
                    }
                }
            },
            'sort': ["_score"]
        }

        session_search_actual = session.search.call_args_list

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

    def test_get_geo_location_filtered_privilege_shortened_result(self):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location
        actual = privilege_utils.get_geo_location_filtered_items(
            session,
            type='privilege',
            lat=13.7565591,
            lon=100.7966439,
            radius=9,
            size=3)

        self.assertEqual(len(actual), 3)

    def test_get_return_privilege_fields(self):
        expected = ['id', 'original_id', 'privilege_list', 'master_id']
        actual = privilege_utils._get_return_privilege_fields()
        self.assertEqual(expected, actual)

    def test_get_return_fields_priv(self):
        expected = [
            'id', 'publish_date', 'original_id', 'tags', 'title', 'detail',
            'article_category', 'HIT_COUNT_WEEK', 'HIT_COUNT_MONTH',
            'HIT_COUNT_YEAR', 'content_type', 'expire_date', 'setting', 'ussd',
            'campaign_code', 'term_and_condition', 'thumb', 'thumb_list', 'allow_app',
            'HIT_COUNT_DAY_1', 'HIT_COUNT_DAY_7', 'HIT_COUNT_DAY_14', 'HIT_COUNT_DAY_30',
            'card_type'
        ]
        actual = privilege_utils._get_return_fields(
            ste.RCOM_ES_PRIVILEGE_PRIVILEGE)
        self.assertEqual(expected, actual)

    def test_get_return_fields_shop(self):
        expected = [
            'id', 'publish_date', 'original_id', 'tags', 'title', 'detail',
            'article_category', 'HIT_COUNT_WEEK', 'HIT_COUNT_MONTH',
            'HIT_COUNT_YEAR', 'content_type', 'expire_date', 'setting', 'address',
            'thumb_list', 'privilege_list', 'discover', 'merchant_type','allow_app',
            'HIT_COUNT_DAY_1', 'HIT_COUNT_DAY_7', 'HIT_COUNT_DAY_14', 'HIT_COUNT_DAY_30',
            'card_type'
        ]
        actual = privilege_utils._get_return_fields(ste.RCOM_ES_PRIVILEGE_SHOP)
        self.assertEqual(expected, actual)

    @ut.mock.patch('rcommsutils.cms.privilege_utils._validate_type')
    def test_get_geo_location_filtered_call_validate_type(
            self, mock_validate_type):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        privilege_utils.get_geo_location_filtered_items(session,
                                                        type='privilege',
                                                        lat=13.7565591,
                                                        lon=100.7966439,
                                                        radius=9)
        expected = [call('privilege')]
        actual = mock_validate_type.call_args_list

        mock_validate_type.assert_called_once_with('privilege')

        self.assertCountEqual(expected, actual)
        self.assertEqual(expected, actual)

    @ut.mock.patch(
        'rcommsutils.cms.privilege_utils._get_default_must_conditions')
    def test_get_geo_location_filtered_call_get_default_must_conditions(
            self, mock_must_conditions):
        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        privilege_utils.get_geo_location_filtered_items(session,
                                                        type='privilege',
                                                        lat=13.7565591,
                                                        lon=100.7966439,
                                                        radius=9,
                                                        language='TH')

        mock_must_conditions.assert_called_once_with('TH')

    @ut.mock.patch(
        'rcommsutils.cms.privilege_utils._get_return_privilege_fields')
    def test_get_geo_location_filtered_call_get_return_privilege_fields(
            self, mock_get_return_privilege_fields):

        session = MagicMock()
        session.search.return_value = rsc.branches_geo_location

        privilege_utils.get_geo_location_filtered_items(session,
                                                        type='privilege',
                                                        lat=13.7565591,
                                                        lon=100.7966439,
                                                        radius=9,
                                                        language='en')

        self.assertEqual(mock_get_return_privilege_fields.call_count, 1)

    def test_get_default_must_conditions(self):
        language = "TH"
        expected = [
            {
                "range": {
                    Fields.PUBLISH_DATE: {
                        "lte": "now/h"
                    }
                }
            },
            {
                "range": {
                    Fields.EXPIRE_DATE: {
                        "gt": "now/h"
                    }
                }
            },
            {
                "match": {
                    Fields.STATUS: "publish"
                }
            },
            {
                "term": {
                    "lang": "th"
                }
            },
        ]

        actual = privilege_utils._get_default_must_conditions(language)
        self.assertEqual(expected, actual)

    def test_get_items_shop_not_quota_over_existed(self):
        session = MagicMock()
        ids = ['NO_THUMBLIST_TEST_SHOP', 'EXPIRED_TEST_SHOP']
        dsl = {
            'sort': {
                'HIT_COUNT_WEEK': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'must': [{
                                'terms': {
                                    '_id': [
                                        'th-NO_THUMBLIST_TEST_SHOP',
                                        'th-EXPIRED_TEST_SHOP'
                                    ]
                                }
                            }],
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }],
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                "term": {
                                    "searchable": "Y"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must_not': [{
                                                'term': {
                                                    'denormalize_privilege.quota_over_existed.keyword':
                                                        1
                                                }
                                            }]
                                        }
                                    }
                                }
                            }]
                        }
                    }
                }
            }
        }

        search_result = {
            "hits": {
                "hits": [{
                    "_source": rsc.shops['NO_THUMBLIST_TEST_SHOP']
                }]
            }
        }

        expected = [rsc.shops['NO_THUMBLIST_TEST_SHOP']]

        session.search.return_value = search_result

        actuals = privilege_utils.get_items(session,
                                            type=ste.RCOM_ES_PRIVILEGE_SHOP,
                                            ids=ids,
                                            language="TH")

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

        self.assert_prvbasic_equal(actuals,
                                   expected,
                                   type=ste.RCOM_ES_PRIVILEGE_SHOP)

    def test_get_items_shop_campaign_type_point(self):
        session = MagicMock()
        ids = ['TEST_SHOP_1', 'TEST_SHOP_2']
        dsl = {
            "sort": {
                "HIT_COUNT_WEEK": {
                    "order": "desc"
                }
            },
            "query": {
                "constant_score": {
                    "filter": {
                        "bool": {
                            "must": [{
                                "terms": {
                                    "_id": ['th-TEST_SHOP_1', 'th-TEST_SHOP_2']
                                }
                            }],
                            "must_not": [{
                                "match": {
                                    "allow_recommend": "false"
                                }
                            }],
                            "filter": [{
                                "range": {
                                    "publish_date": {
                                        "lte": "now/h"
                                    }
                                }
                            }, {
                                "match": {
                                    "status": "publish"
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                "term": {
                                    "searchable": "Y"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must_not': [{
                                                'term': {
                                                    'denormalize_privilege.quota_over_existed.keyword':
                                                        1
                                                }
                                            }]
                                        }
                                    }
                                }
                            }, {
                                "nested": {
                                    "path": "denormalize_privilege",
                                    "query": {
                                        "bool": {
                                            "must": [{
                                                "term": {
                                                    "denormalize_privilege.campaign_type":
                                                        'point'
                                                }
                                            }]
                                        }
                                    }
                                }
                            }]
                        }
                    }
                }
            }
        }

        search_result = {
            "hits": {
                "hits": [{
                    "_source": rsc.shops['TEST_SHOP_1']
                }]
            }
        }

        expected = [rsc.shops['TEST_SHOP_1']]

        session.search.return_value = search_result

        actuals = privilege_utils.get_items(session,
                                            type=ste.RCOM_ES_PRIVILEGE_SHOP,
                                            ids=ids,
                                            campaign_type="point",
                                            language="TH")

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

        self.assert_prvbasic_equal(actuals,
                                   expected,
                                   type=ste.RCOM_ES_PRIVILEGE_SHOP)

    def test_get_items_shop_campaign_type_privilege_1(self):
        session = MagicMock()
        ids = ['TEST_SHOP_1', 'TEST_SHOP_2']
        dsl = {
            'sort': {
                'HIT_COUNT_WEEK': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'must': [{
                                'terms': {
                                    '_id': ['th-TEST_SHOP_1', 'th-TEST_SHOP_2']
                                }
                            }],
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }],
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                "term": {
                                    "searchable": "Y"
                                }
                            },{
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must': [{
                                                'term': {
                                                    'denormalize_privilege.campaign_type':
                                                        'privilege'
                                                }
                                            }]
                                        }
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must_not': [{
                                                'term': {
                                                    'denormalize_privilege.quota_over_existed.keyword':
                                                        1
                                                }
                                            }]
                                        }
                                    }
                                }
                            }]
                        }
                    }
                }
            }
        }

        search_result = {
            "hits": {
                "hits": [{
                    "_source": rsc.shops['TEST_SHOP_2']
                }]
            }
        }

        expected = [rsc.shops['TEST_SHOP_2']]

        session.search.return_value = search_result

        actuals = privilege_utils.get_items(session,
                                            type=ste.RCOM_ES_PRIVILEGE_SHOP,
                                            ids=ids,
                                            campaign_type="privilege",
                                            language="TH")

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

        self.assert_prvbasic_equal(actuals,
                                   expected,
                                   type=ste.RCOM_ES_PRIVILEGE_SHOP)

    def test_get_items_shop_campaign_type_privilege_2(self):
        session = MagicMock()
        ids = ['TEST_SHOP_1', 'TEST_SHOP_2']
        dsl = {
            'sort': {
                'HIT_COUNT_WEEK': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'must': [{
                                'terms': {
                                    '_id': ['th-TEST_SHOP_1', 'th-TEST_SHOP_2']
                                }
                            }],
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }],
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                "term": {
                                    "searchable": "Y"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must_not': [{
                                                'term': {
                                                    'denormalize_privilege.quota_over_existed.keyword':
                                                        1
                                                }
                                            }]
                                        }
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must': [{
                                                'term': {
                                                    'denormalize_privilege.campaign_type':
                                                        'privilege'
                                                }
                                            }]
                                        }
                                    }
                                }
                            }]
                        }
                    }
                }
            }
        }

        search_result = {
            "hits": {
                "hits": [{
                    "_source": rsc.shops['TEST_SHOP_2']
                }]
            }
        }

        expected = [rsc.shops['TEST_SHOP_2']]

        session.search.return_value = search_result

        actuals = privilege_utils.get_items(session,
                                            type=ste.RCOM_ES_PRIVILEGE_SHOP,
                                            ids=ids,
                                            campaign_type="privilege",
                                            language="TH")

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

        self.assert_prvbasic_equal(actuals,
                                   expected,
                                   type=ste.RCOM_ES_PRIVILEGE_SHOP)

    def test_get_items_shop_without_campaign_type(self):
        session = MagicMock()
        ids = ['TEST_SHOP_1', 'TEST_SHOP_2']
        dsl = {
            'sort': {
                'HIT_COUNT_WEEK': {
                    'order': 'desc'
                }
            },
            'query': {
                'constant_score': {
                    'filter': {
                        'bool': {
                            'must': [{
                                'terms': {
                                    '_id': ['th-TEST_SHOP_1', 'th-TEST_SHOP_2']
                                }
                            }],
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }],
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                "term": {
                                    "searchable": "Y"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must_not': [{
                                                'term': {
                                                    'denormalize_privilege.quota_over_existed.keyword':
                                                        1
                                                }
                                            }]
                                        }
                                    }
                                }
                            }]
                        }
                    }
                }
            }
        }

        search_result = {
            "hits": {
                "hits": [{
                    "_source": rsc.shops['TEST_SHOP_1']
                }, {
                    "_source": rsc.shops['TEST_SHOP_2']
                }]
            }
        }

        expected = [rsc.shops['TEST_SHOP_1'], rsc.shops['TEST_SHOP_2']]

        session.search.return_value = search_result

        actuals = privilege_utils.get_items(session,
                                            type=ste.RCOM_ES_PRIVILEGE_SHOP,
                                            ids=ids,
                                            language="TH")

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

        self.assert_prvbasic_equal(actuals,
                                   expected,
                                   type=ste.RCOM_ES_PRIVILEGE_SHOP)

    @patch("rcommsutils.privilege_utils._build_result_set")
    @patch("rcommsutils.privilege_utils._get_return_fields")
    def test_get_merchants_from_rcom_tags_without_geo_location_campaign_type_discover_merchant_type(
            self, mock_get, mock_build):
        session = MagicMock()
        rcom_tags = [
            '1_food-beverage-related', '2_food', '3_food-grilled', '4_yakiniku'
        ]
        campaign_type = None
        maxItems = 10
        lat = None
        lon = None
        radius = None
        dsl = {
            'query': {
                'function_score': {
                    'query': {
                        'bool': {
                            'must': [{
                                'terms': {
                                    'RCOM_TAGS': rcom_tags,
                                    'boost': 5
                                }
                            }],
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }],
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must_not': [{
                                                'term': {
                                                    'denormalize_privilege.quota_over_existed.keyword':
                                                        1
                                                }
                                            }]
                                        }
                                    }
                                }
                            }]
                        }
                    },
                    'functions': [{
                        'field_value_factor': {
                            'field': 'HIT_COUNT_WEEK',
                            'factor': 0.001,
                            'modifier': 'log1p',
                            'missing': 0
                        },
                        'weight': 15
                    }, {
                        'field_value_factor': {
                            'field': 'HIT_COUNT_MONTH',
                            'factor': 0.001,
                            'modifier': 'log1p',
                            'missing': 0
                        },
                        'weight': 10
                    }, {
                        'field_value_factor': {
                            'field': 'HIT_COUNT_YEAR',
                            'factor': 0.001,
                            'modifier': 'log1p',
                            'missing': 0
                        },
                        'weight': 5
                    }],
                    'score_mode': 'sum',
                    'boost_mode': 'sum'
                }
            }
        }
        privilege_utils.get_merchants_from_rcom_tags(
            session=session,
            rcom_tags=rcom_tags,
            language="TH",
            campaign_type=campaign_type,
            size=maxItems * 10,
            lat=lat,
            lon=lon,
            radius=radius)

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

    @patch("rcommsutils.privilege_utils._build_result_set")
    @patch("rcommsutils.privilege_utils._get_return_fields")
    def test_get_merchants_from_rcom_tags_with_geo_location_campaign_type_discover_merchant_type(
            self, mock_get, mock_build):
        session = MagicMock()
        rcom_tags = [
            '1_food-beverage-related', '2_food', '3_food-grilled', '4_yakiniku'
        ]
        campaign_type = 'privilege'
        maxItems = 10
        lat = 13.7565591
        lon = 100.7966439
        radius = 5
        discover = 'y'
        merchant_type = 'ka'
        dsl = {
            'query': {
                'function_score': {
                    'query': {
                        'bool': {
                            'must': [{
                                'terms': {
                                    'RCOM_TAGS': rcom_tags,
                                    'boost': 5
                                },
                            }],
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }],
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must_not': [{
                                                'term': {
                                                    'denormalize_privilege.quota_over_existed.keyword':
                                                        1
                                                }
                                            }]
                                        }
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must': [{
                                                'term': {
                                                    'denormalize_privilege.campaign_type':
                                                        'privilege'
                                                }
                                            }]
                                        }
                                    }
                                }
                            }]
                        }
                    },
                    'functions': [{
                        'field_value_factor': {
                            'field': 'HIT_COUNT_WEEK',
                            'factor': 0.001,
                            'modifier': 'log1p',
                            'missing': 0
                        },
                        'weight': 15
                    }, {
                        'field_value_factor': {
                            'field': 'HIT_COUNT_MONTH',
                            'factor': 0.001,
                            'modifier': 'log1p',
                            'missing': 0
                        },
                        'weight': 10
                    }, {
                        'field_value_factor': {
                            'field': 'HIT_COUNT_YEAR',
                            'factor': 0.001,
                            'modifier': 'log1p',
                            'missing': 0
                        },
                        'weight': 5
                    }, {
                        'filter': {
                            'nested': {
                                'path': 'denormalize_branch',
                                'query': {
                                    'geo_distance': {
                                        'distance': '5km',
                                        'denormalize_branch.location.location':
                                            {
                                                'lat': 13.7565591,
                                                'lon': 100.7966439
                                            }
                                    }
                                }
                            }
                        },
                        'weight': 40
                    }, {
                        'filter': {
                            'term': {
                                'discover': {
                                    'value': discover
                                }
                            }
                        },
                        'weight': 30
                    }, {
                        'filter': {
                            'term': {
                                'merchant_type': {
                                    'value': merchant_type
                                }
                            }
                        },
                        'weight': 0
                    }],
                    'score_mode': 'sum',
                    'boost_mode': 'sum'
                }
            }
        }
        privilege_utils.get_merchants_from_rcom_tags(
            session=session,
            rcom_tags=rcom_tags,
            language="TH",
            campaign_type=campaign_type,
            size=maxItems * 10,
            lat=lat,
            lon=lon,
            radius=radius,
            discover=discover,
            merchant_type=merchant_type)

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

    @patch("rcommsutils.privilege_utils._build_result_set")
    @patch("rcommsutils.privilege_utils._get_return_fields")
    def test_get_merchants_from_rcom_tags_with_geo_location_campaign_type_discover(
            self, mock_get, mock_build):
        session = MagicMock()
        rcom_tags = [
            '1_food-beverage-related', '2_food', '3_food-grilled', '4_yakiniku'
        ]
        campaign_type = 'privilege'
        maxItems = 10
        lat = 13.7565591
        lon = 100.7966439
        radius = 5
        discover = 'y'
        dsl = {
            "query": {
                "function_score": {
                    "query": {
                        "bool": {
                            "must": [{
                                "terms": {
                                    "RCOM_TAGS": rcom_tags,
                                    "boost": 5
                                }
                            }],
                            "must_not": [{
                                "match": {
                                    "allow_recommend": "false"
                                }
                            }],
                            "filter": [{
                                "range": {
                                    "publish_date": {
                                        "lte": "now/h"
                                    }
                                }
                            }, {
                                "match": {
                                    "status": "publish"
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                "range": {
                                    "expire_date": {
                                        "gt": "now/h"
                                    }
                                }
                            }, {
                                "nested": {
                                    "path": "denormalize_privilege",
                                    "query": {
                                        "bool": {
                                            "must_not": [{
                                                "term": {
                                                    "denormalize_privilege.quota_over_existed.keyword":
                                                        1
                                                }
                                            }]
                                        }
                                    }
                                }
                            }, {
                                "nested": {
                                    "path": "denormalize_privilege",
                                    "query": {
                                        "bool": {
                                            "must": [{
                                                "term": {
                                                    "denormalize_privilege.campaign_type":
                                                        "privilege"
                                                }
                                            }]
                                        }
                                    }
                                }
                            }]
                        }
                    },
                    "functions": [{
                        "field_value_factor": {
                            "field": "HIT_COUNT_WEEK",
                            "factor": 0.001,
                            "modifier": "log1p",
                            "missing": 0
                        },
                        "weight": 15
                    }, {
                        "field_value_factor": {
                            "field": "HIT_COUNT_MONTH",
                            "factor": 0.001,
                            "modifier": "log1p",
                            "missing": 0
                        },
                        "weight": 10
                    }, {
                        "field_value_factor": {
                            "field": "HIT_COUNT_YEAR",
                            "factor": 0.001,
                            "modifier": "log1p",
                            "missing": 0
                        },
                        "weight": 5
                    }, {
                        "filter": {
                            "nested": {
                                "path": "denormalize_branch",
                                "query": {
                                    "geo_distance": {
                                        "distance": "5km",
                                        "denormalize_branch.location.location":
                                            {
                                                "lat": 13.7565591,
                                                "lon": 100.7966439
                                            }
                                    }
                                }
                            }
                        },
                        "weight": 40
                    }, {
                        "filter": {
                            "term": {
                                "discover": {
                                    "value": discover
                                }
                            }
                        },
                        "weight": 30
                    }],
                    "score_mode": "sum",
                    "boost_mode": "sum"
                }
            }
        }
        privilege_utils.get_merchants_from_rcom_tags(
            session=session,
            rcom_tags=rcom_tags,
            language="TH",
            campaign_type=campaign_type,
            size=maxItems * 10,
            lat=lat,
            lon=lon,
            radius=radius,
            discover=discover)

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

    @patch("rcommsutils.privilege_utils._build_result_set")
    @patch("rcommsutils.privilege_utils._get_return_fields")
    def test_get_merchants_from_rcom_tags_with_geo_location_campaign_type_merchant_type(
            self, mock_get, mock_build):
        session = MagicMock()
        rcom_tags = [
            '1_food-beverage-related', '2_food', '3_food-grilled', '4_yakiniku'
        ]
        campaign_type = 'privilege'
        maxItems = 10
        lat = 13.7565591
        lon = 100.7966439
        radius = 5
        merchant_type = 'ka'
        dsl = {
            'query': {
                'function_score': {
                    'query': {
                        'bool': {
                            'must': [{
                                'terms': {
                                    'RCOM_TAGS': rcom_tags,
                                    'boost': 5
                                }
                            }],
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }],
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must_not': [{
                                                'term': {
                                                    'denormalize_privilege.quota_over_existed.keyword':
                                                        1
                                                }
                                            }]
                                        }
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must': [{
                                                'term': {
                                                    'denormalize_privilege.campaign_type':
                                                        'privilege'
                                                }
                                            }]
                                        }
                                    }
                                }
                            }]
                        }
                    },
                    'functions': [{
                        'field_value_factor': {
                            'field': 'HIT_COUNT_WEEK',
                            'factor': 0.001,
                            'modifier': 'log1p',
                            'missing': 0
                        },
                        'weight': 15
                    }, {
                        'field_value_factor': {
                            'field': 'HIT_COUNT_MONTH',
                            'factor': 0.001,
                            'modifier': 'log1p',
                            'missing': 0
                        },
                        'weight': 10
                    }, {
                        'field_value_factor': {
                            'field': 'HIT_COUNT_YEAR',
                            'factor': 0.001,
                            'modifier': 'log1p',
                            'missing': 0
                        },
                        'weight': 5
                    }, {
                        'filter': {
                            'nested': {
                                'path': 'denormalize_branch',
                                'query': {
                                    'geo_distance': {
                                        'distance': '5km',
                                        'denormalize_branch.location.location':
                                            {
                                                'lat': 13.7565591,
                                                'lon': 100.7966439
                                            }
                                    }
                                }
                            }
                        },
                        'weight': 40
                    }, {
                        'filter': {
                            'term': {
                                'merchant_type': {
                                    'value': merchant_type
                                }
                            }
                        },
                        'weight': 0
                    }],
                    'score_mode': 'sum',
                    'boost_mode': 'sum'
                }
            }
        }
        privilege_utils.get_merchants_from_rcom_tags(
            session=session,
            rcom_tags=rcom_tags,
            language="TH",
            campaign_type=campaign_type,
            size=maxItems * 10,
            lat=lat,
            lon=lon,
            radius=radius,
            merchant_type=merchant_type)

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

    @patch("rcommsutils.privilege_utils._build_result_set")
    @patch("rcommsutils.privilege_utils._get_return_fields")
    def test_get_merchants_from_rcom_tags_with_is_trueyou_eat(
            self, mock_get, mock_build):
        session = MagicMock()
        rcom_tags = [
            '1_food-beverage-related', '2_food', '3_food-grilled', '4_yakiniku'
        ]
        campaign_type = 'privilege'
        is_trueyou_eat = True
        maxItems = 10
        lat = 13.7565591
        lon = 100.7966439
        radius = 5
        merchant_type = 'ka'
        dsl = {
            'query': {
                'function_score': {
                    'query': {
                        'bool': {
                            'must': [{
                                "match": {
                                    "merchant_type": "ka"
                                }
                            }, {
                                "match": {
                                    "article_category": "dining"
                                }
                            }],
                            'must_not': [{
                                'match': {
                                    'allow_recommend': 'false'
                                }
                            }],
                            'filter': [{
                                'range': {
                                    'publish_date': {
                                        'lte': 'now/h'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                "term": {
                                    "lang": "th"
                                }
                            }, {
                                'range': {
                                    'expire_date': {
                                        'gt': 'now/h'
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must_not': [{
                                                'term': {
                                                    'denormalize_privilege.quota_over_existed.keyword':
                                                        1
                                                }
                                            }]
                                        }
                                    }
                                }
                            }, {
                                'nested': {
                                    'path': 'denormalize_privilege',
                                    'query': {
                                        'bool': {
                                            'must': [{
                                                'term': {
                                                    'denormalize_privilege.campaign_type':
                                                        'privilege'
                                                }
                                            }]
                                        }
                                    }
                                }
                            }]
                        }
                    },
                    'functions': [{
                        'field_value_factor': {
                            'field': 'HIT_COUNT_WEEK',
                            'factor': 0.001,
                            'modifier': 'log1p',
                            'missing': 0
                        },
                        'weight': 15
                    }, {
                        'field_value_factor': {
                            'field': 'HIT_COUNT_MONTH',
                            'factor': 0.001,
                            'modifier': 'log1p',
                            'missing': 0
                        },
                        'weight': 10
                    }, {
                        'field_value_factor': {
                            'field': 'HIT_COUNT_YEAR',
                            'factor': 0.001,
                            'modifier': 'log1p',
                            'missing': 0
                        },
                        'weight': 5
                    }, {
                        'filter': {
                            'nested': {
                                'path': 'denormalize_branch',
                                'query': {
                                    'geo_distance': {
                                        'distance': '5km',
                                        'denormalize_branch.location.location':
                                            {
                                                'lat': 13.7565591,
                                                'lon': 100.7966439
                                            }
                                    }
                                }
                            }
                        },
                        'weight': 40
                    }, {
                        "filter": {
                            'terms': {
                                'RCOM_TAGS': rcom_tags
                            }
                        },
                        "weight": 70
                    }],
                    'score_mode': 'sum',
                    'boost_mode': 'sum'
                }
            }
        }
        privilege_utils.get_merchants_from_rcom_tags(
            session=session,
            rcom_tags=rcom_tags,
            language="TH",
            campaign_type=campaign_type,
            size=maxItems * 10,
            lat=lat,
            lon=lon,
            radius=radius,
            merchant_type=merchant_type,
            is_trueyou_eat=is_trueyou_eat)

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        self.assertEqual(expected_dsl, actual_dsl)

    def test_add_lang_prefix_for_ids_list_en(self):
        ids = ['id_1', 'id_2', 'id_3']
        language = 'EN'
        expect = ['en-id_1', 'en-id_2', 'en-id_3']
        actual = privilege_utils._add_lang_prefix_for_ids(ids, language)
        self.assertListEqual(expect, actual)

    def test_add_lang_prefix_for_ids_list_th(self):
        ids = ['id_1', 'id_2', 'id_3']
        language = 'th'
        expect = ['th-id_1', 'th-id_2', 'th-id_3']
        actual = privilege_utils._add_lang_prefix_for_ids(ids, language)
        self.assertListEqual(expect, actual)

    def test_add_lang_prefix_for_ids_str_en(self):
        id = 'id_1'
        language = 'en'
        expect = 'en-id_1'
        actual = privilege_utils._add_lang_prefix_for_ids([id], language)[0]
        self.assertEqual(expect, actual)

    def test_add_lang_prefix_for_ids_str_th(self):
        id = 'id_1'
        language = 'TH'
        expect = 'th-id_1'
        actual = privilege_utils._add_lang_prefix_for_ids([id], language)[0]
        self.assertEqual(expect, actual)

    def test_get_items_define_fields_privilege(self):
        session = MagicMock()
        dsl = {'query':
                   {'bool':
                        {'filter': [
                            {'bool':
                                 {'must_not': [{'match': {'allow_recommend': 'false'}},
                                               {'range': {'quota_over_existed': {'gt': 0}}}],
                                  'filter': [
                                             {'terms': {'id': ['1','2','3','4','5']}},
                                             {'term': {'searchable': 'Y'}},
                                             {'term': {'status': 'publish'}},
                                             {'term': {'lang': 'th'}},
                                             {'exists': {'field': 'article_category'}},
                                             {'range': {'publish_date': {'lte': 'now/h'}}}],
                                  'should': [{'range': {'expire_date': {'gt': 'now/h'}}},
                                             {'bool': {'must_not': [{'exists': {'field': 'expire_date'}}]}}],
                                  'minimum_should_match': 1}}]}},
                'size': 2,
                '_source': ['id']}

        search_result = {
            "hits": {
                "hits": [{
                    "_source": {'id': "privilege_1"}
                }, {
                    "_source": {'id': "privilege_2"}
                }]
            }
        }

        expected = [
            {'id': "privilege_1"}, {'id': "privilege_2"}
        ]
        session.search.return_value = search_result

        actual = privilege_utils.get_items_define_fields(
            session=session,
            ids=['1','2','3','4','5'],
            size=2,
            type='privilege',
            fields=['id']
        )

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        actual_index = actual_call_kwargs['index']

        self.assertEqual(expected_dsl, actual_dsl)
        self.assertEqual(ste.RCOM_ES_PRIVILEGE_INDEX, actual_index)
        self.assertEqual(actual, expected)

    def test_get_items_define_fields_merchant(self):
        session = MagicMock()
        dsl = {'query':
                   {'bool':
                        {'filter': [
                            {'bool':
                                 {'must_not': [{'match': {'allow_recommend': 'false'}}],
                                  'filter': [
                                             {'terms': {'id': ['1','2','3','4','5']}},
                                             {'match': {'status': 'publish'}},
                                             {'range': {'publish_date': {'lte': 'now/h'}}},
                                             {'term': {'lang': 'th'}},
                                             {'nested': {'path': 'denormalize_privilege',
                                                         'query': {'bool': {'must_not': [
                                                             {'term': {'denormalize_privilege.quota_over_existed.keyword': 1}}]}}}},
                                             {'terms': {'merchant_type': ['ka']}},
                                             {'term': {'searchable': 'Y'}}],
                                  'should': [{'range': {'expire_date': {'gt': 'now/h'}}},
                                             {'bool': {'must_not': [{'exists': {'field': 'expire_date'}}]}}],
                                  'minimum_should_match': 1}}]}},
            'size': 2,
            '_source': ['id']}

        search_result = {
            "hits": {
                "hits": [{
                    "_source": {'id': "merchant_1"}
                }, {
                    "_source": {'id': "merchant_2"}
                }]
            }
        }

        expected = [
            {'id': "merchant_1"}, {'id': "merchant_2"}
        ]
        session.search.return_value = search_result

        actual = privilege_utils.get_items_define_fields(
            session=session,
            size=2,
            ids=['1','2','3','4','5'],
            type='trueyoumerchant',
            fields=['id']
        )

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        actual_index = actual_call_kwargs['index']

        self.assertEqual(expected_dsl, actual_dsl)
        self.assertEqual(ste.RCOM_ES_SHOP_INDEX, actual_index)
        self.assertEqual(actual, expected)

    def test_get_items_define_fields_trueyouarticle(self):
        session = MagicMock()
        dsl = {'query':
                   {'bool':
                        {'filter': [
                            {'bool':
                                 {'must_not': [{'match': {'allow_recommend': 'false'}}],
                                  'filter': [
                                             {'terms': {'id': ['1','2','3','4','5']}},
                                             {'term': {'searchable': 'Y'}},
                                             {'term': {'status': 'publish'}},
                                             {'term': {'lang': 'th'}},
                                             {'range': {'publish_date': {'lte': 'now/h'}}}],
                                  'should': [{'range': {'expire_date': {'gt': 'now/h'}}},
                                             {'bool': {'must_not': [{'exists': {'field': 'expire_date'}}]}}],
                                  'minimum_should_match': 1}}]}},
                'size': 2,
                '_source': ['id']}

        search_result = {
            "hits": {
                "hits": [{
                    "_source": {'id': "article_1"}
                }, {
                    "_source": {'id': "article_2"}
                }]
            }
        }

        expected = [
            {'id': "article_1"}, {'id': "article_2"}
        ]
        session.search.return_value = search_result

        actual = privilege_utils.get_items_define_fields(
            session=session,
            size=2,
            ids=['1','2','3','4','5'],
            type='trueyouarticle',
            fields=['id']
        )

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        actual_index = actual_call_kwargs['index']

        self.assertEqual(expected_dsl, actual_dsl)
        self.assertEqual(ste.RCOM_ES_TRUEYOU_ARTICLE_INDEX, actual_index)
        self.assertEqual(actual, expected)

    def test_get_items_define_fields_hilight(self):
        session = MagicMock()
        dsl = {'query':
                   {'bool':
                        {'filter': [
                            {'bool':
                                 {'must_not': [{'match': {'allow_recommend': 'false'}}],
                                  'filter': [{'terms': {'id': ['1','2','3','4','5']}},
                                             {'term': {'searchable': 'Y'}},
                                             {'term': {'status': 'publish'}},
                                             {'term': {'lang': 'th'}},
                                             {'range': {'publish_date': {'lte': 'now/h'}}},
                                             {'exists': {'field': 'article_category'}},
                                             {'term': {'article_category': 'trueyou-article'}},],
                                  'should': [{'range': {'expire_date': {'gt': 'now/h'}}},
                                             {'bool': {'must_not': [{'exists': {'field': 'expire_date'}}]}}],
                                  'minimum_should_match': 1}}]}},
                'size': 2,
                '_source': ['id']}

        search_result = {
            "hits": {
                "hits": [{
                    "_source": {'id': "hilight_1"}
                }, {
                    "_source": {'id': "hilight_2"}
                }]
            }
        }

        expected = [
            {'id': "hilight_1"}, {'id': "hilight_2"}
        ]
        session.search.return_value = search_result

        actual = privilege_utils.get_items_define_fields(
            session=session,
            size=2,
            ids=['1','2','3','4','5'],
            type='hilight',
            fields=['id']
        )

        _, actual_call_kwargs = session.search.call_args
        actual_dsl = actual_call_kwargs['body']
        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_dsl)
        actual_index = actual_call_kwargs['index']

        self.assertEqual(expected_dsl, actual_dsl)
        self.assertEqual(ste.RCOM_ES_HIGHLIGHT_INDEX, actual_index)
        self.assertEqual(actual, expected)

    def test_get_items_define_fields_privilege_dry_run(self):
        session = MagicMock()
        dsl = {'query':
            {'bool':
                {'filter': [
                    {'bool':
                         {'must_not': [{'match': {'allow_recommend': 'false'}},
                                       {'range': {'quota_over_existed': {'gt': 0}}}],
                          'filter': [{'term': {'searchable': 'Y'}},
                                     {'term': {'status': 'publish'}},
                                     {'term': {'lang': 'th'}},
                                     {'exists': {'field': 'article_category'}},
                                     {'range': {'publish_date': {'lte': 'now/h'}}}],
                          'should': [{'range': {'expire_date': {'gt': 'now/h'}}},
                                     {'bool': {'must_not': [{'exists': {'field': 'expire_date'}}]}}],
                          'minimum_should_match': 1}}]}},
            'size': 2,
            '_source': ['id']}

        actual_index, actual_body = privilege_utils.get_items_define_fields(
            session=session,
            size=2,
            type='privilege',
            fields=['id'],
            dry_run=True
        )

        expected_dsl = self.json_ordered(dsl)
        actual_dsl = self.json_ordered(actual_body)

        self.assertEqual(expected_dsl, actual_dsl)
        self.assertEqual(ste.RCOM_ES_PRIVILEGE_INDEX, actual_index)

    def test_msearch_query(self):
        session = MagicMock()
        body = str({'index': ste.RCOM_ES_PRIVILEGE_INDEX})
        body += str({'query':
            {'bool':
                {'filter': [
                    {'bool':
                         {'must_not': [{'match': {'allow_recommend': 'false'}},
                                       {'range': {'quota_over_existed': {'gt': 0}}}],
                          'filter': [{'term': {'searchable': 'Y'}},
                                     {'term': {'status': 'publish'}},
                                     {'term': {'lang': 'th'}},
                                     {'exists': {'field': 'article_category'}},
                                     {'range': {'publish_date': {'lte': 'now/h'}}}],
                          'should': [{'range': {'expire_date': {'gt': 'now/h'}}},
                                     {'bool': {'must_not': [{'exists': {'field': 'expire_date'}}]}}],
                          'minimum_should_match': 1}}]}},
            'size': 1,
            '_source': ['id']})
        body += str({'index': ste.RCOM_ES_TRUEYOU_ARTICLE_INDEX})
        body += str({'query':
                   {'bool':
                        {'filter': [
                            {'bool':
                                 {'must_not': [{'match': {'allow_recommend': 'false'}}],
                                  'filter': [{'term': {'searchable': 'Y'}},
                                             {'term': {'status': 'publish'}},
                                             {'term': {'lang': 'th'}},
                                             {'range': {'publish_date': {'lte': 'now/h'}}}],
                                  'should': [{'range': {'expire_date': {'gt': 'now/h'}}},
                                             {'bool': {'must_not': [{'exists': {'field': 'expire_date'}}]}}],
                                  'minimum_should_match': 1}}]}},
                'size': 2,
                '_source': ['id']})

        search_result = {
            "responses": [{
            "hits": {
                "hits": [
                    {
                    "_index": ste.RCOM_ES_PRIVILEGE_INDEX,
                    "_source": {'id': "privilege_1"}
                }, {
                    "_index": ste.RCOM_ES_TRUEYOU_ARTICLE_INDEX,
                    "_source": {'id': "trueyouarticle_1"}
                }]
            }
            }]
        }

        expected = {'privilege' : [{'id': "privilege_1"}],
                    'trueyouarticle': [{'id': "trueyouarticle_1"}] }

        session.msearch.return_value = search_result
        actual = privilege_utils.msearch_query(session, body)

        self.assertEqual(expected, actual)


    def test_get_shelf_pin_items_with_shelf_id(self):
        session = MagicMock()
        id = "id__1"
        timestamp = "2022-01-01T10:00:00.011111Z"
        expected_dsl = {
            "query": {
                "bool": {
                    "minimum_should_match": 1,
                    "should": [
                        {
                            "bool": {
                                "filter": [
                                    {"term": {"id.keyword": id}}, 
                                    {"term": {"_index": ste.RCOM_ES_PROGRESSIVE_INDEX}},
                                    {
                                        "nested": {
                                            "path": Fields.SHELF_LIST,
                                            "query": {
                                                "bool": {
                                                    "filter": [
                                                        {"term" : {f"{Fields.SHELF_LIST}.{Fields.SHELF_ACTIVE}": True}},
                                                        {"range": {f"{Fields.SHELF_LIST}.{Fields.SHELF_START_DATE}": {"lte": timestamp}}}
                                                    ],
                                                    "should": [
                                                        {"range": {f"{Fields.SHELF_LIST}.{Fields.SHELF_END_DATE}": {"gte": timestamp}}},
                                                        {"bool" : {"must_not": [{"exists": {"field": f"{Fields.SHELF_LIST}.{Fields.SHELF_END_DATE}"}}]}}
                                                    ],
                                                    "minimum_should_match": 1
                                                }
                                            },
                                            "inner_hits": {
                                                "sort" : [{f"{Fields.SHELF_LIST}.{Fields.SHELF_VERSION}" : "asc"}],
                                                "_source": [
                                                    f"{Fields.SHELF_LIST}.{Fields.SHELF_ACTIVE}",
                                                    f"{Fields.SHELF_LIST}.{Fields.SHELF_PIN_ITEMS}",
                                                    f"{Fields.SHELF_LIST}.{Fields.SHELF_START_DATE}",
                                                    f"{Fields.SHELF_LIST}.{Fields.SHELF_END_DATE}"
                                                ]
                                            }
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            "bool": {
                                "filter": [
                                    {"term": {"id": id}},
                                    {"term": {"_index": ste.RCOM_ES_CONTENTS_SHELF_INDEX}}
                                ]
                            }
                        }
                    ]
                }
            }
        }

        privilege_utils.get_shelf_pin_items(session, shelf_id=id, timestamp=timestamp)
        session.search.assert_called_once_with(
            _source=[Fields.SHELF_PIN_ITEMS],
            body=expected_dsl,
            index=[
                ste.RCOM_ES_CONTENTS_SHELF_INDEX, ste.RCOM_ES_PROGRESSIVE_INDEX
            ],
            size=1,
            timeout="10s")

    def test_get_shelf_metadata(self):
        session = MagicMock()
        item_ids = ["oREog6BKWNg4"]

        # mock session search
        search_result = {
            "took": 4,
            "timed_out": False,
            "_shards": {"total": 6, "successful": 6, "skipped": 0, "failed": 0},
            "hits": {
                "total": {"value": 1, "relation": "eq"},
                "max_score": 0.0,
                "hits": [
                    {
                        "_index": "preprod-content-shelf-master",
                        "_type": "_doc",
                        "_id": "th-oREog6BKWNg4",
                        "_score": 0.0,
                        "_source": {
                            "id": "oREog6BKWNg4",
                            "setting": {
                                "shelf_code": "oREog6BKWNg4",
                                "segments": ["vw_dim_true_x_product_tmh_flag_true"],
                            },
                        },
                    }
                ],
            },
        }

        session.search.return_value = search_result

        # define expect
        expected = [
            {'id': 'oREog6BKWNg4',
                'setting': {'shelf_code': 'oREog6BKWNg4',
                            'segments': ['vw_dim_true_x_product_tmh_flag_true'],
                            }
            }
        ]
        # call actual
        actual = privilege_utils.get_shelf_metadata(
            es_session=session,
            item_ids=item_ids,
            language="th",
            fields=["setting.shelf_code", "setting.segments", "id"],
        )
       
        self.assertEqual(expected, actual)

    def test_get_shelf_items_and_pin_items(self):
        session = MagicMock()
        shelf_id = "id__1"
        timestamp = "2022-01-01T10:00:00.011111Z"
        language = "th"
        active = True
        expected_dsl = {
            "query": {
                "bool": {
                    "minimum_should_match": 1,
                    "should": [
                        {
                            # this query for progressive shelf
                            "bool": {
                                "filter": [
                                    {"term": {"_index": ste.RCOM_ES_PROGRESSIVE_INDEX}},
                                    {"term": {"id.keyword": shelf_id}}, 
                                    {"term": {"status": "publish"}},
                                    {"range": {"publish_date": {"lte": "now"}}},
                                    {
                                        "nested": {
                                            "path": Fields.SHELF_LIST,
                                            "query": {
                                                "bool": {
                                                    "filter": [
                                                        {"term" : {f"{Fields.SHELF_LIST}.{Fields.SHELF_ACTIVE}": active}},
                                                        {"range": {f"{Fields.SHELF_LIST}.{Fields.SHELF_START_DATE}": {"lte": "now"}}}
                                                    ],
                                                    "should": [
                                                        {"range": {f"{Fields.SHELF_LIST}.{Fields.SHELF_END_DATE}": {"gte": timestamp}}},
                                                        {"bool" : {"must_not": [{"exists": {"field": f"{Fields.SHELF_LIST}.{Fields.SHELF_END_DATE}"}}]}}
                                                    ],
                                                    "minimum_should_match": 1
                                                }
                                            },
                                            "inner_hits": {
                                                "sort" : [{f"{Fields.SHELF_LIST}.{Fields.SHELF_VERSION}" : "asc"}],
                                                "_source": [
                                                    f"{Fields.SHELF_LIST}.{Fields.SHELF_PIN_ITEMS}",
                                                    f"{Fields.SHELF_LIST}.{Fields.SHELF_ITEMS}",
                                                ]
                                            }
                                        }
                                    }
                                ],
                                "minimum_should_match": 1,
                                "should": [
                                    {"range": {"expire_date": {"gt": timestamp}}},
                                    {"bool": {"must_not": [{"exists": {"field": "expire_date"}}]}},
                                ],
                            }
                        },
                        {
                            # this query for content shelf
                            "bool": {
                                "filter": [
                                    {"term": {"id": shelf_id}},
                                    {"term": {"_index": ste.RCOM_ES_CONTENTS_SHELF_INDEX}},
                                    {"term": {"status": "publish"}},
                                    {"term": {"lang": language.lower()}},
                                    {"range": {"publish_date": {"lte": "now"}}},
                                ],
                                "minimum_should_match": 1,
                                "should": [
                                    {"range": {"expire_date": {"gt": timestamp}}},
                                    {"bool": {"must_not": [{"exists": {"field": "expire_date"}}]}},
                                ],
                            }
                        }
                    ]
                }
            }
        }

        privilege_utils.get_shelf_items_and_pin_items(session, shelf_id=shelf_id, timestamp=timestamp, language=language)
        session.search.assert_called_once()
        session.search.assert_called_once_with(
            _source=[Fields.SHELF_PIN_ITEMS, Fields.SHELF_ITEMS],
            body=expected_dsl,
            index=[
                ste.RCOM_ES_CONTENTS_SHELF_INDEX, ste.RCOM_ES_PROGRESSIVE_INDEX
            ],
            size=1,
            timeout="10s"
        )


if __name__ == '__main__':
    ut.main()
