"""
# TODO: should implement this test in other file name ex. test_shelf_utils.py
"""
import elasticsearch
import unittest as ut
import orjson as json
from unittest.mock import MagicMock, patch

from rcommsutils import tv_utils
from rcommsutils.cms.tv_utils import Fields
from rcommsutils.cms import settings as ste


class TVUtilsOtherTest(ut.TestCase):
    def test_get_items_metadata_from_all_indexes_elastic_search(self):
        session = MagicMock()
        item_ids = ["123", "456"]
        language = "TH"
        status = "publish"
        timestamp = "now/h"
        fields = ["*"]
        size = 2
        timeout = "10s"

        expected_dsl = {
            "query": {
                "bool": {
                    "minimum_should_match": 1,
                    "should": [
                        # this query for content and shelf
                        {
                            "bool": {
                                "filter": [
                                    # TODO: should we change this index more scope by query only wanted content index and shelf index ?
                                    {"term": {"_index": ste.RCOM_ES_ALL_INDEX}},
                                    {"terms": {f"{Fields.ID}": item_ids}},
                                    {"term": {Fields.LANG: language.lower()}},
                                    {"term": {Fields.STATUS: status}},
                                    {
                                        "range": {
                                            Fields.PUBLISH_DATE: {"lte": timestamp}
                                        }
                                    },
                                ],
                                "minimum_should_match": 1,
                                "should": [
                                    {"range": {Fields.EXPIRY_DATE: {"gt": timestamp}}},
                                    {
                                        "bool": {
                                            "must_not": [
                                                {
                                                    "exists": {
                                                        "field": Fields.EXPIRY_DATE
                                                    }
                                                }
                                            ]
                                        }
                                    },
                                ],
                            }
                        },
                        # this query for progressive shelf
                        {
                            "bool": {
                                "filter": [
                                    {"term": {"_index": ste.RCOM_ES_PROGRESSIVE_INDEX}},
                                    {"terms": {f"{Fields.ID}.keyword": item_ids}},
                                    {"term": {Fields.COUNTRY: language.lower()}},
                                    {"term": {Fields.STATUS: status}},
                                    {
                                        "range": {
                                            Fields.PUBLISH_DATE: {"lte": timestamp}
                                        }
                                    },
                                ],
                                "minimum_should_match": 1,
                                "should": [
                                    {"range": {Fields.EXPIRY_DATE: {"gt": timestamp}}},
                                    {
                                        "bool": {
                                            "must_not": [
                                                {
                                                    "exists": {
                                                        "field": Fields.EXPIRY_DATE
                                                    }
                                                }
                                            ]
                                        }
                                    },
                                ],
                            }
                        },
                    ],
                }
            },
            "_source": fields,
            "size": size,
        }

        tv_utils.get_items_metadata_from_all_indexes_elastic_search(
            es_session=session,
            item_ids=item_ids,
            language=language,
            fields=fields,
            timestamp=timestamp,
            size=size,
        )
        session.search.assert_called_once()
        session.search.assert_called_once_with(
            index="*", body=expected_dsl, timeout=timeout
        )

    @patch(
        "rcommsutils.cms.tv_utils.get_items_metadata_from_all_indexes_elastic_search"
    )
    def test_get_items_metadata_from_all_indexes_cache_process(self, mock_get_es):
        """Test caching process"""
        session = MagicMock()
        redis_session = MagicMock()
        redis_session.mget
        item_ids = ["123", "456"]
        language = "TH"
        status = "publish"
        timestamp = "now/h"
        fields = ["*"]
        size = 2
        timeout = "10s"

        data = [
            {"id": "123", "title": "helloworld"},
            {"id": "456", "title": "hellouniverse"},
        ]

        # mock redis mget
        redis_session.mget.side_effect = [
            [None, None],
            [json.dumps(each_data) for each_data in data],
        ]

        # mock get_items_metadata_from_all_indexes_elastic_search
        mock_get_es.side_effect = [data, []]

        # call first time expected non cached
        actual_1 = tv_utils.get_items_metadata_from_all_indexes(
            es_session=session,
            redis_session=redis_session,
            item_ids=item_ids,
            language=language,
            fields=fields,
            timestamp=timestamp,
            size=size,
        )
        self.assertEqual(actual_1, data)

        # call second time expected redis cached
        actual_2 = tv_utils.get_items_metadata_from_all_indexes(
            es_session=session,
            redis_session=redis_session,
            item_ids=item_ids,
            language=language,
            fields=fields,
            timestamp=timestamp,
            size=size,
        )
        self.assertEqual(actual_2, data)

        # assert call get_items_metadata_from_all_indexes_elastic_search 1 time
        mock_get_es.assert_called_once()
        mock_get_es.assert_called_once_with(
            es_session=session,
            item_ids=item_ids,
            fields=fields,
            language=language,
            status=status,
            timestamp=timestamp,
            timeout=timeout,
            size=size,
        )

    @patch("rcommsutils.cms.tv_utils.get_shelf_items_from_elasticsearch")
    @patch("rcommsutils.cms.tv_utils._get_items_from_redis")
    def test_get_shelf_items_memory_cache(self, mock_get_redis, mock_get_es):
        es_session = MagicMock()
        redis_session = MagicMock()
        shelf_id = "123"
        timestamp = "now/h"
        active = True
        timeout = "10s"
        language = "TH"

        # mock _get_items_from_redis return None
        mock_get_redis.side_effect = [None, None]

        # mock get_shelf_items_from_elasticsearch ["value", None]
        mock_get_es.side_effect = [(["abc", "def", "123"], None), (None, None)]

        expected = ["abc", "def", "123"]

        # call first time expected non cached
        actual_1 = tv_utils.get_shelf_items(
            es_session=es_session,
            redis_session=None,
            shelf_id=shelf_id,
            timestamp=timestamp,
            active=active,
            timeout=timeout,
            language=language,
        )
        self.assertEqual(actual_1, expected)

        # call second time expected memory cached
        actual_2 = tv_utils.get_shelf_items(
            es_session=es_session,
            redis_session=redis_session,
            shelf_id=shelf_id,
            timestamp=timestamp,
            active=active,
            timeout=timeout,
            language=language,
        )
        self.assertEqual(actual_2, expected)

        # assert call get_items_metadata_from_all_indexes_elastic_search 1 time
        mock_get_es.assert_called_once()

    @patch("rcommsutils.cms.tv_utils.get_shelf_items_from_elasticsearch")
    @patch("rcommsutils.cms.tv_utils._get_items_from_redis")
    def test_get_shelf_items_not_cache_when_es_timeout_exception(self, mock_get_redis, mock_get_es):
        """
        usecase
            - es timeout
            - make sure get_shelf_items not cache
        """
        es_session = MagicMock()
        redis_session = MagicMock()
        shelf_id = "1234"
        timestamp = "now/h"
        active = True
        timeout = "10s"
        language = "TH"

        # mock _get_items_from_redis return None
        mock_get_redis.side_effect = [None, None]

        mock_get_es.side_effect = [
            elasticsearch.exceptions.ConnectionTimeout(None, None, "error"),
            (["abc", "def", "123"], None),
            elasticsearch.exceptions.ConnectionTimeout(None, None, "error")
        ]

        expected_1 = []
        expected_3 = expected_2 = ["abc", "def", "123"]


        # call first time expected empty from exception
        actual_1 = tv_utils.get_shelf_items(
            es_session=es_session,
            redis_session=None,
            shelf_id=shelf_id,
            timestamp=timestamp,
            active=active,
            timeout=timeout,
            language=language,
        )
        print("actual1:", actual_1)
        self.assertEqual(actual_1, expected_1)

        # call second time expected call get result from mock es
        actual_2 = tv_utils.get_shelf_items(
            es_session=es_session,
            redis_session=redis_session,
            shelf_id=shelf_id,
            timestamp=timestamp,
            active=active,
            timeout=timeout,
            language=language,
        )
        self.assertEqual(actual_2, expected_2)

        # call third time expecte memory cache from second call
        actual_3 = tv_utils.get_shelf_items(
            es_session=es_session,
            redis_session=redis_session,
            shelf_id=shelf_id,
            timestamp=timestamp,
            active=active,
            timeout=timeout,
            language=language,
        )
        self.assertEqual(actual_3, expected_3)

    def test_tv_get_shelf_items_get_cache_key(self):
        shelf_id = "shelf_id"
        timestamp = "now/h"
        active = True
        language = "th"

        expected = "('shelf_id', 'now/h', True, 'th')"
        
        actual = tv_utils._get_cache_key(shelf_id, timestamp, active, language)
        print("actual:", actual)
        self.assertEqual(actual, expected)

    @patch("rcommsutils.cms.tv_utils._get_items_from_elasticsearch")
    @patch("rcommsutils.cms.tv_utils._filter_items_from_conditions")
    @patch("rcommsutils.cms.tv_utils._build_result_set", lambda x: None)
    @patch("rcommsutils.cms.tv_utils._set_cache_items")
    @patch("rcommsutils.cms.tv_utils._get_items_from_redis")
    def test_get_items_must_nots_prefix(self, mock_get_redis, mock_set_cache, mock_filter_items, mock_get_items_from_es):
        """This test want to confirm
        if must_nots pass to get_items prefix in _get_items_from_redis and _set_cache_items have to include must not field
        and if not pass must_nots it expected to not include '-[]' (empty list of must_nots)
        """
        test_cases = [
            # test must_nots
            {
                "ids": ["1", "2", "3"],
                "must_nots": [{'match': {'partner_related': 'py3K8GdZOOEy'}}],
                "expected_prefix": ste.REDIS_MOVIE_PREFIX + "-" + str([{'match': {'partner_related': 'py3K8GdZOOEy'}}])
            },
            # test not have must_nots
            {
                "ids": ["1", "2", "3"],
                "must_nots": [],
                "expected_prefix": ste.REDIS_MOVIE_PREFIX 
            }
        ]

        for tc in test_cases:
            mock_es_session = MagicMock()
            mock_redis_session = MagicMock()
            mock_get_redis.return_value = [None] * len(tc["ids"])
            mock_filter_items.return_value = [None] * len(tc["ids"])
            mock_get_items_from_es.return_value = tc["ids"]

            _ = tv_utils.get_items(
                    es_session=mock_es_session,
                    redis_session=mock_redis_session,
                    ids=tc["ids"],
                    must_nots=tc["must_nots"]
                )
            mock_get_redis.assert_called_with(mock_redis_session, tc["ids"], prefix=tc["expected_prefix"], language="EN")
            mock_set_cache.assert_called_with(mock_redis_session, items=tc["ids"], prefix=tc["expected_prefix"], language="EN")


if __name__ == "__main__":
    ut.main()
