import orjson as json
import unittest as ut
from unittest.mock import call, patch, MagicMock

import rcommsutils.cms.settings as ste
from requests.exceptions import Timeout
from rcommsutils import tv_utils
from rcommsutils.cms.tv_utils import Fields, _build_tv_result_set, _get_tv_return_fields, _API_TV_COMMON_FIELDS


class MockResponse:

    def __init__(self, data, exception=None):
        self.data = data
        self.exception = exception

    def raise_for_status(self):
        if self.exception:
            raise self.exception

    def json(self):
        return json.loads(self.data)


class TVUtilsTest(ut.TestCase):
    def test_get_tv_return_fields(self):
        expected = _API_TV_COMMON_FIELDS
        actual = _get_tv_return_fields()
        self.assertEqual(expected, actual)

    @patch("rcommsutils.cms.tv_utils._to_timestamp")
    def test_build_tv_result_set(self, mock_to_timestamp):
        stub_tv_item = {
            Fields.TITLE_ID: "item_title_id",
            Fields.THUMBNAIL: "item_thumbnail",
            Fields.ID: "item_id",
            Fields.CHANNEL_CODE: "item_channel_code",
            Fields.START_DATE: "item_start_date",
            Fields.END_DATE: "item_end_date",
            Fields.TITLE: "item_title",
            Fields.DETAIL: "item_detail"
        }
        mock_to_timestamp.side_effect = ["start_time", "end_time"]
        expected = [{
            ste.RCOM_API_TITLE_ID: "item_title_id",
            ste.RCOM_API_TTV_THUMBNAIL: "item_thumbnail",
            ste.RCOM_API_CMS_TITLE_ID: "item_id",
            ste.RCOM_API_CMS_CHANNEL_CODE: "item_channel_code",
            ste.RCOM_API_START_TIME: "start_time",
            ste.RCOM_API_END_TIME: "end_time",
            ste.RCOM_API_NAME: "item_title",
            ste.RCOM_API_DESC: "item_detail"
        }]
        items = [{"_source": stub_tv_item}]
        actual = _build_tv_result_set(items)
        self.assertEqual(expected, actual)

    @patch("rcommsutils.cms.tv_utils._to_timestamp")
    @patch("rcommsutils.cms.tv_utils._validate_tv_interval_time")
    def test_get_tv_items_with_title_id_happy_path(self, mock_validate_tv_interval_time,
                                                   mock_to_timestamp):
        mock_validate_tv_interval_time.return_value = (
            "2019-02-11T07:00:00.000Z", "2019-02-11T07:20:00.000Z", False)
        es_result = {
            'took': 5,
            'timed_out': False,
            '_shards': {
                'total': 3,
                'successful': 3,
                'failed': 0
            },
            'hits': {
                'total': 1,
                'max_score': 0.0,
                'hits': [{
                    '_index': 'contents-epg-v4',
                    '_type': 'data',
                    '_id': 'en-4Ww58RjQgezy',
                    '_score': 0.0,
                    '_source': {
                        'detail': '',
                        'thumb': 'https://epgthumb.dmpcdn.com/thumbnail_large/121/20190211/20190211_140000.jpg',
                        'title_id': '512223',
                        'title': 'The Show : All About K-Pop',
                        'channel_code': '121',
                        'id': '4Ww58RjQgezy',
                        'start_date': '2019-02-11T07:00:00.000Z',
                        'end_date': '2019-02-11T07:00:00.000Z',
                    }
                }]
            }
        }
        es_session = MagicMock()
        es_session.search.return_value = es_result

        expected = [{
            "thumbnail": "https://epgthumb.dmpcdn.com/thumbnail_large/121/20190211/20190211_140000.jpg",
            "cmsTitleId": "4Ww58RjQgezy",
            "titleId": "512223",
            "startTime": "1549843200",
            "endTime": "1549843200",
            "cmsChannelCode": "121",
            "name": "The Show : All About K-Pop",
            "desc": "",
        }]
        start_time = 1549800000
        end_time = 1549843200

        mock_to_timestamp.side_effect = [str(end_time), str(end_time)]
        actual = tv_utils.get_tv_items(es_session,
                                       title_ids=["512223"],
                                       start_time=start_time,
                                       end_time=end_time)
        self.assertEqual(actual, expected)

    @patch("rcommsutils.cms.tv_utils._to_timestamp")
    @patch("rcommsutils.cms.tv_utils._validate_tv_interval_time")
    def test_get_tv_items_with_channel_code_happy_path(self, mock_validate_tv_interval_time,
                                                       mock_to_timestamp):
        mock_validate_tv_interval_time.return_value = (
            "2019-02-11T07:00:00.000Z", "2019-02-11T07:20:00.000Z", False)
        es_result = {
            'took': 5,
            'timed_out': False,
            '_shards': {
                'total': 3,
                'successful': 3,
                'failed': 0
            },
            'hits': {
                'total': 1,
                'max_score': 0.0,
                'hits': [{
                    '_index': 'contents-epg-v4',
                    '_type': 'data',
                    '_id': 'en-4Ww58RjQgezy',
                    '_score': 0.0,
                    '_source': {
                        'detail': '',
                        'thumb': 'https://epgthumb.dmpcdn.com/thumbnail_large/121/20190211/20190211_140000.jpg',
                        'title_id': '512223',
                        'title': 'The Show : All About K-Pop',
                        'channel_code': '121',
                        'id': '4Ww58RjQgezy',
                        'start_date': '2019-02-11T07:00:00.000Z',
                        'end_date': '2019-02-11T07:00:00.000Z',
                    }
                }]
            }
        }
        es_session = MagicMock()
        es_session.search.return_value = es_result

        expected = [{
            "thumbnail": "https://epgthumb.dmpcdn.com/thumbnail_large/121/20190211/20190211_140000.jpg",
            "cmsTitleId": "4Ww58RjQgezy",
            "titleId": "512223",
            "startTime": "1549843200",
            "endTime": "1549843200",
            "cmsChannelCode": "121",
            "name": "The Show : All About K-Pop",
            "desc": "",
        }]
        start_time = 1549800000
        end_time = 1549843200

        mock_to_timestamp.side_effect = [str(end_time), str(end_time)]
        actual = tv_utils.get_tv_items(es_session,
                                       start_time=start_time,
                                       end_time=end_time,
                                       channel_codes=["121"])
        self.assertEqual(actual, expected)

    def test_get_tv_items_without_start_time(self):
        es_session = MagicMock()
        end_time = 1549843200

        with self.assertRaises(TypeError):
            tv_utils.get_tv_items(es_session,
                                  title_ids=["512223"],
                                  end_time=end_time)

    @patch("rcommsutils.cms.tv_utils.datetime")
    def test_get_tv_items_without_end_time(self, mock_datetime):
        es_session = MagicMock()
        start_time = 1549800000
        expected_end_time = 1549800000 + 3600
        expected = call(expected_end_time)

        tv_utils.get_tv_items(es_session,
                              title_ids=["512223"],
                              start_time=start_time)
        actual = mock_datetime.datetime.fromtimestamp.call_args

        self.assertEqual(actual, expected)

    def test_get_tv_items_with_start_time_is_more_than_end_time(self):
        es_session = MagicMock()
        start_time = 1549803600
        end_time = 1549800000

        with self.assertRaises(ValueError):
            tv_utils.get_tv_items(es_session,
                                  title_ids=["512223"],
                                  start_time=start_time,
                                  end_time=end_time)

    def test_get_tv_items_with_start_time_is_not_int(self):
        es_session = MagicMock()
        start_time = "1549803600"

        with self.assertRaises(TypeError):
            tv_utils.get_tv_items(es_session,
                                  title_ids=["512223"],
                                  start_time=start_time)

    def test_get_tv_items_with_end_time_is_not_int(self):
        es_session = MagicMock()
        start_time = 1549803600
        end_time = "1549800000"

        with self.assertRaises(TypeError):
            tv_utils.get_tv_items(es_session,
                                  title_ids=["512223"],
                                  start_time=start_time,
                                  end_time=end_time)

    @patch("rcommsutils.cms.tv_utils._validate_tv_interval_time")
    def test_get_tv_items_should_query_with_start_time_end_time(
            self, mock_validate_tv_interval_time):
        es_session = MagicMock()
        title_ids = ["512223"]
        size = len(title_ids)
        start_time = 1549800000
        end_time = 1549803600
        mock_validate_tv_interval_time.return_value = (
            '2019-02-10T19:00:00.000Z', '2019-02-10T19:20:00.000Z', True)

        expected_dsl = {
            'query': {
                'bool': {
                    'filter': {
                        'bool': {
                            'must': [
                                {'range': {'publish_date': {'lte': 'now/h'}}},
                                {'match': {'status': 'publish'}},
                                {'match': {'searchable': 'Y'}},
                                {"term": {'lang': 'en'}},
                                {'range': {'end_date': {'gt': '2019-02-10T19:00:00.000Z'}}},
                                {'range': {'start_date': {'lte': '2019-02-10T19:00:00.000Z'}}},
                                {'terms': {'title_id.keyword': title_ids}}
                            ],
                            'must_not': [
                                {'match_phrase': {'title': 'empty title'}},
                                {"term": {"black_out": 1}}
                            ]
                        }
                    }
                }
            }
        }

        tv_utils.get_tv_items(es_session,
                              title_ids=title_ids,
                              start_time=start_time,
                              end_time=end_time)
        es_session.search.assert_called_once_with(
            index=ste.RCOM_ES_EPG_INDEX,
            body=expected_dsl,
            _source=tv_utils._get_tv_return_fields(),
            size=size,
            timeout="10s")

    @patch("rcommsutils.cms.tv_utils._validate_tv_interval_time")
    def test_get_tv_items_with_title_ids_channel_codes(self, mock_validate_tv_interval_time):
        session = MagicMock()
        start_time = 1549800000
        end_time = 1549803600
        title_ids = ["512223"]
        channel_codes = ["121"]

        mock_validate_tv_interval_time.return_value = (
            '2019-02-10T19:00:00.000Z', '2019-02-10T19:20:00.000Z', True)

        expected_dsl = {
            'query': {
                'bool': {
                    'filter': {
                        'bool': {
                            'must': [
                                {'range': {'publish_date': {'lte': 'now/h'}}},
                                {'match': {'status': 'publish'}},
                                {'match': {'searchable': 'Y'}},
                                {"term": {'lang': 'en'}},
                                {'range': {'end_date': {'gt': '2019-02-10T19:00:00.000Z'}}},
                                {'range': {'start_date': {'lte': '2019-02-10T19:00:00.000Z'}}},
                                {'terms': {'title_id.keyword': title_ids}},
                                {'terms': {'channel_code.keyword': channel_codes}}
                            ],
                            'must_not': [
                                {'match_phrase': {'title': 'empty title'}},
                                {"term": {"black_out": 1}}
                            ]
                        }
                    }
                }
            }
        }

        tv_utils.get_tv_items(session,
                              title_ids=title_ids,
                              start_time=start_time,
                              end_time=end_time,
                              channel_codes=channel_codes)
        session.search.assert_called_once_with(
            index=ste.RCOM_ES_EPG_INDEX,
            body=expected_dsl,
            _source=tv_utils._get_tv_return_fields(),
            size=1,
            timeout="10s")

    def test_get_tv_return_fields(self):
        expected = [
            "id", "title_id", "thumb", "channel_code", "start_date", "end_date",
            "title", "detail"
        ]
        actual = tv_utils._get_tv_return_fields()
        self.assertEqual(actual, expected)

    @patch("rcommsutils.cms.tv_utils.requests")
    def test_get_tv_channels_call_correct_url(self, mock_requests):
        expected_url = ste.CMS_FN_CONTENT_URL + "?content_type=livetv&limit=300&fields=channel_code%2Csubscription_package"

        headers = {"Authorization": "token"}
        return_fields = ["channel_code", "subscription_package"]
        tv_utils.get_tv_channels(token="token", fields=return_fields)

        mock_requests.get.assert_called_once_with(expected_url, headers=headers,timeout=1.0)

    @patch("rcommsutils.cms.tv_utils.requests")
    def test_get_tv_channels_without_fields_specify(self, mock_requests):
        expected_url = ste.CMS_FN_CONTENT_URL + "?content_type=livetv&limit=300"

        headers = {"Authorization": "token"}
        return_fields = []
        tv_utils.get_tv_channels(token="token", fields=return_fields)

        mock_requests.get.assert_called_once_with(expected_url, headers=headers,timeout=1.0)

    @patch("rcommsutils.cms.tv_utils.hashlib")
    @patch("rcommsutils.cms.tv_utils.requests")
    def test_get_tv_channels_to_get_cache_by_url(self, mock_requests, mock_hashlib):
        expected_url = mock_hashlib.md5.return_value.hexdigest.return_value
        mock_redis = MagicMock()

        return_fields = ["my_fields1", "my_fields2"]
        tv_utils.get_tv_channels(token="token", redis_session=mock_redis, fields=return_fields)
        mock_redis.get.assert_called_once_with(f"TV_CHANNELS:{expected_url}")

    @patch("rcommsutils.cms.tv_utils.requests")
    def test_get_tv_channels_raise_error(self, mock_requests):
        mock_requests.get.side_effect = Exception
        return_fields = []

        with self.assertRaises(Exception):
            tv_utils.get_tv_channels(token="token", fields=return_fields)

    @patch("rcommsutils.cms.tv_utils.requests")
    def test_get_tv_channels(self, mock_requests):
        return_fields = ["channel_code", "subscription_package"]
        channels_data = {
            "lang": "th",
            "id": "69RBGDjWRGk9",
            "original_id": None,
            "content_type": "livetv",
            "title": "à¸‹à¸µ à¸«à¸™à¸±à¸‡",
            "article_category": ["digital-tv", "trueidtv-all"],
            "thumb": "https://cms.dmpcdn.com/livetv/2018/12/06/9f5fc8ec-7128-4c36-9a8e-8e129a49da1d.png",
            "tags": None,
            "status": "publish",
            "count_views": None,
            "publish_date": "2018-12-05T16:20:00.000Z",
            "create_date": "2018-12-05T16:21:28.339Z",
            "update_date": "2019-01-22T08:35:50.195Z",
            "searchable": "Y",
            "create_by": "087900xxxx",
            "create_by_ssoid": "37325036",
            "update_by": "m0892209407",
            "update_by_ssoid": "23645549",
            "source_url": None,
            "count_likes": None,
            "count_ratings": None,
            "channel_code": "sc001",
            "subscription_package": None
        }
        mock_response = {
            "code": 200,
            "lang": "th",
            "count_total": 233,
            "data": [channels_data],
            "nextPage": None
        }
        mock_requests.get.return_value = MockResponse(
            data=json.dumps(mock_response))

        expected = {"sc001": [channels_data]}
        actual = tv_utils.get_tv_channels(token="token", fields=return_fields)

        self.assertEqual(actual, expected)

    @patch("rcommsutils.cms.tv_utils.requests")
    def test_get_tv_metadata(self, mock_requests):
        tv_datas = [{
            "title_id": 589935,
            "cast": "Hua Yueh, Feng Ku, Kai Kang",
            "channel_name": "CELESTIAL",
            "director": "Han Hsiang Li",
            "genres": ["hk drama"],
            "image": "",
            "imdb_image": "",
            "production_year": 1976,
            "program_title": "MOODS OF LOVE",
            "synopsis_en": "A sinfully entertaining Sung Dynasty period piece which shows just how far people will go for love or lust.",
            "synopsis_th": "ภาพยนตร์เกี่ยวกับความรักและความปรารถนาต้องห้ามในยุคราชวงศ์ซ่ง -- นำแสดงโดย โจวหยินหยิน, เชอร์ลี่ย์ ยู, และ เยี่ยหัว",
            "type": "Movie",
            "video": ""
        }]
        mock_response = {"code": 200, "length": 1, "data": tv_datas}
        mock_requests.get.return_value = MockResponse(
            data=json.dumps(mock_response))

        title_id = "589935"
        expected = tv_datas

        actual = tv_utils.get_tv_metadata(token="token", title_ids=[title_id])
        self.assertEqual(actual, expected)

    @patch("rcommsutils.cms.tv_utils.requests")
    def test_get_tv_metadata_with_missing_item(self, mock_requests):
        mock_requests.exceptions.Timeout = Timeout
        mock_requests.get.side_effect = [Exception]

        title_id = "589935"
        expected = []

        actual = tv_utils.get_tv_metadata(token="token", title_ids=[title_id])
        self.assertEqual(actual, expected)

    @patch("rcommsutils.cms.tv_utils.requests")
    def test_get_tv_metadata_with_101_items(self, mock_requests):
        title_ids = ["589935"] * 101
        expected = 2
        tv_utils.get_tv_metadata(token="token", title_ids=title_ids)
        self.assertEqual(expected, mock_requests.get.call_count)

    @patch("rcommsutils.cms.tv_utils.requests")
    def test_get_tv_metadata_with_correct_url(self, mock_requests):
        title_ids = ["589935", "123456"]
        token = "my_token"
        expected_url = f"{ste.CMS_FN_EPG_URL}?title_id=589935,123456"
        expected_headers = {"Authorization": token}
        tv_utils.get_tv_metadata(token=token, title_ids=title_ids)
        mock_requests.get.assert_called_once_with(expected_url,
                                                  headers=expected_headers,
                                                  timeout=1.0)

    @patch("rcommsutils.cms.tv_utils.requests")
    def test_get_tv_metadata_with_duplicate_items(self, mock_requests):
        tv_datas = [{
            "title_id": 589935,
            "cast": "Hua Yueh, Feng Ku, Kai Kang",
            "channel_name": "CELESTIAL",
            "director": "Han Hsiang Li",
            "genres": ["hk drama"],
            "image": "",
            "imdb_image": "",
            "production_year": 1976,
            "program_title": "MOODS OF LOVE",
            "synopsis_en": "A sinfully entertaining Sung Dynasty period piece which shows just how far people will go for love or lust.",
            "synopsis_th": "ภาพยนตร์เกี่ยวกับความรักและความปรารถนาต้องห้ามในยุคราชวงศ์ซ่ง -- นำแสดงโดย โจวหยินหยิน, เชอร์ลี่ย์ ยู, และ เยี่ยหัว",
            "type": "Movie",
            "video": "", },
            {
                "title_id": 589935,
                "cast": "2",
                "channel_name": "2",
                "director": "2",
                "genres": ["hk drama"],
                "image": "",
                "imdb_image": "",
                "production_year": 0,
                "program_title": "2",
                "synopsis_en": "2",
                "synopsis_th": "2",
                "type": "Movie",
                "video": "",
            }]

        mock_response = {"code": 200, "length": 2, "data": tv_datas}
        mock_requests.get.return_value = MockResponse(
            data=json.dumps(mock_response))

        title_id = "589935"
        expected = tv_datas

        actual = tv_utils.get_tv_metadata(token="token", title_ids=[title_id])
        self.assertEqual(actual, expected)

    @patch("rcommsutils.cms.tv_utils._to_timestamp")
    @patch("rcommsutils.cms.tv_utils._validate_tv_interval_time")
    def test_get_latest_tv_items_happy_path(self,
                                            mock_validate_tv_interval_time,
                                            mock_to_timestamp):
        mock_validate_tv_interval_time.return_value = (
            '2019-02-11T07:00:00.000Z', '2019-02-11T08:00:00.000Z', True)
        es_result = {
            'took': 5,
            'timed_out': False,
            '_shards': {
                'total': 3,
                'successful': 3,
                'failed': 0
            },
            'hits': {
                'total': 1,
                'max_score': 0.0,
                'hits': [{
                    '_index': 'contents-epg-v4',
                    '_type': 'data',
                    '_id': 'en-4Ww58RjQgezy',
                    '_score': 0.0,
                    '_source': {
                        'detail': '',
                        'thumb': 'https://epgthumb.dmpcdn.com/thumbnail_large/121/20190211/20190211_140000.jpg',
                        'title_id': '512223',
                        'title': 'The Show : All About K-Pop',
                        'channel_code': '121',
                        'id': '4Ww58RjQgezy',
                        'start_date': '2019-02-11T07:00:00.000Z',
                        'end_date': '2019-02-11T07:00:00.000Z',
                    }
                }]
            }
        }
        es_session = MagicMock()
        es_session.search.return_value = es_result

        expected = [{
            "thumbnail": "https://epgthumb.dmpcdn.com/thumbnail_large/121/20190211/20190211_140000.jpg",
            "cmsTitleId": "4Ww58RjQgezy",
            "titleId": "512223",
            "startTime": "1549843200",
            "endTime": "1549843200",
            "cmsChannelCode": "121",
            "name": "The Show : All About K-Pop",
            "desc": "",
        }]
        start_time = 1549800000
        end_time = 1549843200
        mock_to_timestamp.side_effect = [str(end_time), str(end_time)]
        actual = tv_utils.get_latest_tv_items(es_session,
                                              start_time=start_time,
                                              end_time=end_time)
        self.assertEqual(actual, expected)

    def test_get_latest_tv_items_without_start_time(self):
        es_session = MagicMock()
        end_time = 1549843200

        with self.assertRaises(TypeError):
            tv_utils.get_latest_tv_items(es_session, end_time=end_time)

    @patch("rcommsutils.cms.tv_utils.datetime")
    def test_get_latest_tv_items_without_end_time(self, mock_datetime):
        es_session = MagicMock()
        start_time = 1549800000
        expected_end_time = start_time + 3600
        expected = call(expected_end_time)

        tv_utils.get_latest_tv_items(es_session, start_time=start_time)
        actual = mock_datetime.datetime.fromtimestamp.call_args

        self.assertEqual(actual, expected)

    def test_get_latest_tv_items_with_start_time_is_more_than_end_time(self):
        es_session = MagicMock()
        start_time = 1549803600
        end_time = 1549800000

        with self.assertRaises(ValueError):
            tv_utils.get_latest_tv_items(es_session,
                                         start_time=start_time,
                                         end_time=end_time)

    def test_get_latest_tv_items_with_start_time_is_not_int(self):
        es_session = MagicMock()
        start_time = "1549803600"

        with self.assertRaises(TypeError):
            tv_utils.get_latest_tv_items(es_session, start_time=start_time)

    def test_get_latest_tv_items_with_end_time_is_not_int(self):
        es_session = MagicMock()
        start_time = 1549803600
        end_time = "1549800000"

        with self.assertRaises(TypeError):
            tv_utils.get_latest_tv_items(es_session,
                                         start_time=start_time,
                                         end_time=end_time)

    @patch('rcommsutils.cms.tv_utils._validate_tv_interval_time')
    def test_get_latest_tv_items_should_query_with_start_time_end_time_and_sort_by_asc(
            self, mock_validate_tv_interval_time):
        es_session = MagicMock()
        start_time = 1549800000
        end_time = 1549803600
        sort_field = Fields.END_DATE
        sort_by = "asc"
        size = 10
        mock_validate_tv_interval_time.return_value = (
            '2019-02-10T19:00:00.000Z', '2019-02-10T19:20:00.000Z', True)

        expected_dsl = {
            'sort': {
                sort_field: {
                    'order': sort_by
                }
            },
            'query': {
                'bool': {
                    'filter': {
                        'bool': {
                            'must': [
                                {'range': {'publish_date': {'lte': 'now/h'}}},
                                {'match': {'status': 'publish'}},
                                {'match': {'searchable': 'Y'}},
                                {"term": {'lang': 'en'}},
                                {'range': {'end_date': {'gt': '2019-02-10T19:00:00.000Z'}}},
                                {'range': {'start_date': {'lte': '2019-02-10T19:00:00.000Z'}}
                                 },
                            ],
                            'must_not': [
                                {'match_phrase': {'title': 'empty title'}},
                                {"term": {'black_out':1}},
                            ]
                        }
                    }
                }
            }
        }

        tv_utils.get_latest_tv_items(es_session,
                                     start_time=start_time,
                                     end_time=end_time,
                                     sort_field=sort_field,
                                     sort_by=sort_by)
        es_session.search.assert_called_once_with(
            index=ste.RCOM_ES_EPG_INDEX,
            body=expected_dsl,
            _source=tv_utils._get_tv_return_fields(),
            size=size,
            timeout="10s")

    @patch("rcommsutils.cms.tv_utils._validate_tv_interval_time")
    def test_get_latest_tv_items_should_query_with_sort_by_desc(
            self, mock_validate_tv_interval_time):
        es_session = MagicMock()
        start_time = 1549800000
        end_time = 1549803600
        sort_field = Fields.END_DATE
        sort_by = "desc"
        size = 10
        mock_validate_tv_interval_time.return_value = (
            '2019-02-10T19:00:00.000Z', '2019-02-10T20:00:00.000Z', True)
        expected_dsl = {
            'sort': {
                sort_field: {
                    'order': sort_by
                }
            },
            'query': {
                'bool': {
                    'filter': {
                        'bool': {
                            'must': [
                                {'range': {'publish_date': {'lte': 'now/h'}}},
                                {'match': {'status': 'publish'}},
                                {'match': {'searchable': 'Y'}},
                                {"term": {'lang': 'en'}},
                                {'range': {'end_date': {'gt': '2019-02-10T19:00:00.000Z'}}},
                                {'range': {'start_date': {'lte': '2019-02-10T19:00:00.000Z'}}},
                            ],
                            'must_not': [{'match_phrase': {'title': 'empty title'}},
                                         {"term": {"black_out": 1}},
                            ],
                        }
                    }
                }
            }
        }

        tv_utils.get_latest_tv_items(es_session,
                                     start_time=start_time,
                                     end_time=end_time,
                                     sort_field=sort_field,
                                     sort_by=sort_by)
        es_session.search.assert_called_once_with(
            index=ste.RCOM_ES_EPG_INDEX,
            body=expected_dsl,
            _source=tv_utils._get_tv_return_fields(),
            size=size,
            timeout="10s")

    @patch('rcommsutils.cms.tv_utils.datetime.datetime')
    def test_get_tv_items_when_start_time_is_minus_one_should_call_get_timestamp(
            self, mock_datetime):
        """test_get_tv_items_when_start_time_is_minus_one_should_call_get_timestamp
        This feature refer to https://truedmp.atlassian.net/browse/DMPREC-713
        """
        session = MagicMock()
        expected_title_ids = ["item_1", "item_2"]
        tv_utils.get_tv_items(session,
                              title_ids=expected_title_ids,
                              start_time=-1)
        mock_datetime.utcnow.return_value.timestamp.assert_called_once()

    @patch('rcommsutils.cms.tv_utils.datetime.datetime')
    def test_get_tv_items_when_start_time_is_minus_one_start_time_should_be_current_time(
            self, mock_datetime):
        """test_get_tv_items_when_start_time_is_minus_one_start_time_should_be_current_time
        This feature refer to https://truedmp.atlassian.net/browse/DMPREC-713
        """
        session = MagicMock()
        current_time = 1558417738
        end_time_default_padding = 3600
        mock_datetime.utcnow.return_value.timestamp.return_value = current_time
        expected_title_ids = ["item_1", "item_2"]

        tv_utils.get_tv_items(session,
                              title_ids=expected_title_ids,
                              start_time=-1)

        actual_calls = mock_datetime.fromtimestamp.call_args_list
        expected_calls = [
            call(current_time),
            call(current_time + end_time_default_padding)
        ]
        self.assertEqual(actual_calls, expected_calls)

    @patch('rcommsutils.cms.tv_utils.datetime.datetime')
    def test_get_tv_items_when_start_time_is_minus_one_should_search_all_overlap_programs(
            self, mock_datetime):
        """test_get_tv_items_when_start_time_is_minus_one_should_search_all_overlap_programs
        This feature refer to https://truedmp.atlassian.net/browse/DMPREC-713
        """
        session = MagicMock()
        expected_title_ids = ["item_1", "item_2"]
        expected_start_time = '2019-05-21T01:01:00.000Z'
        expected_end_time = '2019-05-21T01:00:00.000Z'
        expected_start_date = [{'range': {'start_date': {'lte': expected_end_time}}}]
        expected_end_date = [{'range': {'end_date': {'gt': expected_start_time}}}]
        mock_datetime.fromtimestamp.return_value.strftime.side_effect = [
            expected_start_time, expected_end_time
        ]

        tv_utils.get_tv_items(session,
                              title_ids=expected_title_ids,
                              start_time=-1)

        _, kwargs = session.search.call_args
        actual_must_conditions = kwargs['body']['query']['bool']['filter'][
            'bool']['must']
        actual_start_date = list(
            filter(
                lambda condition: 'range' in condition and 'start_date' in
                                  condition['range'], actual_must_conditions))
        actual_end_date = list(
            filter(
                lambda condition: 'range' in condition and 'end_date' in
                                  condition['range'], actual_must_conditions))

        self.assertEqual(actual_start_date, expected_start_date)
        self.assertEqual(actual_end_date, expected_end_date)

    @patch('rcommsutils.cms.tv_utils.datetime.datetime')
    def test_get_latest_tv_items_when_start_time_is_minus_one_should_call_get_timestamp(
            self, mock_datetime):
        """test_get_latest_tv_items_when_start_time_is_minus_one_should_call_get_timestamp
        This feature refer to https://truedmp.atlassian.net/browse/DMPREC-713
        """
        session = MagicMock()
        tv_utils.get_latest_tv_items(session, start_time=-1)
        mock_datetime.utcnow.return_value.timestamp.assert_called_once()

    @patch('rcommsutils.cms.tv_utils.datetime.datetime')
    def test_get_latest_tv_items_when_start_time_is_minus_one_start_time_should_be_current_time(
            self, mock_datetime):
        """test_get_latest_tv_items_when_start_time_is_minus_one_start_time_should_be_current_time
        This feature refer to https://truedmp.atlassian.net/browse/DMPREC-713
        """
        session = MagicMock()
        current_time = 1558417738
        end_time_default_padding = 3600
        mock_datetime.utcnow.return_value.timestamp.return_value = current_time

        tv_utils.get_latest_tv_items(session, start_time=-1)

        actual_calls = mock_datetime.fromtimestamp.call_args_list
        expected_calls = [
            call(current_time),
            call(current_time + end_time_default_padding)
        ]
        self.assertEqual(actual_calls, expected_calls)

    @patch('rcommsutils.cms.tv_utils.datetime.datetime')
    def test_get_latest_tv_items_when_start_time_is_minus_one_should_search_all_overlap_programs(
            self, mock_datetime):
        """test_get_latest_tv_items_when_start_time_is_minus_one_should_search_all_overlap_programs
        This feature refer to https://truedmp.atlassian.net/browse/DMPREC-713
        """
        session = MagicMock()
        expected_start_time = '2019-05-21T01:01:00.000Z'
        expected_end_time = '2019-05-21T01:00:00.000Z'
        expected_start_date = [{
            'range': {
                'start_date': {
                    'lte': expected_end_time
                }
            }
        }]
        expected_end_date = [{
            'range': {
                'end_date': {
                    'gt': expected_start_time
                }
            }
        }]
        mock_datetime.fromtimestamp.return_value.strftime.side_effect = [
            expected_start_time, expected_end_time
        ]

        tv_utils.get_latest_tv_items(session, start_time=-1)

        _, kwargs = session.search.call_args
        actual_must_conditions = kwargs['body']['query']['bool']['filter'][
            'bool']['must']
        actual_start_date = list(
            filter(
                lambda condition: 'range' in condition and 'start_date' in
                                  condition['range'], actual_must_conditions))
        actual_end_date = list(
            filter(
                lambda condition: 'range' in condition and 'end_date' in
                                  condition['range'], actual_must_conditions))

        self.assertEqual(actual_start_date, expected_start_date)
        self.assertEqual(actual_end_date, expected_end_date)

    def test_get_item_tv_titles(self):
        session = MagicMock()
        item_ids = [
            'tv_program_1', 'tv_program_2', 'tv_program_3', 'tv_program_4',
            'tv_program_5'
        ]
        language = "EN"
        dsl = {
            "query": {
                "constant_score": {
                    "filter": {
                        "bool": {
                            "must": [{
                                "terms": {
                                    "_id": [
                                        'en-tv_program_1', 'en-tv_program_2',
                                        'en-tv_program_3', 'en-tv_program_4',
                                        'en-tv_program_5'
                                    ]
                                }
                            }]
                        }
                    }
                }
            }
        }

        tv_utils.get_item_titles(session,
                                 item_ids,
                                 language,
                                 index=ste.RCOM_ES_EPG_INDEX)
        session.search.assert_called_once_with(index=ste.RCOM_ES_EPG_INDEX,
                                               body=dsl,
                                               size=len(item_ids),
                                               _source=["id", "title"])

    @patch("rcommsutils.cms.tv_utils.requests")
    def test_get_tv_top_hit(self, mock_requests):
        tv_datas = [{
            "appid": {
                "antv1": 13704,
                "antvhb1": 1242,
                "hlite": 108,
                "htv": 296,
                "trueidcast": 29,
                "trueidmobile": 1054,
                "trueidv2mobile": 24,
                "trueidv2web": 1,
                "trueidweb": 803
            },
            "content_id": "d43",
            "total": 17261,
            "order": 1,
            "recommend": False,
            "id": "wKngqJ2Vqnl",
            "title": "โมโน 29",
            "status": "publish",
            "publish_date": "2018-03-29T04:16:00.000Z",
            "lang": "th",
            "slug": "mono29",
            "channel_code": "d43",
            "channel_info": {
                "channel_name_cbd": "មូណូ ធីវី",
                "channel_name_eng": "Mono 29",
                "channel_name_mm": "Mono 29",
                "channel_name_th": "โมโน 29"
            },
            "stream_name": None,
            "league_code": None,
            "clip_type": None,
            "stream_endpoint": "live",
            "movie_type": None,
            "stream_info": None,
            "package_code": None,
            "tv_show_code": None,
            "setting": {
                "fnevent_watch": "watch-erd24101801",
                "h1": "โมโน 29 - ดูทีวีออนไลน์",
                "h1_en": "Mono 29 - TV Online",
                "h1_th": "โมโน 29 - ดูทีวีออนไลน์",
                "meta_description": "MONO 29 (Digital TV) ประเภทรายการวาไรตี้",
                "meta_title": "ดูทีวีออนไลน์ โมโน 29 Mono 29 - TrueID TV",
                "watch_duration": "180"
            },
            "thumb": "https://cms.dmpcdn.com/livetv/2019/01/10/35a35017-8473-4953-8474-5c58d805b74a.png",
            "thumb_list": {
                "channel_stripe": "",
                "landscape_image": "",
                "ott_landscape": "",
                "ott_portrait": "",
                "portrait_image": "",
                "poster": "",
                "square_image": "",
                "trueid_landscape": ""
            },
            "black_out": 1,
            "blackout_start_date": "2017-08-26T10:30:00.000Z",
            "blackout_end_date": "2017-08-26T12:00:00.000Z",
            "blackout_message": "เนื่องด้วยเหตุผลทางลิขสิทธิ์จึงไม่สามารถรับชมรายการดังกล่าวในช่วงเวลานี้ได้ ส่วนรายการอื่นยังสามารถรับชมได้ตามปกติ จึงขออภัยในความไม่สะดวก ณ โอกาสนี้",
            "subscriptionoff_requirelogin": "no",
            "allow_chrome_cast": "yes",
            "geo_block": 1,
            "drm": "AES_128",
            "subscription_tiers": ["non_login", "basic", "premium"],
            "is_premium": "no",
            "on_shelf": None,
            "off_shelf": None,
            "expire_date": None,
            "epgs": None,
            "metadatas": None
        }]
        mock_response = {"code": 200, "length": 1, "data": tv_datas}
        mock_requests.get.return_value = MockResponse(
            data=json.dumps(mock_response))

        expected = tv_datas

        actual = tv_utils.get_tv_top_hit(token="token")
        self.assertEqual(actual, expected)

    @patch("rcommsutils.cms.tv_utils.requests")
    def test_get_tv_top_hit_with_recommend_false(self, mock_requests):
        token = "my_token"
        expected_url = f"{ste.CMS_FN_TRENDING_BY_CCU}"
        expected_headers = {"Authorization": token}
        expected_payload = {'lang': 'th',
                            'limit': 50,
                            'recommend': 'false',
                            'app_name': 'aa'}
        tv_utils.get_tv_top_hit(token=token, recommend=False, app_name='aa')
        mock_requests.get.assert_called_once_with(expected_url,
                                                  headers=expected_headers,
                                                  params=expected_payload,
                                                  timeout=1.0)

    def test_set_cache_tv_items_should_set_items_with_correct_name(self):
        redis_session = MagicMock()
        items = [{"_id": "id_1"}, {"_id": "id_2"}]
        tv_utils._set_cache_tv_items(redis_session,
                                     items=items,
                                     key_id="_id",
                                     prefix=ste.REDIS_MOVIE_PREFIX)
        actual_calls = redis_session.set.call_args_list
        expected_calls = [
            call(name=f"{ste.REDIS_MOVIE_PREFIX}id_1",
                 value=json.dumps(items[0]),
                 px=ste.REDIS_DEFAULT_EXPIRE_MS),
            call(name=f"{ste.REDIS_MOVIE_PREFIX}id_2",
                 value=json.dumps(items[1]),
                 px=ste.REDIS_DEFAULT_EXPIRE_MS)
        ]
        self.assertEqual(expected_calls, actual_calls)

    def test_set_cache_tv_items_with_default_expire_ms(self):
        redis_session = MagicMock()
        items = [{"_id": "id_1"}]
        tv_utils._set_cache_tv_items(redis_session,
                                     items=items,
                                     key_id="_id",
                                     prefix=ste.REDIS_MOVIE_PREFIX,
                                     expire_ms=20)
        redis_session.set.assert_called_once_with(
            name=f"{ste.REDIS_MOVIE_PREFIX}id_1",
            value=json.dumps(items[0]),
            px=20)

    def test_set_cache_tv_items_with_item_expire_date_less_than_dafault_default_expire_ms(
            self):
        redis_session = MagicMock()
        items = [{
            "_id": "id_1",
            "_source": {
                "expire_date": "2020-03-12T09:34:00.000Z"
            }
        }]

        tv_utils._set_cache_tv_items(redis_session,
                                     items=items,
                                     key_id="_id",
                                     prefix=ste.REDIS_MOVIE_PREFIX,
                                     expire_ms=20)
        redis_session.set.assert_called_once_with(
            name=f"{ste.REDIS_MOVIE_PREFIX}id_1",
            value=json.dumps(items[0]),
            px=20)

    def test_set_cache_tv_items_with_redis_session_is_None(self):
        redis_session = None
        items = [{"_id": "id_1"}]
        try:
            tv_utils._set_cache_tv_items(redis_session,
                                         items=items,
                                         key_id="_id",
                                         prefix=ste.REDIS_MOVIE_PREFIX,
                                         expire_ms=20)
        except Exception:
            self.fail("Failed when calling _set_cache_items")

    def test_set_cache_tv_items_when_redis_session_raises(self):
        redis_session = MagicMock()
        redis_session.set.side_effect = Exception()
        items = [{"_id": "id_1"}]
        try:
            tv_utils._set_cache_tv_items(redis_session,
                                         items=items,
                                         key_id="_id",
                                         prefix=ste.REDIS_MOVIE_PREFIX,
                                         expire_ms=20)
        except Exception:
            self.fail("Failed when calling _set_cache_items")

    def test_set_cache_tv_items_with_id(self):
        redis_session = MagicMock()
        items = [{"id": "id_1"}]
        tv_utils._set_cache_tv_items(redis_session,
                                     items=items,
                                     key_id="_id",
                                     prefix=ste.REDIS_MOVIE_PREFIX,
                                     expire_ms=20)
        redis_session.set.assert_called_once_with(
            name=f"{ste.REDIS_MOVIE_PREFIX}th-id_1",
            value=json.dumps(items[0]),
            px=20)

    def test_set_cache_tv_items_with_type_string(self):
        redis_session = MagicMock()
        items = ["id_1"]
        tv_utils._set_cache_tv_items(redis_session,
                                     items=items,
                                     key_id="_id",
                                     prefix=ste.REDIS_MOVIE_PREFIX,
                                     expire_ms=20)
        redis_session.set.assert_not_called()

    def test_set_cache_tv_items_with_incorrect_id(self):
        redis_session = MagicMock()
        items = [{"incorrect_id": "id_1"}, {"id": "id_2"}]
        tv_utils._set_cache_tv_items(redis_session,
                                     items=items,
                                     key_id="_id",
                                     prefix=ste.REDIS_MOVIE_PREFIX,
                                     expire_ms=20)
        redis_session.set.assert_called_once_with(
            name=f"{ste.REDIS_MOVIE_PREFIX}th-id_2",
            value=json.dumps(items[1]),
            px=20)

    def test_set_cache_tv_items_with_title_id(self):
        redis_session = MagicMock()
        items = [{"title_id": "title_id_1"}]
        tv_utils._set_cache_tv_items(redis_session,
                                     items=items,
                                     key_id="title_id",
                                     prefix=ste.REDIS_MOVIE_PREFIX,
                                     expire_ms=20)
        redis_session.set.assert_called_once_with(
            name=f"{ste.REDIS_MOVIE_PREFIX}th-title_id_1",
            value=json.dumps(items[0]),
            px=20)


if __name__ == '__main__':
    ut.main()
