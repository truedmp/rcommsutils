#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 19 12:15:37 2017

@author: root
"""

import orjson as json
import unittest as ut
from unittest.mock import call, patch, MagicMock

import rcommsutils.cms.settings as ste
from rcommsutils.cms import article_utils
from rcommsutils.cms.article_utils import Fields, ContentTypes


class MockResponse:

    def __init__(self, data, exception=None):
        self.data = data
        self.exception = exception

    def raise_for_status(self):
        if self.exception:
            raise self.exception

    def json(self):
        return json.loads(self.data)


class ArticleUtilsTest(ut.TestCase):

    def test_build_result_set(self):

        stub_tv_item = {
            Fields.TIMESTAMP: "item_timestamp",
            Fields.ARTICLE_CATEGORY: "item_article_category",
            Fields.CONTENT_TYPE: "item_content_type",
            Fields.COUNT_LIKES: "item_count_likes",
            Fields.COUNT_RATINGS: "item_count_ratings",
            Fields.COUNT_VIEWS: "item_count_views",
            Fields.CREATE_BY: "item_create_by",
            Fields.CREATE_BY_SSOID: "item_create_by_ssoid",
            Fields.CREATE_DATE: "item_create_date",
            Fields.DETAIL: "item_detail",
            Fields.ID: "item_id",
            Fields.LANG: "item_lang",
            Fields.ORIGINAL_ID: "item_original_id",
            Fields.PUBLISH_DATE: "item_publish_date",
            Fields.SEARCHABLE: "item_searchable",
            Fields.SOURCE_URL: "item_source_url",
            Fields.STATUS: "item_status",
            Fields.TAGS: "item_tags",
            Fields.THUMBNAIL: "item_thumb",
            Fields.TITLE: "item_title",
            Fields.UPDATE_BY: "item_update_by",
            Fields.UPDATE_BY_SSOID: "item_update_by_ssoid",
            Fields.UPDATE_DATE: "item_update_date",
        }
        expected = [{
            ste.RCOM_API_ARTICLE_TIMESTAMP: "item_timestamp",
            ste.RCOM_API_ARTICLE_CONTENT_TYPE: "item_content_type",
            ste.RCOM_API_ARTICLE_COUNT_LIKES: "item_count_likes",
            ste.RCOM_API_ARTICLE_COUNT_RATINGS: "item_count_ratings",
            ste.RCOM_API_ARTICLE_COUNT_VIEWS: "item_count_views",
            ste.RCOM_API_ARTICLE_CREATE_BY: "item_create_by",
            ste.RCOM_API_ARTICLE_CREATE_BY_SSOID: "item_create_by_ssoid",
            ste.RCOM_API_ARTICLE_CREATE_DATE: "item_create_date",
            ste.RCOM_API_ARTICLE_DETAIL: "item_detail",
            ste.RCOM_API_ARTICLE_ID: "item_id",
            ste.RCOM_API_ARTICLE_LANG: "item_lang",
            ste.RCOM_API_ARTICLE_ORIGINAL_ID: "item_original_id",
            ste.RCOM_API_ARTICLE_PUBLISH_DATE: "item_publish_date",
            ste.RCOM_API_ARTICLE_SOURCE_URL: "item_source_url",
            ste.RCOM_API_ARTICLE_STATUS: "item_status",
            ste.RCOM_API_ARTICLE_TAGS: "item_tags",
            ste.RCOM_API_ARTICLE_THUMBNAIL: "item_thumb",
            ste.RCOM_API_ARTICLE_TITLE: "item_title",
            ste.RCOM_API_ARTICLE_UPDATE_BY: "item_update_by",
            ste.RCOM_API_ARTICLE_UPDATE_BY_SSOID: "item_update_by_ssoid",
            ste.RCOM_API_ARTICLE_UPDATE_DATE: "item_update_date",
        }]
        items = [{"_source": stub_tv_item}]
        actual = article_utils._build_result_set(items)
        self.assertEqual(expected, actual)

    def test_build_result_set_empty_input(self):
        expected = []
        articles = []
        actual = article_utils._build_result_set(articles)
        self.assertEqual(expected, actual)

    def test_get_items_from_elasticsearch_article_no_criteria_specified_expect_exception_thrown(
            self):
        session = MagicMock()
        with self.assertRaisesRegex(ValueError, "'ids' must be specified"):
            article_utils._get_items_from_elasticsearch(session)

    def test_get_items_from_redis_should_call_redis_mget_with_prefix(self):
        redis_session = MagicMock()
        ids = ["id_1", "id_2"]
        prefix = "my_prefix:"
        expected = ["my_prefix:th-id_1", "my_prefix:th-id_2"]
        article_utils._get_items_from_redis(redis_session,
                                            ids=ids,
                                            prefix=prefix,
                                            language='TH')
        redis_session.mget.assert_called_once_with(expected)

    def test_get_items_from_redis_should_return_none_when_no_redis_session(
            self):
        redis_session = None
        ids = ["id_1", "id_2"]
        prefix = "my_prefix:"
        expected = [None, None]
        actual = article_utils._get_items_from_redis(redis_session,
                                                     ids=ids,
                                                     prefix=prefix)
        self.assertEqual(expected, actual)

    def test_get_items_from_redis_should_return_none_when_redis_raise_error(
            self):
        redis_session = MagicMock()
        redis_session.mget.side_effect = ConnectionError()
        ids = ["id_1", "id_2"]
        prefix = "my_prefix:"
        expected = [None, None]
        actual = article_utils._get_items_from_redis(redis_session,
                                                     ids=ids,
                                                     prefix=prefix)
        self.assertEqual(expected, actual)

    def test_get_items_from_redis_should_return_dict_items(self):
        redis_session = MagicMock()
        ids = ["id_1", "id_2"]
        prefix = "my_prefix:"
        expected = [{"id": "id_1"}, {"id": "id_2"}]
        redis_session.mget.return_value = [
            json.dumps({"id": "id_1"}),
            json.dumps({"id": "id_2"})
        ]
        actual = article_utils._get_items_from_redis(redis_session,
                                                     ids=ids,
                                                     prefix=prefix)
        self.assertEqual(expected, actual)

    def test_get_items_from_redis_should_return_None_if_item_not_found(self):
        redis_session = MagicMock()
        ids = ["id_1", "id_2"]
        prefix = "my_prefix:"
        expected = [{"id": "id_1"}, None]
        redis_session.mget.return_value = [json.dumps({"id": "id_1"}), None]
        actual = article_utils._get_items_from_redis(redis_session,
                                                     ids=ids,
                                                     prefix=prefix)
        self.assertEqual(expected, actual)

    def test_set_cache_items_should_set_items_with_correct_name(self):
        redis_session = MagicMock()
        items = [{"_id": "id_1"}, {"_id": "id_2"}]
        article_utils._set_cache_items(redis_session,
                                       items=items,
                                       prefix=ste.REDIS_ARTICLE_PREFIX)
        actual_calls = redis_session.set.call_args_list
        expected_calls = [
            call(name=f"{ste.REDIS_ARTICLE_PREFIX}id_1",
                 value=json.dumps(items[0]),
                 px=ste.REDIS_DEFAULT_EXPIRE_MS),
            call(name=f"{ste.REDIS_ARTICLE_PREFIX}id_2",
                 value=json.dumps(items[1]),
                 px=ste.REDIS_DEFAULT_EXPIRE_MS)
        ]
        self.assertEqual(expected_calls, actual_calls)

    def test_set_cache_items_with_expire_ms(self):
        redis_session = MagicMock()
        items = [{"_id": "id_1"}]
        article_utils._set_cache_items(redis_session,
                                       items=items,
                                       prefix=ste.REDIS_ARTICLE_PREFIX,
                                       expire_ms=20)
        redis_session.set.assert_called_once_with(
            name=f"{ste.REDIS_ARTICLE_PREFIX}id_1",
            value=json.dumps(items[0]),
            px=20)

    def test_set_cache_items_with_redis_session_is_None(self):
        redis_session = None
        items = [{"_id": "id_1"}]
        try:
            article_utils._set_cache_items(redis_session,
                                           items=items,
                                           prefix=ste.REDIS_ARTICLE_PREFIX,
                                           expire_ms=20)
        except Exception:
            self.fail("Failed when calling _set_cache_items")

    def test_set_cache_items_when_redis_session_raises(self):
        redis_session = MagicMock()
        redis_session.set.side_effect = Exception()
        items = [{"_id": "id_1"}]
        try:
            article_utils._set_cache_items(redis_session,
                                           items=items,
                                           prefix=ste.REDIS_ARTICLE_PREFIX,
                                           expire_ms=20)
        except Exception:
            self.fail("Failed when calling _set_cache_items")

    def test_set_cache_items_with_id(self):
        redis_session = MagicMock()
        items = [{"id": "id_1"}]
        article_utils._set_cache_items(redis_session,
                                       items=items,
                                       prefix=ste.REDIS_ARTICLE_PREFIX,
                                       expire_ms=20)
        redis_session.set.assert_called_once_with(
            name=f"{ste.REDIS_ARTICLE_PREFIX}th-id_1",
            value=json.dumps(items[0]),
            px=20)

    def test_set_cache_items_with_type_string(self):
        redis_session = MagicMock()
        items = ["id_1"]
        article_utils._set_cache_items(redis_session,
                                       items=items,
                                       prefix=ste.REDIS_ARTICLE_PREFIX,
                                       expire_ms=20)
        redis_session.set.assert_not_called()

    def test_set_cache_items_with_incorrect_id(self):
        redis_session = MagicMock()
        items = [{"incorrect_id": "id_1"}, {"id": "id_2"}]
        article_utils._set_cache_items(redis_session,
                                       items=items,
                                       prefix=ste.REDIS_ARTICLE_PREFIX,
                                       expire_ms=20)
        redis_session.set.assert_called_once_with(
            name=f"{ste.REDIS_ARTICLE_PREFIX}th-id_2",
            value=json.dumps(items[1]),
            px=20)

    def test_get_index_with_exists_content_type(self):
        content_types = [ContentTypes.DARA, ContentTypes.HOROSCOPE]
        expect = [
            f'{ste.RCOM_ES_ARTICLE_DARA_INDEX}',
            f'{ste.RCOM_ES_ARTICLE_HOROSCOPE_INDEX}'
        ]
        actual = article_utils._get_index(content_types)
        self.assertEqual(actual, expect)

    def test_get_index_with_empty_content_type(self):
        content_types = []
        expect = ste.RCOM_ES_ARTICLE_INDEX
        actual = article_utils._get_index(content_types)
        self.assertEqual(actual, expect)

    def test_get_index_with_not_exists_and_exists_content_type(self):
        content_types = [ContentTypes.DARA, 'test']
        expect = [
            f'{ste.RCOM_ES_ARTICLE_DARA_INDEX}',
        ]
        actual = article_utils._get_index(content_types)
        self.assertEqual(actual, expect)

    def test_get_index_with_not_exists_content_type(self):
        content_types = ['test1', 'test']
        expect = ste.RCOM_ES_ARTICLE_INDEX
        actual = article_utils._get_index(content_types)
        self.assertEqual(actual, expect)

    def test_get_default_must_conditions_without_publish_date(self):
        expect = [{
            "match": {
                Fields.SEARCHABLE: "Y"
            }
        }, {
            "term": {
                Fields.LANG: "th"
            }
        }, {
            "range": {
                Fields.PUBLISH_DATE: {
                    "gte": "now-3y/d"
                }
            }
        }]
        actual = article_utils._get_default_must_conditions()
        self.assertEqual(actual, expect)

    def test_get_default_must_conditions_with_publish_date(self):
        publish_date = "2020-01-24T03:00:00.00Z"
        expect = [{
            "match": {
                Fields.SEARCHABLE: "Y"
            }
        }, {
            "term": {
                Fields.LANG: "th"
            }
        }, {
            "range": {
                Fields.PUBLISH_DATE: {
                    "gte": "2020-01-24T03:00:00.00Z"
                }
            }
        }]
        actual = article_utils._get_default_must_conditions(publish_date)
        self.assertEqual(actual, expect)

    def test_get_default_must_conditions_with_publish_date_other_format(self):
        publish_date = "2020-01-24T03:00:00.00Z"
        expect = [{
            "match": {
                Fields.SEARCHABLE: "Y"
            }
        }, {
            "term": {
                Fields.LANG: "th"
            }
        }, {
            "range": {
                Fields.PUBLISH_DATE: {
                    "gte": "2020-01-24T03:00:00.00Z"
                }
            }
        }]
        actual = article_utils._get_default_must_conditions(publish_date)
        self.assertEqual(actual, expect)

    def test_get_validate_format_publish_date_with_correct_format(self):
        publish_date = "2020-01-24T03:00:00.00Z"
        expect = True
        actual = article_utils._get_validate_format_publish_date(publish_date)
        self.assertEqual(actual, expect)

    def test_get_validate_format_publish_date_with_incorrect_format(self):
        publish_date = "2020-01-24"
        expect = False
        actual = article_utils._get_validate_format_publish_date(publish_date)
        self.assertEqual(actual, expect)

    @patch("rcommsutils.cms.article_utils._build_result_set")
    @patch("rcommsutils.cms.article_utils._filter_items_from_conditions")
    @patch("rcommsutils.cms.article_utils._set_cache_items")
    @patch("rcommsutils.cms.article_utils._get_items_from_elasticsearch")
    @patch("rcommsutils.cms.article_utils._get_items_from_redis")
    def test_get_items_should_call_get_items_from_redis(
            self, mock_get_items_from_redis, mock_get_items_from_elasticsearch,
            mock_set_cache_items, mock_filter_items_from_conditions,
            mock_build_result_set):
        es_session = MagicMock()
        redis_session = MagicMock()
        ids = ["id_1", "id_2", "id_3"]
        article_utils.get_items(es_session, redis_session, ids=ids)
        mock_get_items_from_redis.assert_called_once_with(
            redis_session, ["id_1", "id_2", "id_3"],
            prefix=ste.REDIS_ARTICLE_PREFIX,
            language='TH')

    @patch("rcommsutils.cms.article_utils._build_result_set")
    @patch("rcommsutils.cms.article_utils._filter_items_from_conditions")
    @patch("rcommsutils.cms.article_utils._set_cache_items")
    @patch("rcommsutils.cms.article_utils._get_items_from_elasticsearch")
    @patch("rcommsutils.cms.article_utils._get_items_from_redis")
    def test_get_items_should_call_elastic_if_no_cached(
            self, mock_get_items_from_redis, mock_get_items_from_elasticsearch,
            mock_set_cache_items, mock_filter_items_from_conditions,
            mock_build_result_set):
        es_session = MagicMock()
        redis_session = MagicMock()
        mock_get_items_from_redis.return_value = [{
            "id": "id_1"
        }, None, {
            "id": "id_3"
        }]
        ids = ["id_1", "id_2", "id_3"]
        expected_ids = ["id_2"]
        article_utils.get_items(es_session,
                                redis_session,
                                ids=ids,
                                content_types=[],
                                status='publish',
                                publish_date=None,
                                timeout="10s")
        mock_get_items_from_elasticsearch.assert_called_once_with(
            es_session=es_session,
            ids=expected_ids,
            content_types=[],
            language='TH',
            status='publish',
            publish_date=None,
            timeout="10s")

    @patch("rcommsutils.cms.article_utils._build_result_set")
    @patch("rcommsutils.cms.article_utils._filter_items_from_conditions")
    @patch("rcommsutils.cms.article_utils._set_cache_items")
    @patch("rcommsutils.cms.article_utils._get_items_from_elasticsearch")
    @patch("rcommsutils.cms.article_utils._get_items_from_redis")
    def test_get_items_should_call_set_cache_items(
            self, mock_get_items_from_redis, mock_get_items_from_elasticsearch,
            mock_set_cache_items, mock_filter_items_from_conditions,
            mock_build_result_set):
        es_session = MagicMock()
        redis_session = MagicMock()
        expected_non_cached_items = [{"_source": {"id": "id_2"}}]
        mock_get_items_from_redis.return_value = [{
            "id": "id_1"
        }, None, {
            "id": "id_3"
        }]
        ids = ["id_1", "id_2", "id_3"]
        mock_get_items_from_elasticsearch.return_value = expected_non_cached_items
        article_utils.get_items(es_session, redis_session, ids=ids)
        mock_set_cache_items.assert_called_once_with(
            redis_session,
            items=expected_non_cached_items,
            prefix=ste.REDIS_ARTICLE_PREFIX,
            language='TH')

    @patch("rcommsutils.cms.article_utils._build_result_set")
    @patch("rcommsutils.cms.article_utils._filter_items_from_conditions")
    @patch("rcommsutils.cms.article_utils._set_cache_items")
    @patch("rcommsutils.cms.article_utils._get_items_from_elasticsearch")
    @patch("rcommsutils.cms.article_utils._get_items_from_redis")
    def test_get_items_should_call_filtered_items_from_condition(
            self, mock_get_items_from_redis, mock_get_items_from_elasticsearch,
            mock_set_cache_items, mock_filter_items_from_conditions,
            mock_build_result_set):
        es_session = MagicMock()
        redis_session = MagicMock()
        mock_get_items_from_redis.return_value = [{
            "_source": {
                "id": "id_1"
            }
        }, None, {
            "_source": {
                "id": "id_3"
            }
        }]
        ids = ["id_1", "id_2", "id_3"]
        expected_items = [{
            "_source": {
                "id": "id_1"
            }
        }, {
            "_source": {
                "id": "id_3"
            }
        }]
        article_utils.get_items(es_session,
                                redis_session,
                                ids=ids,
                                content_types=[],
                                status='publish',
                                publish_date=None,
                                timeout="10s")
        mock_filter_items_from_conditions.assert_called_once_with(
            items=expected_items, status='publish', publish_date=None)

    @patch("rcommsutils.cms.article_utils._build_result_set")
    @patch("rcommsutils.cms.article_utils._filter_items_from_conditions")
    @patch("rcommsutils.cms.article_utils._set_cache_items")
    @patch("rcommsutils.cms.article_utils._get_items_from_elasticsearch")
    @patch("rcommsutils.cms.article_utils._get_items_from_redis")
    def test_get_items_should_call_build_result_set(
            self, mock_get_items_from_redis, mock_get_items_from_elasticsearch,
            mock_set_cache_items, mock_filter_items_from_conditions,
            mock_build_result_set):
        es_session = MagicMock()
        redis_session = MagicMock()
        ids = ["id_1", "id_2", "id_3"]
        cached_items = [{
            "_source": {
                "id": "id_1"
            }
        }, {
            "_source": {
                "id": "id_3"
            }
        }]
        non_cached_items = [{"_source": {"id": "id_2"}}]
        expected_items = [{
            "_source": {
                "id": "id_1"
            }
        }, {
            "_source": {
                "id": "id_3"
            }
        }, {
            "_source": {
                "id": "id_2"
            }
        }]
        mock_get_items_from_redis.return_value = [
            expected_items[0], None, expected_items[2]
        ]
        mock_get_items_from_elasticsearch.return_value = non_cached_items
        mock_filter_items_from_conditions.return_value = cached_items
        article_utils.get_items(es_session,
                                redis_session,
                                ids=ids,
                                content_types=[],
                                status='publish',
                                publish_date=None,
                                timeout="10s")
        mock_build_result_set.assert_called_once_with(
            [*cached_items, *non_cached_items])

    def test_get_items_from_elasticsearch_articles_with_content_type(self):
        session = MagicMock()
        expected_dsl = {
            'query': {
                'bool': {
                    'filter': {
                        'bool': {
                            'must': [{
                                'match': {
                                    'searchable': 'Y'
                                }
                            }, {
                                "term": {
                                    'lang': "th"
                                }
                            }, {
                                'range': {
                                    'publish_date': {
                                        'gte': 'now-3y/d'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                'terms': {
                                    '_id': ["th-id_1", "th-id_2"]
                                }
                            }]
                        }
                    }
                }
            }
        }
        ids = ["id_1", "id_2"]
        expected_index = [
            f'{ste.RCOM_ES_ARTICLE_HOROSCOPE_INDEX}',
            f'{ste.RCOM_ES_ARTICLE_DARA_INDEX}'
        ]
        article_utils._get_items_from_elasticsearch(
            session, ids=ids, content_types=["horoscope", "dara"])
        session.search.assert_called_once_with(
            index=expected_index,
            _source=article_utils._API_COMMON_FIELDS,
            body=expected_dsl,
            size=2,
            timeout="10s")

    def test_get_items_from_elasticsearch_articles_with_id(self):
        session = MagicMock()
        expected_dsl = {
            'query': {
                'bool': {
                    'filter': {
                        'bool': {
                            'must': [{
                                'match': {
                                    'searchable': 'Y'
                                }
                            }, {
                                "term": {
                                    'lang': "th"
                                }
                            }, {
                                'range': {
                                    'publish_date': {
                                        'gte': 'now-3y/d'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'publish'
                                }
                            }, {
                                'terms': {
                                    '_id': ["th-id_1", "th-id_2"]
                                }
                            }]
                        }
                    }
                }
            }
        }
        ids = ["id_1", "id_2"]
        expected_index = ste.RCOM_ES_ARTICLE_INDEX
        article_utils._get_items_from_elasticsearch(session, ids=ids)
        session.search.assert_called_once_with(
            index=expected_index,
            _source=article_utils._API_COMMON_FIELDS,
            body=expected_dsl,
            size=2,
            timeout="10s")

    def test_get_items_from_elasticsearch_articles_with_status(self):
        session = MagicMock()
        expected_dsl = {
            'query': {
                'bool': {
                    'filter': {
                        'bool': {
                            'must': [{
                                'match': {
                                    'searchable': 'Y'
                                }
                            }, {
                                "term": {
                                    'lang': "th"
                                }
                            }, {
                                'range': {
                                    'publish_date': {
                                        'gte': 'now-3y/d'
                                    }
                                }
                            }, {
                                'match': {
                                    'status': 'private'
                                }
                            }, {
                                'terms': {
                                    '_id': ["th-id_1", "th-id_2"]
                                }
                            }]
                        }
                    }
                }
            }
        }
        ids = ["id_1", "id_2"]
        expected_index = ste.RCOM_ES_ARTICLE_INDEX
        article_utils._get_items_from_elasticsearch(session,
                                                    ids=ids,
                                                    status='private')
        session.search.assert_called_once_with(
            index=expected_index,
            _source=article_utils._API_COMMON_FIELDS,
            body=expected_dsl,
            size=2,
            timeout="10s")

    def test_to_timestamp_incorrect_format(self):
        datetime_string = "2020-01-24 03:00:00"
        with self.assertRaises(ValueError):
            article_utils._to_timestamp(datetime_string)

    def test_filter_items_from_conditions_with_status(self):
        items = [{
            "_source": {
                "id": "id_1",
                "status": 'private'
            }
        }, {
            "_source": {
                "id": "id_3",
                "status": 'public'
            }
        }]
        expect = [{"_source": {"id": "id_3", "status": 'public'}}]
        status = 'public'
        actual = article_utils._filter_items_from_conditions(items,
                                                             status=status,
                                                             publish_date=None)
        self.assertEqual(actual, expect)

    def test_filter_items_from_conditions_with_publish_date(self):
        items = [{
            "_source": {
                "id": "id_1",
                "publish_date": '2020-01-10T03:00:00.000Z',
                "status": 'public'
            }
        }, {
            "_source": {
                "id": "id_3",
                "publish_date": '2020-01-24T03:00:00.000Z',
                "status": 'public'
            }
        }]
        expect = [{
            "_source": {
                "id": "id_3",
                "publish_date": '2020-01-24T03:00:00.000Z',
                "status": 'public'
            }
        }]
        status = 'public'
        publish_date = '2020-01-15T03:00:00.000Z'
        actual = article_utils._filter_items_from_conditions(
            items, status=status, publish_date=publish_date)
        self.assertEqual(actual, expect)

    def test_add_lang_prefix_for_ids_list_en(self):
        ids = ['id_1', 'id_2', 'id_3']
        language = 'EN'
        expect = ['en-id_1', 'en-id_2', 'en-id_3']
        actual = article_utils._add_lang_prefix_for_ids(ids, language)
        self.assertListEqual(expect, actual)

    def test_add_lang_prefix_for_ids_list_th(self):
        ids = ['id_1', 'id_2', 'id_3']
        language = 'th'
        expect = ['th-id_1', 'th-id_2', 'th-id_3']
        actual = article_utils._add_lang_prefix_for_ids(ids, language)
        self.assertListEqual(expect, actual)

    def test_add_lang_prefix_for_ids_str_en(self):
        id = 'id_1'
        language = 'en'
        expect = 'en-id_1'
        actual = article_utils._add_lang_prefix_for_ids([id], language)[0]
        self.assertEquals(expect, actual)

    def test_add_lang_prefix_for_ids_str_th(self):
        id = 'id_1'
        language = 'TH'
        expect = 'th-id_1'
        actual = article_utils._add_lang_prefix_for_ids([id], language)[0]
        self.assertEquals(expect, actual)


if __name__ == '__main__':
    ut.main()
