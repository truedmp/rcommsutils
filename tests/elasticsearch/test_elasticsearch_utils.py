#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 26 09:18:22 2016

@author: Champ
"""
import unittest as ut
from unittest.mock import patch

from nose.tools import assert_true
from rcommsutils import elasticsearch_utils
from rcommsutils.elasticsearch import settings as ste

# Note that you need to connect to the dev environmenmt to run these tests


class ElasticSearchUtilsTest(ut.TestCase):

    @ut.skip("This is connect to Elasticsearch!")
    def test_get_connection_status(self):
        stats = elasticsearch_utils.get_connection_status()
        assert_true("cluster_name" in stats.keys())
        assert_true("nodes" in stats.keys())

        nodes = stats["nodes"]
        assert_true(type(nodes) == list)

        node = nodes[0]
        assert_true("ip" in node.keys())
        assert_true("roles" in node.keys())
        assert_true("status" in node.keys())

    @patch("rcommsutils.elasticsearch.elasticsearch_utils.Elasticsearch")
    def test_get_session_should_return_es_session(self, mock_elasticsearch):
        hosts = [{"host": "es_host_1", "port": 9200}]
        expected = "es_session"
        mock_elasticsearch.return_value = "es_session"
        actual = elasticsearch_utils.get_session(hosts, http_compress=True)
        self.assertEqual(expected, actual)

    @patch("rcommsutils.elasticsearch.elasticsearch_utils.Elasticsearch")
    def test_get_session_with_single_host(self, mock_elasticsearch):
        hosts = [{"host": "es_host_1", "port": 9200}]
        cert_path = "ca_ai_nonprod.crt"
        elasticsearch_utils.get_session(hosts, http_compress=True)
        mock_elasticsearch.assert_called_once_with(
            hosts,
            http_auth=(ste.RCOM_ES_USERNAME, ste.RCOM_ES_PASSWORD),
            use_ssl=True,
            verify_certs=True,
            ca_certs=cert_path,
            retry_on_timeout=True,
            max_retries=1,
            http_compress=True)

    @patch("rcommsutils.elasticsearch.elasticsearch_utils.Elasticsearch")
    def test_get_session_with_two_hosts(self, mock_elasticsearch):
        hosts = ["es_host_1:9200", "es_host_2:9200"]
        cert_path = "ca_ai_nonprod.crt"
        elasticsearch_utils.get_session(hosts, http_compress=True)
        mock_elasticsearch.assert_called_once_with(
            hosts,
            http_auth=(ste.RCOM_ES_USERNAME, ste.RCOM_ES_PASSWORD),
            use_ssl=True,
            verify_certs=True,
            ca_certs=cert_path,
            retry_on_timeout=True,
            max_retries=1,
            http_compress=True)


if __name__ == '__main__':
    ut.main()
