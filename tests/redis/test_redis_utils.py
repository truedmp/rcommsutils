import unittest as ut
from unittest.mock import patch, MagicMock
from rcommsutils import redis_utils


class RedisUtilsTest(ut.TestCase):

    def test_get_connection_status(self):
        redis = MagicMock()
        redis.info.return_value = {
            'redis_version': '4.0.11',
            'redis_mode': 'standalone'
        }
        redis.connection_pool.connection_kwargs = {'host': '10.198.107.106'}
        expected = {
            'version': '4.0.11',
            'mode': 'standalone',
            'status': 1,
            'ip': '10.198.107.106'
        }
        actual = redis_utils.get_connection_status(redis)

        self.assertEquals(expected, actual)

    @patch("rcommsutils.redis.redis_utils.Redis")
    def test_get_redis(self, mock_redis):
        host = "10.198.107.106"
        port = "7000"
        password = "abcd"
        redis_utils.get_redis(host=host, port=port, password=password)
        mock_redis.assert_called_once_with(host=host,
                                           port=port,
                                           db=0,
                                           password=password,
                                           decode_responses=True)

    def test_redis_cluster_get_connection_status(self):
        redis = MagicMock()
        redis.info.return_value = {
            '10.198.107.106:7000': {
                'redis_version': '5.0.7',
                'redis_mode': 'cluster'
            },
            '10.198.107.106:7001': {
                'redis_version': '5.0.7',
                'redis_mode': 'cluster'
            }
        }
        expected = {
            'version': ['5.0.7'],
            'mode': ['cluster'],
            'status': 1,
            'ip': ['10.198.107.106:7000', '10.198.107.106:7001']
        }
        actual = redis_utils.get_connection_status(redis, is_cluster=True)

        self.assertEquals(expected, actual)

    @patch("rcommsutils.redis.redis_utils.RedisCluster")
    def test_get_redis_cluster(self, mock_redis):
        host = "10.198.107.106"
        port = "7000"
        password = "abcd"
        redis_utils.get_redis(host=host,
                              port=port,
                              password=password,
                              is_cluster=True)
        mock_redis.assert_called_once_with(startup_nodes=[{
            "host": host,
            "port": port
        }],
                                           decode_responses=True,
                                           password=password)

    @patch("rcommsutils.redis.redis_utils.RedisCluster")
    def test_get_redis_cluster_with_startup_nodes(self, mock_redis):
        host = "10.198.107.106"
        port = "7000"
        startup_nodes = [{
            "host": "10.198.107.106",
            "port": "7000"
        }, {
            "host": "10.198.64.103",
            "port": "5000"
        }]
        password = "abcd"
        redis_utils.get_redis(host=host,
                              port=port,
                              password=password,
                              startup_nodes=startup_nodes,
                              is_cluster=True)
        mock_redis.assert_called_once_with(startup_nodes=[{
            "host": "10.198.107.106",
            "port": "7000"
        }, {
            "host": "10.198.64.103",
            "port": "5000"
        }],
                                           decode_responses=True,
                                           password=password)


if __name__ == '__main__':
    ut.main()
