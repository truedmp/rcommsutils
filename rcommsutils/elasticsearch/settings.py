#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Recommendation model server's Elasticsearch settings

Created on Fri Feb  3 21:50:18 2017

@author: Champ
"""

import os

DEV = "dev"
ALPHA = "alpha"
PREPRODUCTION = "preprod"
PRODUCTION = "prod"
PRODUCTION_CERT = 'ca.crt'
NON_PRODUCTION_CERT = 'ca_ai_nonprod.crt'


def _get_cert_path(environment: str, cert_folder: str) -> str:
    """
      Get certificate path for elasticsearch 7 based on environment
      :param environment: string of environment
      :param cert_folder: string of cert_path
      :return: certificate path
      """
    environment = environment.lower()
    assert environment in [DEV, ALPHA, PREPRODUCTION, PRODUCTION]

    if environment == PRODUCTION:
        return os.path.join(cert_folder, PRODUCTION_CERT)
    else:
        return os.path.join(cert_folder, NON_PRODUCTION_CERT)


# Elasticsearch configs
# https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-scroll.html
RCOM_ES_HOSTS = os.getenv("RCOM_ES_HOSTS",
                          'd2d-rec-esco-a01.dmp.true.th:9200').split(",")

RCOM_TEST_ES_HOSTS = os.getenv("RCOM_ES_HOSTS", '127.0.0.1:9200').split(",")

RCOM_ES_KEEPALIVE_MINS = int(os.getenv('RCOM_ES_KEEPALIVE_MINS', 1)) * 60
RCOM_ES_SNIFF_TIMEOUT_SEC = int(os.getenv('RCOM_ES_SNIFF_TIMEOUT_SEC', 10))
RCOM_ES_USERNAME = os.getenv('RCOM_ES_USERNAME', 'dev')
RCOM_ES_PASSWORD = os.getenv('RCOM_ES_PASSWORD', 'password')
RCOM_ES_MAX_RETRIES = int(os.getenv('RCOM_ES_MAX_RETRIES', 1))
ENVIRONMENT = os.getenv("ENVIRONMENT", DEV)
RCOM_ES_CERT_FOLDER = os.getenv("RCOM_ES_CERT_FOLDER", "")
RCOM_ES_CERT_PATH = _get_cert_path(ENVIRONMENT, RCOM_ES_CERT_FOLDER)
