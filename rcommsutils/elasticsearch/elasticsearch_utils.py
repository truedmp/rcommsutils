# -*- coding: utf-8 -*-
"""
A helper to get a session to the Elasticsearch cluster. Use get_session() to
get the session object.

@author: robroo, modified by Coco / Champ

"""
import time
import traceback
import threading
from typing import List, Dict, Union

import rcommsutils.elasticsearch.settings as ste

from elasticsearch import Elasticsearch
from rcomloggingutils.applog_utils import AppLogger

_logger = AppLogger(name=__name__)
_helper_session = None


class KeepAliveThread(threading.Thread):

    def __init__(self, session, *args, **kwargs):
        super(KeepAliveThread, self).__init__(*args, **kwargs)
        self.session = session
        _logger.info("Elasticsearch keep-alive thread created")

    def run(self):
        while True:
            try:
                # The documentation says sniff_hosts() will create a new
                # connection pool. However what it actually does in the code is
                # it gets all host info by calling each of them, try to reuse
                # existing connection objects to the hosts first if there are
                # no changes to the hosts. Then it recreates a connection pool
                # object over those connection objects. So this isn't so costly
                # as it might sound (creating a new connection pool). We can
                # use this to trigger calling APIs to the host to have some
                # activities on the connection and make sure they aren't dead.

                # See version 5.1.0 of the connector for the actual code.
                # https://github.com/elastic/elasticsearch-py/blob/master/elasticsearch/transport.py
                _logger.debug("Sniffing hosts")
                self.session.transport.sniff_hosts(False)
                time.sleep(ste.RCOM_ES_KEEPALIVE_MINS)
            except:
                _logger.error(traceback.format_exc())


def _run_keep_alive(session):
    thread = KeepAliveThread(session, daemon=True)
    thread.start()


def get_session(hosts: List[Union[str, Dict]],
                http_compress: bool = True,
                cert_path: str = ste.RCOM_ES_CERT_PATH,
                username: str = ste.RCOM_ES_USERNAME,
                password: str = ste.RCOM_ES_PASSWORD,
                max_retries: int = ste.RCOM_ES_MAX_RETRIES):
    """A utility function to get a new ES session. A session instance is a long-lived object and it should not be
    used in a request/response short-lived fashion.

    Parameters
    ----------
    hosts : list
        host/IP and port
        eg. [{'host': host, 'port': port}]

    http_compress: bool
        Option to compress http request using gzip (default: True)

    cert_path: string
        ES Certification path

    username: string
        ES user_name

    password: string
        ES password

    max_retries: int
        ES max retries

    Returns
    ----------
    an ES session
    """
    _logger.info(f"Connecting to Elasticsearch cluster at {hosts}")

    # There are options to config Elastic search to automatically sniff the
    # connections periodically for us (sniffer_timeout). But there seems to
    # be bugs with this version after several trial-and-error attempts. So
    # we need to manually create a thread to do this ourselves.
    session = Elasticsearch(
        hosts,
        http_auth=(username, password),
        use_ssl=True,
        verify_certs=True,
        ca_certs=cert_path,
        retry_on_timeout=True,
        max_retries=max_retries,
        http_compress=http_compress)

    return session


def get_connection_status():
    """
    Get connection status to all connected hosts
    """
    global _helper_session
    if _helper_session is None:
        _helper_session = get_session(ste.RCOM_ES_HOSTS)
    stats = _helper_session.nodes.stats(metric="http")
    connection_dict = {"cluster_name": stats["cluster_name"]}
    node_list = []

    for node in stats["nodes"]:
        node_dict = {
            "ip": stats["nodes"][node]["ip"],
            "roles": stats["nodes"][node]["roles"],
            "status": 1
        }
        node_list.append(node_dict)

    connection_dict["nodes"] = node_list
    return connection_dict
