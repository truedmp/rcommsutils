#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 18 22:08:20 2017

@author: root
"""
import orjson as json
from rcomloggingutils.applog_utils import AppLogger
from re import search
from typing import List
from datetime import datetime
from . import settings as ste

_logger = AppLogger(name=__name__)


# Utility class to refer to searchable fields in Elastic Search
class Fields:
    TIMESTAMP = '@timestamp'
    ARTICLE_CATEGORY = 'article_category'
    CONTENT_TYPE = 'content_type'
    COUNT_LIKES = 'count_likes'
    COUNT_RATINGS = 'count_ratings'
    COUNT_VIEWS = 'count_views'
    CREATE_BY = 'create_by'
    CREATE_BY_SSOID = 'create_by_ssoid'
    CREATE_DATE = 'create_date'
    DETAIL = 'detail'
    ID = 'id'
    ID_ = '_id'
    SOURCE_ = '_source'
    LANG = 'lang'
    ORIGINAL_ID = 'original_id'
    PUBLISH_DATE = 'publish_date'
    SEARCHABLE = 'searchable'
    SOURCE_URL = 'source_url'
    STATUS = 'status'
    TAGS = 'tags'
    THUMBNAIL = 'thumb'
    TITLE = 'title'
    UPDATE_BY = 'update_by'
    UPDATE_BY_SSOID = 'update_by_ssoid'
    UPDATE_DATE = 'update_date'


class ContentTypes:
    HOROSCOPE = 'horoscope'
    TNN = 'tnn'
    MOVIEARTICLE = 'moviearticle'
    MUSICARTICLE = 'musicarticle'
    WOMEN = 'women'
    SPORTARTICLE = 'sportarticle'
    DARA = 'dara'
    TRAVEL = 'travel'


class MapperIndex:
    mapper = {
        ContentTypes.HOROSCOPE: ste.RCOM_ES_ARTICLE_HOROSCOPE_INDEX,
        ContentTypes.TNN: ste.RCOM_ES_ARTICLE_TNN_INDEX,
        ContentTypes.MOVIEARTICLE: ste.RCOM_ES_ARTICLE_MOVIEARTICLE_INDEX,
        ContentTypes.MUSICARTICLE: ste.RCOM_ES_ARTICLE_MUSICARTICLE_INDEX,
        ContentTypes.WOMEN: ste.RCOM_ES_ARTICLE_WOMEN_INDEX,
        ContentTypes.SPORTARTICLE: ste.RCOM_ES_ARTICLE_SPORTARTICLE_INDEX,
        ContentTypes.DARA: ste.RCOM_ES_ARTICLE_DARA_INDEX,
        ContentTypes.TRAVEL: ste.RCOM_ES_ARTICLE_TRAVEL_INDEX
    }


_API_COMMON_FIELDS = [
    Fields.TIMESTAMP, Fields.ARTICLE_CATEGORY, Fields.CONTENT_TYPE,
    Fields.COUNT_LIKES, Fields.COUNT_RATINGS, Fields.COUNT_VIEWS,
    Fields.CREATE_BY, Fields.CREATE_BY_SSOID, Fields.CREATE_DATE, Fields.DETAIL,
    Fields.ID, Fields.LANG, Fields.ORIGINAL_ID, Fields.PUBLISH_DATE,
    Fields.SOURCE_URL, Fields.STATUS, Fields.TAGS, Fields.THUMBNAIL,
    Fields.TITLE, Fields.UPDATE_BY, Fields.UPDATE_BY_SSOID, Fields.UPDATE_DATE
]


def _build_result_set(items):
    """
    build an item result set of a given input language as specified in the API

    Parameters
    ----------
    items : an array of dictionary containing item details from ES

    Returns
    -------
    An array of dictionaries containing item details of a given input language as specified in the API
    """
    results = []
    for item_source in items:
        item = item_source['_source']
        item_dict = dict()
        # common fields
        item_dict[ste.RCOM_API_ARTICLE_TIMESTAMP] = item.get(Fields.TIMESTAMP)
        item_dict[ste.RCOM_API_ARTICLE_CONTENT_TYPE] = item.get(
            Fields.CONTENT_TYPE)
        item_dict[ste.RCOM_API_ARTICLE_CREATE_BY] = item.get(Fields.CREATE_BY)
        item_dict[ste.RCOM_API_ARTICLE_CREATE_BY_SSOID] = item.get(
            Fields.CREATE_BY_SSOID)
        item_dict[ste.RCOM_API_ARTICLE_CREATE_DATE] = item.get(
            Fields.CREATE_DATE)
        item_dict[ste.RCOM_API_ARTICLE_DETAIL] = item.get(Fields.DETAIL)
        item_dict[ste.RCOM_API_ARTICLE_ID] = item.get(Fields.ID)
        item_dict[ste.RCOM_API_ARTICLE_LANG] = item.get(Fields.LANG)
        item_dict[ste.RCOM_API_ARTICLE_ORIGINAL_ID] = item.get(
            Fields.ORIGINAL_ID)
        item_dict[ste.RCOM_API_ARTICLE_PUBLISH_DATE] = item.get(
            Fields.PUBLISH_DATE)
        item_dict[ste.RCOM_API_ARTICLE_SOURCE_URL] = item.get(Fields.SOURCE_URL)
        item_dict[ste.RCOM_API_ARTICLE_STATUS] = item.get(Fields.STATUS)
        item_dict[ste.RCOM_API_ARTICLE_TAGS] = item.get(Fields.TAGS)

        item_dict[ste.RCOM_API_ARTICLE_THUMBNAIL] = item.get(Fields.THUMBNAIL)
        item_dict[ste.RCOM_API_ARTICLE_TITLE] = item.get(Fields.TITLE)
        item_dict[ste.RCOM_API_ARTICLE_UPDATE_BY] = item.get(Fields.UPDATE_BY)
        item_dict[ste.RCOM_API_ARTICLE_UPDATE_BY_SSOID] = item.get(
            Fields.UPDATE_BY_SSOID)
        item_dict[ste.RCOM_API_ARTICLE_UPDATE_DATE] = item.get(
            Fields.UPDATE_DATE)

        # stats fields
        item_dict[ste.RCOM_API_ARTICLE_COUNT_LIKES] = item.get(
            Fields.COUNT_LIKES, 0)
        item_dict[ste.RCOM_API_ARTICLE_COUNT_RATINGS] = item.get(
            Fields.COUNT_RATINGS, 0)
        item_dict[ste.RCOM_API_ARTICLE_COUNT_VIEWS] = item.get(
            Fields.COUNT_VIEWS, 0)

        results.append(item_dict)

    return results


def _get_items_from_redis(redis_session, ids=[], prefix="", language='TH'):
    """
    Get items which already cached in redis
    :param redis_session: Redis session instance
    :param ids: List of item's ID without language in item's ID
    :param prefix: Prefix key in redis (eg. MOVIE_ITEM:[ITEM_ID])
    :param language : a language, either 'TH' or 'EN', default to 'TH'
    :return: List of elasticseacrh items store in redis, if item was not found return as None
        eg. [{"id": "item_1"}, None, {"id": "item_2"}]
    """
    if not redis_session:
        _logger.warning(
            "Caching has been disabled, no redis session specified ({redis_sesion})."
        )
        return [None] * len(ids)

    ids = _add_lang_prefix_for_ids(ids, language)
    ids = [f"{prefix}{_id}" for _id in ids]
    try:
        items = redis_session.mget(ids)
    except Exception:
        _logger.warning(
            "Caching has been disabled, an error occurred when trying to connect to the redis."
        )
        return [None] * len(ids)

    for idx, item in enumerate(items):
        if item:
            items[idx] = json.loads(item)
        else:
            items[idx] = None
    return items


def _to_timestamp(datetime_string, string_format="%Y-%m-%dT%H:%M:%S.000Z"):
    """
    convert datetime string to timestamp string

    Parameters
    ----------
    datetime_string : input string
    string_format : original format of datetime_string (default: elasticsearch format)

    Return
    ------
    a string of timestamp
    """
    timestamp_string = datetime.strptime(datetime_string, string_format)
    timestamp_string = timestamp_string.timestamp()
    timestamp = int(timestamp_string)
    return timestamp


def _filter_items_from_conditions(items, status='publish', publish_date=None):
    """
    Filter items according to supported conditions
    :param items: List of items (Elasticsearch format [{"_source": {"id": "item_1"}}])
    :param status: a status, either 'publish', 'draft', 'private', default 'publish'
    :param publish_date: a date of content will publish to user. This will filter only greater than default None
    :return: List of filtered items
    """
    filtered_items = items.copy()

    if status:
        filtered_items = filter(
            lambda item: item.get(Fields.SOURCE_, {}).get(Fields.STATUS) ==
            status, filtered_items)

    if publish_date:
        filtered_items = filter(
            lambda item: _to_timestamp(
                item.get(Fields.SOURCE_, {}).get(Fields.PUBLISH_DATE)) >=
            _to_timestamp(publish_date), filtered_items)

    return list(filtered_items)


def _set_cache_items(redis_session,
                     items,
                     prefix="",
                     expire_ms=ste.REDIS_DEFAULT_EXPIRE_MS,
                     language='TH'):
    """
    Add item to redis with `expire_ms` expiration
    :param redis_session: Redis session instance
    :param items: List of items
    :param prefix: Prefix key in redis (eg. MOVIE_ITEM:[ITEM_ID])
    :param expire_ms: Expire time in milliseconds
    :param language : a language, either 'TH' or 'EN', default to 'TH'
    :return: None
    """
    for item in items:
        if not isinstance(item, dict):
            _logger.warning(
                f"Item type: `{type(item)}` is not supported. Trying to skip it"
            )
            continue

        item_id = item.get(Fields.ID_)
        if not item_id:
            item_id = item.get(Fields.ID)
            # need to add language because documents in different language are kept in different _id in es7
            if item_id:
                item_id = _add_lang_prefix_for_ids([item_id], language)[0]

        if not item_id:
            _logger.warning(f"`_id` is missing skip caching this item. {item}")
            continue

        name = f"{prefix}{item_id}"
        _logger.debug(f"Set key: {name}")
        try:
            redis_session.set(name=name, value=json.dumps(item), px=expire_ms)
        except Exception as error:
            _logger.warning(f"{error}: Skip {name}")


def get_items(es_session,
              redis_session=None,
              ids=[],
              content_types=[],
              status='publish',
              publish_date=None,
              timeout="10s",
              language="TH"):
    """
    A utility function that retrieve items data from Redis (once they exists). Otherwise, it will performs a filter
    search in ES. 'ids' need to be passed to this utility.

    Parameters
    ----------
    es_session : ES session instance
    redis_session: Redis session instance
    ids : a list of item ids to search
    content_types : filters out items that don't match the given content_type. The comparison is in an OR fashion.
    status : a status, either 'publish', 'draft', 'private', default 'publish'
    publish_date : a date of content will publish to user. This will filter only greater than default None
                   eg. 2020-01-24T03:00:00.000Z
    timeout: Request timeout when calling elasticsearch
    language : a language, either 'TH' or 'EN', default to 'TH'

    Returns
    -------
    An array of dictionaries containing item details of a given input language as specified in the API

    Raises
    ------
    ValueError: raised if no 'ids' is passed
    """
    items_from_redis = _get_items_from_redis(redis_session,
                                             ids,
                                             prefix=ste.REDIS_ARTICLE_PREFIX,
                                             language=language)
    cached_items = filter(lambda item: item is not None, items_from_redis)
    cached_items = _filter_items_from_conditions(items=list(cached_items),
                                                 status=status,
                                                 publish_date=publish_date)

    non_cached_ids = [
        ids[i] for i, v in enumerate(items_from_redis) if v is None
    ]
    non_cached_items = []
    if non_cached_ids:
        try:
            non_cached_items = _get_items_from_elasticsearch(
                es_session=es_session,
                ids=non_cached_ids,
                content_types=content_types,
                status=status,
                publish_date=publish_date,
                timeout=timeout,
                language=language,
            )
            _set_cache_items(redis_session,
                             items=non_cached_items,
                             prefix=ste.REDIS_ARTICLE_PREFIX,
                             language=language)
        except Exception as error:
            _logger.error(f"Error: {error}")
            _logger.error(f"Cannot retrieve items from elasticsearch")

    items = [*cached_items, *non_cached_items]
    return _build_result_set(items)


def _get_items_from_elasticsearch(es_session,
                                  ids=[],
                                  content_types=[],
                                  status='publish',
                                  publish_date=None,
                                  timeout="10s",
                                  language="TH"):
    """
    A utility function that performs a filter search in ES. 'ids' need to be passed to this utility.

    Parameters
    ----------
    es_session : ES session instance
    ids : a list of item ids to search
    content_types : filters out items that don't match the given content_type. The comparison is in an OR fashion.
    status : a status, either 'publish', 'draft', 'private', default 'publish'
    publish_date : a date of content will publish to user. This will filter only greater than default None
    timeout: Request timeout when calling elasticsearch
    language : a language, either 'TH' or 'EN', default to 'TH'

    Returns
    -------
    An array of elasticsearch dictionaries

    Raises
    ------
    ValueError: raised if no 'ids' is passed
    """
    dsl = {
        "query": {
            "bool": {
                "filter": {
                    "bool": {
                        "must":
                            _get_default_must_conditions(
                                publish_date, language),
                    }
                }
            }
        }
    }
    dsl_must_conditions = dsl['query']['bool']['filter']['bool']['must']

    if status:
        dsl_must_conditions.append({"match": {Fields.STATUS: status}})

    if ids:
        ids = _add_lang_prefix_for_ids(ids, language)
        terms = dict(terms={Fields.ID_: ids})
        size = len(ids)
    else:
        raise ValueError("'ids' must be specified")

    dsl_must_conditions.append(terms)
    index = _get_index(content_types)
    results = es_session.search(index=index,
                                body=dsl,
                                _source=_API_COMMON_FIELDS,
                                size=size,
                                timeout=timeout)
    hits = results["hits"]["hits"]
    return hits


def _get_index(content_types=None):
    """
    get index of elasticsearch for article content type
    :param content_types: from ContentTypes class as list ['dara', 'musicarticle', 'horoscope']
    :return: str or list of index
    """
    if content_types:
        index = []
        for content_type in content_types:
            try:
                index.append(MapperIndex.mapper[content_type.lower()])
            except KeyError:
                pass

        if index:
            return index
    return ste.RCOM_ES_ARTICLE_INDEX


def _get_validate_format_publish_date(time):
    """
    validate time format for search in elasticsearch
    :param time: In string format
    :return: boolean
    """
    return bool(search("\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d\.\d{1,3}Z", time))


def _get_default_must_conditions(publish_date=None, language='TH'):
    """
    get default must conditions for query elasticsearch
    :param publish_date: time format "yyyy-MM-dd'T'HH:mm:ss.SSSZ" eg. "2020-01-24T03:00:00.00Z" or None default None
    :param language : a language, either 'TH' or 'EN', default to 'TH'
    :return: list of conditions
    """
    conditions = [{
        "match": {
            Fields.SEARCHABLE: "Y"
        }
    }, {
        "term": {
            Fields.LANG: language.lower()
        }
    }]
    if publish_date and _get_validate_format_publish_date(publish_date):
        conditions.append(
            {"range": {
                Fields.PUBLISH_DATE: {
                    "gte": publish_date
                }
            }})
    else:
        conditions.append({"range": {Fields.PUBLISH_DATE: {"gte": "now-3y/d"}}})

    return conditions


def _add_lang_prefix_for_ids(ids: List[str], language: str) -> List[str]:
    """
        Add language in front of id for _id in es7 format
        :param ids: List of item's ID
        :param language : a language, either 'TH' or 'EN', default to 'TH'
        :return: List of item's ID with language
    """
    if isinstance(ids, list):
        return [f"{language.lower()}-{id}" for id in ids]
    else:
        raise TypeError(f"`ids` should be list. found {type(ids)}")
