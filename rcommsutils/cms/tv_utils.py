#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 18 22:08:20 2017

@author: root
"""
import datetime
import functools
import orjson as json
import time
import urllib
import random
from typing import List, Dict, Optional, Tuple
import cachetools

import requests
import hashlib
from rcomloggingutils.applog_utils import AppLogger

import rcommsutils.cms.settings as ste


_logger = AppLogger(name=__name__)


GET_SHELF_ITEMS_CACHE = cachetools.TTLCache(
    maxsize=ste.RCOM_TV_GET_SHELF_ITEMS_CACHE_SIZE,
    ttl=ste.RCOM_TV_GET_SHELF_ITEMS_CACHE_TTL,
)

# Utility class to refer to searchable fields in Elastic Search
class Fields:
    LANG = "lang"
    ID = "id"
    _ID = "_id"
    AUDIO = "audio"
    SUBTITLE = "subtitle"
    TITLE_ID = "title_id"
    ORIGINAL_ID = "original_id"
    CONTENT_TYPE = "content_type"
    TITLE = "title"
    DETAIL = "detail"
    CATEGORY = "article_category"
    GENRES = "genres"
    THUMBNAIL = "thumb"
    THUMBLIST = "thumb_list"
    TAGS = "tags"
    STATUS = "status"
    COUNT_VIEWS = "count_views"
    PUBLISH_DATE = "publish_date"
    START_DATE = "start_date"
    END_DATE = "end_date"
    EXPIRY_DATE = "expire_date"
    SEARCHABLE = "searchable"
    SYNOPSIS = "synopsis"
    RELEASE_YEAR = "release_year"
    RATE = "rate"
    ACTORS = "actor"
    DIRECTORS = "director"
    COUNTRY = "country"
    DURATION = "duration"
    BUSINESS_MODEL = "business_models"
    TV_SHOW_CODE = "tv_show_code"
    HIT_COUNT_WEEK = "HIT_COUNT_WEEK"
    HIT_COUNT_MONTH = "HIT_COUNT_MONTH"
    HIT_COUNT_YEAR = "HIT_COUNT_YEAR"
    TVOD_FLAG = "tvod_flag"
    SUBSCRIPTION_TIERS = "subscription_tiers"
    COUNT_LIKES = "count_likes"
    EP_ITEMS = "ep_items"
    MOVIE_TYPE = "movie_type"
    CHANNEL_CODE = "channel_code"
    SUBSCRIPTION_PACKAGE = "subscription_package"
    DRM = "drm"
    IS_TRAILER = "is_trailer"
    DISPLAY_QUALITIES = "display_qualities"
    SHELF_ITEMS = "shelf_items"
    SHELF_LIST = "shelf_list"
    SHELF_ACTIVE = "active"
    SHELF_START_DATE = "start_date"
    SHELF_END_DATE = "end_date"
    EP_MASTER = "ep_master"
    PACKAGE_ALACARTE_LIST = "package_alacarte_list"
    TRAILER = "trailer"
    CONTENT_RIGHTS = "content_rights"
    SUB_GENRES = "sub_genres"
    OTHER_TYPE = "other_type"
    NEWEPI_BADGE_START = "newepi_badge_start"
    NEWEPI_BADGE_END = "newepi_badge_end"
    NEWEPI_BADGE_TYPE = "newepi_badge_type"
    CHANNEL_NAME = "channel_name"
    IS_VOD_LAYER = "is_vod_layer"
    CONTENT_TYPE = "content_type"
    PARTNER_RELATED = "partner_related"
    EXCLUSIVE_BADGE_TYPE = "exclusive_badge_type"
    EXCLUSIVE_BADGE_START = "exclusive_badge_start"
    EXCLUSIVE_BADGE_END = "exclusive_badge_end"
    IS_PROMO = 'is_promo'
    CONTENT_PROVIDER = "content_provider"


# Utility class representing popularity period
class PopularityPeriod:
    PAST_WEEK = Fields.HIT_COUNT_WEEK
    PAST_MONTH = Fields.HIT_COUNT_MONTH
    PAST_YEAR = Fields.HIT_COUNT_YEAR


_API_COMMON_FIELDS = [
    Fields.ID, Fields.RELEASE_YEAR, Fields.THUMBNAIL, Fields.HIT_COUNT_WEEK,
    Fields.HIT_COUNT_MONTH, Fields.HIT_COUNT_YEAR, Fields.TVOD_FLAG,
    Fields.SUBSCRIPTION_TIERS, Fields.COUNT_LIKES, Fields.EP_ITEMS,
    Fields.MOVIE_TYPE, Fields.THUMBLIST, Fields.COUNT_VIEWS, Fields.RATE,
    Fields.AUDIO, Fields.SUBTITLE, Fields.DURATION, Fields.DRM,
    Fields.DISPLAY_QUALITIES, Fields.EP_MASTER, Fields.TRAILER,
    Fields.CONTENT_RIGHTS, Fields.OTHER_TYPE, Fields.NEWEPI_BADGE_START,
    Fields.NEWEPI_BADGE_END, Fields.NEWEPI_BADGE_TYPE, Fields.TITLE,
    Fields.ACTORS, Fields.DETAIL, Fields.SYNOPSIS, Fields.CATEGORY,
    Fields.GENRES, Fields.PACKAGE_ALACARTE_LIST, Fields.SUB_GENRES,
    Fields.IS_VOD_LAYER, Fields.CONTENT_TYPE, Fields.PARTNER_RELATED,
    Fields.EXCLUSIVE_BADGE_TYPE, Fields.EXCLUSIVE_BADGE_START, Fields.EXCLUSIVE_BADGE_END,
    Fields.IS_PROMO, Fields.EXPIRY_DATE
]

_API_TV_COMMON_FIELDS = [
    Fields.ID, Fields.TITLE_ID, Fields.THUMBNAIL, Fields.CHANNEL_CODE,
    Fields.START_DATE, Fields.END_DATE, Fields.TITLE, Fields.DETAIL
]


def ttl_cache(timeout: int, maxsize: int = 128, typed: bool = False):
    """
    Decorator for lru_cache with adjustable time-to-live
    :param timeout: time to expire in minute
    :param maxsize: size of lru_cache
    :param typed: whether to cache argument with different type seperately
    :return: lru_cache with time-to-live
    """
    def wrapper_cache(func):
        func = functools.lru_cache(maxsize=maxsize, typed=typed)(func)
        # expire after {timeout} minutes
        func.delta = timeout * 60
        func.expiration = time.monotonic() + func.delta

        @functools.wraps(func)
        def wrapped_func(*args, **kwargs):
            if time.monotonic() >= func.expiration:
                func.cache_clear()
                func.expiration = time.monotonic() + func.delta
            return func(*args, **kwargs)

        wrapped_func.cache_info = func.cache_info
        wrapped_func.cache_clear = func.cache_clear
        return wrapped_func

    return wrapper_cache


def _get_return_fields(fields):
    return fields if fields else _API_COMMON_FIELDS


def _get_tv_return_fields():
    return _API_TV_COMMON_FIELDS


def _build_result_set(items):
    """
    build an item result set

    Parameters
    ----------
    items : an array of dictionary containing item details from ES

    Returns
    -------
    An array of dictionaries containing item details of a given input language as specified in the API
    """
    # yapf: disable
    results = []
    for item_source in items:
        item = item_source['_source']
        item_dict = dict()
        # common fields
        item_dict[ste.RCOM_API_GLOBAL_ITEM_ID] = item.get(Fields.ID)
        item_dict[ste.RCOM_API_RELEASE_YEAR] = item.get(Fields.RELEASE_YEAR)
        item_dict[ste.RCOM_API_TTV_THUMBNAIL] = item.get(Fields.THUMBNAIL)
        item_dict[ste.RCOM_API_TTV_THUMBLIST] = item.get(Fields.THUMBLIST)
        item_dict[ste.RCOM_API_TTV_TVOD_FLAG] = item.get(Fields.TVOD_FLAG)
        item_dict[ste.RCOM_API_TTV_SUBSCRIPTION_TIERS] = item.get(Fields.SUBSCRIPTION_TIERS)
        item_dict[ste.RCOM_API_TTV_EP_ITEMS] = item.get(Fields.EP_ITEMS)
        item_dict[ste.RCOM_API_TTV_MOVIE_TYPE] = item.get(Fields.MOVIE_TYPE)
        item_dict[ste.RCOM_API_TTV_RATE] = item.get(Fields.RATE)
        item_dict[ste.RCOM_API_TTV_AUDIO] = item.get(Fields.AUDIO)
        item_dict[ste.RCOM_API_TTV_SUBTITLE] = item.get(Fields.SUBTITLE)
        item_dict[ste.RCOM_API_TTV_DURATION] = item.get(Fields.DURATION)
        item_dict[ste.RCOM_API_TTV_DRM] = item.get(Fields.DRM)
        item_dict[ste.RCOM_API_TTV_DISPLAY_QUALITIES] = item.get(Fields.DISPLAY_QUALITIES)
        item_dict[ste.RCOM_API_TTV_TRAILER] = item.get(Fields.TRAILER)
        item_dict[ste.RCOM_API_TTV_CONTENT_RIGHTS] = item.get(Fields.CONTENT_RIGHTS)
        item_dict[ste.RCOM_API_TTV_OTHER_TYPE] = item.get(Fields.OTHER_TYPE)
        item_dict[ste.RCOM_API_NEWEPI_BADGE_START] = item.get(Fields.NEWEPI_BADGE_START)
        item_dict[ste.RCOM_API_NEWEPI_BADGE_END] = item.get(Fields.NEWEPI_BADGE_END)
        item_dict[ste.RCOM_API_NEWEPI_BADGE_TYPE] = item.get(Fields.NEWEPI_BADGE_TYPE)
        item_dict[ste.RCOM_API_EXPIRE_DATE] = item.get(Fields.EXPIRY_DATE)

        # stats fields
        item_dict[ste.RCOM_API_HIT_COUNT_WEEK] = item.get(Fields.HIT_COUNT_WEEK, 0)
        item_dict[ste.RCOM_API_HIT_COUNT_MONTH] = item.get(Fields.HIT_COUNT_MONTH, 0)
        item_dict[ste.RCOM_API_HIT_COUNT_YEAR] = item.get(Fields.HIT_COUNT_YEAR, 0)
        item_dict[ste.RCOM_API_TTV_COUNT_LIKES] = item.get(Fields.COUNT_LIKES, 0)
        item_dict[ste.RCOM_API_TTV_COUNT_VIEWS] = item.get(Fields.COUNT_VIEWS, 0)

        item_dict[ste.RCOM_API_NAME] = item.get(Fields.TITLE)
        item_dict[ste.RCOM_API_ACTORS] = item.get(Fields.ACTORS)
        item_dict[ste.RCOM_API_DESC] = item.get(Fields.DETAIL)
        item_dict[ste.RCOM_API_ARTICLE_CATEGORY] = item.get(Fields.CATEGORY)
        item_dict[ste.RCOM_API_GENRES] = item.get(Fields.GENRES)
        item_dict[ste.RCOM_API_SUB_GENRES] = item.get(Fields.SUB_GENRES)
        item_dict[ste.RCOM_API_TTV_SYNOPSIS] = item.get(Fields.SYNOPSIS)
        item_dict[ste.RCOM_API_TTV_PACKAGE_ALACARTE] = item.get(Fields.PACKAGE_ALACARTE_LIST)
        
        item_dict[ste.RCOM_API_TTV_CONTENT_TYPE] = item.get(Fields.CONTENT_TYPE)
        item_dict[ste.RCOM_API_TTV_PARTNER_RELATED] = item.get(Fields.PARTNER_RELATED)
        item_dict[ste.RCOM_API_TTV_EXCLUSIVE_BADGE_TYPE] = item.get(Fields.EXCLUSIVE_BADGE_TYPE)
        item_dict[ste.RCOM_API_TTV_EXCLUSIVE_BADGE_START] = item.get(Fields.EXCLUSIVE_BADGE_START)
        item_dict[ste.RCOM_API_TTV_EXCLUSIVE_BADGE_END] = item.get(Fields.EXCLUSIVE_BADGE_END)

        results.append(item_dict)
    # yapf: enable
    return results


def _build_fields_result(items):
    """
    Build result set for specific field
    :param items:
    :return:
    """
    results = []
    for item_source in items:
        item = item_source['_source']
        results.append(item)
    return results


def _to_timestamp(datetime_string, string_format="%Y-%m-%dT%H:%M:%S.000Z"):
    """
    convert datetime string to timestamp string

    Parameters
    ----------
    datetime_string : input string
    string_format : original format of datetime_string (default: elasticsearch format)

    Return
    ------
    a string of timestamp
    """
    timestamp_string = datetime.datetime.strptime(datetime_string,
                                                  string_format)
    timestamp_string = timestamp_string.timestamp()
    timestamp_string = int(timestamp_string)
    timestamp_string = str(timestamp_string)
    return timestamp_string


def _build_tv_result_set(items):
    """
    build an item result set of a given input language as specified in the API

    Parameters
    ----------
    items : an array of dictionary containing item details from ES

    Returns
    -------
    An array of dictionaries containing item details of a given input language as specified in the API
    """

    results = []
    for item_source in items:
        item = item_source['_source']
        item_dict = dict()

        # common fields
        item_dict[ste.RCOM_API_TITLE_ID] = item.get(Fields.TITLE_ID)
        item_dict[ste.RCOM_API_TTV_THUMBNAIL] = item.get(Fields.THUMBNAIL)
        item_dict[ste.RCOM_API_CMS_TITLE_ID] = item.get(Fields.ID)
        item_dict[ste.RCOM_API_CMS_CHANNEL_CODE] = item.get(Fields.CHANNEL_CODE)

        # datetime fields
        start_time = item.get(Fields.START_DATE)
        end_time = item.get(Fields.END_DATE)
        item_dict[ste.RCOM_API_START_TIME] = _to_timestamp(start_time)
        item_dict[ste.RCOM_API_END_TIME] = _to_timestamp(end_time)

        item_dict[ste.RCOM_API_NAME] = item.get(Fields.TITLE)
        item_dict[ste.RCOM_API_DESC] = item.get(Fields.DETAIL)

        results.append(item_dict)

    return results

def _hashable_must_nots(must_nots: Tuple[Dict]) -> Tuple[str]:
    return tuple(json.dumps(must_not) for must_not in must_nots)

def _unhashable_must_nots(hashabled_must_nots: Tuple[str]) -> Tuple[Dict]:
    return tuple(json.loads(must_not) for must_not in hashabled_must_nots)


def get_tv_items(session,
                 title_ids=[],
                 start_time=None,
                 end_time=None,
                 padding_time=3600,
                 language="EN",
                 timeout="10s",
                 size=10,
                 channel_codes=[]):
    """
    A utility function that performs a filter search in ES.

    Parameters
    ----------
    session : ES session instance
    title_ids : a list of item ids to search
    start_time : an int of a start timestamp (seconds from epoch) (if start_time = -1, it should refer to DMPREC-713)
    end_time : an int of a end timestamp (seconds from epoch)
    padding_time: seconds for padding end_time when do not specify (default: next 3600 seconds)
    language : a language, either 'EN' or 'TH', default 'EN'
    timeout: Request timeout when calling elasticsearch
    size: number of returned documents
    channel_codes: a list of channel_code to search

    Returns
    -------
    An array of dictionaries containing item details of a given input language as specified in the API

    Raises
    ------
    ValueError: raised if no 'title_ids' or 'start_time' is more than 'end_time'.
    TypeError: raised if 'start_time' or 'end_time' is not int.
    """
    start_time, end_time, is_only_current_show = _validate_tv_interval_time(start_time, end_time, padding_time)

    dsl = {
        "query": {
            "bool": {
                "filter": {
                    "bool": {
                        "must": _get_tv_default_must_conditions(language),
                        "must_not": _get_tv_default_must_not_conditions()
                    }
                }
            }
        }
    }

    # start_time and end_time
    dsl["query"]["bool"]["filter"]["bool"]["must"].append(
        {"range": {
            Fields.END_DATE: {
                "gt": start_time
            }
        }})
    if is_only_current_show:
        dsl["query"]["bool"]["filter"]["bool"]["must"].append(
            {"range": {
                Fields.START_DATE: {
                    "lte": start_time
                }
            }})
    else:
        dsl["query"]["bool"]["filter"]["bool"]["must"].append(
            {"range": {
                Fields.START_DATE: {
                    "lte": end_time
                }
            }})

    if title_ids:
        terms = dict(terms={"title_id.keyword": title_ids})
        size = len(title_ids)
        dsl['query']['bool']['filter']['bool']['must'].append(terms)

    if channel_codes:
        terms = dict(terms={"channel_code.keyword": channel_codes})
        dsl['query']['bool']['filter']['bool']['must'].append(terms)

    _logger.debug("dsl: {}".format(dsl))
    results = session.search(index=ste.RCOM_ES_EPG_INDEX,
                             body=dsl,
                             _source=_get_tv_return_fields(),
                             size=size,
                             timeout=timeout)

    hits = results["hits"]["hits"]
    return _build_tv_result_set(hits)


def get_latest_tv_items(session,
                        start_time=None,
                        end_time=None,
                        padding_time=3600,
                        language="EN",
                        size=10,
                        sort_field=Fields.END_DATE,
                        sort_by="asc",
                        timeout="10s"):
    """
    A utility function that performs a filter search in ES.

    Parameters
    ----------
    session : ES session instance
    start_time : an int of a start timestamp (seconds from epoch) (if start_time = -1, it should refer to DMPREC-713)
    end_time : an int of a end timestamp (seconds from epoch)
    padding_time: seconds for padding end_time when do not specify (default: next 3600 seconds)
    language : a language, either 'EN' or 'TH', default 'EN'
    sort_field : field to be sorted, default to Fields.END_DATE.
    sort_by : a field to be sorted by, either 'asc' or 'desc', default to 'asc'.
    timeout: Request timeout when calling elasticsearch

    Returns
    -------
    An array of dictionaries containing item details of a given input language as specified in the API

    Raises
    ------
    ValueError: raised if no 'title_ids' or 'start_time' is more than 'end_time'.
    TypeError: raised if 'start_time' or 'end_time' is not int.
    """
    start_time, end_time, is_only_current_show = _validate_tv_interval_time(
        start_time, end_time, padding_time)

    dsl = {
        "query": {
            "bool": {
                "filter": {
                    "bool": {
                        "must": _get_tv_default_must_conditions(language),
                        "must_not": _get_tv_default_must_not_conditions()
                    }
                }
            }
        }
    }
    if size > 0:
        dsl["sort"] = {sort_field: {"order": sort_by}}

    # start_time and end_time
    dsl["query"]["bool"]["filter"]["bool"]["must"].append(
        {"range": {
            Fields.END_DATE: {
                "gt": start_time
            }
        }})
    if is_only_current_show:
        dsl["query"]["bool"]["filter"]["bool"]["must"].append(
            {"range": {
                Fields.START_DATE: {
                    "lte": start_time
                }
            }})
    else:
        dsl["query"]["bool"]["filter"]["bool"]["must"].append(
            {"range": {
                Fields.START_DATE: {
                    "lte": end_time
                }
            }})

    results = session.search(index=ste.RCOM_ES_EPG_INDEX,
                             body=dsl,
                             _source=_get_tv_return_fields(),
                             size=size,
                             timeout=timeout)

    hits = results["hits"]["hits"]
    return _build_tv_result_set(hits)


def _get_items_from_redis(redis_session, ids=[], prefix="", language='TH'):
    """
    Get items which already cached in redis
    :param redis_session: Redis session instance
    :param ids: List of item's ID without language in item's ID
    :param prefix: Prefix key in redis (eg. MOVIE_ITEM:[ITEM_ID])
    :param language : a language, either 'TH' or 'EN', default to 'TH'
    :return: List of elasticseacrh items store in redis, if item was not found return as None
        eg. [{"id": "item_1"}, None, {"id": "item_2"}]
    """
    if not redis_session:
        _logger.warning(
            f"Caching has been disabled, no redis session specified ({redis_session})."
        )
        return [None] * len(ids)

    ids = _add_lang_prefix_for_ids(ids, language)
    ids = [f"{prefix}{_id}" for _id in ids]

    try:
        items = redis_session.mget(ids)
    except Exception:
        _logger.warning(
            "Caching has been disabled, an error occurred when trying to connect to the redis."
        )
        return [None] * len(ids)

    for idx, item in enumerate(items):
        if item:
            items[idx] = json.loads(item)
        else:
            items[idx] = None
    return items


def _get_item_source(item):
    return item.get("_source", {})


def _filter_items_from_conditions(items,
                                  type=None,
                                  genres=[],
                                  article_categories=[],
                                  tvod_flag=None,
                                  content_rights=[],
                                  subscription_tiers=None,
                                  rates=[],
                                  package_alacarte_list=[],
                                  is_vod_layer=None,
                                  other_types=[]):
    """
    Filter items according to supported conditions
    TODO: Add arguments' typing
    :param items: List of items (Elasticsearch format [{"_source": {"id": "item_1"}}])
    :param: type: movie_type ("movie" or "series")
    :param genres: List of genres for filtering (OR fashioned)
    :param article_categories: List of article_categories for filtering (OR fashioned)
    :param tvod_flag: tvod_flag for filtering ("Y" or "N")
    :param content_rights: a List of content rights (e.g., svod, avod, tvod, etc.)
    :param subscription_tiers: List of subscription_tiers for filtering (OR fashioned)
    :param rates: rates for fitlering (according to CMS data)
    :param package_alacarte_list: List of package_alacarte_list for filtering (OR fashioned)
    :param is_vod_layer: is_vod_layer for filtering ("Y" or other)
    :param other_types: a list of other item types (string)
    :return: List of filtered items
    """
    
    def is_in_list(filter_list, field):
        # if no filter is defined, still return True. So it won't skip the item.
        if filter_list and isinstance(filter_list, list):
            if item_source.get(field) not in filter_list:
                return False
        return True
    
    def is_list_equal(filter_list, field):
        # if no filter is defined, still return True. So it won't skip the item.
        if filter_list and isinstance(filter_list, list):
            item_list = item_source.get(field)
            if not isinstance(item_list, list):
                return False

            intersect = set(filter_list).intersection(set(item_list))
            if not intersect: return False
        return True

    filtered_items = []
    for item in items:
        item_source = _get_item_source(item)

        # check if item_source_str equals filter_str
        if type:
            if type != item_source.get(Fields.MOVIE_TYPE): continue
            if type == 'series':
                if (item_source.get(Fields.EP_MASTER) != 'Y'): continue

                _is_vod_layer = item_source.get(Fields.IS_VOD_LAYER)
                if is_vod_layer:
                    if is_vod_layer != _is_vod_layer: continue
                else:
                    if _is_vod_layer == 'Y': continue
        else:
            if item_source.get(Fields.MOVIE_TYPE) == 'series':
                _is_vod_layer = item_source.get(Fields.IS_VOD_LAYER)
                if is_vod_layer:
                    if is_vod_layer != _is_vod_layer: continue
                else:
                    if _is_vod_layer == 'Y': continue

        if tvod_flag:
            if tvod_flag != item_source.get(Fields.TVOD_FLAG):
                continue

        # check if item_source_str in filter_list
        if not is_in_list(rates, Fields.RATE): continue
        if not is_in_list(other_types, Fields.OTHER_TYPE): continue
        if not is_in_list(content_rights, Fields.CONTENT_RIGHTS): continue

        # check if item_source_list equals filter_list
        if not is_list_equal(genres, Fields.GENRES): continue
        if not is_list_equal(article_categories, Fields.CATEGORY): continue
        if not is_list_equal(subscription_tiers, Fields.SUBSCRIPTION_TIERS): continue
        if not is_list_equal(package_alacarte_list, Fields.PACKAGE_ALACARTE_LIST): continue

        filtered_items.append(item)

    return filtered_items


def _set_cache_items(redis_session,
                     items,
                     prefix="",
                     default_expire_ms=ste.REDIS_DEFAULT_EXPIRE_MS,
                     language='TH'):
    """
    Add item to redis with `default_expire_ms` expiration
    :param redis_session: Redis session instance
    :param items: List of items
    :param prefix: Prefix key in redis (eg. MOVIE_ITEM:[ITEM_ID])
    :param default_expire_ms: Expire time in milliseconds
    :param language : a language, either 'TH' or 'EN', default to 'TH'
    :return: None
    """

    if not redis_session:
        _logger.warning(
            f"Caching has been disabled, no redis session specified ({redis_session})."
        )
        return None

    pipeline = redis_session.pipeline()
    for item in items:
        if not isinstance(item, dict):
            _logger.warning(
                f"Item type: `{type(item)}` is not supported. Trying to skip it"
            )
            continue

        item_id = item.get(Fields._ID)
        if not item_id:
            item_id = item.get(Fields.ID)
            # need to add language because documents in different language are kept in different _id in es7
            if item_id:
                item_id = _add_lang_prefix_for_ids([item_id], language)[0]

        if not item_id:
            _logger.warning(f"`_id` is missing skip caching this item. {item}")
            continue

        item_expire_date = item.get("_source", {}).get(Fields.EXPIRY_DATE,
                                                       default_expire_ms)
        item_expire_date_from_now = _get_milliseconds_from_now(
            item_expire_date, format="%Y-%m-%dT%H:%M:%S.%fZ")

        if item_expire_date_from_now and item_expire_date_from_now < default_expire_ms:
            expire_ms = item_expire_date_from_now
        else:
            expire_ms = default_expire_ms

        if expire_ms <= 0:
            continue

        name = f"{prefix}{item_id}"

        _logger.debug(f"Set key: {name}: expire in {expire_ms} milliseconds")

        try:
            pipeline.set(name=name, value=json.dumps(item), px=expire_ms)
        except Exception as error:
            _logger.warning(f"{error}: Skip {name}")
    pipeline.execute()


def _get_milliseconds_from_now(date_string,
                               format="%Y-%m-%dT%H:%M:%S.%fZ",
                               now=datetime.datetime.now()):
    if not isinstance(date_string, str):
        return date_string

    datetime_object = datetime.datetime.strptime(date_string, format)
    delta_from_now = datetime_object - now
    return delta_from_now.total_seconds() * 1000


def get_items_define_fields(es_session,
                            ids=[],
                            type=None,
                            genres=[],
                            article_categories=[],
                            language="EN",
                            tvod_flag=None,
                            content_rights=[],
                            subscription_tiers=[],
                            rates=[],
                            package_alacarte_list=[],
                            timeout="10s",
                            fields=['id'],
                            is_vod_layer=None,
                            other_types=[],
                            must_nots=[]):
    """
    A utility function that retrieve items data from the Elasticsearch (No cache), it will performs a filter
    search in ES. 'ids' need to be passed to this utility. This function can specify the return field you want

    Parameters
    ----------
    es_session : ES session instance
    ids : a list of item ids to search
    type: filter item with movie_type
    genres : filters out items that don't match the given genres. The comparison is in an OR fashion.
    article_categories : filters out items that don't match the given article_categories. The comparison is in an OR fashion.
    language : a language, either 'EN' or 'TH', default 'EN'
    tvod_flag : flag to indicate tvod or not, it can be 'Y' or 'N'
    content_rights : filter out items that don't match the given list of content rights (e.g., 'svod', 'tvod', 'avod', etc.)
    subscription_tiers : a list of item's subscription iters to search. The comparison is in an OR fashion.
    rates : filter out items that don't match the given rates (according to CMS data)
    package_alacarte_list : filter out items that don't match the given package_alacarte_list.
    timeout: Request timeout when calling elasticsearch
    fields: return only field in fields list (Call directly to elasticsearch no cache) Please becareful to use this param
    is_vod_layer: flag for filter is_vod_layer.
    other_types: a list of other item types, e.g., 'news', 'animation', 'entertainment', 'knowledge', 'documentary', etc.
    must_nots: a list of query than contain condition to use in elastic search must_nots e.g., [{"match": {"partner_related": "abc"}}]

    Returns
    -------
    An array of dictionaries containing item details of a given input language as specified in the API

    Raises
    ------
    ValueError: raised if no 'ids' is passed
    """
    items = _get_items_from_elasticsearch(
        es_session=es_session,
        ids=ids,
        type=type,
        genres=genres,
        article_categories=article_categories,
        language=language,
        tvod_flag=tvod_flag,
        content_rights=content_rights,
        subscription_tiers=subscription_tiers,
        rates=rates,
        package_alacarte_list=package_alacarte_list,
        timeout=timeout,
        fields=fields,
        is_vod_layer=is_vod_layer,
        other_types=other_types,
        must_nots=must_nots)
    return _build_fields_result(items)


def get_items_with_filter(es_session,
                          redis_session=None,
                          ids=[],
                          type=None,
                          genres=[],
                          article_categories=[],
                          language="EN",
                          tvod_flag=None,
                          content_rights=[],
                          subscription_tiers=[],
                          rates=[],
                          package_alacarte_list=[],
                          timeout="10s",
                          max_items=10,
                          is_sort_item=False,
                          is_vod_layer=None,
                          other_types=[],
                          must_nots=[]):
    """
        A utility function that retrieve items data from Redis (once they exists). Otherwise, it will performs a filter
        search in ES. 'ids' need to be passed to this utility. This function will filter useable id before query full metadata

        Parameters
        ----------
        es_session : ES session instance
        redis_session: Redis session instance
        ids : a list of item ids to search
        type: filter item with movie_type
        genres : filters out items that don't match the given genres. The comparison is in an OR fashion.
        article_categories : filters out items that don't match the given article_categories. The comparison is in an OR fashion.
        language : a language, either 'EN' or 'TH', default 'EN'
        tvod_flag : flag to indicate tvod or not, it can be 'Y' or 'N'
        content_rights : filter out items that don't match the given list of content rights (e.g., 'svod', 'tvod', 'avod', etc.)
        subscription_tiers : a list of item's subscription iters to search. The comparison is in an OR fashion.
        rates : filter out items that don't match the given rates (according to CMS data)
        package_alacarte_list : filter out items that don't match the given package_alacarte_list.
        timeout: Request timeout when calling elasticsearch
        max_items: Number of return items
        is_sort_item: Flag to enable to sort filtered ids follow input ids respectively
        is_vod_layer: flag for filter is_vod_layer ('Y', 'N' or 'other')
        other_types: a list of other item types, e.g., 'news', 'animation', 'entertainment', 'knowledge', 'documentary', etc.
        must_nots: a list of query than contain condition to use in elastic search must_nots e.g., [{"match": {"partner_related": "abc"}}]

        Returns
        -------
        An array of dictionaries containing item details of a given input language as specified in the API

        Raises
        ------
        ValueError: raised if no 'ids' is passed
        """
    filter_ids = get_items_define_fields(
        es_session,
        ids=ids,
        type=type,
        genres=genres,
        fields=['id'],
        article_categories=article_categories,
        language=language,
        tvod_flag=tvod_flag,
        content_rights=content_rights,
        subscription_tiers=subscription_tiers,
        rates=rates,
        package_alacarte_list=package_alacarte_list,
        timeout=timeout,
        is_vod_layer=is_vod_layer,
        other_types=other_types,
        must_nots=must_nots)

    filter_ids = [i.get('id') for i in filter_ids if i and 'id' in i]
    if is_sort_item:
        keymap = keymap = {o: i for i, o in enumerate(ids)}
        filter_ids.sort(key=lambda x: keymap[x])

    filter_ids = filter_ids[:max_items]
    return get_items(es_session,
                     redis_session=redis_session,
                     ids=filter_ids,
                     type=type,
                     genres=genres,
                     article_categories=article_categories,
                     language=language,
                     tvod_flag=tvod_flag,
                     content_rights=content_rights,
                     subscription_tiers=subscription_tiers,
                     rates=rates,
                     package_alacarte_list=package_alacarte_list,
                     timeout=timeout,
                     is_vod_layer=is_vod_layer,
                     other_types=other_types,
                     must_nots=must_nots)


def get_items(es_session,
              redis_session=None,
              ids=[],
              type=None,
              genres=[],
              article_categories=[],
              language="EN",
              tvod_flag=None,
              content_rights=[],
              subscription_tiers=[],
              rates=[],
              package_alacarte_list=[],
              timeout="10s",
              is_vod_layer=None,
              other_types=[],
              must_nots=[]):
    """
    A utility function that retrieve items data from Redis (once they exists). Otherwise, it will performs a filter
    search in ES. 'ids' need to be passed to this utility.

    Parameters
    ----------
    es_session : ES session instance
    redis_session: Redis session instance
    ids : a list of item ids to search 
    type: filter item with movie_type
    genres : filters out items that don't match the given genres. The comparison is in an OR fashion.
    article_categories : filters out items that don't match the given article_categories. The comparison is in an OR fashion.
    language : a language, either 'EN' or 'TH', default 'EN'
    tvod_flag : flag to indicate tvod or not, it can be 'Y' or 'N'
    content_rights : filter out items that don't match the given list of content rights (e.g., 'svod', 'tvod', 'avod', etc.)
    subscription_tiers : a list of item's subscription iters to search. The comparison is in an OR fashion.
    rates : filter out items that don't match the given rates (according to CMS data)
    package_alacarte_list : filter out items that don't match the given package_alacarte_list.
    timeout: Request timeout when calling elasticsearch
    is_vod_layer: flag for filter is_vod_layer ('Y', 'N' or 'other')
    other_types: a list of other item types, e.g., 'news', 'animation', 'entertainment', 'knowledge', 'documentary', etc.
    must_nots: a list of additional must not conditions

    Returns
    -------
    An array of dictionaries containing item details of a given input language as specified in the API

    Raises
    ------
    ValueError: raised if no 'ids' is passed
    """
    # concat prefix with must_nots to seperate cache by must_nots
    if must_nots:
        cache_key_prefix = f"{ste.REDIS_MOVIE_PREFIX}-{must_nots}"
    else:
        cache_key_prefix = ste.REDIS_MOVIE_PREFIX

    items_from_redis = _get_items_from_redis(redis_session,
                                             ids,
                                             prefix=cache_key_prefix,
                                             language=language)
    cached_items = filter(lambda item: item is not None, items_from_redis)
    cached_items = _filter_items_from_conditions(
        items=list(cached_items),
        type=type,
        genres=genres,
        article_categories=article_categories,
        tvod_flag=tvod_flag,
        content_rights=content_rights,
        subscription_tiers=subscription_tiers,
        rates=rates,
        package_alacarte_list=package_alacarte_list,
        is_vod_layer=is_vod_layer,
        other_types=other_types)
    non_cached_ids = [
        ids[i] for i, v in enumerate(items_from_redis) if v is None
    ]
    _logger.debug(f"cached_items: {len(cached_items)} items")
    _logger.debug(f"non_cached_items: {len(non_cached_ids)} items")

    non_cached_items = []
    if non_cached_ids:
        try:
            non_cached_items = _get_items_from_elasticsearch(
                es_session=es_session,
                ids=non_cached_ids,
                type=type,
                genres=genres,
                article_categories=article_categories,
                language=language,
                tvod_flag=tvod_flag,
                content_rights=content_rights,
                subscription_tiers=subscription_tiers,
                rates=rates,
                package_alacarte_list=package_alacarte_list,
                timeout=timeout,
                is_vod_layer=is_vod_layer,
                other_types=other_types,
                must_nots=must_nots)
            _set_cache_items(redis_session,
                             items=non_cached_items,
                             prefix=cache_key_prefix,
                             language=language)
            
            _logger.debug(f"\t _get_items_from_elasticsearch(non_cached_ids) found: {len(non_cached_items)} items")
        except Exception as error:
            _logger.error(f"Error: {error}")
            _logger.error(f"Cannot retrieve items from elasticsearch")

    items = [*cached_items, *non_cached_items]
    return _build_result_set(items)


def _get_items_from_elasticsearch(es_session,
                                  ids=[],
                                  fields=None,
                                  type=None,
                                  tvod_flag=None,
                                  is_vod_layer=None,
                                  rates=[],
                                  content_rights=[],
                                  genres=[],
                                  article_categories=[],
                                  subscription_tiers=[],
                                  package_alacarte_list=[],
                                  other_types=[],
                                  language="EN",
                                  timeout="10s",
                                  must_nots=[]
                                ):
    # base query
    conditions = {}
    dsl = {"query": {"bool": {"filter": {"bool": conditions}}}}

    conditions['must'] = _get_default_must_conditions(language)
    conditions['must'] += list(filter(None, [
        _get_ids_condition(ids, language),
        _get_tvod_flag_contidion(tvod_flag),
        _get_content_rights_condition(content_rights),
        _get_rates_condition(rates),
        _get_subscription_tiers_condition(subscription_tiers),
        _get_genres_condition(genres),
        _get_categories_condition(article_categories),
        _get_package_alacarte_list_condition(package_alacarte_list),
        _get_other_types_condition(other_types),
    ]))
    conditions['must_not'] = _get_default_must_not_conditions()
    if must_nots:
        conditions['must_not'] += must_nots

    conditions['should'] = _get_default_should_conditions()
    conditions['minimum_should_match'] = 1

    # custom/mixed conditions
    conditions = _get_movie_types_conditions(conditions, type, is_vod_layer)

    _logger.debug(f"_get_items_from_elasticsearch: DSL {dsl}")
    results = es_session.search(index=ste.RCOM_ES_TRUETV_INDEX,
                                body=dsl,
                                _source=_get_return_fields(fields),
                                size=len(ids),
                                timeout=timeout)

    hits = results["hits"]["hits"]
    return hits


def _get_random_items_from_elasticsearch(es_session,
                                  seed=1,
                                  size=10,
                                  type=None,
                                  genres=[],
                                  article_categories=[],
                                  language="EN",
                                  tvod_flag=None,
                                  content_rights=[],
                                  subscription_tiers=[],
                                  rates=[],
                                  package_alacarte_list=[],
                                  timeout="10s",
                                  fields=None,
                                  is_vod_layer=None,
                                  other_types=[],
                                  must_nots=[]):

    conditions = {}
    dsl = {
        "query": {
            "bool": {
                "must": {
                    "function_score": {
                        "functions": [
                            {"random_score": {"seed": seed, "field": "_seq_no"}}
                        ]
                    }
                },
                "filter": {"bool": conditions}
            }
        }
    }

    conditions['must'] = _get_default_must_conditions(language)
    conditions['must'] += list(filter(None, [
        _get_tvod_flag_contidion(tvod_flag),
        _get_content_rights_condition(content_rights),
        _get_rates_condition(rates),
        _get_subscription_tiers_condition(subscription_tiers),
        _get_genres_condition(genres),
        _get_categories_condition(article_categories),
        _get_package_alacarte_list_condition(package_alacarte_list),
        _get_other_types_condition(other_types),
    ]))
    conditions['must_not'] = _get_default_must_not_conditions()
    if must_nots:
        conditions["must_not"] += must_nots 

    conditions['should'] = _get_default_should_conditions()
    conditions['minimum_should_match'] = 1

    # custom/mixed conditions
    conditions = _get_movie_types_conditions(conditions, type, is_vod_layer)

    _logger.debug(f"_get_random_items_from_elasticsearch: DSL {dsl}")
    results = es_session.search(index=ste.RCOM_ES_TRUETV_INDEX,
                                body=dsl,
                                _source=_get_return_fields(fields),
                                size=size,
                                timeout=timeout)

    hits = results["hits"]["hits"]
    return _build_result_set(hits)


def _get_cache_key(*args):
    return str(args)


def get_shelf_items(es_session,
                    redis_session=None,
                    shelf_id="",
                    timestamp="",
                    active=True,
                    timeout="10s",
                    language='TH',
                    must_nots: Optional[List[Dict]] = None):
    """
     utility function that performs searching items given a 'shelf_id' needed to passes to this utility.
     Searching items from redis caching.
     If there is no cache, then search from elasticsearch and cache items results to redis

     Parameters
     __________
     es_session : ES session instance
     redis_session : Redis session instance
     shelf_id : a string of shelf_id to search
     index: index elasticsearch
     timestamp: timestamp that would like to get shelf
     active: active shelf status
     timeout : timeout for elasticsearch
     language : specific language
     return : a list of shelf_items with a given shelf
     must_nots: a list of query than contain condition to use in elastic search must_nots e.g., [{"match": {"partner_related": "abc"}}]
    """
    if must_nots is None:
        must_nots = []
    
    # get cache key
    cache_key = _get_cache_key(shelf_id, timestamp, active, language, must_nots)
    if cache_key in GET_SHELF_ITEMS_CACHE:
        _logger.debug(f"tv_utils.get_shelf_items: shelf_id: {shelf_id} hitted memory cache")
        return GET_SHELF_ITEMS_CACHE[cache_key]

    _logger.debug(f"tv_utils.get_shelf_items: shelf_id: {shelf_id} not hit memory cache")

    # concat prefix with must_nots to seperate cache by must_nots
    if must_nots:
        redis_key_prefix = f"{ste.REDIS_SHELF_PREFIX}-{must_nots}"
    else:
        redis_key_prefix = ste.REDIS_SHELF_PREFIX

    shelf = _get_items_from_redis(redis_session, ids=[shelf_id], 
                                  prefix=redis_key_prefix,
                                  language=language)

    if isinstance(shelf, list) and shelf[0]:
        return shelf[0].get(Fields.SHELF_ITEMS)

    if timestamp == "":
        timestamp = datetime.datetime.fromtimestamp(time.time()).strftime(ste.DATETIME_FORMAT)

    try:
        # get shelf
        shelf_items, expire_date = get_shelf_items_from_elasticsearch(
            es_session, shelf_id, timestamp, active, timeout, _hashable_must_nots(must_nots))

        # manage expire date for caching
        if expire_date:
            expire_date_ms = _get_milliseconds_from_now(expire_date, format=ste.DATETIME_FORMAT)
            if expire_date_ms >= ste.REDIS_DEFAULT_EXPIRE_MS:
                expire_date_ms = ste.REDIS_DEFAULT_EXPIRE_MS
        else:
            expire_date_ms = ste.REDIS_DEFAULT_EXPIRE_MS

        shelf = [{Fields.ID: shelf_id, Fields.SHELF_ITEMS: shelf_items}]
        _set_cache_items(redis_session,
                         items=shelf,
                         prefix=redis_key_prefix,
                         language=language,
                         default_expire_ms=expire_date_ms)

        GET_SHELF_ITEMS_CACHE[cache_key] = shelf_items

    except Exception as error:
        shelf_items = []
        _logger.error(f"Error: {error}")
        _logger.error(f"Cannot retrieve shelf items from elasticsearch")

    return shelf_items


@ttl_cache(timeout=240, maxsize=64)
def get_shelf_items_from_elasticsearch(es_session,
                                       shelf_id,
                                       timestamp="",
                                       active=True,
                                       timeout="10s",
                                       must_nots: Optional[Tuple] = None):
    """
     utility function that performs searching items given a 'shelf_id' from elasticsearch

     Parameters
     __________
     es_session : ES session instance
     shelf_id : a string of shelf_id to search
     timestamp: timestamp that would like to get shelf
     active: active shelf status
     timeout : timeout for elasticsearch
     must_nots: a list of query than contain condition to use in elastic search must_nots e.g., [{"match": {"partner_related": "abc"}}]
     return : a list of shelf_items with a given shelf
    """

    dsl = {
        "query": {
            "bool": {
                "minimum_should_match": 1,
                "should": [
                    {
                        "bool": {
                            "filter": [
                                {"term": {"id.keyword": shelf_id}}, 
                                {"term": {"_index": ste.RCOM_ES_PROGRESSIVE_INDEX}},
                                {
                                    "nested": {
                                        "path": Fields.SHELF_LIST,
                                        "query": {
                                            "bool": {
                                                "filter": [
                                                    {"term" : {f"{Fields.SHELF_LIST}.{Fields.SHELF_ACTIVE}": active}},
                                                    {"range": {f"{Fields.SHELF_LIST}.{Fields.SHELF_START_DATE}": {"lte": timestamp}}}
                                                ],
                                                "should": [
                                                    {"range": {f"{Fields.SHELF_LIST}.{Fields.END_DATE}": {"gte": timestamp}}},
                                                    {"bool" : {"must_not": [{"exists": {"field": f"{Fields.SHELF_LIST}.{Fields.END_DATE}"}}]}}
                                                ],
                                                "minimum_should_match": 1
                                            }
                                        },
                                        "inner_hits": {
                                            "_source": [
                                                f"{Fields.SHELF_LIST}.{Fields.SHELF_ACTIVE}",
                                                f"{Fields.SHELF_LIST}.{Fields.SHELF_ITEMS}",
                                                f"{Fields.SHELF_LIST}.{Fields.SHELF_START_DATE}",
                                                f"{Fields.SHELF_LIST}.{Fields.SHELF_END_DATE}"
                                            ]
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    {
                        "bool": {
                            "filter": [
                                {"term": {"id": shelf_id}},
                                {"term": {"_index": ste.RCOM_ES_CONTENTS_SHELF_INDEX}}
                            ]
                        }
                    }
                ]
            }
        }
    }

    # add must_nots to dsl
    if must_nots:
        must_nots = list(_unhashable_must_nots(must_nots))
        dsl["query"]["bool"]["must_not"] = list(must_nots)

    _logger.debug(f"get_shelf_items_from_elasticsearch: DSL {dsl}")
    results = es_session.search(
        index=[ste.RCOM_ES_CONTENTS_SHELF_INDEX, ste.RCOM_ES_PROGRESSIVE_INDEX],
        body=dsl,
        _source=[Fields.SHELF_ITEMS],
        size=1,
        timeout=timeout)

    hits = results["hits"]["hits"]
    if not hits:
        raise ValueError(f"No shelf_id : {shelf_id}")

    index = hits[0]['_index']
    if index == ste.RCOM_ES_PROGRESSIVE_INDEX:
        inner_hits = hits[0]["inner_hits"][Fields.SHELF_LIST]["hits"]["hits"]

        if not inner_hits:
            raise ValueError(f"No shelf_id : {shelf_id}")

        shelf_items = inner_hits[0].get("_source", {}).get(Fields.SHELF_ITEMS, [])
        expire_date = inner_hits[0].get("_source", {}).get(Fields.SHELF_END_DATE, None)
    else:
        shelf_items = hits[0].get("_source", {}).get(Fields.SHELF_ITEMS, [])
        expire_date = None

    return shelf_items, expire_date


# =================================================================
# ===================== Condition Builders ========================
# =================================================================

def _get_remove_empty_title_condition():
    return {"match_phrase": {'title': "empty title"}}


def _get_remove_blackout_epg():
    return {"term": {"black_out": 1}}


def _get_default_must_not_conditions():
    return [
        _get_remove_empty_title_condition(),
        _get_is_trailer_condition(),
        _get_is_promo_condition(),
        _get_content_providers_condition(['true_vision'])
    ]


def _get_default_must_conditions(language):
    return [
        {"range": {Fields.PUBLISH_DATE: {"lte": "now/h"}}},
        {"match": {Fields.STATUS: "publish"}},
        {"match": {Fields.SEARCHABLE: "Y"}},
        {"term" : {Fields.LANG: language.lower()}},
    ]


def _get_default_should_conditions():
    return  [
        {"range": {Fields.EXPIRY_DATE: {"gt": "now/h"}}},
        {"bool" : {"must_not": [{"exists": {"field": Fields.EXPIRY_DATE}}]}}
    ]


def _get_tv_default_must_conditions(language):
    return [
        {"range": {Fields.PUBLISH_DATE: {"lte": "now/h"}}}, 
        {"match": {Fields.STATUS: "publish"}}, 
        {"match": {Fields.SEARCHABLE: "Y"}},
        {"term": {Fields.LANG: language.lower()}}
    ]


def _get_tv_default_must_not_conditions():
    return [
        _get_remove_empty_title_condition(),
        _get_remove_blackout_epg(),
    ]


def _get_movie_types_conditions(conditions, movie_type, is_vod_layer):
    """
    To support is_vod_layer conditions related to movie_types,
    there are three main cases as follows:

    1. If movie_type = movie specified,
        - then return both vod and non-vod items (discard `is_vod_layer` input).
    2. If movie_type = series specified,
        - then check if `is_vod_layer` is specified then use that value,
          else return non-vod items.
    3. If movie_type is not specified,
        - then match aboved movie and series cases.

    Parameters
    ----------
    param: dsl_bool: DSL bool query dict
    param: movie_type: movie_type ('movie', 'series', or None)
    param: is_vod_layer: vod or non-vod layer ('Y', None)
    ----------
    return: DSL bool query dict
    """

    if movie_type == 'movie':
        conditions['must'].append({"match": {Fields.MOVIE_TYPE: movie_type}})
    elif movie_type == 'series':
        conditions['must'].append({"match": {Fields.MOVIE_TYPE: movie_type}})
        conditions['must'].append({"match": {Fields.EP_MASTER: "Y"}})
        if is_vod_layer:
            conditions['must'].append({"term": {Fields.IS_VOD_LAYER: is_vod_layer}})
        else:
            conditions['must_not'].append({"term": {Fields.IS_VOD_LAYER: "Y"}})
    else:
        conditions['should'].append({"bool": {"must": {"term": {Fields.MOVIE_TYPE: "movie"}}}})
        if is_vod_layer:
            conditions['should'].append({
                "bool": {
                    "must": [
                        {"term": {Fields.MOVIE_TYPE: "series"}},
                        {"term": {Fields.IS_VOD_LAYER: is_vod_layer}}
                    ]
                }
            })
        else:
            conditions['should'].append({
                "bool": {
                    "must": {"term": {Fields.MOVIE_TYPE: "series"}},
                    "must_not": {"term": {Fields.IS_VOD_LAYER: "Y"}}
                }
            })

        conditions['minimum_should_match'] = 1 + conditions.get('minimum_should_match', 0)

    return conditions


def _get_ids_condition(ids, language, required_ids=True):
    if ids:
        ids = _add_lang_prefix_for_ids(ids, language)
        return {'terms': {Fields._ID: ids}}
    
    if required_ids:
        raise ValueError("'ids' must be specified")


def _get_is_promo_condition(is_promo='yes'):
    return {'match': {Fields.IS_PROMO: is_promo}}


def _get_is_trailer_condition(is_trailer='yes'):
    return {'match': {Fields.IS_TRAILER: is_trailer}}


def _get_tvod_flag_contidion(tvod_flag):
    if not tvod_flag: return
    return {'match': {Fields.TVOD_FLAG: tvod_flag}}


def _get_other_types_condition(other_types):
    if not other_types: return
    other_types_lower = [str(i).lower() for i in other_types]    
    return {'terms': {f"{Fields.OTHER_TYPE}.keyword": other_types_lower}}


def _get_content_rights_condition(content_rights):
    if not content_rights: return
    return {'terms': {f'{Fields.CONTENT_RIGHTS}.keyword': content_rights}}


def _get_subscription_tiers_condition(subscription_tiers):
    if not subscription_tiers: return
    return {'terms': {f"{Fields.SUBSCRIPTION_TIERS}.keyword": subscription_tiers}}


def _get_genres_condition(genres):
    if not genres: return
    genres_str = [str(i).lower() for i in genres]
    return {'terms': {f"{Fields.GENRES}.keyword": genres_str}}


def _get_categories_condition(categories):
    if not categories: return
    categories_str = [str(i).lower() for i in categories]
    return {'terms': {f"{Fields.CATEGORY}.keyword": categories_str}}


def _get_rates_condition(rates):
    if not rates: return
    return {'terms': {f"{Fields.RATE}.keyword": rates}}


def _get_package_alacarte_list_condition(package_alacarte_list):
    if not package_alacarte_list: return
    package_alacarte_str = [str(i).upper() for i in package_alacarte_list]
    return {'terms': {f"{Fields.PACKAGE_ALACARTE_LIST}.keyword": package_alacarte_str}}


def _get_content_providers_condition(content_provider):
    if not content_provider: return
    content_provider_str = [str(i) for i in content_provider]
    return {'terms': {f"{Fields.CONTENT_PROVIDER}": content_provider_str}}


def _sort_es_doc(session,
                 type,
                 size=10,
                 language="EN",
                 genres=[],
                 article_categories=[],
                 sort_field=Fields.PUBLISH_DATE,
                 sort_by="desc",
                 tvod_flag=None,
                 subscription_tiers=[],
                 rates=[],
                 package_alacarte_list="",
                 timeout="10s",
                 filter_ids=[],
                 fields=None,
                 is_vod_layer=None,
                 content_rights=[],
                 other_types=[],
                 must_nots=[]):

    if size <= 0: return []

    conditions = {}
    dsl = {
        "query": {"constant_score": {"filter": {"bool": conditions}}},
        "sort": {sort_field: {"order": sort_by}}
    }
    
    conditions['must'] = _get_default_must_conditions(language)
    conditions['must'] += list(filter(None, [
        _get_tvod_flag_contidion(tvod_flag),
        _get_content_rights_condition(content_rights),
        _get_rates_condition(rates),
        _get_subscription_tiers_condition(subscription_tiers),
        _get_genres_condition(genres),
        _get_categories_condition(article_categories),
        _get_package_alacarte_list_condition(package_alacarte_list),
        _get_other_types_condition(other_types),
    ]))
    
    conditions['must_not'] = _get_default_must_not_conditions()
    conditions['must_not'] += list(filter(None, [
         _get_ids_condition(filter_ids, language, required_ids=False),
    ]))
    if must_nots:
        conditions['must_not'] += must_nots
        
    conditions['should'] = _get_default_should_conditions()
    conditions['minimum_should_match'] = 1

    # custom/mixed conditions
    conditions = _get_movie_types_conditions(conditions, type, is_vod_layer)

    _logger.debug(f"_sort_es_doc: DSL {dsl}")
    results = session.search(index=ste.RCOM_ES_TRUETV_INDEX,
                             body=dsl,
                             size=size,
                             _source=_get_return_fields(fields),
                             timeout=timeout)

    hits = results["hits"]["hits"]
    return _build_fields_result(hits) if fields else _build_result_set(hits)


def _validate_type(type):
    if type not in ('movie', 'series', None):
        raise ValueError("Valid types are 'movie', 'series', or 'None'.")


def get_sorted_items(session,
                     sort_field,
                     sort_by='desc',
                     type=None,
                     genres=[],
                     article_categories=[],
                     size=10,
                     language="EN",
                     tvod_flag=None,
                     subscription_tiers=[],
                     rates=[],
                     package_alacarte_list=[],
                     filter_ids=[],
                     fields=None,
                     is_vod_layer=None,
                     content_rights=[],
                     other_types=[],
                     must_nots=[]):
    """
    A utility function that returns latest items.

    Parameters
    ----------
    session : ES session instance.
    sort_field : field to be sorted
    sort_by : a field to be sorted by, either 'asc' or 'desc', default to 'desc'.
    type : item type, either 'movie', or 'series'.
    size : size of the return resultset, default to 10.
    language : a language, either 'EN' or 'TH', default 'EN'. 
    genres : filters out items that don't match the given genres. The comparison is in an OR fashion.
    article_categories : filters out items that don't match the given article_categories. The comparison is in an OR fashion.
    tvod_flag : a tvod flag, either 'Y', or 'N'.
    subscription_tiers : filters out items that don't match the given subscription tiers. The comparison is in an OR fashion.
    rates : filter out items that don't match the given rates (according to CMS data)
    package_alacarte_list: filters out items that don't match the given package_alacarte. The comparison is in an OR fashion.
    filter_ids: list of id for filter out ex. ["15qaWX93Qrd5", "Z6exd4rmnV8y"]
    fields: return only field in fields list
    is_vod_layer: flag for filter is_vod_layer ('Y', 'N' or 'other')
    other_types: a list of other item types, e.g., 'news', 'animation', 'entertainment', 'knowledge', 'documentary', etc.
    must_nots: a list of query than contain condition to use in elastic search must_nots e.g., [{"match": {"partner_related": "abc"}}]

    Returns
    -------
    An array of dictionaries containing item details of a given input language as specified in the API
    """
    _validate_type(type)
    return _sort_es_doc(session=session,
                        type=type,
                        size=size,
                        language=language,
                        genres=genres,
                        article_categories=article_categories,
                        sort_field=sort_field,
                        sort_by=sort_by,
                        tvod_flag=tvod_flag,
                        subscription_tiers=subscription_tiers,
                        rates=rates,
                        package_alacarte_list=package_alacarte_list,
                        filter_ids=filter_ids,
                        fields=fields,
                        is_vod_layer=is_vod_layer,
                        content_rights=content_rights,
                        other_types=other_types,
                        must_nots=must_nots)


def get_random_items(session,
                     seed=None,
                     type=None,
                     genres=[],
                     article_categories=[],
                     size=10,
                     language="EN",
                     tvod_flag=None,
                     subscription_tiers=[],
                     rates=[],
                     package_alacarte_list=[],
                     fields=None,
                     is_vod_layer=None,
                     content_rights=[],
                     other_types=[],
                     must_nots=[]):
    """
    A utility function that returns random items.

    Parameters
    ----------
    session : ES session instance.
    seed : the number use as seed to random (Default: 1)
    type : item type, either 'movie', or 'series'.
    size : size of the return resultset, default to 10.
    language : a language, either 'EN' or 'TH', default 'EN'. 
    genres : filters out items that don't match the given genres. The comparison is in an OR fashion.
    article_categories : filters out items that don't match the given article_categories. The comparison is in an OR fashion.
    tvod_flag : a tvod flag, either 'Y', or 'N'.
    subscription_tiers : filters out items that don't match the given subscription tiers. The comparison is in an OR fashion.
    rates : filter out items that don't match the given rates (according to CMS data)
    package_alacarte_list: filters out items that don't match the given package_alacarte. The comparison is in an OR fashion.
    fields: return only field in fields list
    is_vod_layer: flag for filter is_vod_layer ('Y', 'N' or 'other')
    other_types: a list of other item types, e.g., 'news', 'animation', 'entertainment', 'knowledge', 'documentary', etc.
    must_nots: a list of query than contain condition to use in elastic search must_nots e.g., [{"match": {"partner_related": "abc"}}]

    Returns
    -------
    An array of dictionaries containing item details of a given input language as specified in the API
    """
    _validate_type(type)
    if seed == None:
        seed = time.time_ns()
    return _get_random_items_from_elasticsearch(es_session=session,
                                                seed=seed,
                                                type=type,
                                                size=size,
                                                language=language,
                                                genres=genres,
                                                article_categories=article_categories,
                                                tvod_flag=tvod_flag,
                                                subscription_tiers=subscription_tiers,
                                                rates=rates,
                                                package_alacarte_list=package_alacarte_list,
                                                fields=fields,
                                                is_vod_layer=is_vod_layer,
                                                content_rights=content_rights,
                                                other_types=other_types,
                                                must_nots=must_nots)


def get_latest_items(session,
                     type=None,
                     genres=[],
                     article_categories=[],
                     size=10,
                     language="EN",
                     tvod_flag=None,
                     subscription_tiers=[],
                     rates=[],
                     package_alacarte_list=[],
                     filter_ids=[],
                     fields=None,
                     is_vod_layer=None,
                     content_rights=[],
                     other_types=[],
                     must_nots=[]):
    """
    A utility function that returns latest items.

    Parameters
    ----------
    session : ES session instance.
    type : item type, either 'movie', or 'series'.
    size : size of the return resultset, default to 10.
    language : a language, either 'EN' or 'TH', default 'EN'. 
    genres : filters out items that don't match the given genres. The comparison is in an OR fashion.
    article_categories : filters out items that don't match the given article_categories. The comparison is in an OR fashion.
    tvod_flag : a tvod flag, either 'Y', or 'N'.
    subscription_tiers : filters out items that don't match the given subscription tiers. The comparison is in an OR fashion.
    rates : filter out items that don't match the given rates (according to CMS data)
    package_alacarte_list: filters out items that don't match the given package_alacarte. The comparison is in an OR fashion.
    filter_ids: list of id for filter out ex. ["15qaWX93Qrd5", "Z6exd4rmnV8y"]
    fields: return only field in fields list
    is_vod_layer: flag for filter is_vod_layer ('Y', 'N' or 'other')
    other_types: a list of other item types, e.g., 'news', 'animation', 'entertainment', 'knowledge', 'documentary', etc.
    must_nots: a list of query than contain condition to use in elastic search must_nots e.g., [{"match": {"partner_related": "abc"}}]

    Returns
    -------
    An array of dictionaries containing item details of a given input language as specified in the API
    """
    _validate_type(type)
    return _sort_es_doc(session=session,
                        type=type,
                        size=size,
                        language=language,
                        genres=genres,
                        article_categories=article_categories,
                        sort_field=Fields.PUBLISH_DATE,
                        sort_by="desc",
                        tvod_flag=tvod_flag,
                        subscription_tiers=subscription_tiers,
                        rates=rates,
                        package_alacarte_list=package_alacarte_list,
                        filter_ids=filter_ids,
                        fields=fields,
                        is_vod_layer=is_vod_layer,
                        content_rights=content_rights,
                        other_types=other_types,
                        must_nots=must_nots)


def get_most_popular_items(session,
                           type=None,
                           genres=[],
                           article_categories=[],
                           popularity_sort=PopularityPeriod.PAST_WEEK,
                           size=10,
                           language="EN",
                           tvod_flag=None,
                           subscription_tiers=[],
                           rates=[],
                           package_alacarte_list=[],
                           filter_ids=[],
                           fields=None,
                           is_vod_layer=None,
                           content_rights=[],
                           other_types=[],
                           must_nots=[]):
    """
    A utility function that returns the most popular items.

    Parameters
    ----------
    session : ES session instance.
    type : item type, either 'movie', or 'series'.
    genres : filters out items that don't match the given genres. The comparison is in an OR fashion.
    article_categories : filters out items that don't match the given article_categories. The comparison is in an OR fashion.
    popularity_sort : Whether how the degree of popularity is computed, either by 'weekly', 'monthly', or 'yearly' fashion.
    size : size of the return resultset, default to 10.
    language : a language, either 'EN' or 'TH', default 'EN'.
    tvod_flag : a tvod flag, either 'Y', or 'N'.
    subscription_tiers : filters out items that don't match the given subscription tiers. The comparison is in an OR fashion.
    rates : filter out items that don't match the given rates (according to CMS data)
    package_alacarte_list: filters out items that don't match the given package_alacarte. The comparison is in an OR fashion.
    filter_ids: list of id for filter out ex. ["15qaWX93Qrd5", "Z6exd4rmnV8y"]
    fields: return only field in fields list
    is_vod_layer: flag for filter is_vod_layer ('Y', 'N' or 'other')
    other_types: a list of other item types, e.g., 'news', 'animation', 'entertainment', 'knowledge', 'documentary', etc.
    must_nots: a list of query than contain condition to use in elastic search must_nots e.g., [{"match": {"partner_related": "abc"}}]

    Returns
    -------
    An array of dictionaries containing item details of a given input language as specified in the API
    """
    _validate_type(type)
    return _sort_es_doc(session=session,
                        type=type,
                        size=size,
                        language=language,
                        genres=genres,
                        article_categories=article_categories,
                        sort_field=popularity_sort,
                        sort_by="desc",
                        tvod_flag=tvod_flag,
                        subscription_tiers=subscription_tiers,
                        rates=rates,
                        package_alacarte_list=package_alacarte_list,
                        filter_ids=filter_ids,
                        fields=fields,
                        is_vod_layer=is_vod_layer,
                        content_rights=content_rights,
                        other_types=other_types,
                        must_nots=must_nots)


def get_item_titles(session,
                    item_ids,
                    language,
                    index=ste.RCOM_ES_TRUETV_INDEX):
    """returns movies' title given a list of movie ids

    Parameters
    ----------
    session : object
        elasticsearch session

    item_ids : list
        a list of movie ids

    language : str
        language to return

    index: str
        ES index

    Returns
    -------
    dict

        a dictionary containing movie ids and its title
    """
    item_ids = _add_lang_prefix_for_ids(item_ids, language)
    dsl = {
        "query": {
            "constant_score": {
                "filter": {
                    "bool": {
                        "must": [{
                            "terms": {
                                Fields._ID: item_ids
                            }
                        }]
                    }
                }
            }
        }
    }

    results = session.search(index=index,
                             body=dsl,
                             size=len(item_ids),
                             _source=[Fields.ID, Fields.TITLE])
    hits = results["hits"]["hits"]

    items_dict = {
        hit['_source'][Fields.ID]: hit['_source'][Fields.TITLE] for hit in hits
    }
    return items_dict


def get_tv_channels(token,
                    redis_session=None,
                    fields=None,
                    limit=ste.CMS_FN_LIMIT):
    """
    A utiltity function for get data from CMS fn content with additional fields.

    Parameters
    ----------
    token: auth token for CMS fncontent
    fields : additional fields for that content (eg. channel_code, subscription_package)
    limit: limit of return items

    Returns
    -------
    An array of dictionary containing channels data.
    None if request to CMS exceeds timeout
    """
    tv_channels = []
    query_parameters = {'content_type': "livetv", 'limit': limit}

    if fields:
        query_parameters.update(
            {"fields": ",".join([field for field in fields])})

    url = f"{ste.CMS_FN_CONTENT_URL}?{urllib.parse.urlencode(query_parameters)}"
    hashed_url = hashlib.md5(url.encode()).hexdigest()

    if redis_session:
        try:
            tv_channels = redis_session.get(f"TV_CHANNELS:{hashed_url}")
            tv_channels = json.loads(tv_channels)
        except Exception as error:
            _logger.error(f"Get TV Channel redis error: {error}")

    if not tv_channels:
        if fields is None:
            fields = []

        headers = {"Authorization": token}
        try:
            resp = requests.get(url, headers=headers, timeout=ste.REQUEST_TIMEOUT_MS/1000)
        except Exception as error:
            _logger.error(f"{error}")
            if isinstance (error, requests.exceptions.Timeout):
                raise

        resp.raise_for_status()
        resp = resp.json()
    
        tv_channels = resp["data"]
        if redis_session:
            redis_session.set(name=f"TV_CHANNELS:{hashed_url}",
                              value=json.dumps(tv_channels,
                                               default=default_time),
                              px=ste.REDIS_DEFAULT_EXPIRE_MS)

    return _build_tv_channels_mapping(tv_channels)


def default_time(o):
    if isinstance(o, (datetime.date, datetime.datetime)):
        return o.isoformat()


def get_tv_metadata(token: str, title_ids: List[str]) -> List:
    """
    A utility function for get data from CMS fn epg with specify title_ids
    If the title_id is duplicated it will return first item in the list

    Parameters
    ----------
    token: auth token for CMS fnepg
    title_ids : List of title_id of tv program

    Returns
    -------
    A tv program metadata.
    None if request to CMS exceeded timeout
    """
    headers = {"Authorization": token}
    _CMS_EPG_BATCH_SIZE = 100

    items_data = []
    for i in range(0, len(title_ids), _CMS_EPG_BATCH_SIZE):
        batch_title_ids = ','.join(title_ids[i:i + _CMS_EPG_BATCH_SIZE])
        try:
            url = f"{ste.CMS_FN_EPG_URL}?title_id={batch_title_ids}"
            resp = requests.get(url, headers=headers, timeout=ste.REQUEST_TIMEOUT_MS/1000)
            resp.raise_for_status()
            resp = resp.json()
            items = resp.get("data", [])
            items_data.extend(items)
        except Exception as error:
            _logger.error(f"{error}")
            if isinstance (error, requests.exceptions.Timeout):
                raise 

    return items_data


def _build_tv_channels_mapping(tv_channels):
    channels_mapping = dict()

    for tv_channel in tv_channels:
        channel_code = tv_channel.get(Fields.CHANNEL_CODE)
        if channel_code not in channels_mapping.keys():
            channels_mapping[channel_code] = []
        channels_mapping[channel_code].append(tv_channel)
    return channels_mapping


def _validate_tv_interval_time(start_time, end_time, padding_time):
    is_only_current_show = True
    if not isinstance(start_time, int):
        raise TypeError(
            "`start_time` should be a start timestamp (seconds from epoch)")

    if start_time == -1:
        """https://truedmp.atlassian.net/secure/RapidBoard.jspa?rapidView=185&modal=detail&selectedIssue=DMPREC-713
        """
        is_only_current_show = False
        start_time = int(datetime.datetime.utcnow().timestamp())

    if start_time and not end_time:
        end_time = start_time + padding_time

    if not isinstance(end_time, int):
        raise TypeError(
            "`end_time` should be an end timestamp (seconds from epoch)")

    if start_time >= end_time:
        raise ValueError(
            "`start_time` cannot greater than or equal to `end_time`")

    start_time = datetime.datetime.fromtimestamp(start_time).strftime(
        "%Y-%m-%dT%H:%M:%S.000Z")
    end_time = datetime.datetime.fromtimestamp(end_time).strftime(
        "%Y-%m-%dT%H:%M:%S.000Z")

    return start_time, end_time, is_only_current_show


def _add_lang_prefix_for_ids(ids: List[str], language: str) -> List[str]:
    """
        Add language in front of id for _id in es7 format
        :param ids: List of item's ID
        :param language : a language, either 'TH' or 'EN', default to 'TH'
        :return: List of item's ID with language
    """
    if isinstance(ids, list):
        return [f"{language.lower()}-{id}" for id in ids]
    else:
        raise TypeError(f"`ids` should be list. found {type(ids)}")


def get_tv_top_hit(token, lang='th', recommend=True, limit=50, app_name=None):
    """
        Add language in front of id for _id in es7 format
        :param token: auth token for CMS fnepg
        :param lang : a language, either 'TH' or 'EN', default to 'TH'
        :param limit: limit of return items
        :param app_name: application name
        :return: List of item's ID with language, None if timeout exceeded
    """
    headers = {"Authorization": token}

    payload = {'lang': lang, 'limit': limit}

    if recommend:
        payload['recommend'] = 'true'
    else:
        payload['recommend'] = 'false'

    if app_name:
        payload['app_name'] = app_name

    items_data = []
    try:
        url = f"{ste.CMS_FN_TRENDING_BY_CCU}"
        resp = requests.get(url, headers=headers, params=payload, timeout=ste.REQUEST_TIMEOUT_MS/1000)
        resp.raise_for_status()
        resp = resp.json()
        items = resp.get("data", [])
        items_data.extend(items)
    except Exception as error:
        _logger.error(f"{error}")
        if isinstance (error, requests.exceptions.Timeout):
            raise

    return items_data


def _set_cache_tv_items(redis_session,
                        items,
                        key_id=Fields._ID,
                        prefix="",
                        expire_ms=ste.REDIS_DEFAULT_EXPIRE_MS,
                        language='TH'):
    """
    Add item to redis with `expire_ms` expiration
    :param redis_session: Redis session instance
    :param items: List of items
    :param key_id: indicated field for set cache (e.g. "_id", "id", "title_id")
    :param prefix: Prefix key in redis (eg. MOVIE_ITEM:[ITEM_ID])
    :param expire_ms: Expire time in milliseconds
    :param language: a language, either 'TH' or 'EN', default to 'TH'
    :return: None
    """
    for item in items:
        if not isinstance(item, dict):
            _logger.warning(
                f"Item type: `{type(item)}` is not supported. Trying to skip it"
            )
            continue

        item_id = item.get(key_id)
        # need to add language because documents in different language are kept in different _id in es7
        if item_id and key_id != Fields._ID:
            item_id = _add_lang_prefix_for_ids([item_id], language)[0]

        if not item_id:
            item_id = item.get(Fields.ID)
            # need to add language because documents in different language are kept in different _id in es7
            if item_id:
                item_id = _add_lang_prefix_for_ids([item_id], language)[0]

        if not item_id:
            _logger.warning(
                f"`{key_id}` is missing skip caching this item. {item}")
            continue

        name = f"{prefix}{item_id}"
        _logger.debug(f"Set key: {name}")
        try:
            redis_session.set(name=name,
                              value=json.dumps(item, default=default_time),
                              px=expire_ms)
        except Exception as error:
            _logger.warning(f"{error}: Skip {name}")


# This function will deprecated soon move to use get_items_metadata_define_indexes instead
def get_items_metadata_from_all_indexes(
    es_session: object,
    redis_session: object,
    item_ids: List[str],
    fields: List[str] = ["*"],
    language: str = "th",
    status: str = "publish",
    timestamp: str = None,
    timeout: str = "10s", # TODO: should it be string or int
    size: int = 10,
    default_expire_ms=ste.REDIS_DEFAULT_EXPIRE_MS
) -> List[Dict]:
    """Get content items and shelf (both shelf and progressive) metadata with filtery by language, status, timestamp and use redis cache
    **Important Note** : order of return items not follow input item_ids 

    Args:
        es_session (object): ES session instance
        redi_session (object): redis session instance
        item_ids (List[str]): list of item IDs
        language (str, optional): a language, either 'TH' or 'EN', default to 'TH'.
        fields (List[str], optional): fields that want from query. Defaults to ["*"].
        timestamp (str, optional): timestamp that use to for filter publish date and expire date. Defaults to None.
        status (str, optional): status that  use for filter. Defaults to "publish".
        timeout (str, optional): timeout for es search. Defaults to "10s".
        size (int, optional): limit size of return document

    Returns:
        List[Dict]: items
    """

    def _get_redis_key(item_id: str, fields: List[str], language: str, status: str, timestamp: str, prefix="CMS_ID"):
        return f"{prefix}:{str((item_id, fields, language, status, timestamp))}"

    if timestamp is None:
        timestamp = "now/h"

    non_cached_item_ids = []
    non_cached_items = []
    cached_item_ids = []
    cached_items = []

    if redis_session:
        # since have redis_session try to search is it have cached_items
        redis_keys = [_get_redis_key(item_id=item_id, fields=fields, language=language, status=status, timestamp=timestamp) for item_id in item_ids]
        redis_items = redis_session.mget(redis_keys)
        for idx, redis_item in enumerate(redis_items):
            if redis_item:
                redis_items[idx] = json.loads(redis_item)
            else:
                redis_items[idx] = None
        cached_items = list(filter(None, redis_items))

        # get non_cached_item_ids and cached_items
        for redis_item in redis_items:
            if redis_item:
                cached_item_ids.append(redis_item[Fields.ID])
        non_cached_item_ids = [item_id for item_id in item_ids if item_id not in cached_item_ids]

        _logger.debug(f"get_items_metadata_from_all_indexes: non_cached_item_ids: {non_cached_item_ids}")
        _logger.debug(f"get_items_metadata_from_all_indexes: cached_item_ids: {cached_item_ids}")


    else:
        # since not have cahe then use item_ids as non_cached_item_ids
        non_cached_item_ids = item_ids
    
    if non_cached_item_ids:
        # since non_cached_item_ids is not empty then try to query from es
        # query elastic search
        non_cached_items = get_items_metadata_from_all_indexes_elastic_search(
            es_session=es_session,
            item_ids=non_cached_item_ids,
            fields= fields,
            language=language,
            status=status,
            timestamp=timestamp,
            timeout=timeout,
            size=size
        )

        # set non cached items
        if redis_session:
            pipeline = redis_session.pipeline()
            for item in non_cached_items:
                redis_key = _get_redis_key(item_id=item[Fields.ID], fields=fields, language=language, status=status, timestamp=timestamp)
                try:
                    pipeline.set(name=redis_key, value=json.dumps(item), px=default_expire_ms)
                except Exception as error:
                    _logger.warning(f"{error}: Skip {redis_key}")
            pipeline.execute()

    # concat cached and non cached_items
    items = cached_items + non_cached_items
    # sort item by item_ids
    keymap = keymap = {o: i for i, o in enumerate(item_ids)}
    items.sort(key=lambda x: keymap[x[Fields.ID]])
    return items

# This function will deprecated soon move to use get_items_metadata_define_indexes_elastic_search instead
def get_items_metadata_from_all_indexes_elastic_search(
    es_session: object,
    item_ids: List[str],
    fields: List[str] = ["*"],
    language: str = "th",
    status: str = "publish",
    timestamp: str = None,
    timeout: str = "10s", # TODO: should it be string or int
    size: int = 10
) -> List[Dict]:
    """Get content items and shelf (both shelf and progressive) metadata with filtery by language, status, timestamp from elastic search
    TODO: should we move this fucntion outside tv_utils because this query from all indexes ? ex. create shelf_utils.py

    Args:
        es_session (object): ES session instance
        item_ids (List[str]): list of item IDs
        language (str, optional): a language, either 'TH' or 'EN', default to 'TH'.
        fields (List[str], optional): fields that want from query. Defaults to ["*"].
        timestamp (str, optional): timestamp that use to for filter publish date and expire date. Defaults to None.
        status (str, optional): status that  use for filter. Defaults to "publish".
        timeout (str, optional): timeout for es search. Defaults to "10s".
        size (int, optional): limit size of return document

    Returns:
        List[Dict]: items
    """
    # assign default timestamp
    if timestamp is None:
        timestamp = "now/h"

    # es query
    dsl = {
        "query": {
            "bool": {
                "minimum_should_match": 1,
                "should": [
                    # this query for content and shelf
                    {
                        "bool": {
                            "filter": [
                                # TODO: should we change this index more scope by query only wanted content index and shelf index ?
                                {"term": {"_index": ste.RCOM_ES_ALL_INDEX}},
                                {
                                    "terms": {
                                        f"{Fields.ID}": item_ids
                                    }
                                },
                                {"term": {Fields.LANG: language.lower()}},
                                {"term": {Fields.STATUS: status}},
                                {"range": {Fields.PUBLISH_DATE: {"lte": timestamp}}},
                            ],
                            "minimum_should_match": 1,
                            "should": [
                                {"range": {Fields.EXPIRY_DATE: {"gt": timestamp}}},
                                {
                                    "bool": {
                                        "must_not": [{"exists": {"field": Fields.EXPIRY_DATE}}]
                                    }
                                }
                            ]
                        }
                    },
                    # this query for progressive shelf
                    {
                        "bool": {
                            "filter": [
                                {"term": {"_index": ste.RCOM_ES_PROGRESSIVE_INDEX}},
                                {
                                    "terms": {
                                        f"{Fields.ID}.keyword": item_ids
                                    }
                                },
                                {"term": {Fields.COUNTRY: language.lower()}},
                                {"term": {Fields.STATUS: status}},
                                {"range": {Fields.PUBLISH_DATE: {"lte": timestamp}}},
                            ],
                            "minimum_should_match": 1,
                            "should": [
                                {"range": {Fields.EXPIRY_DATE: {"gt": timestamp}}},
                                {
                                    "bool": {
                                        "must_not": [{"exists": {"field": Fields.EXPIRY_DATE}}]
                                    }
                                }
                            ]
                        }
                    }

                ]
            }
        },
        "_source": fields,
        "size": size
    }
    _logger.debug(f"get_items_metadata_from_all_indexes_elastic_search: dsl: {dsl}")

    # run query
    results = es_session.search(
        index="*",
        body=dsl,
        timeout=timeout
    )

    items = [hit["_source"] for hit in results["hits"]["hits"]]

    return items


def get_items_metadata_define_indexes(
    es_session: object,
    redis_session: object,
    item_ids: List[str],
    fields: List[str] = ["*"],
    indexs: List[str] = ["*"],
    language: str = "th",
    status: str = "publish",
    timestamp: str = None,
    timeout: str = "10s", # TODO: should it be string or int
    size: int = 10,
    default_expire_ms=ste.REDIS_DEFAULT_EXPIRE_MS
) -> List[Dict]:
    """Get content items and shelf (both shelf and progressive) metadata with filtery by language, status, timestamp and use redis cache
    **Important Note** : order of return items not follow input item_ids 

    Args:
        es_session (object): ES session instance
        redi_session (object): redis session instance
        item_ids (List[str]): list of item IDs
        language (str, optional): a language, either 'TH' or 'EN', default to 'TH'.
        fields (List[str], optional): fields that want from query. Defaults to ["*"].
        timestamp (str, optional): timestamp that use to for filter publish date and expire date. Defaults to None.
        status (str, optional): status that  use for filter. Defaults to "publish".
        timeout (str, optional): timeout for es search. Defaults to "10s".
        size (int, optional): limit size of return document

    Returns:
        List[Dict]: items
    """
    def _get_redis_key(item_id: str, fields: List[str], language: str, status: str, timestamp: str, prefix="CMS_ID"):
        return f"{prefix}:{str((item_id, fields, language, status, timestamp))}"

    if timestamp is None:
        timestamp = "now/h"

    non_cached_item_ids = []
    non_cached_items = []
    cached_item_ids = []
    cached_items = []

    if redis_session:
        # since have redis_session try to search is it have cached_items
        redis_keys = [_get_redis_key(item_id=item_id, fields=fields, language=language, status=status, timestamp=timestamp) for item_id in item_ids]
        redis_items = redis_session.mget(redis_keys)
        for idx, redis_item in enumerate(redis_items):
            if redis_item:
                redis_items[idx] = json.loads(redis_item)
            else:
                redis_items[idx] = None
        cached_items = list(filter(None, redis_items))

        # get non_cached_item_ids and cached_items
        for redis_item in redis_items:
            if redis_item:
                cached_item_ids.append(redis_item[Fields.ID])
        non_cached_item_ids = [item_id for item_id in item_ids if item_id not in cached_item_ids]

        _logger.debug(f"get_items_metadata_from_all_indexes: non_cached_item_ids: {non_cached_item_ids}")
        _logger.debug(f"get_items_metadata_from_all_indexes: cached_item_ids: {cached_item_ids}")


    else:
        # since not have cahe then use item_ids as non_cached_item_ids
        non_cached_item_ids = item_ids
    
    if non_cached_item_ids:
        # since non_cached_item_ids is not empty then try to query from es
        # query elastic search
        non_cached_items = get_items_metadata_define_indexes_elastic_search(
            es_session=es_session,
            item_ids=non_cached_item_ids,
            fields= fields,
            indexs=indexs,
            language=language,
            status=status,
            timestamp=timestamp,
            timeout=timeout,
            size=size
        )

        # set non cached items
        if redis_session:
            pipeline = redis_session.pipeline()
            for item in non_cached_items:
                redis_key = _get_redis_key(item_id=item[Fields.ID], fields=fields, language=language, status=status, timestamp=timestamp)
                try:
                    pipeline.set(name=redis_key, value=json.dumps(item), px=default_expire_ms)
                except Exception as error:
                    _logger.warning(f"{error}: Skip {redis_key}")
            pipeline.execute()

    # concat cached and non cached_items
    items = cached_items + non_cached_items
    # sort item by item_ids
    keymap = keymap = {o: i for i, o in enumerate(item_ids)}
    items.sort(key=lambda x: keymap[x[Fields.ID]])
    return items

def get_items_metadata_define_indexes_elastic_search(
    es_session: object,
    item_ids: List[str],
    fields: List[str] = ["*"],
    indexs: List[str] = ["*"],
    language: str = "th",
    status: str = "publish",
    timestamp: str = None,
    timeout: str = "10s", # TODO: should it be string or int
    size: int = 10
) -> List[Dict]:
    """Get content items and shelf (both shelf and progressive) metadata with filtery by language, status, timestamp from elastic search
    TODO: should we move this fucntion outside tv_utils because this query from all indexes ? ex. create shelf_utils.py

    Args:
        es_session (object): ES session instance
        item_ids (List[str]): list of item IDs
        language (str, optional): a language, either 'TH' or 'EN', default to 'TH'.
        fields (List[str], optional): fields that want from query. Defaults to ["*"].
        indexs (List[str], optional): index of elasticsearch. Defaults to ["*"].
        timestamp (str, optional): timestamp that use to for filter publish date and expire date. Defaults to None.
        status (str, optional): status that  use for filter. Defaults to "publish".
        timeout (str, optional): timeout for es search. Defaults to "10s".
        size (int, optional): limit size of return document

    Returns:
        List[Dict]: items
    """

    # assign default timestamp
    if timestamp is None:
        timestamp = "now/h"

    # es query
    dsl = {
        "query": {
            "bool": {
                "minimum_should_match": 1,
                "should": [
                    # this query for content and shelf
                    {
                        "bool": {
                            "filter": [
                                # TODO: should we change this index more scope by query only wanted content index and shelf index ?
                                {"term": {"_index": ste.RCOM_ES_ALL_INDEX}},
                                {
                                    "terms": {
                                        f"{Fields.ID}": item_ids
                                    }
                                },
                                {"term": {Fields.LANG: language.lower()}},
                                {"term": {Fields.STATUS: status}},
                                {"range": {Fields.PUBLISH_DATE: {"lte": timestamp}}},
                            ],
                            "minimum_should_match": 1,
                            "should": [
                                {"range": {Fields.EXPIRY_DATE: {"gt": timestamp}}},
                                {
                                    "bool": {
                                        "must_not": [{"exists": {"field": Fields.EXPIRY_DATE}}]
                                    }
                                }
                            ]
                        }
                    },
                    # this query for progressive shelf
                    {
                        "bool": {
                            "filter": [
                                {"term": {"_index": ste.RCOM_ES_PROGRESSIVE_INDEX}},
                                {
                                    "terms": {
                                        f"{Fields.ID}.keyword": item_ids
                                    }
                                },
                                {"term": {Fields.COUNTRY: language.lower()}},
                                {"term": {Fields.STATUS: status}},
                                {"range": {Fields.PUBLISH_DATE: {"lte": timestamp}}},
                            ],
                            "minimum_should_match": 1,
                            "should": [
                                {"range": {Fields.EXPIRY_DATE: {"gt": timestamp}}},
                                {
                                    "bool": {
                                        "must_not": [{"exists": {"field": Fields.EXPIRY_DATE}}]
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        },
        "_source": fields,
        "size": size
    }
    _logger.debug(f"get_items_metadata_from_all_indexes_elastic_search: dsl: {dsl}")
    query_index = indexs[0] if len(indexs) == 1 else indexs
    # run query
    results = es_session.search(
        index=query_index,
        body=dsl,
        timeout=timeout
    )

    items = [hit["_source"] for hit in results["hits"]["hits"]]

    return items