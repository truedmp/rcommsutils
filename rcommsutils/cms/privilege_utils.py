#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 18 22:08:20 2017

@author: root
"""
import datetime
import random
import time
from collections import defaultdict
from elasticsearch.exceptions import RequestError
from typing import List, Dict, Tuple
import rcommsutils.cms.settings as ste
from rcomloggingutils.applog_utils import AppLogger

from elasticsearch_dsl import query


class Quota:
    Exceed = 1
    Not_Exceed = 0


class Fields:
    """Utility class to refer to searchable fields in Elastic Search
    """
    # es fields
    _ID = "_id"
    ID = "id"
    ORIGINAL_ID = "original_id"
    MASTER_ID = "master_id"
    CONTENT_TYPE = "content_type"
    TITLE = "title"
    DETAIL = "detail"
    ADDRESS = "address"
    TERM_CONDITION = "term_and_condition"
    TAGS = "tags"
    ARTICLE_CATEGORY = "article_category"
    PUBLISH_DATE = "publish_date"
    USSD = "ussd"
    LOCATION = "location"
    EXPIRE_DATE = "expire_date"
    CAMPAIGN_CODE = "campaign_code"
    THUMB = "thumb"
    HIT_COUNT_DAY = "HIT_COUNT_DAY"
    HIT_COUNT_WEEK = "HIT_COUNT_WEEK"
    HIT_COUNT_MONTH = "HIT_COUNT_MONTH"
    HIT_COUNT_YEAR = "HIT_COUNT_YEAR"
    HIT_COUNT_DAY_1 = "HIT_COUNT_DAY_1"
    HIT_COUNT_DAY_7 = "HIT_COUNT_DAY_7"
    HIT_COUNT_DAY_14 = "HIT_COUNT_DAY_14"
    HIT_COUNT_DAY_30 = "HIT_COUNT_DAY_30"
    STATUS = "status"
    SEARCHABLE = "searchable"
    PRIVILEGE_LIST = "privilege_list"
    THUMB_LIST = "thumb_list"
    HIGHLIGHT = "highlight"
    RCOM_TAGS = 'RCOM_TAGS'
    ALLOW_RECOMMEND = "allow_recommend"
    ALLOW_APP = "allow_app"
    CARD_TYPE = 'card_type'
    SETTING = 'setting'

    DENORMALIZE_PRIVILEGE = "denormalize_privilege"
    DENORMALIZE_BRANCH = "denormalize_branch"
    QUOTA_OVER_EXISTED = "quota_over_existed"
    EXCEEDED_VALUE = 1
    NOT_EXCEEDED_VALUE = 0
    CAMPAIGN_TYPE = "campaign_type"
    DISCOVER = "discover"
    MERCHANT_TYPE = "merchant_type"
    LANG = "lang"

    BOOL = "bool"
    BOOST_FACTOR = "boost_factor"
    CONSTANT_SCORE = "constant_score"
    FUNCTION_SCORE = "function_score"
    QUERY = "query"
    FIELD_VALUE_FACTOR = "field_value_factor"
    TERM = "term"
    TERMS = "terms"
    MATCH = "match"
    RANGE = "range"
    NESTED = "nested"
    EXISTS = "exists"
    GEO_DISTANCE = "geo_distance"
    MULTI_MATCH = "multi_match"
    IDS = "ids"

    SHELF_PIN_ITEMS = "pin_items"
    SHELF_ITEMS = "shelf_items"
    SHELF_LIST = "shelf_list"
    SHELF_ACTIVE = "active"
    SHELF_START_DATE = "start_date"
    SHELF_END_DATE = "end_date"
    SHELF_VERSION = "version"


class PopularityPeriod:
    """Utility class representing popularity period"""
    PAST_DAY = Fields.HIT_COUNT_DAY
    PAST_WEEK = Fields.HIT_COUNT_WEEK
    PAST_MONTH = Fields.HIT_COUNT_MONTH
    PAST_YEAR = Fields.HIT_COUNT_YEAR


_API_COMMON_FIELDS = [
    Fields.ID, Fields.PUBLISH_DATE, Fields.ORIGINAL_ID, Fields.TAGS,
    Fields.TITLE, Fields.DETAIL, Fields.ARTICLE_CATEGORY, Fields.HIT_COUNT_WEEK,
    Fields.HIT_COUNT_MONTH, Fields.HIT_COUNT_YEAR, Fields.CONTENT_TYPE,
    Fields.EXPIRE_DATE, Fields.SETTING
]

_API_PRIVILEGE_RETURN_FIELDS = _API_COMMON_FIELDS + [
    Fields.USSD, Fields.CAMPAIGN_CODE, Fields.TERM_CONDITION, Fields.THUMB,
    Fields.THUMB_LIST, Fields.ALLOW_APP, Fields.HIT_COUNT_DAY_1, Fields.HIT_COUNT_DAY_7,
    Fields.HIT_COUNT_DAY_14, Fields.HIT_COUNT_DAY_30, Fields.CARD_TYPE
]

_API_SHOP_RETURN_FIELDS = _API_COMMON_FIELDS + [
    Fields.ADDRESS, Fields.THUMB_LIST, Fields.PRIVILEGE_LIST, Fields.DISCOVER,
    Fields.MERCHANT_TYPE, Fields.ALLOW_APP, Fields.HIT_COUNT_DAY_1, Fields.HIT_COUNT_DAY_7,
    Fields.HIT_COUNT_DAY_14, Fields.HIT_COUNT_DAY_30, Fields.CARD_TYPE
]

_API_RETURN_PRIVILEGE_FIELDS = [
    Fields.ID, Fields.ORIGINAL_ID, Fields.PRIVILEGE_LIST, Fields.MASTER_ID
]

_MERCHANT_MAP_KEY_MERCHANT_ID = "merchant_id"
_MERCHANT_MAP_KEY_MASTER_ID = "master_id"
_MERCHANT_MAP_KEY_BRANCH_ID = "branch_id"
_MERCHANT_MAP_KEY_NAME = "merchant_name"
_MERCHANT_MAP_ALL_BRANCHES = "All branches"
_MERCHANT_MAP_MERCHANT_NOT_FOUND = "Not found"

_logger = AppLogger(name=__name__)
_blacklist = None


def _get_campaign_code_blacklist(
        blacklist_str=ste.RCOM_ES_PRIVILEGE_CAMPAIGN_BLACKLIST):
    """
    A getter for campaign code blacklist, to ease unit testing. After the fist
    call to this, _blacklist will either be [] or a list populated with campaign
    codes set in ste.RCOM_ES_PRIVILEGE_CAMPAIGN_BLACKLIST

    Params:
        blacklist_str - A string of comma-separated campaign codes
    """
    global _blacklist
    if _blacklist:
        return _blacklist
    else:
        if blacklist_str:
            _blacklist = blacklist_str.split(",")
        else:
            _blacklist = []
        return _blacklist


def _get_return_fields(type):
    if type == ste.RCOM_ES_PRIVILEGE_PRIVILEGE:
        return _API_PRIVILEGE_RETURN_FIELDS
    return _API_SHOP_RETURN_FIELDS


def _get_return_privilege_fields():
    """
    List of fields containing privilge IDs the merchant / branch indices hold,
    and the merchant's / branch's IDs themselves.
    """
    return _API_RETURN_PRIVILEGE_FIELDS


def _build_msearch_query_body(index, priv_ids, language):
    """
    Build a query body for the ES's msearch API to search the given index
    (merchant or branch) that contains the privileges given in priv_ids.
    Note that only merchants / branches with status = publish will be included.
    Parameters
    ----------
    index : an ES index
    priv_ids : list of privilege id
    language : a language, either 'EN' or 'TH', default 'TH'

    """
    header = {}
    header["index"] = index
    query_body = ""

    for priv_id in priv_ids:
        body = {
            "query": {
                "bool": {
                    "filter": {
                        "bool": {
                            "must": [{
                                "match": {
                                    Fields.PRIVILEGE_LIST: priv_id
                                }
                            }, {
                                "match": {
                                    Fields.STATUS: "publish"
                                }
                            }]
                        }
                    }
                }
            }
        }
        query_body = query_body + str(header) + "\n" + str(body) + "\n"

    # ES's msearch API expects double quote, not single quote on field names.
    return query_body.replace("'", "\"")


def _get_merchant_id_for_privileges(session, items, language="TH"):
    """
    Get corresponding merchant_id and master_id for the given privileges. Note
    that only merchants with status = publish will be returned.

    See
    https://www.elastic.co/guide/en/elasticsearch/reference/current/search-multi-search.html

    Parameters
    ----------
    session : an ES session
    items : an array of dictionary containing items details from ES
    language : a language, either 'EN' or 'TH', default 'TH'

    Returns
    -------
    A dictionary between privilege_id and its corresponding merchant_id,
    master_id, branch_id, merchant_name
    """
    result = {}
    priv_ids_to_search_at_branch = []
    priv_ids = list(map(lambda x: x.get("_source", {}).get(Fields.ID), items))

    if len(priv_ids) == 0:
        return result

    _logger.debug("Searching for parent merchant IDs for %i privileges" %
                  (len(priv_ids)))
    merchant_query_body = _build_msearch_query_body(ste.RCOM_ES_SHOP_INDEX,
                                                    priv_ids, language)
    try:
        msearch_results = session.msearch(merchant_query_body)
    except RequestError as error:
        raise ValueError(
            f"Request error index (`{ste.RCOM_ES_SHOP_INDEX}`) is not compatible, found: {error}"
        )

    responses = msearch_results["responses"]

    # The responses from msearch is returned in the same query order as was given to it. So
    # we can build the result mapping using this fact.
    for i in range(len(priv_ids)):
        priv_id = priv_ids[i]
        hits = responses[i].get("hits", {}).get("hits")

        if len(hits) > 0:
            merchant = hits[0].get("_source", {})
            merchant_id = merchant.get(Fields.ID)
            master_id = merchant.get(Fields.ORIGINAL_ID)
            merchant_name = merchant.get(Fields.TITLE)
            result[priv_id] = {
                _MERCHANT_MAP_KEY_MERCHANT_ID: merchant_id,
                _MERCHANT_MAP_KEY_MASTER_ID: master_id,
                _MERCHANT_MAP_KEY_BRANCH_ID: _MERCHANT_MAP_ALL_BRANCHES,
                _MERCHANT_MAP_KEY_NAME: merchant_name
            }
        else:
            # This one isn't found in the merchant index. Search in later in branch index.
            priv_ids_to_search_at_branch.append(priv_id)

    if len(priv_ids_to_search_at_branch) == 0:
        return result

    _logger.debug("Searching for parent branch IDs for %i privileges" %
                  (len(priv_ids_to_search_at_branch)))
    branch_query_body = _build_msearch_query_body(ste.RCOM_ES_BRANCH_INDEX,
                                                  priv_ids_to_search_at_branch,
                                                  language)
    try:
        msearch_results = session.msearch(branch_query_body)
    except RequestError as error:
        raise ValueError(
            f"Request error index (`{ste.RCOM_ES_BRANCH_INDEX}`) is not compatible, found: {error}"
        )

    responses = msearch_results["responses"]

    for i in range(len(priv_ids_to_search_at_branch)):
        priv_id = priv_ids_to_search_at_branch[i]
        hits = responses[i].get("hits", {}).get("hits")

        if len(hits) > 0:
            branch = hits[0].get("_source", {})
            branch_id = branch.get(Fields.ID)
            merchant_id = branch.get(
                Fields.MASTER_ID
            )  # in branch index, merchant_id is stored in master_id field
            master_id = branch.get(
                Fields.ORIGINAL_ID
            )  # in branch index, master_id is stored in original_id field
            merchant_name = branch.get(Fields.TITLE)
            result[priv_id] = {
                _MERCHANT_MAP_KEY_MERCHANT_ID: merchant_id,
                _MERCHANT_MAP_KEY_MASTER_ID: master_id,
                _MERCHANT_MAP_KEY_BRANCH_ID: branch_id,
                _MERCHANT_MAP_KEY_NAME: merchant_name
            }
        else:
            # WTF case. Just log warning and ignore.
            _logger.warning(
                "Privilege %s was not found in both merchant and branch indices"
                % (priv_id))
            result[priv_id] = {
                _MERCHANT_MAP_KEY_MERCHANT_ID: _MERCHANT_MAP_MERCHANT_NOT_FOUND,
                _MERCHANT_MAP_KEY_MASTER_ID: _MERCHANT_MAP_MERCHANT_NOT_FOUND,
                _MERCHANT_MAP_KEY_BRANCH_ID: _MERCHANT_MAP_MERCHANT_NOT_FOUND,
                _MERCHANT_MAP_KEY_NAME: _MERCHANT_MAP_MERCHANT_NOT_FOUND
            }

    return result


def _build_result_set(items, type, priv_merchant_map=None):
    """
    build a privilege/shop result set of a given input

    Parameters
    ----------
    items : an array of dictionary containing items details from ES
    type : type of documents, 'privilege' or 'shop'
    priv_merchant_map : a dictionary between priv_id (CMS ID) and a map
        containing "merchant_id" and "master_id" corresponding to the priv_id.
        This is needed when type = "privilege", otherwise it's ignored.

    Returns
    -------
    An array of dictionaries containing items details as specified in the API
    """

    results = []

    for item_source in items:
        item = item_source.get('_source', {})
        item_dict = dict()

        if type == ste.RCOM_ES_PRIVILEGE_PRIVILEGE:
            if priv_merchant_map == None:
                raise Exception(
                    "priv_merchant_map is needed when type is privilege")

            # get specific fields for privilege
            priv_id = item.get(Fields.ID)
            merchant_info = priv_merchant_map.get(priv_id, {})

            if ((merchant_info.get(_MERCHANT_MAP_KEY_MERCHANT_ID)
                 == _MERCHANT_MAP_MERCHANT_NOT_FOUND)
                    or (merchant_info.get(_MERCHANT_MAP_KEY_MASTER_ID)
                        == _MERCHANT_MAP_MERCHANT_NOT_FOUND)
                    or (merchant_info.get(_MERCHANT_MAP_KEY_NAME)
                        == _MERCHANT_MAP_MERCHANT_NOT_FOUND)
                    or (merchant_info.get(_MERCHANT_MAP_KEY_BRANCH_ID)
                        == _MERCHANT_MAP_MERCHANT_NOT_FOUND)):
                # This privilege doesn't have associated merchant info (probably
                # the owning merchant is "private", not "public"). We have to
                # exclude this privilege from the result
                _logger.debug(
                    "Skipping privilege %s due to missing merchant info" %
                    priv_id)
                continue

            # Note that the field "title" for privilege actually contains the
            # name of the parent merchant. The real privilege's name is contained
            # in the "title_privilege" field
            item_dict[ste.RCOM_API_TITLE] = merchant_info.get(
                _MERCHANT_MAP_KEY_NAME)
            item_dict[ste.RCOM_API_MERCHANT_ID] = merchant_info.get(
                _MERCHANT_MAP_KEY_MERCHANT_ID)
            item_dict[ste.RCOM_API_OLD_MERCHANT_ID] = merchant_info.get(
                _MERCHANT_MAP_KEY_MASTER_ID)
            item_dict[ste.RCOM_API_BRANCH_ID] = merchant_info.get(
                _MERCHANT_MAP_KEY_BRANCH_ID)

            item_dict[ste.RCOM_API_PRIV_ID] = priv_id
            item_dict[ste.RCOM_API_OLD_PRIV_ID] = item.get(Fields.ORIGINAL_ID)
            item_dict[ste.RCOM_API_USSD] = item.get(Fields.USSD)
            item_dict[ste.RCOM_API_CAMPAIGN_CODE] = item.get(
                Fields.CAMPAIGN_CODE)
            item_dict[ste.RCOM_API_TERM_CONDITION] = item.get(
                Fields.TERM_CONDITION)

            item_dict[ste.RCOM_API_TITLE_PRIVILEGE] = item.get(Fields.TITLE)
            item_dict[ste.RCOM_API_THUMBNAIL] = item.get(Fields.THUMB)
            item_dict[ste.RCOM_API_PRV_ALLOW_APP] = item.get(Fields.ALLOW_APP)
            item_dict[ste.RCOM_API_PRV_CARD_TYPE] = item.get(Fields.CARD_TYPE)


        else:
            # get specific fields for merchant
            item_dict[ste.RCOM_API_MERCHANT_ID] = item.get(Fields.ID)
            item_dict[ste.RCOM_API_OLD_MERCHANT_ID] = item.get(
                Fields.ORIGINAL_ID)
            item_dict[ste.RCOM_API_LOCATION] = item.get(Fields.LOCATION)
            item_dict[ste.RCOM_API_ADDRESS] = item.get(Fields.ADDRESS)
            item_dict[ste.RCOM_API_THUMBNAIL] = None if item.get(
                Fields.THUMB_LIST) is None else item.get(Fields.THUMB_LIST).get(
                Fields.HIGHLIGHT)
            item_dict[ste.RCOM_API_THUMBLIST] = item.get(Fields.THUMB_LIST)
            item_dict[ste.RCOM_API_PRIVILEGE_LIST] = item.get(
                Fields.PRIVILEGE_LIST)
            item_dict[ste.RCOM_API_DISCOVER] = item.get(Fields.DISCOVER)
            item_dict[ste.RCOM_API_MERCHANT_TYPE] = item.get(
                Fields.MERCHANT_TYPE)

            item_dict[ste.RCOM_API_TITLE] = item.get(Fields.TITLE)

        # common fields
        item_dict[ste.RCOM_API_ID] = item.get(Fields.ID)
        item_dict[ste.RCOM_API_EXPIRE_DATE] = item.get(Fields.EXPIRE_DATE)
        item_dict[ste.RCOM_API_CONTENT_TYPE] = item.get(Fields.CONTENT_TYPE)
        item_dict[ste.RCOM_API_PUBLISH_DATE] = item.get(Fields.PUBLISH_DATE)
        item_dict[ste.RCOM_API_PRV_HIT_COUNT_WEEK] = item.get(
            Fields.HIT_COUNT_WEEK, 0)
        item_dict[ste.RCOM_API_PRV_HIT_COUNT_MONTH] = item.get(
            Fields.HIT_COUNT_MONTH, 0)
        item_dict[ste.RCOM_API_PRV_HIT_COUNT_YEAR] = item.get(
            Fields.HIT_COUNT_YEAR, 0)
        item_dict[ste.RCOM_API_PRV_HIT_COUNT_DAY_1] = item.get(
            Fields.HIT_COUNT_DAY_1, 0)
        item_dict[ste.RCOM_API_PRV_HIT_COUNT_DAY_7] = item.get(
            Fields.HIT_COUNT_DAY_7, 0)
        item_dict[ste.RCOM_API_PRV_HIT_COUNT_DAY_14] = item.get(
            Fields.HIT_COUNT_DAY_14, 0)
        item_dict[ste.RCOM_API_PRV_HIT_COUNT_DAY_30] = item.get(
            Fields.HIT_COUNT_DAY_30, 0)

        item_dict[ste.RCOM_API_DETAIL] = item.get(Fields.DETAIL)
        item_dict[ste.RCOM_API_TAGS] = item.get(Fields.TAGS)
        item_dict[ste.RCOM_API_ARTICLE_CATEGORY] = item.get(
            Fields.ARTICLE_CATEGORY)
        item_dict[ste.RCOM_API_SETTING] = item.get(Fields.SETTING, {})

        results.append(item_dict)

    return results


def get_items_define_fields(session,
                            type,

                            # search filters
                            ids=[],
                            tags=[],
                            allow_app=[],
                            article_category=[],
                            campaign_type=[],
                            card_type=[],
                            merchant_type=[],
                            language="th",

                            # search function
                            fields=['id'],
                            size=10,
                            dry_run=False,
                            timeout=10):
    """
        A utility function that sorts documents in ES based on specific fields and condition.
        Currently, use in Privilege Personalized (PP2) and Special Deals for You (PP3)

        Parameters
        ----------
        size             : size of the return resultset, default to 10.
        ids              : a list of item ids to search (exact match)
        tags             : filters out items that don't match the given categories. The comparison is in an OR fashion.
        article_category : filter privilege by list of article_category
        campaign_type    : filters out items that don't match the given campaign_type. There are 2 types: privilege, point.
        card_type        : filter privilege by type of card
        merchant_type    : filters out items that don't match the given merchant_type (e.g., ka, none_ka).
        allow_app        : filter privilege by allow app
        sort_field       : field to be sorted, default to PopularityPeriod.PAST_WEEK
        sort_by          : a field to be sorted by, either 'asc' or 'desc', default to 'desc'
        type             : types of document for search. There are 2 types: privilege(default) and shop
        language         : a language, either 'EN' or 'TH', default 'TH'
        dry_run          : whether to return built search query, or execute search to get items (bool)
        timeout          : timeout for es search, default to 10.

        Returns
        -------
        An array of dictionaries containing items details as specified in the API
        """
    index = None
    filter_conditions   = get_default_filter_conditions(language)
    should_conditions   = get_default_should_conditions()
    must_not_conditions = get_default_must_not_conditions()

    # type-specific conditions
    if type == 'privilege':
        index = ste.RCOM_ES_PRIVILEGE_INDEX
        filter_conditions += list(filter(None, [
            query.Q(Fields.EXISTS, field='article_category'),
            get_allow_app_condition(allow_app),
            get_campaign_type_condition(campaign_type),
            get_card_type_condition(card_type),
        ]))
        must_not_conditions += get_default_privilege_must_not_coditions()

    elif type=='trueyoumerchant':
        index = ste.RCOM_ES_SHOP_INDEX
        filter_conditions = get_default_shop_filter_conditions(language, filter_expired=False)
        filter_conditions += get_campaign_type_filtering(campaign_type)
        filter_conditions += list(filter(None, [
            get_allow_app_condition(allow_app),
            get_merchant_type_condition(merchant_type, default=['ka']),  # key account
            query.Q(Fields.TERM, searchable='Y')
        ]))

    elif type=='trueyouarticle':
        index = ste.RCOM_ES_TRUEYOU_ARTICLE_INDEX

    elif type=='highlight' or type == 'hilight':
        index = ste.RCOM_ES_HIGHLIGHT_INDEX
        filter_conditions += get_default_highlight_filter_conditions()

    else:
        raise ValueError(f"Unsuported value type: {type}")

    # common conditions
    filter_conditions += list(filter(None, [
        get_id_condition(ids),
        get_tags_condition(tags),
        get_article_category_condition(article_category),
    ]))

    # search body
    body = {
        "query": query.Q(Fields.BOOL,
                    filter=query.Q(Fields.BOOL,
                        must_not=must_not_conditions,
                        filter=filter_conditions,
                        should=should_conditions,
                        minimum_should_match=1,
                    )).to_dict(),
        "size" : size,
        "_source": fields
    }
    if dry_run: return index, body

    # search session
    try:
        results = session.search(index=index, body=body, request_timeout=timeout)
    except RequestError as error:
        raise ValueError(f"Request error index (`{index}`): {error}")

    return [i['_source'] for i in results.get("hits", {}).get("hits", [])]


def msearch_query(session, body, timeout=10):
    responses = {}
    try:
        responses = session.msearch(
            body.replace("'", "\""),
            filter_path="responses.hits.hits",
            request_timeout = timeout
        )
    except Exception as error:
        raise ValueError(f"Request msearch error: {error}")

    results = defaultdict(list)
    for response in responses.get("responses", []):
        hits = response.get("hits", {}).get("hits", [])

        for hit in hits:
            _index = hit["_index"]
            _index = _index[_index.index("content-"):].replace("content-","").replace("-master","")
            results[_index].append(hit['_source'])

    return results



def _sort_es_doc(session,
                 type,
                 size=10,
                 tags=[],
                 ids=[],
                 campaign_type=None,
                 card_type=[],
                 article_category=[],
                 allow_app=[],
                 sort_field=PopularityPeriod.PAST_WEEK,
                 sort_by="desc",
                 language="TH",
                 filter_blacklisted_privileges=True,
                 exclude_ids=[],
                 search_after=[],
                 merchant_type=[]):
    """
    A utility function that sorts documents in ES based on specific fields and
    condition.

    Parameters
    ----------
    size : size of the return resultset, default to 10.
    tags : filters out items that don't match the given categories. The comparison is in an OR fashion.
    campaign_type : filters out items that don't match the given campaign_type. There are 2 types: privilege, point.
    ids : a list of item ids to search (exact match)
    card_type : filter privilege by type of card
    article_category : filter privilege by list of article_category
    allow_app: filter privilege by allow app
    sort_field : field to be sorted, default to PopularityPeriod.PAST_WEEK
    sort_by : a field to be sorted by, either 'asc' or 'desc', default to 'desc'
    type : types of document for search. There are 2 types: privilege(default) and shop
    language : a language, either 'EN' or 'TH', default 'TH'
    filter_blacklisted_privileges : If True, filter privileges whose campaign_code
        is specified in RCOM_ES_PRIVILEGE_CAMPAIGN_BLACKLIST. Note that this
        will filter out those whose id or original_id fields are specified in
        the given ids and original_ids too.
    exclude_ids : ids to be excluded from the result
    search_after : values for search_after
    merchant_type: type of merchant (list)
    Returns
    -------
    An array of dictionaries containing items details as specified in the API
    """
    must_conditions = get_default_must_conditions()
    must_not_conditions = get_default_must_not_conditions()
    filter_conditions = []
    if type == 'privilege':
        must_not_conditions += get_default_privilege_must_not_coditions()

        filter_conditions = get_default_privilege_filter_conditions(language)
        index = ste.RCOM_ES_PRIVILEGE_INDEX
        if filter_blacklisted_privileges and (len(_get_campaign_code_blacklist()) > 0):
            must_not_conditions.append(
                query.Q(Fields.TERMS,
                        campaign_code__keyword=_get_campaign_code_blacklist()))

    else:
        filter_conditions = get_default_shop_filter_conditions(language)
        filter_conditions.append(query.Q(Fields.TERM, searchable='Y'))
        index = ste.RCOM_ES_SHOP_INDEX

    if card_type:
        filter_conditions.append(query.Q(Fields.TERMS, card_type__keyword=card_type))
    if article_category:
        filter_conditions.append(query.Q(Fields.TERMS, article_category=article_category))
    if allow_app:
        filter_conditions.append(query.Q(Fields.TERMS, allow_app__keyword=allow_app))
    if merchant_type:
        filter_conditions.append(get_merchant_type_condition(merchant_type, default=['ka']))
    if tags:
        filter_conditions.append(query.Q(Fields.TERMS, tags=tags))
    if ids:
        ids = _add_lang_prefix_for_ids(ids, language)
        must_conditions.append(query.Q(Fields.TERMS, _id=ids))
    if exclude_ids:
        exclude_ids = _add_lang_prefix_for_ids(exclude_ids, language)
        must_not_conditions.append(query.Q(Fields.IDS, values=exclude_ids))

    filter_conditions.extend(get_campaign_type_filtering(campaign_type))
    q = query.Q(Fields.CONSTANT_SCORE,
                filter=query.Q(Fields.BOOL,
                               must=must_conditions,
                               must_not=must_not_conditions,
                               filter=filter_conditions))
    dsl = {"query": q.to_dict()}
    if size > 0:
        dsl["sort"] = {sort_field: {"order": sort_by}}
    
    if search_after:
        dsl["sort"] = [{sort_field: {"order": sort_by}},
                       {Fields.ID: {"order": sort_by}}]
        dsl["search_after"] = search_after

    try:
        results = session.search(index=index,
                                 body=dsl,
                                 size=size,
                                 _source=_get_return_fields(type))
    except RequestError as error:
        raise ValueError(
            f"Request error index (`{index}`) is not compatible, found: {error}"
        )

    hits = results.get("hits", {}).get("hits")

    if type == 'privilege':
        priv_merchant_map = _get_merchant_id_for_privileges(
            session, hits, language)
        return _build_result_set(hits, type, priv_merchant_map)
    else:
        return _build_result_set(hits, type)


def _get_default_must_conditions(language):
    # Note that {"match": {Fields.SEARCHABLE: "Y"}} has been excluded from the
    # must conditions based on Gang's recommendation
    """

    :param language: a language, either 'EN' or 'TH', default 'TH'
    :return: list of es conditions
    """

    return [{
        "range": {
            Fields.PUBLISH_DATE: {
                "lte": "now/h"
            }
        }
    }, {
        "range": {
            Fields.EXPIRE_DATE: {
                "gt": "now/h"
            }
        }
    }, {
        "match": {
            Fields.STATUS: "publish"
        }
    }, {
        "term": {
            Fields.LANG: language.lower()
        }
    }]


def _get_default_must_not_conditions():
    return [{"match": {Fields.ALLOW_RECOMMEND: "false"}}]


def _validate_type(type):
    if type not in ('privilege', 'shop', 'branch'):
        raise ValueError("Valid types are 'privilege', 'branch' and 'shop'.")


def get_items(session,
              type,
              ids=[],
              tags=[],
              card_type=None,
              article_category=None,
              allow_app=None,
              language="TH",
              campaign_type=None,
              filter_blacklisted_privileges=True):
    """
    A utility function that performs a filter search in ES. Either 'ids' or
    'original_ids', can be passed to this utility.

    Parameters
    ----------
    session : ES session instance
    ids : a list of item ids to search
    campaign_type : filters out items that don't match the given campaign_type. There are 2 types: privilege, point.
    card_type : filter privilege by type of card
    article_category : filter privilege by list of article_category
    allow_app: filter privilege by allow app
    type : types of document for search. There are 2 types: privilege and shop
    language : a language, either 'EN' or 'TH', default 'TH'
    filter_blacklisted_privileges : If True, filter privileges whose campaign_code
        is specified in RCOM_ES_PRIVILEGE_CAMPAIGN_BLACKLIST. Note that this
        will filter out those whose id or original_id fields are specified in
        the given ids and original_ids too.

    Returns
    -------
    An array of dictionaries containing item details of a given input language
    as specified in the API.

    Raises
    ------
    ValueError: raised if no 'ids' and 'original_id' are passed
    """
    return _sort_es_doc(
        session,
        type=type,
        size=len(ids),
        ids=ids,
        tags=tags,
        campaign_type=campaign_type,
        card_type=card_type,
        article_category=article_category,
        allow_app=allow_app,
        language=language,
        filter_blacklisted_privileges=filter_blacklisted_privileges)


def get_most_popular_items(session,
                           type='privilege',
                           size=10,
                           tags=[],
                           campaign_type=None,
                           card_type=None,
                           article_category=None,
                           allow_app=None,
                           language="TH",
                           popularity_sort=PopularityPeriod.PAST_WEEK,
                           exclude_ids=[],
                           search_after=[],
                           merchant_type=[]):
    """
    A utility function that returns the most popular privileges or shops.
    The most popular privileges is sorted from popularity_sort
    However, the most popular shops is calculated by weighting hit_count_score with geo_location and
    geo-location filtering supports only popular shops


    Parameters
    ----------
    session : ES session instance.
    type : type of document for search. There are 2 types: privilege(default) and shop.
    popularity_sort : Whether how the degree of popularity is computed, either by 'weekly', 'monthly', or 'yearly' fashion,
                      default is weekly.
    size : size of the return result set, default to 10.
    tags : filters out items that don't match the given tags. The comparison is in an OR fashion.
    campaign_type : filters out items that don't match the given campaign_type. There are 2 types: privilege, point.
    card_type : filter privilege by type of card
    article_category : filter privilege by list of article_category
    allow_app: filter privilege by allow app
    language : a language, either 'TH' or 'EN', default to 'TH'
    exclude_ids : ids to be excluded from the result (List)
    search_after : search_after values for search_after(List)
    merchant_type: type of merchant (list)

    Returns
    -------
    An array of dictionaries containing items details as specified in the API
    """
    _validate_type(type)
    return _sort_es_doc(session,
                            type=type,
                            size=size,
                            tags=tags,
                            campaign_type=campaign_type,
                            card_type=card_type,
                            article_category=article_category,
                            allow_app=allow_app,
                            sort_field=popularity_sort,
                            sort_by="desc",
                            language=language,
                            exclude_ids=exclude_ids,
                            search_after=search_after,
                            merchant_type=merchant_type)


def get_randomized_items(session,
                         lat=None,
                         lon=None,
                         radius=None,
                         type='privilege',
                         size=10,
                         seed=None,
                         score_mode="sum",
                         boost_mode="sum",
                         geo_weight=50,
                         language="TH",
                         is_trueyou_eat=False):
    """
    Alias for get_geo_location_filtered_items where randomized=True
     Parameters
    ---------
    session: ES session instance.
    lat:    Latitude point (float)
    lon:    Longitude point (float)
    radius: Radius distance from lat, lon (unit: Kilometre)
    type:   Type of document for search. There are 3 types: privilege(default),
        branch and shop.
    size:   Size of the return result set, default to 10.
    seed:   Random seed, only used when randomized is True (integer). If not
            given, timestamp (ms from epoch) is used.
    score_mode: method to calculate score when searching es with function_score ('sum', 'multiply', etc)
    boost_mode: method to calculate score from query and function_score ('sum', 'multiply', etc)
    geo_weight: weight of geo location in function_score
    language : a language, either 'TH' or 'EN', default to 'TH'

    Returns
    -------
    List of item IDs
    """
    return get_geo_location_filtered_items(session,
                                           lat,
                                           lon,
                                           radius,
                                           type,
                                           size,
                                           randomized=True,
                                           seed=seed,
                                           score_mode=score_mode,
                                           boost_mode=boost_mode,
                                           geo_weight=geo_weight,
                                           language=language,
                                           is_trueyou_eat=is_trueyou_eat)


def get_geo_location_filtered_items(session,
                                    lat=None,
                                    lon=None,
                                    radius=None,
                                    type='privilege',
                                    size=10,
                                    randomized=False,
                                    seed=None,
                                    score_mode="sum",
                                    boost_mode="sum",
                                    geo_weight=50,
                                    language="TH",
                                    is_trueyou_eat=False):
    """
    A Utility function that returns a list of privilege, branch or shops IDs
    based on some basic constraints (valid range covering current time, searchable
    and published). Additionally if the type is shop or branch, they must contain
    at least 1 privilege too. If geo-location params are given, they will also
    be filtered based on location. If randomized is set, the results are further
    randomized (on top of other filters).

    Parameters
    ---------
    session: ES session instance.
    type:   Type of document for search. There are 3 types: privilege(default),
            branch and shop.
    lat:    Latitude point (float)
    lon:    Longitude point (float)
    radius: Radius distance from lat, lon (unit: Kilometre)
    size:   Size of the return result set, default to 10.
    randomized: Randomize the results after applying all other filters
    seed:   Random seed, only used when randomized is True (integer). If not
            given, timestamp (ms from epoch) is used.
    score_mode: method to calculate score when searching es with function_score ('sum', 'multiply', etc)
    boost_mode: method to calculate score from query and function_score ('sum', 'multiply', etc)
    geo_weight: weight of geo location in function_score
    language : a language, either 'TH' or 'EN', default to 'TH'

    Returns
    -------
    List of item IDs
    """
    _validate_type(type)

    # Build the base DSL query
    must_condition = _get_default_must_conditions(language)

    if type == "privilege":
        # privileges can be at both merchant and branch levels
        if (lat is not None) and (lon is not None) and (radius is not None):
            indices = [ste.RCOM_ES_BRANCH_INDEX]
        else:
            indices = [ste.RCOM_ES_SHOP_INDEX, ste.RCOM_ES_BRANCH_INDEX]

    elif type == "branch":
        indices = [ste.RCOM_ES_BRANCH_INDEX]

    else:
        # since geo-location search is not available in merchant index
        # change to search from branch
        if (lat is not None) and (lon is not None) and (radius is not None):
            indices = [ste.RCOM_ES_BRANCH_INDEX]
        else:
            indices = [ste.RCOM_ES_SHOP_INDEX]

        if is_trueyou_eat:
            must_condition.append(
                {"match": {
                    Fields.ARTICLE_CATEGORY: "dining"
                }})
            must_condition.append({"match": {Fields.MERCHANT_TYPE: "ka"}})

    # Build the base DSL query
    dsl = {
        "query": {
            "function_score": {
                "query": {
                    "bool": {
                        "filter": must_condition,
                        "must_not": _get_default_must_not_conditions()
                    }
                }
            }
        }
    }

    if size > 0:
        dsl["sort"] = ["_score"]

    # Enforce returned branches / merchants to have at least 1 privilege
    privilege_list_condition = {"exists": {"field": Fields.PRIVILEGE_LIST}}
    dsl["query"]["function_score"]["query"]["bool"]["filter"].append(
        privilege_list_condition)

    if type == "shop" and ste.RCOM_ES_BRANCH_INDEX in indices:
        master_id_condition = {"exists": {"field": Fields.MASTER_ID}}
        dsl["query"]["function_score"]["query"]["bool"]["filter"].append(
            master_id_condition)

    # If geo-search is desired, add that to the must condition
    if (lat is not None) and (lon is not None) and (radius is not None):
        # TODO: Geo distance sorting is nice to have https://www.elastic.co/guide/en/elasticsearch/reference/6.2/search-request-sort.html#geo-sorting
        geo_condition = {
            "geo_distance": {
                "distance": "{}km".format(str(radius)),
                "location.location": {
                    "lat": float(lat),
                    "lon": float(lon)
                }
            }
        }

        dsl["query"]["function_score"]["functions"] = [{
            "filter": geo_condition,
            "weight": geo_weight
        }]
        dsl["query"]["function_score"]["score_mode"] = score_mode
        dsl["query"]["function_score"]["boost_mode"] = boost_mode

        sort_condition = {
            "_geo_distance": {
                "location.location": {
                    "lat": float(lat),
                    "lon": float(lon)
                },
                "order": "asc",
                "unit": "km",
                "mode": "min",
                "distance_type": "arc"
            }
        }

        if size > 0:
            dsl["sort"].append(sort_condition)

    # If randomized, add random_score to the function
    if randomized:
        seed_val = str(seed) if seed else str(int(time.time() * 1000))
        random_score = {"seed": seed_val}
        if (lat is not None) and (lon is not None) and (radius is not None):
            dsl["query"]["function_score"]["functions"][0][
                "random_score"] = random_score
        else:
            dsl["query"]["function_score"]["random_score"] = random_score

    # query all indices
    hits = []

    for index in indices:
        try:
            results = session.search(index=index,
                                     body=dsl,
                                     size=size,
                                     _source=_get_return_privilege_fields())
        except RequestError as error:
            raise ValueError(
                f"Request error index (`{index}`) is not compatible, found: {error}"
            )

        hits.extend(results.get("hits", {}).get("hits"))

    item_ids = list(map(lambda x: x.get("_source", {}).get(Fields.ID), hits))

    if type == 'privilege':
        # Requested data is privilege. We need to get them from privilege list
        # contained in each branch / merchant first.
        privilege_id_list = []
        for i in range(len(item_ids)):
            if hits[i].get("_source", {}).get(Fields.PRIVILEGE_LIST):
                privilege_list = hits[i].get("_source",
                                             {}).get(Fields.PRIVILEGE_LIST)

                if len(privilege_list) > 0:
                    privilege_id_list.extend(privilege_list)

        # Ensure that the item_ids contains only "size" privileges, not suplicated
        # and that they are randomized, such that privileges from the same
        # merchants are less likely to be returned together.
        item_ids = list(set(privilege_id_list))
        random.shuffle(item_ids)

        if len(item_ids) > size:
            item_ids = item_ids[0:size]

        return item_ids

    elif type == "shop" and ste.RCOM_ES_BRANCH_INDEX in indices:
        # Requested data is shop. We need to get them from master_id contained in each branch first.
        merchant_id_list = list(
            set(map(lambda x: x.get("_source", {}).get(Fields.MASTER_ID),
                    hits)))

        if len(merchant_id_list) > size:
            merchant_id_list = merchant_id_list[0:size]

        return merchant_id_list

    else:
        # Requested data is branch. Just return items in the hits array
        return item_ids


def get_default_must_conditions():
    return []


def get_default_must_not_conditions():
    return [
        query.Q(Fields.MATCH, allow_recommend='false')
    ]


def get_default_should_conditions():
    return [
        {"range": {"expire_date": {"gt": "now/h"}}},
        {"bool": {"must_not": [{"exists": {"field": "expire_date"}}]}}
    ]


def get_default_privilege_must_not_coditions():
    return [
        query.Q(Fields.RANGE, quota_over_existed={"gt": 0}),
    ]


def get_default_filter_conditions(language='TH'):
    conditions = [
        query.Q(Fields.TERM, searchable='Y'),
        query.Q(Fields.TERM, status='publish'),
        query.Q(Fields.TERM, lang=language.lower()),
        query.Q(Fields.RANGE, publish_date={'lte': 'now/h'}),
    ]
    return conditions

def get_default_privilege_filter_conditions(language='TH', filter_expired=True):
    conditions = [
        query.Q(Fields.MATCH, searchable='Y'),
        query.Q(Fields.EXISTS, field='article_category'),
        query.Q(Fields.MATCH, status='publish'),
        query.Q(Fields.RANGE, publish_date={'lte': 'now/h'}),
        query.Q(Fields.TERM, lang=language.lower())
    ]
    if filter_expired:
        conditions.append(query.Q(Fields.RANGE, expire_date={"gt": "now/h"}))
    return conditions


def get_default_shop_filter_conditions(language='TH', filter_expired=True):
    conditions = [
        query.Q(Fields.MATCH, status='publish'),
        query.Q(Fields.RANGE, publish_date={'lte': 'now/h'}),
        query.Q(Fields.TERM, lang=language.lower()),
        query.Q(
            Fields.NESTED,
            path=Fields.DENORMALIZE_PRIVILEGE,
            query=query.Q(
                Fields.BOOL,
                must_not=[
                    query.Q(
                        Fields.TERM,
                        denormalize_privilege__quota_over_existed__keyword=1)
                ]))
    ]
    if filter_expired:
        conditions.append(query.Q(Fields.RANGE, expire_date={"gt": "now/h"}))
    return conditions

def get_default_highlight_filter_conditions(language='TH') :
    conditions=[
        query.Q(Fields.EXISTS, field='article_category'),
        query.Q(Fields.TERM, article_category='trueyou-article')
    ]

    return conditions

def get_default_function_conditions():
    return []


def get_geo_location_filtering(lat=None, lon=None, radius=None, weight=None):
    if lat and lon and radius:
        return [
            query.SF(Fields.BOOST_FACTOR,
                     filter=query.Q(Fields.NESTED,
                                    path=Fields.DENORMALIZE_BRANCH,
                                    query=query.Q(
                                        'geo_distance',
                                        distance=f'{radius}km',
                                        denormalize_branch__location__location={
                                            'lat': lat,
                                            'lon': lon
                                        })),
                     weight=weight)
        ]
    return []


def get_campaign_type_filtering(campaign_type=None):
    if campaign_type:
        return [
            query.Q(
                Fields.NESTED,
                path=Fields.DENORMALIZE_PRIVILEGE,
                query=query.Q(
                    Fields.BOOL,
                    must=[
                        query.Q(
                            Fields.TERM,
                            denormalize_privilege__campaign_type=campaign_type)
                    ]))
        ]
    return []


def get_merchants_from_rcom_tags(session,
                                 rcom_tags=[],
                                 language="TH",
                                 campaign_type=None,
                                 size=10,
                                 score_mode="sum",
                                 boost_mode="sum",
                                 hit_count_week_weight=15,
                                 hit_count_month_weight=10,
                                 hit_count_year_weight=5,
                                 geo_weight=40,
                                 discover_weight=30,
                                 merchant_type_weight=0,
                                 lat=None,
                                 lon=None,
                                 radius=None,
                                 discover=None,
                                 merchant_type=None,
                                 is_trueyou_eat=False):
    """
        A utility function that returns the shops based on input rcom_tags.

        Parameters
        ----------
        session : ES session instance.
        rcom_tags : merchant tags for search.
        language : a language, either 'TH' or 'EN', default to 'TH'
        campaign_type : filters out items that don't match the given campaign_type. There are 2 types: privilege, point.
        size : size of the return result set, default to 10.
        score_mode: method to calculate score when searching es with function_score ('sum', 'multiply', etc)
        boost_mode: method to calculate score from query and function_score ('sum', 'multiply', etc)
        hit_count_month_weight: weight of HIT_COUNT_MONTH in function_score
        hit_count_year_weight: weight of HIT_COUNT_YEAR in function_score
        hit_count_week_weight: weight of HIT_COUNT_WEEK in function_score
        geo_weight: weight of geo location in function_score
        discover_weight = weight of discover in function_score
        merchant_type_weight= weight of merchant_type in function_score
        lat:    Latitude point (float)
        lon:    Longitude point (float)
        radius: Radius distance from lat, lon (unit: Kilometre)
        discover: discover flag for recommending merchants ('y', 'n')
        merchant_type: type of merchant ('none_ka', 'ka', 'qr')

        Returns
        -------
        An array of dictionaries containing items details as specified in the API
        """

    must_conditions = get_default_must_conditions()
    must_not_conditions = get_default_must_not_conditions()
    filter_conditions = get_default_shop_filter_conditions(language)
    function_conditions = get_default_function_conditions()

    if is_trueyou_eat:
        must_conditions.append(query.Q(Fields.MATCH, merchant_type='ka'))
        must_conditions.append(query.Q(Fields.MATCH, article_category='dining'))
        function_conditions.append(
            query.SF(Fields.BOOST_FACTOR,
                     filter=query.Q(Fields.TERMS, RCOM_TAGS=rcom_tags),
                     weight=70))
    else:
        must_conditions.append(
            query.Q(Fields.TERMS, RCOM_TAGS=rcom_tags, boost=5))
        if merchant_type:
            function_conditions.append(
                query.SF(Fields.BOOST_FACTOR,
                         filter=query.Q(Fields.TERM,
                                        merchant_type={'value': merchant_type}),
                         weight=merchant_type_weight))

    filter_conditions.extend(get_campaign_type_filtering(campaign_type))

    function_conditions.extend(
        get_geo_location_filtering(lat, lon, radius, weight=geo_weight))

    if discover:
        function_conditions.append(
            query.SF(Fields.BOOST_FACTOR,
                     filter=query.Q(Fields.TERM, discover={'value': discover}),
                     weight=discover_weight))

    function_conditions.extend([
        query.SF(Fields.FIELD_VALUE_FACTOR,
                 field=Fields.HIT_COUNT_WEEK,
                 factor=0.001,
                 modifier='log1p',
                 missing=0,
                 weight=hit_count_week_weight),
        query.SF(Fields.FIELD_VALUE_FACTOR,
                 field=Fields.HIT_COUNT_MONTH,
                 factor=0.001,
                 modifier='log1p',
                 missing=0,
                 weight=hit_count_month_weight),
        query.SF(Fields.FIELD_VALUE_FACTOR,
                 field=Fields.HIT_COUNT_YEAR,
                 factor=0.001,
                 modifier='log1p',
                 missing=0,
                 weight=hit_count_year_weight),
    ])

    q = query.Q(
        Fields.FUNCTION_SCORE,
        query=query.Q(Fields.BOOL,
                      must=must_conditions,
                      must_not=must_not_conditions,
                      filter=filter_conditions),
        functions=function_conditions,
        score_mode=score_mode,
        boost_mode=boost_mode,
    )
    try:
        results = session.search(index=ste.RCOM_ES_SHOP_INDEX,
                                 body={"query": q.to_dict()},
                                 size=size,
                                 _source=_get_return_fields(
                                     ste.RCOM_ES_PRIVILEGE_SHOP))
    except RequestError as error:
        raise ValueError(
            f"Request error index (`{ste.RCOM_ES_SHOP_INDEX}`) is not compatible, found: {error}"
        )

    hits = results.get("hits", {}).get("hits")

    return _build_result_set(hits, ste.RCOM_ES_PRIVILEGE_SHOP)


def _add_lang_prefix_for_ids(ids: List[str], language: str) -> List[str]:
    """
        Add language in front of id for _id in es7 format
        :param ids: List of item's ID
        :param language : a language, either 'TH' or 'EN', default to 'TH'
        :return: List of item's ID with language
    """
    if isinstance(ids, list):
        return [f"{language.lower()}-{id}" for id in ids]
    else:
        raise TypeError(f"`ids` should be list. found {type(ids)}")


def get_mixed_privileges(es_session,
                         item_ids,
                         language="th",
                         fields=['id'],
                         size=10,
                         timestamp="now/h"):
    """
    Elasticsearch query to return currently available deals
    Parameters
    ---------
    session: ES session instance.
    item_ids: list of item IDs.
    language: a language, either 'TH' or 'EN', default to 'TH'
    size: Size of the return result set, default to 10.

    Returns
    -------
    List of item IDs
    """
    dsl = {
        "query": {
            "bool": {
                "filter": [
                    {"terms": {"id": item_ids}},
                    {"term": {"lang": language.lower()}},
                    {"term": {"status": "publish"}},
                    {"range": {"publish_date": {"lte": "now"}}}
                ],
                "minimum_should_match": 1,
                "should": [
                    {"range": {"expire_date": {"gt": timestamp}}},
                    {"bool": {"must_not": [{"exists": {"field": "expire_date"}}]}}
                ]
            }
        },
        "_source": fields
    }
    results = es_session.search(index=ste.RCOM_ES_MIXED_PRIVILEGE_INDEXES, body=dsl, size=size)
    items = [hit['_source'] for hit in results["hits"]["hits"]]
    return items


def get_shelf_pin_items(es_session,
                    shelf_id="",
                    timestamp="",
                    active=True,
                    timeout="10s"):
    """
     Get pin_items from a shelf/progressive shelf in Elasticsearch

     Parameters
     __________
     es_session : ES session instance
     shelf_id : a string of shelf_id to search
     index: index elasticsearch
     timestamp: timestamp that would like to get shelf
     active: active shelf status
     timeout : timeout for elasticsearch
     return : a list of shelf_items with a given shelf
    """

    if timestamp == "":
        timestamp = datetime.datetime.fromtimestamp(time.time()).strftime(ste.DATETIME_FORMAT)

    dsl = {
        "query": {
            "bool": {
                "minimum_should_match": 1,
                "should": [
                    {
                        "bool": {
                            "filter": [
                                {"term": {"id.keyword": shelf_id}}, 
                                {"term": {"_index": ste.RCOM_ES_PROGRESSIVE_INDEX}},
                                {
                                    "nested": {
                                        "path": Fields.SHELF_LIST,
                                        "query": {
                                            "bool": {
                                                "filter": [
                                                    {"term" : {f"{Fields.SHELF_LIST}.{Fields.SHELF_ACTIVE}": active}},
                                                    {"range": {f"{Fields.SHELF_LIST}.{Fields.SHELF_START_DATE}": {"lte": timestamp}}}
                                                ],
                                                "should": [
                                                    {"range": {f"{Fields.SHELF_LIST}.{Fields.SHELF_END_DATE}": {"gte": timestamp}}},
                                                    {"bool" : {"must_not": [{"exists": {"field": f"{Fields.SHELF_LIST}.{Fields.SHELF_END_DATE}"}}]}}
                                                ],
                                                "minimum_should_match": 1
                                            }
                                        },
                                        "inner_hits": {
                                            "sort" : [{f"{Fields.SHELF_LIST}.{Fields.SHELF_VERSION}" : "asc" }],
                                            "_source": [
                                                f"{Fields.SHELF_LIST}.{Fields.SHELF_ACTIVE}",
                                                f"{Fields.SHELF_LIST}.{Fields.SHELF_PIN_ITEMS}",
                                                f"{Fields.SHELF_LIST}.{Fields.SHELF_START_DATE}",
                                                f"{Fields.SHELF_LIST}.{Fields.SHELF_END_DATE}"
                                            ]
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    {
                        "bool": {
                            "filter": [
                                {"term": {"id": shelf_id}},
                                {"term": {"_index": ste.RCOM_ES_CONTENTS_SHELF_INDEX}}
                            ]
                        }
                    }
                ]
            }
        }
    }
    _logger.debug(f"get_shelf_pin_items_from_elasticsearch: DSL {dsl}")
    
    try:
        results = es_session.search(
            index=[ste.RCOM_ES_CONTENTS_SHELF_INDEX, ste.RCOM_ES_PROGRESSIVE_INDEX],
            body=dsl,
            _source=[Fields.SHELF_PIN_ITEMS],
            size=1,
            timeout=timeout)
        hits = results["hits"]["hits"]
        if not hits:
            raise ValueError(f"No shelf_id : {shelf_id}")

        index = hits[0]['_index']
        if index == ste.RCOM_ES_PROGRESSIVE_INDEX:
            inner_hits = hits[0]["inner_hits"][Fields.SHELF_LIST]["hits"]["hits"]

            if not inner_hits:
                raise ValueError(f"No shelf_id : {shelf_id}")

            pin_items = inner_hits[0].get("_source", {}).get(Fields.SHELF_PIN_ITEMS, [])
        else:
            pin_items = hits[0].get("_source", {}).get(Fields.SHELF_PIN_ITEMS, [])

    except Exception as error:
        pin_items = []
        _logger.error(f"Error: {error}")
        _logger.error(f"Cannot retrieve shelf pin_items from elasticsearch")

    return pin_items

def get_shelf_items_and_pin_items(
    es_session,
    shelf_id,
    timestamp="now",
    language="",
    active=True,
    timeout="10s"
) -> Tuple[List[str], List[str]]:
    """
     Get shelf_items and pin_items from a shelf/progressive shelf in Elasticsearch

     Parameters
     __________
     es_session : ES session instance
     shelf_id : a string of shelf_id to searc
     timestamp: timestamp that would like to get shelf
     language: a language, either 'TH' or 'EN', default to 'TH'
     active: active shelf status
     timeout : timeout for elasticsearch
     return : a tuple of list of shelf_items and pin_items
    """

    if timestamp == "":
        timestamp = datetime.datetime.fromtimestamp(time.time()).strftime(ste.DATETIME_FORMAT)

    dsl = {
        "query": {
            "bool": {
                "minimum_should_match": 1,
                "should": [
                    {
                        # this query for progressive shelf
                        "bool": {
                            "filter": [
                                {"term": {"_index": ste.RCOM_ES_PROGRESSIVE_INDEX}},
                                {"term": {"id.keyword": shelf_id}}, 
                                {"term": {"status": "publish"}},
                                {"range": {"publish_date": {"lte": "now"}}},
                                {
                                    "nested": {
                                        "path": Fields.SHELF_LIST,
                                        "query": {
                                            "bool": {
                                                "filter": [
                                                    {"term" : {f"{Fields.SHELF_LIST}.{Fields.SHELF_ACTIVE}": active}},
                                                    {"range": {f"{Fields.SHELF_LIST}.{Fields.SHELF_START_DATE}": {"lte": "now"}}}
                                                ],
                                                "should": [
                                                    {"range": {f"{Fields.SHELF_LIST}.{Fields.SHELF_END_DATE}": {"gte": timestamp}}},
                                                    {"bool" : {"must_not": [{"exists": {"field": f"{Fields.SHELF_LIST}.{Fields.SHELF_END_DATE}"}}]}}
                                                ],
                                                "minimum_should_match": 1
                                            }
                                        },
                                        "inner_hits": {
                                            "sort" : [{f"{Fields.SHELF_LIST}.{Fields.SHELF_VERSION}" : "asc"}],
                                            "_source": [
                                                f"{Fields.SHELF_LIST}.{Fields.SHELF_PIN_ITEMS}",
                                                f"{Fields.SHELF_LIST}.{Fields.SHELF_ITEMS}",
                                            ]
                                        }
                                    }
                                }
                            ],
                            "minimum_should_match": 1,
                            "should": [
                                {"range": {"expire_date": {"gt": timestamp}}},
                                {"bool": {"must_not": [{"exists": {"field": "expire_date"}}]}},
                            ],
                        }
                    },
                    {
                        # this query for content shelf
                        "bool": {
                            "filter": [
                                {"term": {"id": shelf_id}},
                                {"term": {"_index": ste.RCOM_ES_CONTENTS_SHELF_INDEX}},
                                {"term": {"status": "publish"}},
                                {"term": {"lang": language.lower()}},
                                {"range": {"publish_date": {"lte": "now"}}},
                            ],
                            "minimum_should_match": 1,
                            "should": [
                                {"range": {"expire_date": {"gt": timestamp}}},
                                {"bool": {"must_not": [{"exists": {"field": "expire_date"}}]}},
                            ],
                        }
                    }
                ]
            }
        }
    }
    _logger.debug(f"get_shelf_pin_items_from_elasticsearch: DSL {dsl}")
    try:
        results = es_session.search(
            index=[ste.RCOM_ES_CONTENTS_SHELF_INDEX, ste.RCOM_ES_PROGRESSIVE_INDEX],
            body=dsl,
            _source=[Fields.SHELF_PIN_ITEMS, Fields.SHELF_ITEMS],
            size=1,
            timeout=timeout)
        hits = results["hits"]["hits"]
        if not hits:
            raise ValueError(f"No shelf_id : {shelf_id}")

        index = hits[0]['_index']
        if index == ste.RCOM_ES_PROGRESSIVE_INDEX:
            inner_hits = hits[0]["inner_hits"][Fields.SHELF_LIST]["hits"]["hits"]

            if not inner_hits:
                raise ValueError(f"No shelf_id : {shelf_id}")

            pin_items = inner_hits[0].get("_source", {}).get(Fields.SHELF_PIN_ITEMS, [])
            shelf_items = inner_hits[0].get("_source", {}).get(Fields.SHELF_ITEMS, [])
        else:
            pin_items = hits[0].get("_source", {}).get(Fields.SHELF_PIN_ITEMS, [])
            shelf_items = hits[0].get("_source", {}).get(Fields.SHELF_ITEMS, [])

    except Exception as error:
        pin_items = []
        shelf_items = []
        _logger.error(f"Error: {error}")
        _logger.error(f"Cannot retrieve shelf pin_items from elasticsearch")

    return shelf_items, pin_items


def get_shelf_metadata(
    es_session,
    item_ids,
    language="th",
    fields=["id"],
    size=10,
    timestamp="now/h"
) -> List[Dict]:
    """
    Get fields from a content shelf/progressive shelf in Elasticsearch.
    In progressive shelf will not filter with lang.
    In content shelf will filter with lang.
    Parameters
    ---------
    session: ES session instance.
    item_ids: list of item IDs.
    language: a language, either 'TH' or 'EN', default to 'TH'
    fields: fields that want from query
    size: size of the return resultset, default to 10

    Returns
    -------
    List of selected fields
    """
    dsl = {
        "query": {
                "bool": {
                    "minimum_should_match": 1,
                    "should": [
                        # this query for progressive shelf
                        {
                            "bool": {
                                "filter": [
                                    {"terms": {"id.keyword": item_ids}},
                                    {"term": {"status": "publish"}},
                                    {"range": {"publish_date": {"lte": "now"}}},
                                ],
                                "minimum_should_match": 1,
                                "should": [
                                    {"range": {"expire_date": {"gt": timestamp}}},
                                    {"bool": {"must_not": [{"exists": {"field": "expire_date"}}]}},
                                ],
                            }
                        },
                        # this query for content shelf
                        {
                            "bool": {
                                "filter": [
                                    {"terms": {"id": item_ids}},
                                    {"term": {"lang": language.lower()}},
                                    {"term": {"status": "publish"}},
                                    {"range": {"publish_date": {"lte": "now"}}},
                                ],
                                "minimum_should_match": 1,
                                "should": [
                                    {"range": {"expire_date": {"gt": timestamp}}},
                                    {"bool": {"must_not": [{"exists": {"field": "expire_date"}}]}},
                                ],
                            }
                        },
                    ]
                }
        },
        "size": size,
        "_source": fields,
    }
    index = ",".join([ste.RCOM_ES_CONTENTS_SHELF_INDEX, ste.RCOM_ES_PROGRESSIVE_INDEX])
    results = es_session.search(index=index, body=dsl)
    items = [hit["_source"] for hit in results["hits"]["hits"]]
    return items

# ===============================================================================================
# Filter conditions
# ===============================================================================================

# TERM condition
def get_quota_over_existed():
    return query.Q(Fields.TERM, quota_over_existed="1")

# TERMS condition
def get_id_condition(value: list, default=None):
    if not value: value = default
    if value: return query.Q(Fields.TERMS, id=value)

def get_tags_condition(value: list, default=None):
    if not value: value = default
    if value: return query.Q(Fields.TERMS, tags=value)

def get_card_type_condition(value: list, default=None):
    if not value: value = default
    if value: return query.Q(Fields.TERMS, card_type=value)

def get_article_category_condition(value: list, default=None):
    if not value: value = default
    if value: return query.Q(Fields.TERMS, article_category=value)

def get_allow_app_condition(value: list, default=None):
    if not value: value = default
    if value: return query.Q(Fields.TERMS, allow_app=value)

def get_campaign_type_condition(value: list, default=None):
    if not value: value = default
    if value: return query.Q(Fields.TERMS, campaign_type=value)

def get_merchant_type_condition(value: list, default=None):
    if not value: value = default
    if value: return query.Q(Fields.TERMS, merchant_type=value)