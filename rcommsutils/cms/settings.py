import os
# Indexes for products' meta data
DEV = "dev"
ALPHA = "alpha"
PREPRODUCTION = "preprod"
PRODUCTION = "prod"


def get_environment_prefix(environment: str, index: str) -> str:
    """
          Add environment prefix in front of elasticsearch 7 index for non-prod env only
          :param environment: string of environment
          :param index : es7 index
          :return: es 7 index with environment
  """
    environment = environment.lower()
    assert environment in [DEV, ALPHA, PREPRODUCTION, PRODUCTION]

    if environment == PRODUCTION:
        return index
    return f"{environment}-{index}"


ENVIRONMENT = os.getenv("ENVIRONMENT", DEV)

DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'
#Request timeout in millisecond
REQUEST_TIMEOUT_MS = int(os.getenv("REQUEST_TIMEOUT_MS", 1000))

# Original Indexes
# shop
RCOM_ES_PRIVILEGE_PRIVILEGE = os.getenv('RCOM_ES_PRIVILEGE_PRIVILEGE','privilege')
RCOM_ES_PRIVILEGE_SHOP = os.getenv('RCOM_ES_PRIVILEGE_SHOP', 'shop')

RCOM_ES_PRIVILEGE_INDEX_ORG = os.getenv('RCOM_ES_PRIVILEGE_INDEX', 'content-privilege-master')
RCOM_ES_SHOP_INDEX_ORG = os.getenv('RCOM_ES_SHOP_INDEX', 'content-trueyoumerchant-master')
RCOM_ES_BRANCH_INDEX_ORG = os.getenv('RCOM_ES_BRANCH_INDEX', 'content-trueyoubranch-master')
# watch
RCOM_ES_TRUETV_INDEX_ORG = os.getenv('RCOM_ES_TRUETV_INDEX', 'content-movie-master')
RCOM_ES_EPG_INDEX_ORG = os.getenv('RCOM_ES_EPG_INDEX', 'content-epg-master')
# article
RCOM_ES_TRUEYOU_ARTICLE_INDEX_ORG = os.getenv('RCOM_ES_TRUEYOU_ARTICLE_INDEX_ORG','content-trueyouarticle-master')
RCOM_ES_ARTICLE_HOROSCOPE_INDEX_ORG = os.getenv('RCOM_ES_ARTICLE_HOROSCOPE_INDEX', 'content-horoscope-master')
RCOM_ES_ARTICLE_TNN_INDEX_ORG = os.getenv('RCOM_ES_ARTICLE_TNN_INDEX','content-tnn-master')
RCOM_ES_ARTICLE_MOVIEARTICLE_INDEX_ORG = os.getenv('RCOM_ES_ARTICLE_MOVIEARTICLE_INDEX', 'content-moviearticle-master')
RCOM_ES_ARTICLE_MUSICARTICLE_INDEX_ORG = os.getenv('RCOM_ES_ARTICLE_MUSICARTICLE_INDEX', 'content-musicarticle-master')
RCOM_ES_ARTICLE_WOMEN_INDEX_ORG = os.getenv('RCOM_ES_ARTICLE_WOMEN_INDEX','content-women-master')
RCOM_ES_ARTICLE_SPORTARTICLE_INDEX_ORG = os.getenv('RCOM_ES_ARTICLE_SPORT_INDEX', 'content-sportarticle-master')
RCOM_ES_ARTICLE_DARA_INDEX_ORG = os.getenv('RCOM_ES_ARTICLE_DARA_INDEX','content-dara-master')
RCOM_ES_ARTICLE_TRAVEL_INDEX_ORG = os.getenv('RCOM_ES_ARTICLE_TRAVEL_INDEX','content-travel-master')
# shelf
RCOM_ES_CONTENTS_SHELF_INDEX_ORG = os.getenv('RCOM_ES_CONTENTS_SHELF_INDEX','content-shelf-master')
RCOM_ES_PROGRESSIVE_INDEX_ORG = os.getenv('RCOM_ES_CONTENTS_SHELF_INDEX','cms-shelf-progressive')

RCOM_ES_HIGHLIGHT_INDEX_ORG = os.getenv('RCOM_ES_HIGHLIGHT_INDEX_ORG','content-hilight-master')
RCOM_ES_COUPON_INDEX_ORG = os.getenv('RCOM_ES_COUPON_INDEX_ORG','content-coupon-master')
RCOM_ES_MISC_INDEX_ORG = os.getenv('RCOM_ES_MISC_INDEX_ORG','content-misc-master')

# Env prefix-indexes
RCOM_ES_PRIVILEGE_INDEX = get_environment_prefix(ENVIRONMENT, RCOM_ES_PRIVILEGE_INDEX_ORG)
RCOM_ES_SHOP_INDEX   = get_environment_prefix(ENVIRONMENT, RCOM_ES_SHOP_INDEX_ORG)
RCOM_ES_BRANCH_INDEX = get_environment_prefix(ENVIRONMENT, RCOM_ES_BRANCH_INDEX_ORG)
RCOM_ES_TRUETV_INDEX = get_environment_prefix(ENVIRONMENT, RCOM_ES_TRUETV_INDEX_ORG)
RCOM_ES_EPG_INDEX    = get_environment_prefix(ENVIRONMENT, RCOM_ES_EPG_INDEX_ORG)
RCOM_ES_TRUEYOU_ARTICLE_INDEX = get_environment_prefix(ENVIRONMENT, RCOM_ES_TRUEYOU_ARTICLE_INDEX_ORG)
RCOM_ES_HIGHLIGHT_INDEX = get_environment_prefix(ENVIRONMENT, RCOM_ES_HIGHLIGHT_INDEX_ORG)
RCOM_ES_COUPON_INDEX = get_environment_prefix(ENVIRONMENT, RCOM_ES_COUPON_INDEX_ORG)
RCOM_ES_MISC_INDEX = get_environment_prefix(ENVIRONMENT, RCOM_ES_MISC_INDEX_ORG)

RCOM_ES_MIXED_PRIVILEGE_INDEXES = ','.join([
    RCOM_ES_TRUEYOU_ARTICLE_INDEX,
    RCOM_ES_PRIVILEGE_INDEX,
    RCOM_ES_SHOP_INDEX,
    RCOM_ES_BRANCH_INDEX,
    RCOM_ES_HIGHLIGHT_INDEX,
    RCOM_ES_COUPON_INDEX,
    RCOM_ES_MISC_INDEX
])

RCOM_ES_ARTICLE_HOROSCOPE_INDEX = get_environment_prefix(ENVIRONMENT, RCOM_ES_ARTICLE_HOROSCOPE_INDEX_ORG)
RCOM_ES_ARTICLE_TNN_INDEX = get_environment_prefix(ENVIRONMENT, RCOM_ES_ARTICLE_TNN_INDEX_ORG)
RCOM_ES_ARTICLE_MOVIEARTICLE_INDEX = get_environment_prefix(ENVIRONMENT, RCOM_ES_ARTICLE_MOVIEARTICLE_INDEX_ORG)
RCOM_ES_ARTICLE_MUSICARTICLE_INDEX = get_environment_prefix(ENVIRONMENT, RCOM_ES_ARTICLE_MUSICARTICLE_INDEX_ORG)
RCOM_ES_ARTICLE_WOMEN_INDEX = get_environment_prefix(ENVIRONMENT, RCOM_ES_ARTICLE_WOMEN_INDEX_ORG)
RCOM_ES_ARTICLE_SPORTARTICLE_INDEX = get_environment_prefix(ENVIRONMENT, RCOM_ES_ARTICLE_SPORTARTICLE_INDEX_ORG)
RCOM_ES_ARTICLE_DARA_INDEX = get_environment_prefix(ENVIRONMENT, RCOM_ES_ARTICLE_DARA_INDEX_ORG)
RCOM_ES_ARTICLE_TRAVEL_INDEX = get_environment_prefix(ENVIRONMENT, RCOM_ES_ARTICLE_TRAVEL_INDEX_ORG)
RCOM_ES_CONTENTS_SHELF_INDEX = get_environment_prefix(ENVIRONMENT, RCOM_ES_CONTENTS_SHELF_INDEX_ORG)
RCOM_ES_PROGRESSIVE_INDEX = get_environment_prefix(ENVIRONMENT, RCOM_ES_PROGRESSIVE_INDEX_ORG)
RCOM_ES_ALL_INDEX = get_environment_prefix(ENVIRONMENT, "*")

RCOM_ES_ARTICLE_INDEX = os.getenv(
    'RCOM_ES_ARTICLE_INDEX',
    f'{RCOM_ES_ARTICLE_HOROSCOPE_INDEX},{RCOM_ES_ARTICLE_TNN_INDEX},\
{RCOM_ES_ARTICLE_MOVIEARTICLE_INDEX},{RCOM_ES_ARTICLE_MUSICARTICLE_INDEX},{RCOM_ES_ARTICLE_WOMEN_INDEX},{RCOM_ES_ARTICLE_SPORTARTICLE_INDEX},\
{RCOM_ES_ARTICLE_DARA_INDEX},{RCOM_ES_ARTICLE_TRAVEL_INDEX}')

# Blacklist items to filter out from ES results
RCOM_ES_PRIVILEGE_CAMPAIGN_BLACKLIST = os.getenv(
    "RCOM_ES_PRIVILEGE_CAMPAIGN_BLACKLIST"
)    # comma-seperated list of campaign_code

RCOM_ES_MUSIC_MINSCORE = 0.1

# CMS FN settings
CMS_FN_LIMIT = 300
CMS_FN_EPG_URL = os.getenv(
    "CMS_FN_EPG_URL", "http://dmpapi2.dmp.true.th/cms-fnepg/v1/epg/metadatas")
CMS_FN_CONTENT_URL = os.getenv("CMS_FN_CONTENT_URL",
                               "http://dmpapi2.dmp.true.th/cms-fncontent/v2/")
CMS_FN_TRENDING_BY_CCU = os.getenv(
    "CMS_FN_TRENDING_BY_CCU",
    "http://dmpapi2.dmp.true.th/cms-fntrending/v1/trending/tvprogram")

# REDIS related
REDIS_DEFAULT_EXPIRE_MS = int(
    os.getenv("REDIS_DEFAULT_EXPIRE_MS", 60 * 60 * 12 * 1000))


REDIS_MOVIE_PREFIX = os.getenv("REDIS_MOVIE_PREFIX", "MOVIE_ITEM_4_3_0:")
REDIS_ARTICLE_PREFIX = os.getenv("REDIS_MOVIE_PREFIX", "ARTICLE_ITEMS_VOD:")
REDIS_SHELF_PREFIX = os.getenv("REDIS_MOVIE_PREFIX", "SHELF_ITEMS_VOD:")

# ========================= API settings ===========================

# Common product response
RCOM_API_NAME = "name"
RCOM_API_ITEMID1 = "itemId1"
RCOM_API_ITEMID2 = "itemId2"
RCOM_API_GLOBAL_ITEM_ID = "globalItemId"
RCOM_API_HIT_COUNT_WEEK = "hitcountWeek"
RCOM_API_HIT_COUNT_MONTH = "hitcountMonth"
RCOM_API_HIT_COUNT_YEAR = "hitcountYear"

# Music product response
RCOM_API_ARTISTS = "artists"
RCOM_API_ALBUM = "album"
RCOM_API_RELEASE_YEAR = "releaseYear"
RCOM_API_DESC = "desc"

# Movie product parameters
RCOM_API_ACTORS = "actors"
RCOM_API_TTV_THUMBNAIL = "thumbnail"
RCOM_API_TTV_THUMBLIST = "thumb_list"
RCOM_API_TTV_TVOD_FLAG = "tvod_flag"
RCOM_API_TTV_SUBSCRIPTION_TIERS = "subscription_tiers"
RCOM_API_TTV_COUNT_LIKES = "count_likes"
RCOM_API_TTV_COUNT_VIEWS = "count_views"
RCOM_API_TTV_EP_ITEMS = "ep_items"
RCOM_API_TTV_MOVIE_TYPE = "movie_type"
RCOM_API_TTV_RATE = "rate"
RCOM_API_TTV_SYNOPSIS = "synopsis"
RCOM_API_TTV_AUDIO = "audio"
RCOM_API_TTV_SUBTITLE = "subtitle"
RCOM_API_TTV_DURATION = "duration"
RCOM_API_TTV_DRM = "drm"
RCOM_API_TTV_DISPLAY_QUALITIES = "display_qualities"
RCOM_API_TTV_PACKAGE_ALACARTE = "package_alacarte"
RCOM_API_TTV_TRAILER = "trailer"
RCOM_API_TTV_CONTENT_RIGHTS = "content_rights"
RCOM_API_TTV_CONTENT_TYPE = "content_type"
RCOM_API_TTV_PARTNER_RELATED = "partner_related"
RCOM_API_TTV_EXCLUSIVE_BADGE_TYPE = "exclusive_badge_type"
RCOM_API_TTV_EXCLUSIVE_BADGE_START = "exclusive_badge_start"
RCOM_API_TTV_EXCLUSIVE_BADGE_END = "exclusive_badge_end"
RCOM_API_TTV_OTHER_TYPE = "other_type"
RCOM_API_GENRES = "genres"
RCOM_API_SUB_GENRES = "sub_genres"
RCOM_API_NEWEPI_BADGE_START = "newepi_badge_start"
RCOM_API_NEWEPI_BADGE_END = "newepi_badge_end"
RCOM_API_NEWEPI_BADGE_TYPE = "newepi_badge_type"

# Article product parameters
RCOM_API_ARTICLE_TIMESTAMP = 'timestamp'
RCOM_API_ARTICLE_CONTENT_TYPE = 'content_type'
RCOM_API_ARTICLE_COUNT_LIKES = 'count_likes'
RCOM_API_ARTICLE_COUNT_RATINGS = 'count_ratings'
RCOM_API_ARTICLE_COUNT_VIEWS = 'count_views'
RCOM_API_ARTICLE_CREATE_BY = 'create_by'
RCOM_API_ARTICLE_CREATE_BY_SSOID = 'create_by_ssoid'
RCOM_API_ARTICLE_CREATE_DATE = 'create_date'
RCOM_API_ARTICLE_DETAIL = 'detail'
RCOM_API_ARTICLE_ID = 'id'
RCOM_API_ARTICLE_LANG = 'lang'
RCOM_API_ARTICLE_ORIGINAL_ID = 'original_id'
RCOM_API_ARTICLE_PUBLISH_DATE = 'publish_date'
RCOM_API_ARTICLE_SOURCE_URL = 'source_url'
RCOM_API_ARTICLE_STATUS = 'status'
RCOM_API_ARTICLE_TAGS = 'tags'
RCOM_API_ARTICLE_THUMBNAIL = 'thumb'
RCOM_API_ARTICLE_TITLE = 'title'
RCOM_API_ARTICLE_UPDATE_BY = 'update_by'
RCOM_API_ARTICLE_UPDATE_BY_SSOID = 'update_by_ssoid'
RCOM_API_ARTICLE_UPDATE_DATE = 'update_date'

# TV product paramters
RCOM_API_TITLE_ID = "titleId"
RCOM_API_CMS_TITLE_ID = "cmsTitleId"
RCOM_API_START_TIME = "startTime"
RCOM_API_END_TIME = "endTime"
RCOM_API_CMS_CHANNEL_CODE = "cmsChannelCode"

# Privilege-Shop product response
RCOM_API_ID = "id"
RCOM_API_PRIV_ID = "priv_id"
RCOM_API_OLD_PRIV_ID = "privilege_id"
RCOM_API_MERCHANT_ID = "merchant_id"
RCOM_API_OLD_MERCHANT_ID = "master_id"
RCOM_API_BRANCH_ID = "branch_id"
RCOM_API_TITLE = "title"
RCOM_API_TITLE_PRIVILEGE = "title_privilege"
RCOM_API_DETAIL = "detail"
RCOM_API_PUBLISH_DATE = "publish_date"
RCOM_API_TAGS = "tags"
RCOM_API_CONTENT_TYPE = "content_type"
RCOM_API_ARTICLE_CATEGORY = "article_category"
RCOM_API_THUMBNAIL = "thumb"
RCOM_API_THUMBLIST = "thumb_list"
RCOM_API_PRIVILEGE_LIST = "privilege_list"
RCOM_API_PRV_HIT_COUNT_WEEK = "week_hitcount"
RCOM_API_PRV_HIT_COUNT_MONTH = "month_hitcount"
RCOM_API_PRV_HIT_COUNT_YEAR = "year_hitcount"
RCOM_API_PRV_HIT_COUNT_DAY_1 = "HIT_COUNT_DAY_1"
RCOM_API_PRV_HIT_COUNT_DAY_7 = "HIT_COUNT_DAY_7"
RCOM_API_PRV_HIT_COUNT_DAY_14 = "HIT_COUNT_DAY_14"
RCOM_API_PRV_HIT_COUNT_DAY_30 = "HIT_COUNT_DAY_30"
RCOM_API_PRV_ALLOW_APP = "allow_app"
RCOM_API_PRV_CARD_TYPE = "card_type"
RCOM_API_DISCOVER = "discover"
RCOM_API_MERCHANT_TYPE = "merchant_type"
RCOM_API_SETTING = "setting"

# Privilege product response
RCOM_API_USSD = "ussd"
RCOM_API_EXPIRE_DATE = "expire_date"
RCOM_API_CAMPAIGN_CODE = "campaign_code"
RCOM_API_TERM_CONDITION = "term_condition"

# Shop product response
RCOM_API_LOCATION = "location"
RCOM_API_ADDRESS = "address"

# Shelf product parameter
RCOM_TV_GET_SHELF_ITEMS_CACHE_SIZE = os.getenv("RCOM_TV_GET_SHELF_ITEMS_CACHE_SIZE", 64)
RCOM_TV_GET_SHELF_ITEMS_CACHE_TTL = int(os.getenv("RCOM_TV_GET_SHELF_ITEMS_CACHE_TTL", 60 * 5))
