from rediscluster import RedisCluster
from redis import Redis


def get_connection_status(redis, is_cluster=False):
    """A utility function to get a Redis connection status.

    Parameters
    ----------
    redis : Redis
        Redis instance

    Returns
    -------
    a dict of Redis status
    """
    info = redis.info()

    if is_cluster:
        version = []
        mode = []
        host = []
        for key, value in info.items():
            version.append(value.get('redis_version'))
            mode.append(value.get('redis_mode'))
            host.append(key)

        version = list(set(version))
        mode = list(set(mode))
        host = sorted(list(set(host)))

    else:
        version = info.get('redis_version')
        mode = info.get('redis_mode')
        host = redis.connection_pool.connection_kwargs.get('host')

    return {'version': version, 'mode': mode, 'status': 1, 'ip': host}


def get_redis(host,
              port=6379,
              password=None,
              db=0,
              decode_responses=True,
              startup_nodes=None,
              is_cluster=False):
    """A utility function to get a new redis instance. The redis instance is a long-lived object and it should not be
    used in a request/response short-lived fashion.

    Since Python 3 changed to Unicode strings from Python 2’s ASCII,
    the return type of most commands will be binary strings,
    unless the class is instantiated with the option decode_responses=True

    Parameters
    ----------
    host : str
        host ip.

    port : int, default=6379
        port.

    password : str
        redis password

    db : int, default=0
        The Redis logical database having the specified zero-based numeric index.
        New connections always use the database 0.

    startup_nodes: list of dict default= None
                    Containing redis host and port
                    e.g. [{"host": "redis_host_1", "port": "7000"}, {"host": "redis_host_2", "port": "5000"}]

    is_cluster: bool
        indicate redis cluster
    Returns
    -------
    a redis instance
    """

    # if startup_nodes has values , it will connect to that hosts in startup_nodes,
    # if not, it creates startup_nodes with specific `host` and `port`
    if is_cluster:
        if not startup_nodes:
            startup_nodes = [{"host": host, "port": port}]

        redis = RedisCluster(startup_nodes=startup_nodes,
                             password=password,
                             decode_responses=decode_responses)

    else:
        redis = Redis(host=host,
                      port=port,
                      db=db,
                      password=password,
                      decode_responses=decode_responses)

    return redis
