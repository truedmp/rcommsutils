#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Recommendation model server's Cassandra DB settings

Created on Fri Feb  3 21:40:47 2017

@author: Champ
"""
import os

# Cassandra DB configs
RCOM_CASSANDRA_IP = os.getenv('RCOM_CASSANDRA_IP', "10.198.107.107")
RCOM_CASSANDRA_PORT = int(os.getenv('RCOM_CASSANDRA_PORT', 9042))
RCOM_CASSANDRA_USER = os.getenv("RCOM_CASSANDRA_USER", "rec_dev")
RCOM_CASSANDRA_PASSWORD = os.getenv("RCOM_CASSANDRA_PASSWORD", "rec_dev")

# keyspace and table storing model_update_info.
RCOM_CASSANDRA_COMMON_KEYSPACE = os.getenv("RCOM_CASSANDRA_COMMON_KEYSPACE",
                                           "common")
RCOM_CASSANDRA_MODEL_UPDATE_TABLE = os.getenv(
    "RCOM_CASSANDRA_MODEL_UPDATE_TABLE", "model_update_info")

# Test configs, default to dev env
RCOM_CASSANDRA_TEST_IP = os.getenv('RCOM_CASSANDRA_TEST_IP', "10.198.107.107")
RCOM_CASSANDRA_TEST_PORT = int(os.getenv('RCOM_CASSANDRA_TEST_PORT', 9042))
RCOM_CASSANDRA_TEST_USER = os.getenv("RCOM_CASSANDRA_TEST_USER", "rec_dev")
RCOM_CASSANDRA_TEST_PASSWORD = os.getenv("RCOM_CASSANDRA_TEST_PASSWORD",
                                         "rec_dev")
RCOM_CASSANDRA_TEST_KEYSPACE = os.getenv('RCOM_CASSANDRA_TEST_KEYSPACE',
                                         "test_keyspace")
RCOM_CASSANDRA_TEST_MODEL_UPDATE_TABLE = os.getenv(
    "RCOM_CASSANDRA_TEST_MODEL_UPDATE_TABLE", "model_update_info")
