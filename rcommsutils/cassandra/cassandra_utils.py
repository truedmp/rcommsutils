from cassandra.cluster import Cluster
from cassandra.query import SimpleStatement
from cassandra.concurrent import execute_concurrent as _execute_concurrent
from rcomloggingutils.applog_utils import AppLogger
from cassandra.auth import PlainTextAuthProvider

import rcommsutils.cassandra.settings as stc

_SEPARATOR = "_"
TABLE_PLACEHOLDER = "<table_name>"

_logger = AppLogger(name=__name__)
_helper_session = None


class HostDetails():
    """
    An object of this class stores Cassandra host details.
    
    Parameters
    ----------
    hosts : list
        a list of Cassandra hosts/IP's, or string of comma-separated IPs. E.g.
        "x.x.x.x, y.y.y.y, z.z.z.z, ..."

    port : int
        port connecting to Cassandra
        
    user: str
        a user name if the Cassandra host requires user / password authentication,
        None if not.
        
    password: str
        a password if the Cassandra host requires user / password authentication,
        None if not.
    """

    def __init__(self, hosts, port, user=None, password=None):

        if isinstance(hosts, str):
            self.hosts = hosts.split(',')
        elif isinstance(hosts, list):
            self.hosts = hosts
        else:
            raise ValueError("hosts %s invalid" % hosts)

        self.port = int(port)
        self.user = user
        self.password = password


def _get_host_details(host_details):
    """
    Get host details from the given HostDetails object. If None, get it from
    env variables
    """
    if host_details and isinstance(host_details, HostDetails):
        hosts = host_details.hosts
        port = host_details.port
        user = host_details.user
        password = host_details.password
    else:
        hosts = stc.RCOM_CASSANDRA_IP
        port = stc.RCOM_CASSANDRA_PORT
        user = stc.RCOM_CASSANDRA_USER
        password = stc.RCOM_CASSANDRA_PASSWORD

    return hosts, port, user, password


def _get_helper_session(host_details=None):
    """
    Get helper session connected to the common keyspace at the specified 
    host_details, or from env variables if not given.
    """
    global _helper_session

    if not _helper_session:

        if not host_details:
            hosts, port, user, password = _get_host_details(None)
            host_details = HostDetails(hosts, port, user, password)

        _helper_session = get_session(stc.RCOM_CASSANDRA_COMMON_KEYSPACE,
                                      host_details=host_details)

    return _helper_session


def get_connection_status(host_details=None):
    """
    Get connection status to all connected hosts of all sessions
    
    Parameters
    ----------
    host_details : HostDetails
        if given, use the host details in this object. Otherwise use the 
        preconfigured values via env variables.
    """
    _logger.info("Checking Cassandra connection status")
    session_list = []
    _helper_session = _get_helper_session(host_details)
    session_pool_state = _helper_session.get_pool_state()
    pool_list = []

    for host in session_pool_state.keys():
        host_dict = {}
        host_state = session_pool_state[host]
        host_dict["ip"] = str(host)
        if (host_state["shutdown"] == False) and \
                (host_state["open_count"] > 0):
            host_dict["status"] = 1
        else:
            host_dict["status"] = 0
        pool_list.append(host_dict)

    session_dict = {
        "session_id": "model_server_session",
        "keyspace": _helper_session.keyspace,
        "hosts": pool_list
    }

    session_list.append(session_dict)

    connection_dict = {"sessions": session_list}

    return connection_dict


def get_table_update_info(recommender, host_details=None):
    """
    A utility function to get the model update info of the given recommender name, 
    as stored in common.model_update_info table at the specified host details.
    
    Note that if the host details are not given, the preconfigured values via
    env variables will be used.

    Parameters
    ----------
    recommender : str
        recommender code name.
        
    host_details : HostDetails
        if given, use the host details in this object. Otherwise use the 
        preconfigured values via env variables.
        
    Returns
    ----------
    model_update_info with keyspace_name, deployed_version and latest_version properties set.
    """
    try:
        _helper_session = _get_helper_session(host_details)
        get_model_update_info_cql = \
            "SELECT keyspace_name, deployed_version, latest_version \
             FROM %s.%s WHERE model_code_name = '%s'" \
             % (stc.RCOM_CASSANDRA_COMMON_KEYSPACE, stc.RCOM_CASSANDRA_MODEL_UPDATE_TABLE, recommender)
        rows = _helper_session.execute(get_model_update_info_cql)
        return rows.current_rows[0]
    except:
        msg = "Failed to refresh prepared statement for model %s. Skipping update." % recommender
        _logger.info(msg)
        raise RuntimeError(msg)


def get_session(keyspace,
                control_connection_timeout=4,
                connect_timeout=10,
                host_details=None):
    """
    A utility function to get a new Cassandra session. A session instance is a 
    long-lived object and it should not be used in a request/response short-lived 
    fashion. One should use at most one session per keyspace, or use a single
    session and explicitely specify the keyspace in your queries.
    
    For best practices, see 
    https://www.datastax.com/dev/blog/4-simple-rules-when-using-the-datastax-drivers-for-cassandra

    Parameters
    ----------
    control_connection_timeout : str
        a timeout, in seconds, for queries made by the control connectio, such as querying the current schema and
        information about nodes in the cluster. If set to None, there will be no timeout for these queries.

    connect_timeout : int
        a timeout, in seconds, for creating new connections.
        
    host_details : HostDetails
        if given, use the host details in this object. Otherwise use the 
        preconfigured values via env variables.

    Returns
    ----------
    a Cassandra session
    """
    hosts, port, user, password = _get_host_details(host_details)

    if user and password:
        _logger.info(
            "Connecting to Cassandra cluster at %s port %d using authentication",
            hosts, port)
        auth_provider = PlainTextAuthProvider(username=user, password=password)
        cluster = Cluster(hosts,
                          port=port,
                          control_connection_timeout=control_connection_timeout,
                          connect_timeout=connect_timeout,
                          auth_provider=auth_provider)
    else:
        _logger.info("Connecting to Cassandra cluster at %s port %d", hosts,
                     port)
        cluster = Cluster(hosts,
                          port=port,
                          control_connection_timeout=control_connection_timeout,
                          connect_timeout=connect_timeout)

    _logger.info("Creating Cassandra Session to keyspace %s", keyspace)
    return cluster.connect(keyspace)


def execute_concurrent(statements_and_params, session, max_threads=10):
    """Execute CQL statements and their params concurrently. The results for each query are merged and returned as an
    array of Row instances.

    Parameters
    ----------
    statements_and_params : a list of tuples.
        Each tuple contains (ps, params) where ps is a prepared statement and params is a list of parameters to use
        in the ps.

    session : Cassandra session
        a Cassandra session instance.

    max_threads : int
        The maximum number of threads to execute the queries concurrently.
    """
    n_threads = min(len(statements_and_params), max_threads)
    results = _execute_concurrent(session,
                                  statements_and_params,
                                  concurrency=n_threads,
                                  raise_on_first_error=False,
                                  results_generator=False)
    all_results = []
    for (success, result) in results:
        if success:
            all_results.extend(result._current_rows)
        else:
            _logger.debug("Query failed from %s" % (result))

    return all_results


class Statement:
    """A class to execute simple, un-prepared query in DMP recommender systems. This class takes care of determining the
    correct keyspace and model versioning in DMP recommender systems. The Statement is meant to execute a single-used
    query. Use PreparedStatement if a query is to be executed many times for better performance.

    Parameters
    ----------
    recommender_code_name : str
        Recommender code name. Must be exactly the same as stored in the Cassandra's common.model_update_info table.

    query : str
        The query string, with the name of the
        table replaced with "<table_name>". E.g., if the statement is "SELECT * FROM some_table WHERE key = ?"
        then you must put in the string like this "SELECT * FROM <table_name> WHERE key = ?"
        or this "SELECT * FROM " + TABLE_PLACEHOLDER + " WHERE key = ?"

    table_name : str
        The name of the table used in the `query`, without the running number after it.
    """

    def __init__(self, recommender_name, query_placeholder, table_name,
                 session):
        self.table_name = table_name
        self.recommender_name = recommender_name
        self.session = session

        recommender_info = get_table_update_info(recommender_name)

        self.deployed_version = int(recommender_info.deployed_version)
        self.keyspace_name = recommender_info.keyspace_name
        self.current_table = self.table_name + _SEPARATOR + str(
            self.deployed_version)

        self.query = query_placeholder.replace(
            TABLE_PLACEHOLDER, self.keyspace_name + "." + self.current_table)
        _logger.info("Preparing %s for Query: %s" %
                     (str(self.__class__), self.query))
        self._statement = SimpleStatement(self.query)

    def execute(self, params=()):
        """A method to execute a statement.

        Parameters
        ----------
        params : tuple
            a tuple of parameters required in the statement.
        """
        return self.session.execute(self._statement, params)

    @property
    def statement(self):
        """An instance of a statement."""
        return self._statement


class PreparedStatement(Statement):
    """PreparedStatement is a statement that is prepared for Cassandra. This class takes care of determining the correct
    keyspace and deployed model versioning in DMP recommender systems. The PreparedStatement should be instantiated only
    once and re-used as needed. Re-instantiating this many times may incure performance.

    Parameters
    ----------
    recommender_name : str
        Name of the recommender model. Must be exactly the same as stored in the Cassandra's common.model_update_info table.

    query : str
        The query string, with the name of the
        table replaced with "<table_name>". E.g., if the statement is "SELECT * FROM some_table WHERE key = ?"
        then you must put in the string like this "SELECT * FROM <table_name> WHERE key = ?"
        or this "SELECT * FROM " + TABLE_PLACEHOLDER + " WHERE key = ?"

    table_name : str
        The name of the table used in the `query`, without the running number after it.
    """

    def __init__(self, recommender_name, query, table_name, session):
        super().__init__(recommender_name, query, table_name, session)
        self._statement = self.session.prepare(
            self.query)    # instantiate a prepared statement
