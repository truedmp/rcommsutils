# Rcom-ms-utils

Utils for recommendation projects.

---

# Versions

4.7.15

---

# Release detail

4.7.15
- Add function `get_items_metadata_define_indexes_elastic_search`

4.7.14
- fix tv_utils.get_shelf_items and tv_utils.get_shelf_items_from_elasticsearch to hashable must_nots for memory cache

4.7.13
- modify following function in tv_utils to support must_nots
  - get_items_define_feilds
  - get_items_with_filter
  - _get_random_items_from_elasticsearch
  - get_shelf_items
  - get_shelf_items_from_elastic_search
  - sort_es_doc
  - get_sort_items
  - get_random_items
  - get_latest_items
  - get_most_popular_items


4.7.13
- modify `tv_utils.get_items` to include must_nots in cache prefix

4.7.12
- modify `get_shelf_metadata` to make timestamp configurable and modify `get_shelf_and_pin` to use `now` for publish_date

4.7.11
- fix `get_items_metadata_from_all_indexes` to sort result before return

4.7.10
- modify `get_mixed_privilege` to make timestamp configurable

4.7.9
- sort progressive shelf by version when getting shelf items and pin_items

4.7.8
- add new parameter `must_nots` for `tv_utils.get_items` to allow extra filtering conditions

4.7.7
- Update deps `orjson==3.6.1`

4.7.6
- Update deps `orjson>=3.8.5`

4.7.5
- add tv_utils.get_get_shelf_items to not cache when elastic_serach timeout or exception, with unit test

4.7.4
- add tv_utils.get_get_items_metadata_from_all_indexes

4.7.3
- add prv.get_shelf_metadata and prv.get_shelf_items_and_pin_items from shp5
- update CMS base url from https://dmpapi2.trueid.net to http://dmpapi2.dmp.true.th (DMPREC-4896)

4.7.2
- add hit count fields for trueyoumerchant in privilege utils

4.7.1
- return ID from `_build_result_set` to privilege utils

4.7.0
- remove old codes in `privilege utils._get_most_popular_items` for merchant
- Add field `merchant_type` in  `_sort_es_doc` to privilege utils
- remove fields `is_trueyou_eat` and `original_ids` on get `_sort_es_doc` and `get_items`

4.6.6
- Add new function `_get_content_providers_condition`
- Add `_get_content_providers_condition(["true_vision"])` to `_get_default_must_not_conditions` for filtering out truevision provider

4.6.5
- Add field `setting` in  `_build_result_set` to privilege utils

4.6.4
- Add new function `get_remove_blackout_epg` and `_get_tv_default_must_not_conditions` to tv utils for filtering out blackout EPG

4.6.3
- Add new function `get_shelf_pin_items` to privilege utils
- Add `exlude_ids` and `search_after` for `get_most_popular_items` for pinned items and pagination

4.6.2
- Add new `REQUEST_TIMEOUT` parameter for cms
- Add timeout to `get_tv_channels`, `get_tv_metadata`, `get_tv_top_hit` in tv_utils

4.6.1
- Fix `get_item_define_fields`, reorder append common filter_conditions.

4.6.0
- Modify function `get_item_define_fields` to query four types of special deals.

4.5.5
- Add function get_item_define_field
- Modify get_items function to support tags filtering

4.5.4
- Add return field `allow_app` and `card_type`
- Add return field `day_x_hitcount`

4.5.3
- Support filter allow_app
- Change filter card_type to support multiple type

4.5.2
- Support filter card_type in privilege utils
- Support filter article_category in privilege utils
- Add filter out `quota_over_exited > 0` in privilege criteria
- Add filter out `not exists article_category` in privilege criteria
- Add filter out `allow_recommend = false` in privilege criteria
- Add support filter in function `get_most_popular_item`, `get_item`, `_sort_es-doc`

4.5.1
- Add field hit_count_day

4.5.0
- DMPREC-3858: add `end_date`, `expire_date`, `is_promo`, and `other_type` filters.

4.4.3
- DMPREC-4194: remove shelf's language prefix query to source.id

4.4.2
- Refactor unittest

4.4.1
- DMPRED-3923: add `get_mixed_privileges`, a mixed types query for deals/privileges.

4.4.0
-  Support is_vod_layer query parameter (DMPREC-3851)
-  Add `_get_movie_types_conditions`
-  Refactor `dsl_bool` and `must_not` conditions

4.3.4
- Add build result set in random function

4.3.3
- hotfix bug in function random

4.3.2
- Add `get_random_item` fucntion to support query movie, series and random the result after filter(DMPREC-3744)

4.3.1
- Add `get_sorted_items` function for support query movie,series sorted field  (DMPREC-3744)

4.3.0

- Support filtering by `is_promo` (DMPREC-3193)
- Return more fields (DMPREC-2227)
- Update cache key (MOVIE_ITEMS) to (MOVIE_ITEMS_version)
- Add content_rights filter

4.2.8

- Hotfix, removing elasticsearch host sniffing, due to connection issue[Incident page](https://truedmp.atlassian.net/wiki/spaces/AI/pages/2705981621/21+August+-+1+September+2021+UFS+has+high+error+rate+on+weekend)

4.2.7

- Fix TrueTv index name for non-prod env

4.2.6
- Update ES Query in `tv_utils.get_tv_items` to search using channel_codes

4.2.5
- Fix bug when query with filter vod5layer and got cache
- Add feature set prefix cache movie, article, shelf

4.2.4
- Support filter is_vod_layer

4.2.3
- Fix caching key on get_tv_channels, by using cache by full URL instead of one global key.

4.2.2

- Fix bug can't filter genres, article_categories, package_alacarte_list, subscription_tiers if filters more than one in each param.

4.2.1

- add default time for get shelf items function

4.2.0

- DMPREC-2104: Support Content rights filter (e.g., svod, tvod, etc.) in tv_utils.get_items

4.1.0

- DMPREC-2203: Support progressive shelf data

4.0.4

- Fix privilege_utils, add default filter for expire_date and change multi_match to term match

4.0.3

- DMPREC-2190 - Fix bug in `tv_utils._set_cache_tv_items` by adding language prefix to id before caching

4.0.2

- DMPREC-1933: Fix `tv_utils.get_latest_items` and `tv_utils.get_popular_items` to support both movie and series within one request

4.0.1

- Merge tv_utils function used in TP2 to tv_utils in rcommsutils
- Add unittest

4.0.0

- Merge codes between 'master_es7' and 'master' (version 3.0.2) braches
- Edit privilege_utils, tv_utils, article_utils to support ES 7
- Edit unittest

3.0.2

- Change `json` to `orjson` for improve performance

3.0.1

- modify lru_cache in get_shelf_item* so that they expire after 4 hours

3.0.0

- Update cassandra-driver from 3.15 to 3.24
- Update elasticsearch from 6.3.1 to 7.10.0
- Update elasticsearch-dsl from 6.3.1 to 7.3.0
- Update redis from 2.10.6 to 3.5.3
- Update redis-py-cluster from 2.0.0 to 2.1.0
- Update request from 2.18.4 to 2.25.0
- Remove tensorflow from repo

2.8.1

- Support return specific fields

2.8.0

- Change set cache to pipeline to reduce redis connection
- Add condition to check redis session on set cache items
- add 3 fields `newepi_badge_start`, `newepi_badge_end` and `newepi_badge_type` - DMPREC-1453

2.7.1

- Return more fields in `tv_utils` (`content_rights, sub_genres, other_type`) - DMPREC-1401

2.7.0

- Use `elasticsearch-dsl` in `privilege_utils`
- Support channel_code and refactor tv channels
- Support `is_trueyou_eat` for trueyou eat filtering

2.6.6

- Add filter ids movie in get_most_popular_items and get_latest_items

2.6.5

- Fix `trailer` field issue

2.6.4

- Add `trailer` return field for `tv_utils` DMPREC-1332

2.6.3

- Update tv_utils codes and test to support query items by given package_alacarte_list DMPREC-1276

2.6.2

- Update codes and test to support RedisCluster DMPREC-1227

2.7.0
- Edit codes and unittest to support ElasticSearch version 7

2.6.1

 - Fix `expire_date` for `tv_utils` DMPREC-1235

2.6.0

- Filter out series with `ep_master` != `Y` DMPREC-1086
- Add new return fields named `display_qualities` to `tv_utils` DMPREC-1219
- Add caching layer for get_shelf_items

2.5.4

- Filter out merchants with `allow_recommend` = `false` DMPREC-1187

2.5.3

- Support query items by given a shelf_id [DMPREC-1104](https://truedmp.atlassian.net/browse/DMPREC-1104)

2.5.2

- Fix `rate` filtering to be `terms` instead of `match` to be exact value matching [DMPREC-1190]

2.5.1

- Fix bug wrong article index in setting


2.5.0

- Support filter movies/series by `rates` [DMPREC-1171]
- Support filter movies/series by `article_category` [DMPREC-1171]


2.4.0

- Add article utils


2.3.0

- ElasticSearch utils support multiple hosts connection.


2.2.0

- Get TV Metadata support batch query - DMPREC-979
- `tv_utils.get_tv_metadata(token: str, title_ids: List[str]) -> List`

2.1.1

- Fix when `genres` or `subscription_tiers` from elasticsearch return as `None`

2.1.0

- When calling to `tv_utils` return only cached items when elasticsearch raise error
- Add elasticsearch timeout to `tv_utils`

2.0.2

- Fix when redis session cannot connect to the server.

2.0.1

- Fix return field `tv_utils.get_items` by adding multi-language field to get `genres` back from ElasticSearch
- Set default `genres` in `filter_items_from_conditions` to **an empty list** when not found instead of `None`

2.0.0

- Add caching layer to `tv_utils.get_items` DMPREC-968
- Add `decode_responses` option to `get_redis(..., decode_responses=False)`
- `package_codes` and `original_ids` are **no longer supported** in `tv_utils.get_items`

1.8.0
- add sort by score and geo in get_geo_location_filtered


1.7.3

- Add `http_compress` option to ElasticSearch [DMPREC-903](https://truedmp.atlassian.net/browse/DMPREC-903)

1.7.2

- Edit bug when getting data from missing fields in dict


1.7.1

- Change codes to score items with geo location instead of filtering geo location
- Change codes to score items with discover, merchant_type


1.7.0

- Support `discover` and `merchant_type` flags for merchants in `get_merchants_from_rcom_tags`, `get_items`, and `get_most_popular_items` [DMPREC-780](https://truedmp.atlassian.net/browse/DMPREC-780)


1.6.0

- Support `tvod_flag` and `subscription_tiers` in `get_items`, `get_latest_items`, and `get_most_popular_items` [DMPREC-794](https://truedmp.atlassian.net/browse/DMPREC-794)

1.5.0

- add method for searching merchants with RCOM_TAGS

1.4.1

- privilege_utils.get_items() returns the ID of the privileges under the merchant
  when type="shop"

1.4.0

- Filtering tv_items when startTime = -1 [DMPREC-713](https://truedmp.atlassian.net/browse/DMPREC-713)
- Filtering out trailer by add `is_trailer = no` to must_condition

1.3.11

- Support field `drm` for movie/series

1.3.10

- Add Redis utils to create Redis instance and return status.

1.3.9

- Change `publish_date` and `expire_date` date/time must condition from `now/d` to `now/h`

1.3.8

- Change filtering criteria for `genres` (Use `genres` field in CMS instead of `article_category`)

1.3.7

- Add get_latest_tv_items for basic tv model to query current on-air tv programs sorted by END_DATE

1.3.6

- Support more fields for `movie/series`
    - `audio`
    - `subtitle`
    - `rate`
    - `synopsis`
    - `duration`

1.3.5

- Support `count_views` and `thumb_list` for `movie/series` [DMPREC-609](https://truedmp.atlassian.net/browse/DMPREC-609)

1.3.4

- ANN lib uses 5 hash tables, each with 10 bins by default.
- ANN lib with improved error handling here and there.
- ANN lib with improved unit testing.

1.3.3

- ANN engine creation util accepts df instead of file path. Provides util to
  ensure the given df is of correct format.
- ANN util functions for MS does not return engine object. Use the provided
  get_nearest_neighbors() to get ANN list instead. Proper locking mechanisms
  are implemented internally to ensure thread safety.

1.3.2

- Add ANN utility based on NearPy

1.3.1

- `privilege_utils`: Filter by `campaign_type` [DMPREC-487](https://truedmp.atlassian.net/browse/DMPREC-487)
- `privilege_utils`: Filter out `quota_over_existed` [DMPREC-551](https://truedmp.atlassian.net/browse/DMPREC-551)
- `privilege_utils`: Return all thumbnail size [DMPREC-586](https://truedmp.atlassian.net/browse/DMPREC-586)
- `thumb` field for `shop` type is removed
- Change `ES_TEST_IP` to `127.0.0.1` to prevent local test

1.3.0

- Breaking change!! (change product related-utils directory from `elasticsearch` to `cms` directory)
- Support get tv channels and get tv metadata for tv product

1.2.0

- Provide support for cassandra_utils to connect to Cassandra with user / password
  authentication. Break backward compatibility with previous version.
