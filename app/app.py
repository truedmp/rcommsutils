import json
from rcommsutils.cms.privilege_utils import *

def pretty(data):
    print(json.dumps(data, indent=2, sort_keys=True, ensure_ascii=False))

def run(sess):
    types = ['privilege','trueyoumerchant','trueyouarticle','hilight']
    msearch_body = ""
    for name in types:
        index, body = get_items_define_fields(
            sess,
            ids=["1","2","3","4","5","6","7","8","9"],
            type=name,
            dry_run=True,
            size=3,
            fields=["id","title"],
            timeout=1,
        )
        print(index, body)
        msearch_body += str({'index': index}) + "\n" + str(body) + "\n"

    results = msearch_query(sess, msearch_body, timeout=1)
    pretty(results)